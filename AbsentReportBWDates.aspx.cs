﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;


public partial class AbsentReportBWDates : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    BALDataAccess objdata = new BALDataAccess();

    System.Web.UI.WebControls.DataGrid grid =
                             new System.Web.UI.WebControls.DataGrid();

    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    string Division;
    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();

            SessionUserType = Session["Isadmin"].ToString();

            Division = Request.QueryString["Division"].ToString();
            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");
            //WagesType = Request.QueryString["Wages"].ToString();

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            if (SessionUserType == "2")
            {

            }
            else
            {
                AbsentReport();
            }
        }
    }

    public void AbsentReport()
    {
        DataTable AutoDTable = new DataTable();
        AutoDTable.Columns.Add("DeptName");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("EmpNo");
        AutoDTable.Columns.Add("ExistingCode");
        AutoDTable.Columns.Add("FirstName");

        DataTable mLogTime = new DataTable();
        DataTable mEmployee = new DataTable();
        DateTime date1;
        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        DateTime Date2 = Convert.ToDateTime(dat);
        int daycount = (int)((Date2 - date1).TotalDays);
        int daycolspan = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;

        SSQL = "";
        SSQL = "select isnull(DeptName,'') as [DeptName], Cast(MachineID As int) As MachineID";
        SSQL = SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
        SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
        SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And (IsActive='Yes' or CONVERT(DATETIME,DOR, 103)>=CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103))";

        if (WagesType != "-Select-")
        {
            SSQL = SSQL + " And Wages='" + WagesType + "'";
        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }
        mEmployee = objdata.RptEmployeeMultipleDetails(SSQL);

        if (mEmployee.Rows.Count < 0)
        {
        }
        else
        {
            int i1 = 0;

            for (int j1 = 0; j1 < mEmployee.Rows.Count; j1++)
            {
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DeptName"] = mEmployee.Rows[j1]["DeptName"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["MachineID"] = mEmployee.Rows[j1]["MachineID"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpNo"] = mEmployee.Rows[j1]["MachineID"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ExistingCode"] = mEmployee.Rows[j1]["ExistingCode"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["FirstName"] = mEmployee.Rows[j1]["FirstName"].ToString();

            }

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                AutoDTable.Columns.Add(Convert.ToString(dayy.ToString("dd/MM/yyyy")));

                daycount -= 1;
                daysAdded += 1;
            }

            AutoDTable.Columns.Add("Total Days");

            int j = 0;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                string mID = AutoDTable.Rows[i]["MachineID"].ToString();


                if (mID == "376")
                {
                    mID = "376";
                }
                
                int daycount1 = (int)((Date2 - date1).TotalDays);
                int daysAdded1 = 0;
                int day_col = 5;
                int day_col1 = 4;
                int count = 0;
                while (daycount1 >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded1).ToShortDateString());

                    SSQL = "select * from LogTime_Days where MachineID='" + mID + "'";
                    SSQL = SSQL + " And CompCode= '" + SessionCcode + "' And LocCode= '" + SessionLcode + "'";
                    SSQL = SSQL + " And Attn_Date_Str='" + dayy.ToString("dd/MM/yyyy") + "'";
                    // SSQL = SSQL + " And (Present_Absent='Absent' or Present_Absent='Leave') And Wh_Check='False'";
                    SSQL = SSQL + " and Present='0.0'";

                    mLogTime = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (mLogTime.Rows.Count > 0)
                    {

                        AutoDTable.Rows[i][dayy.ToString("dd/MM/yyyy")] = 'A';
                        count += 1;

                    }
                    daycount1 -= 1;
                    daysAdded1 += 1;
                    day_col += 1;
                }
                if (count != 0)
                {
                    AutoDTable.Rows[i]["Total Days"] = count.ToString();
                }
            }

            if (AutoDTable.Rows.Count > 0)
            {
                foreach (DataRow dr in AutoDTable.Rows)
                {
                    if (dr["Total Days"].ToString() == "" || dr["Total Days"] == null || dr["Total Days"].ToString() == string.Empty)
                    {
                        dr.Delete();
                        dr.AcceptChanges();
                    }
                }
            }

            DataTable dt = new DataTable();
            string Compname = "";
            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count != 0)
            {
                Compname = dt.Rows[0]["CompName"].ToString();
            }
            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=ABSENT REPORT BETWEEN DATES.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write("<table>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 7) + "'>");
            Response.Write("<a style=\"font-weight:bold\">" + Compname + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 7) + "'>");
            Response.Write("<a style=\"font-weight:bold\">ABSENT REPORT BETWEEN DATES</a>");
            Response.Write("  ");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='" + Convert.ToInt32(daycolspan + 7) + "'>");

            Response.Write("<a style=\"font-weight:bold\"> FROM  -" + FromDate + "</a>");
            Response.Write("&nbsp;&nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            Response.Write("  ");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }
}
