﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ESIMonthlyStatementClass
/// </summary>
public class ESIMonthlyStatementClass
{
    private string _EmpNo;
    private string _ESINumber;
    private string _ESIAmount;
    private string _BannkNm;
    private string _Branch;
    private string _OfficeNm;
	public ESIMonthlyStatementClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string EmpNo
    {
        get { return _EmpNo; }
        set { _EmpNo = value; }

    }
    public string ESINumber
    {
        get { return _ESINumber; }
        set { _ESINumber = value; }

    }
    public string ESIAmount
    {
        get { return _ESIAmount; }
        set { _ESIAmount = value; }

    }
    public string BankNm
    {
        get { return _BannkNm; }
        set { _BannkNm = value; }

    }
    public string Branch
    {
        get { return _Branch; }
        set { _Branch = value; }
    }
    public string OfficeNm
    {
        get { return _OfficeNm; }
        set { _OfficeNm = value; }
    }
}
