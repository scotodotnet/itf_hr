﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for ExperienceDetailsClass
/// </summary>
public class ExperienceDetailsClass
{
	public ExperienceDetailsClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private string _Ccode;

    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }

    private string _Lcode;

    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }

    private string _ExistingNo;

    public string ExistingNo
    {
        get { return _ExistingNo; }
        set { _ExistingNo = value; }
    }

    private string _MachineID;

    public string MachineID
    {
        get { return _MachineID; }
        set { _MachineID = value; }
    }


    private string _TokenNo;

    public string TokenNo
    {
        get { return _TokenNo; }
        set { _TokenNo = value; }
    }

    private string _YearExp;

    public string YearExp
    {
        get { return _YearExp; }
        set { _YearExp = value; }
    }

    private string _CompName;

    public string CompName
    {
        get { return _CompName; }
        set { _CompName = value; }
    }

    private string _Designation;

    public string Designation
    {
        get { return _Designation; }
        set { _Designation = value; }
    }

    private string _Department;

    public string Department
    {
        get { return _Department; }
        set { _Department = value; }
    }

    private string _PeriodFrom;

    public string PeriodFrom
    {
        get { return _PeriodFrom; }
        set { _PeriodFrom = value; }
    }

    private string _PeriodTo;

    public string PeriodTo
    {
        get { return _PeriodTo; }
        set { _PeriodTo = value; }
    }

    private string _ContactName;

    public string ContactName
    {
        get { return _ContactName; }
        set { _ContactName = value; }
    }

    private string _Mobile;

    public string Mobile
    {
        get { return _Mobile; }
        set { _Mobile = value; }
    }
}
