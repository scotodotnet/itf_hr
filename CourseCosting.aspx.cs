﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;

public partial class CourseCosting : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlg = false;
    string Coursecode;
    string units;
    protected void Page_Load(object sender, EventArgs e)
    {
      
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            btnCancel_Click(sender, e);
            Load_Course();
            Load_Mill();
            Load_Designation();
            if ((string)Session["Designation"] != null)
            {
                btnSearch_Click(sender, e);
            }

        }
       
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

        SSQL = "";
        SSQL = "select Distinct CC.Designation,CC.UnitCode,CC.Amount,CC.CourseName,MC.CourseCode ";
        SSQL = SSQL + " from Course_Costing CC inner join MstCourse MC on MC.CourseName=CC.CourseName";
        SSQL = SSQL + " where CC.Ccode='" + SessionCcode + "' and CC.Lcode='" + SessionLcode + "' and CC.Designation='" + Session["Designation"].ToString() + "'";
      //  SSQL = SSQL + " and CC.CourseName='" + Session["CourseName"].ToString() + "' and EM.MillName='" + Session["MillName"].ToString() + "'";
        SSQL = SSQL + " and CC.CourseName='" + Session["CourseName"].ToString() + "'";

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt != null && dt.Rows.Count > 0)
            
        {
            btnSave.Text = "Update";
            
            //units = dt.Rows[0]["MillUnit"].ToString();
            //ddlMillCode.SelectedItem.Text = dt.Rows[0]["MillCode"].ToString();
            ddlDesignationName.Text = dt.Rows[0]["Designation"].ToString();
          //  ddlCourseName.SelectedItem.Text = dt.Rows[0]["CourseName"].ToString();
            ddlCourseName.SelectedValue = dt.Rows[0]["CourseCode"].ToString();
            txtAmount.Text= dt.Rows[0]["Amount"].ToString();
            ddlMillCode_SelectedIndexChanged(sender, e);
            ddlDesignationName.Enabled = false;
           // ddlMillCode.Enabled = false;
          //  ddlUnits.Enabled = false;

        }
    }
    protected void Load_Course()
    {
        SSQL = "";
        SSQL = "Select * from MstCourse where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        ddlCourseName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlCourseName.DataTextField = "CourseName";
        ddlCourseName.DataValueField = "CourseCode";
        ddlCourseName.DataBind();
        ddlCourseName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
   
    
    protected void btnSave_Click(object sender, EventArgs e)
    {
        
        //if (ddlMillCode.Text == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Mill Code not Found');", true);
        //    ErrFlg = true;
        //    return;
        //}
     
        if (ddlCourseName.SelectedItem.Text == "-Select-")
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose the Course Name');", true);
            ErrFlg = true;
            return;
        }
        if (!ErrFlg)
        {
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from Course_Costing where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and CourseName='" + ddlCourseName.SelectedItem.Text + "'and Designation='"+ddlDesignationName.SelectedItem.Text + "'and Amount='" +txtAmount.Text+"'";
               
                DataTable Chk_Dt = new DataTable();
                Chk_Dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (Chk_Dt != null && Chk_Dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Program Details is Already Present');", true);
                    ErrFlg = true;
                    return;
                }
                if (!ErrFlg)
                {
                    SSQL = "";
                    SSQL = "insert into Course_Costing (Ccode,Lcode,Designation,CourseName,Amount)";
                  //  SSQL = SSQL + "Values('" + SessionCcode + "','" + SessionLcode + "','" + ddlMillCode.SelectedValue + "','" + ddlUnits.SelectedItem.Text + "',";
                    SSQL = SSQL + "Values('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + "'" + ddlDesignationName.SelectedItem.Text + "','" + ddlCourseName.SelectedItem.Text + "', '" +txtAmount.Text + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Program Costing Added Succesfully');", true);
                    btnCancel_Click(sender, e);
                    
                }
               
            }
            else
            {
                SSQL = "";
                SSQL = "Update Course_Costing set CourseName='" + ddlCourseName.SelectedItem.Text+ "',Amount= '" + txtAmount.Text + "'";  
              //  SSQL = SSQL + " where Designation='" + ddlDesignationName.SelectedItem.Text + "'  and UnitCode= '" + ddlUnits.SelectedItem.Text + "'";
                SSQL = SSQL + " where Designation='" + ddlDesignationName.SelectedItem.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('program Level Updated Succesfully');", true);
                btnCancel_Click(sender, e);
            }
            Response.Redirect("CourseCosting_Main.aspx");
        }

    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        
      //  ddlUnits.ClearSelection();
        ddlCourseName.ClearSelection();
      //  ddlMillCode.ClearSelection();
      ////  ddlMillCode.SelectedItem.Text = "-Select-";
      //  ddlUnits.SelectedItem.Text = "-Select-";
      //  ddlDesignationName.SelectedItem.Text = "-Select-";
      //  txtAmount.Text = "";
        btnSave.Text = "Save";
      

    }
    protected void ddlMillCode_SelectedIndexChanged(object sender, EventArgs e)
    {
       
        if (btnSave.Text == "Update")
        {
            Load_unit(units);
        }
        else
        {
         //   Load_unit(ddlMillCode.SelectedValue);
        }
    }
   
   
    protected void Load_Mill()
    {
        //SSQL = "";
        //SSQL = "Select (MillCode +' - '+ MillName) as MillName,MillCode from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        //DataTable dt = new DataTable();
        //dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //ddlMillCode.DataSource = dt;
        //ddlMillCode.DataTextField = "MillName";
        //ddlMillCode.DataValueField = "MillCode";
        //ddlMillCode.DataBind();
        //ddlMillCode.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void Load_Designation()
    {
        SSQL = "";
        SSQL = "Select Distinct DesignName,DesignCode from  Designation_Mst";
        ddlDesignationName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
      //  ddlDesignationName.DataValueField = "DesignCode";
        ddlDesignationName.DataTextField = "DesignName";
        ddlDesignationName.DataBind();
        ddlDesignationName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

    }
    protected void Load_unit(string MillCode)
    {
        SSQL = "";
     //   SSQL = "Select (UnitName+'-'+UnitTypeName) as UnitName,UnitCode from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + MillCode + "'";
        SSQL = "Select (UnitName) as UnitName,UnitCode from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + MillCode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count <= 0)
        {
            SSQL = "";
            SSQL = "select Units from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Millcode='" + MillCode + "'";
            DataTable Dt_get = new DataTable();
            Dt_get = objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable Unit_dt = new DataTable();

            Unit_dt.Columns.Add("UnitCode");
            Unit_dt.Columns.Add("UnitName");

            int Unitcode = 0;
            if (Dt_get != null && Dt_get.Rows.Count > 0)
            {

                Unitcode = Convert.ToInt32(Dt_get.Rows[0]["Units"]);
                DataRow dr;

                for (int i = 1; i <= Unitcode; i++)
                {
                    dr = Unit_dt.NewRow();
                    dr["UnitCode"] = "UNIT " + i;
                    dr["UnitName"] = "UNIT " + i;
                    Unit_dt.Rows.Add(dr);
                }

                if (Unit_dt.Rows.Count > 0)
                {
                    //ddlUnits.DataSource = Unit_dt;
                    //ddlUnits.DataTextField = "UnitName";
                    //ddlUnits.DataValueField = "UnitCode";
                    //ddlUnits.DataBind();
                    //ddlUnits.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
                }
            }
        }
        else
        {
            //ddlUnits.DataSource = dt;
            //ddlUnits.DataTextField = "UnitName";
            //ddlUnits.DataValueField = "UnitCode";
            //ddlUnits.DataBind();
          //  ddlUnits.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }
}

