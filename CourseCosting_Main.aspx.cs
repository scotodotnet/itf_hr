﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;


public partial class CourseCosting_Main : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlg = false;
    string Coursecode;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        Session.Remove("Designation");
        Load_Data();
    }
    protected void Load_Data()
    {
        SSQL = "";
        //SSQL = "Select Distinct MD.MillName,CC.Designation,CC.CourseName,CC.Amount,CC.UnitCode from Course_Costing CC inner join Employee_Mst EM on EM.MillCode=CC.MillCode";
        //SSQL = SSQL + " Inner join MstMill MD on MD.MillCode=CC.MillCode where CC.Ccode='" + SessionCcode + "' and CC.Lcode='" + SessionLcode + "'";
        SSQL = "Select Designation,CourseName,Amount from Course_Costing";


        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
       
        Response.Redirect("CourseCosting.aspx");
    }
    protected void btnEditIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
       
       // string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string[] arg = new string[3];
        arg = e.CommandArgument.ToString().Split(',');
        string Designation = arg[0];
        string CourseName = arg[1];
       // string MillName = arg[2];
        string Amount = arg[2];

        Session["Designation"] = Designation;
        Session["CourseName"] = CourseName;
        //Session["MillName"] = MillName;
        Session["Amount"] = Amount;


        Response.Redirect("CourseCosting.aspx");

    }

}