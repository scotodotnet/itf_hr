﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CourseLevelUpdate.aspx.cs" Inherits="CourseLevelUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="assets/plugins/ModalPopup/CSS/Popup.css" rel="stylesheet" type="text/css" />
    <script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
    <script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <asp:UpdatePanel runat="server" EnableViewState="true">
        <ContentTemplate>
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Program Level Update</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Employee Profile</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Program Level Update</li>
                        </ol>
                    </nav>
                </div>
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="forms-sample">
                                    <div class="row">
                                        <div class="col-md-3">
                                         <div class="form-group">
                                            <label>Mill Code</label><span class="asterisk_input"></span>
                                            <asp:TextBox ID="txtMillCode" MaxLength="30" class="form-control" AutoPostBack="true" OnTextChanged="txtMillCode_TextChanged" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtMillCode" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->

                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Mill Name</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtMillName" class="form-control" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtMillName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                      
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Units</label><span class="asterisk_input"></span>
                                                <asp:DropDownList ID="ddlUnits" CssClass="form-control" Style="width: 100%" AutoPostBack="true" OnSelectedIndexChanged="ddlUnits_SelectedIndexChanged" runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlUnits" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>ITF No</label><span class="asterisk_input"></span>
                                                 <asp:DropDownList ID="ddlIFTNo" CssClass="form-control" Style="width: 100%" AutoPostBack="true" OnSelectedIndexChanged="ddlIFTNo_TextChanged" runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlIFTNo" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <div class="row">
                                         <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Participant Name</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtParticipantName" class="form-control" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtParticipantName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                          <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Date Of Joining</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtDOJ" class="form-control" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtDOJ" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Dept Name</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtDeptName" class="form-control" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtDeptName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                         <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Designation Name</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtDesignationName" class="form-control" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtDesignationName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Program Name</label><span class="asterisk_input"></span>
                                                <asp:DropDownList ID="ddlCourseName" CssClass="form-control" Style="width: 100%" AutoPostBack="true" OnSelectedIndexChanged="ddlCourseName_TextChanged" runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlCourseName" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                         <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Total Level</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtTotalLevel" Enabled="false" class="form-control" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtTotalLevel" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                          <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>From Date</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtFromDate" Enabled="true" class="form-control datepicker" placeholder="dd/MM/yyyy" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtFromDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                         <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>To Date</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtToDate" Enabled="true" class="form-control datepicker" placeholder="dd/MM/yyyy" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtToDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Program Level</label><span class="asterisk_input"></span>
                                                <asp:DropDownList ID="ddlcourselevel" CssClass="form-control" Style="width: 100%"  runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlcourselevel" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                           <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Status</label><span class="asterisk_input"></span>
                                                <asp:DropDownList ID="ddlStatus" CssClass="form-control" Style="width: 100%"  runat="server">
                                                    <asp:ListItem Text="On Roll" Value="On Roll"></asp:ListItem>
                                                    <asp:ListItem Text="Complete" Value="Complete"></asp:ListItem>
                                                    <asp:ListItem Text="-Select-" Value="-Select-" Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlStatus" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                       
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                                <asp:Button runat="server" ID="btnCancel" Text="Cancel" class="btn btn-danger" OnClick="btnCancel_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
                </div>
            </footer>
        </ContentTemplate>
    </asp:UpdatePanel>

  <script>
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
            
        });
        
    });
</script>
      <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable();
            $('.js-example-basic-single').select2();
        });

    </script>
     <script type="text/javascript">
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        $('#example').dataTable();
                        $('.js-example-basic-single').select2();
                        $('.datepicker').datepicker({
                            format: "dd/mm/yyyy",
                            autoclose: true
                        });
                    }
                });
            };
        </script>
</asp:Content>

