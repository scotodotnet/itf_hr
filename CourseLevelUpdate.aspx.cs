﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;

public partial class CourseLevelUpdate : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlg = false;
    string Coursecode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Load_Course();

            if ((string)Session["ITFID"] != null)
            {
                btnSearch_Click(sender, e);
            }
        }
       
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select CU.ITFNo,CU.MillCode,CU.MillName,CU.UnitCode,EM.FirstName,EM.Designation,CU.CourseCode,CU.CourseLevel,CU.FromDate,CU.ToDate, ";
        SSQL = SSQL + "Convert(varchar,EM.DOJ,105) as DOJ,EM.DeptName,EM.Designation,CU.Status,CD.Level from CourseLevelUpdate CU inner join Employee_Mst EM on EM.MachineID=CU.ITFNo";
        SSQL = SSQL + " Inner join MstCourse CD on CD.CourseCode=CU.CourseCode  where CU.Ccode='" + SessionCcode + "' and CU.Lcode='" + SessionLcode + "' and CU.ITFNo='"+ Session["ITFID"].ToString() + "'";
        SSQL = SSQL + " and CU.Status='On Roll'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt != null && dt.Rows.Count > 0)
        {
            txtMillCode.Text = dt.Rows[0]["MillCode"].ToString();
            txtMillName.Text= dt.Rows[0]["MillName"].ToString();
            Load_Units(dt.Rows[0]["MillCode"].ToString());
            ddlUnits.SelectedValue = dt.Rows[0]["UnitCode"].ToString();
            ddlIFTNo.SelectedValue = dt.Rows[0]["ITFNo"].ToString();
            
            txtParticipantName.Text= dt.Rows[0]["FirstName"].ToString();
            if(dt.Rows[0]["DOJ"].ToString()!=null&& dt.Rows[0]["DOJ"].ToString() != "")
            {
                txtDOJ.Text = dt.Rows[0]["DOJ"].ToString();
            }
            txtDeptName.Text= dt.Rows[0]["DeptName"].ToString();
            txtDesignationName.Text= dt.Rows[0]["Designation"].ToString();
            ddlCourseName.SelectedValue = dt.Rows[0]["CourseCode"].ToString();
            txtFromDate.Text= dt.Rows[0]["FromDate"].ToString();
            txtToDate.Text= dt.Rows[0]["ToDate"].ToString();
            ddlStatus.SelectedValue= dt.Rows[0]["Status"].ToString();
            ddlCourseName_TextChanged(sender,e);
            ddlcourselevel.SelectedValue= dt.Rows[0]["CourseLevel"].ToString();
        }
    }
    protected void Load_Course()
    {
        SSQL = "";
        SSQL = "Select * from MstCourse where Ccode='"+SessionCcode+"' and Lcode='"+SessionLcode+"'";
        ddlCourseName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlCourseName.DataTextField = "CourseName";
        ddlCourseName.DataValueField = "CourseCode";
        ddlCourseName.DataBind();
        ddlCourseName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void txtMillCode_TextChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select MillName from MstMill Where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + txtMillCode.Text.ToString().Trim() + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if(dt!=null && dt.Rows.Count > 0)
        {
            txtMillName.Text = dt.Rows[0]["MillName"].ToString().Trim();
            Load_Units(txtMillCode.Text.ToString().Trim());
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsg('Mill Code Not Register')", true);
        }
        
    }
    private void Load_Units(string MillCode)
    {
        SSQL = "";
        SSQL = "Select * from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + MillCode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count <= 0)
        {
            SSQL = "";
            SSQL = "select Units from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Millcode='" + MillCode + "'";
            DataTable Dt_get = new DataTable();
            Dt_get = objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable Unit_dt = new DataTable();

            Unit_dt.Columns.Add("UnitCode");
            Unit_dt.Columns.Add("UnitName");

            int Unitcode = 0;
            if (Dt_get != null && Dt_get.Rows.Count > 0)
            {

                Unitcode = Convert.ToInt32(Dt_get.Rows[0]["Units"]);
                DataRow dr;

                for (int i = 1; i <= Unitcode; i++)
                {
                    dr = Unit_dt.NewRow();
                    dr["UnitCode"] = "UNIT " + i;
                    dr["UnitName"] = "UNIT " + i;
                    Unit_dt.Rows.Add(dr);
                }

                if (Unit_dt.Rows.Count > 0)
                {
                    ddlUnits.DataSource = Unit_dt;
                    ddlUnits.DataTextField = "UnitName";
                    ddlUnits.DataValueField = "UnitCode";
                    ddlUnits.DataBind();
                    ddlUnits.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
                }
            }
        }
        else
        {
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlUnits.DataSource = dt;
            ddlUnits.DataTextField = "UnitName";
            ddlUnits.DataValueField = "UnitCode";
            ddlUnits.DataBind();
            ddlUnits.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtMillCode.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Mill Code not Found');", true);
            ErrFlg = true;
            return;
        }
        if (txtMillName.Text == "")
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter Mill Name');", true);
            ErrFlg = true;
            return;
        }
        if (ddlUnits.SelectedItem.Text == "-Select-")
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose the Unit Name');", true);
            ErrFlg = true;
            return;
        }
        if (ddlIFTNo.SelectedItem.Text == "")
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose ITF Code');", true);
            ErrFlg = true;
            return;
        }
        if (txtParticipantName.Text == "")
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter the Participant Name');", true);
            ErrFlg = true;
            return;
        }
        if (txtDOJ.Text == "")
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Of Joining');", true);
            ErrFlg = true;
            return;
        }
        if (txtDeptName.Text == "")
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter the Dept Name');", true);
            ErrFlg = true;
            return;
        }
        if (ddlCourseName.SelectedItem.Text == "-Select-")
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose the Course Name');", true);
            ErrFlg = true;
            return;
        }
        if (txtFromDate.Text == "")
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter From Date');", true);
            ErrFlg = true;
            return;
        }

        if (txtToDate.Text == "")
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter To Date');", true);
            ErrFlg = true;
            return;
        }

        if (ddlcourselevel.SelectedItem.Text == "-Select-")
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose Course Level');", true);
            ErrFlg = true;
            return;

        }
        if (ddlcourselevel.SelectedItem.Text == "-Select-")
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose Status');", true);
            ErrFlg = true;
            return;
        }


        if (!ErrFlg)
        {
            SSQL = "";
            SSQL = "Select * from CourseLevel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ITFNo='" + ddlIFTNo.SelectedValue + "' and Status='On Roll'";
            DataTable Chdt = new DataTable();
            Chdt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Chdt != null && Chdt.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Participant is Already On Roll');", true);
                ErrFlg = true;
                return;
            }
        }

        if (!ErrFlg)
        {
            SSQL = "";
            SSQL = "Select * from CourseLevel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ITFNo='" + ddlIFTNo.SelectedValue + "'";
            SSQL = SSQL + " and CourseCode='" + ddlCourseName.SelectedValue + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt != null && dt.Rows.Count > 0)
            {

                SSQL = "";
                SSQL = "Update CourseLevel set Status='" + ddlStatus.SelectedItem.Text + "',CourseLevel='" + ddlcourselevel.SelectedItem.Text + "',LastUpdate=Convert(Varchar,getDate(),105)";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                SSQL = "";
                SSQL = "insert into CourseLevel(Ccode,Lcode,MillCode,UnitCode,ITFNo,DOJ,CourseCode,CourseName,CourseLevel,Status,LastUpdate)";
                SSQL = SSQL + "Values('" + SessionCcode + "','" + SessionLcode + "','" + txtMillCode.Text + "','" + ddlUnits.SelectedItem.Text + "','" + ddlIFTNo.SelectedItem.Text + "','" + txtDOJ.Text + "','" + ddlCourseName.SelectedValue + "',";
                SSQL = SSQL + "'" + ddlCourseName.SelectedItem.Text + "','" + ddlcourselevel.SelectedItem.Text + "','" + ddlStatus.SelectedItem.Text + "',Convert(Varchar,getDate(),105))";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
        }

        if (!ErrFlg)
        {
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from CourseLevelUpdate where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ITFNo='" + ddlIFTNo.SelectedItem.Text + "'";
                SSQL = SSQL + " and CourseCode='" + ddlCourseName.SelectedValue + "' and CourseLevel='" + ddlcourselevel.SelectedItem.Text + "' and Status='" + ddlStatus.SelectedItem.Text + "'";
                SSQL = SSQL + " and FromDate='" + txtFromDate.Text + "'and Todate='" + txtToDate.Text + "'";
                DataTable Chk_Dt = new DataTable();
                Chk_Dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (Chk_Dt != null && Chk_Dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Level is Already Present');", true);
                    ErrFlg = true;
                    return;
                }
                if (!ErrFlg)
                {
                    SSQL = "";
                    SSQL = "insert into CourseLevelUpdate (Ccode,Lcode,ITFNo,MillCode,MillName,UnitCode,DeptName,DesignName,CourseCode,CourseLevel,FromDate,ToDate,Status)";
                    SSQL = SSQL + "Values('" + SessionCcode + "','" + SessionLcode + "','"+ddlIFTNo.SelectedItem.Text+"','" + txtMillCode.Text + "','" + txtMillName.Text + "','" + ddlUnits.SelectedItem.Value + "',";
                    SSQL = SSQL + "'" + txtDeptName.Text + "','" + txtDesignationName.Text + "','" + ddlCourseName.SelectedValue + "','" + ddlcourselevel.SelectedItem.Text + "','" + txtFromDate.Text + "','" + txtToDate.Text + "','" + ddlStatus.SelectedItem.Text + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Course level Added Succesfully');", true);
                    btnCancel_Click(sender, e);
                }
            }
            else
            {
                SSQL = "";
                SSQL = "Update CourseLevelUpdate set MillCode='" + txtMillCode.Text + "',MillName='" + txtMillName.Text + "',UnitCode= '" + ddlUnits.SelectedItem.Text + "',";
                SSQL = SSQL + "DeptName='" + txtDeptName.Text + "',DesignName='" + txtDesignationName.Text + "',CourseCode='" + ddlCourseName.SelectedValue + "',";
                SSQL = SSQL + "CourseLevel='" + ddlcourselevel.SelectedItem.Text + "',FromDate='" + txtFromDate.Text + "',ToDate='" + txtToDate.Text + "',Status='" + ddlStatus.SelectedItem.Text + "' where ITFNo='" + ddlIFTNo.SelectedItem.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Course Level Updated Succesfully');", true);
                btnCancel_Click(sender, e);
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ddlcourselevel.ClearSelection();
        ddlUnits.ClearSelection();
        ddlIFTNo.ClearSelection();
        ddlCourseName.ClearSelection();
        ddlStatus.ClearSelection();
        txtMillCode.Text = "";
        txtMillName.Text = "";
        ddlStatus.ClearSelection();
        txtParticipantName.Text = "";
        txtDOJ.Text = "";
        txtDeptName.Text = "";
        txtDesignationName.Text = "";
        txtTotalLevel.Text = "";
        txtFromDate.Text = "";
        btnSave.Text = "Save";
        txtToDate.Text = "";

    }

    protected void ddlIFTNo_TextChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select FirstName,convert(varchar,DOJ,105) as DOJ,DeptName,Designation from Employee_Mst where CompCode='"+SessionCcode+"'";
        SSQL = SSQL + " and LocCOde='" + SessionLcode + "' and machineID='" + ddlIFTNo.SelectedItem.Text + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt != null && dt.Rows.Count > 0)
        {
            txtParticipantName.Text = dt.Rows[0]["Firstname"].ToString();
            txtDOJ.Text = dt.Rows[0]["DOJ"].ToString();
            txtDeptName.Text = dt.Rows[0]["DeptName"].ToString();
            txtDesignationName.Text = dt.Rows[0]["Designation"].ToString();

        }
    }

    protected void ddlCourseName_TextChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select Level from MstCourse where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and CourseCode='" + ddlCourseName.SelectedValue + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt != null && dt.Rows.Count > 0)
        {
            txtTotalLevel.Text = dt.Rows[0]["Level"].ToString();
            load_LastLevel(dt.Rows[0]["Level"].ToString());
        }
    }
    protected void load_LastLevel(string Courrselevel)
    {
        int Courselevel_Int = Convert.ToInt32(Courrselevel);

        DataTable dt = new DataTable();
        dt.Columns.Add("Courselevel");
        DataRow Dr;
        for(int i = 1; i <= Courselevel_Int; i++)
        {
            Dr = dt.NewRow();
            Dr["Courselevel"] = i;
            dt.Rows.Add(Dr);
        }
        ddlcourselevel.DataSource = dt;
        ddlcourselevel.DataTextField = "Courselevel";
        ddlcourselevel.DataValueField = "Courselevel";
        ddlcourselevel.DataBind();
        ddlcourselevel.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

        SSQL = "";
        SSQL = "Select CourseLevel from CourseLevelUpdate where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ITFNo='" + ddlIFTNo.SelectedValue + "'";
        DataTable dts = new DataTable();
        dts = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dts != null && dts.Rows.Count > 0)
        {
            ddlcourselevel.SelectedValue = dts.Rows[0]["CourseLevel"].ToString();
        }

    }
    protected void ddlUnits_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select MachineID from Employee_Mst Where CompCode='"+SessionCcode+"'";
        SSQL = SSQL + " and LocCode='" + SessionLcode + "' and MillCode='" + txtMillCode.Text.ToString().Trim() + "' and UnitCode='" + ddlUnits.SelectedValue + "'";
        DataTable Dt = new DataTable();
        Dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if(Dt!=null && Dt.Rows.Count > 0)
        {
            ddlIFTNo.DataSource = Dt;
            ddlIFTNo.DataTextField = "MachineID";
            ddlIFTNo.DataValueField = "MachineID";
            ddlIFTNo.DataBind();
            ddlIFTNo.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }
}