﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CourseLevelUpdate_Main.aspx.cs" Inherits="CourseLevelUpdate_Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel runat="server" EnableViewState="true">
        <ContentTemplate>
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Program Level Details</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Employee Profile</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Program Level Details</li>
                           
                        </ol>
                    </nav>
                </div>
                     <asp:Button runat="server" ID="btnSave" Text="Add New" class="btn btn-success"  OnClick="btnSave_Click" />
                <br />
                <br />  
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="forms-sample">
                                    
                             <!-- table start -->
                                    <div class="col-lg-12">  
                               <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
					                    <HeaderTemplate>
                                            <table id="example" class="display table">
                                                <thead>
                                                    <tr>
                                                        <th>S.No</th>
                                                       <th>ITF ID</th>
                                                        <th>Name</th>
                                                        <th>MillName</th>
                                                        <th>Designation</th>
                                                        <th>Program Name</th>
                                                        <th>Program Level</th>
                                                    <%--    <th>Last Update</th>--%>
                                                        <th>Edit</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex+1 %></td>
                                                <td><%# Eval("ITFNo")%></td>
                                                <td><%# Eval("EmpName")%></td>
                                                <td><%# Eval("MillName")%></td>
                                                <td><%# Eval("Designation")%></td>
                                                <td><%# Eval("CourseName")%></td>
                                                <td><%# Eval("CourseLevel")%></td>
                                               <%-- <td><%# Eval("LastUpDate")%></td>--%>
                                                <td>
                                                    <asp:LinkButton ID="btnEditIssueEntry_Grid"  class="btn-sm btn-success btn-icon icon-pencil"   runat="server" 
                                                        Text="" OnCommand="btnEditIssueEntry_Grid_Command" CommandArgument="Edit" CommandName='<%# Eval("ITFNo")%>'>
                                                    </asp:LinkButton>
                                                    </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
					                </asp:Repeater>
                             </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
                </div>
            </footer>
        </ContentTemplate>
    </asp:UpdatePanel>
     <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>
</asp:Content>

