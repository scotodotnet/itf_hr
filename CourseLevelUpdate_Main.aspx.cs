﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;


public partial class CourseLevelUpdate_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlg = false;
    string Coursecode;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        Session.Remove("ITFID");
        Load_Data();
       
    }
    protected void Load_Data()
    {
        SSQL = "";
        SSQL = "Select CL.ITFNo,MD.MillName,EM.FirstName as EmpName,EM.Designation,CL.CourseName,CL.CourseLevel,LastUpdate from CourseLevel CL inner join Employee_Mst EM on EM.MachineID=CL.ITFNo";
        SSQL = SSQL + " Inner join MstMill MD on MD.MillCode=CL.MillCode where CL.Ccode='" + SessionCcode + "' and CL.Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }
    protected void btnEditIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
         Session["ITFID"] = e.CommandName.ToString();
        Response.Redirect("CourseLevelUpdate.aspx");
        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Session.Remove("ITFID");
        Response.Redirect("CourseLevelUpdate.aspx");
    }
}