﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;

public partial class DOJList : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    string Month;
    string Status;
    string TokenNo = "";
    string FinYearVal = "";
    string FinYearCode = "";
    string Wages = "";
    string Department = "";

    DataTable dt_Cat = new DataTable();

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
   

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Employee List";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();

            FinYearCode = Request.QueryString["FinYearCode"].ToString();
            FinYearVal = Request.QueryString["FinYearVal"].ToString();
            Month = Request.QueryString["Month"].ToString();

            //Wages = Request.QueryString["Wages"].ToString();

            //if (Request.QueryString["DeptName"].ToString() != "-Select-")
            //{
            //    Department = Request.QueryString["DeptName"].ToString();
            //}

            // TokenNo = Request.QueryString["TokenNo"].ToString();

            string SessionUserType_1 = SessionUserType;

            if (SessionUserType_1 == SessionUserType)
            {
                Report();
            }
        }
    }

    private void Report()
    {
        DataTable AutoDTable = new DataTable();
        AutoDTable.Columns.Add("EmpNo");
        AutoDTable.Columns.Add("ExistingCode");
        AutoDTable.Columns.Add("FirstName");
        AutoDTable.Columns.Add("Wages");
        AutoDTable.Columns.Add("DepartmentName");
        AutoDTable.Columns.Add("Designation");
        AutoDTable.Columns.Add("DateOfJoin");

        SSQL = "";
        SSQL = "SELECT isnull(EmpNo,'') as [EmpNo],isnull(ExistingCode,'') as [ExistingCode], isnull(FirstName,'') as [EmpName] ,isnull (Wages,'') as [Wages],";
        SSQL = SSQL + "isnull(DeptName,'') as [DepartmentName],isnull(Designation,'') as [Designation],convert(varchar,ISNULL(DOJ,''),103) as[DateOfJoin] from Employee_Mst ";
        SSQL = SSQL + "Where CompCode = '" + SessionCcode + "' and LocCode = '" + SessionLcode + "' And ";
        SSQL = SSQL + "IsActive = 'Yes'"; //And Wages= '" + Wages + "' ";

        //if (Department != "")
        //{
        //    SSQL = SSQL + "And DeptName ='" + Department + "' ";
        //}

        SSQL = SSQL + "Order By EmpNo";
       
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);
        
        if (AutoDTable.Rows.Count > 0)
        {
            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionEpay + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            //dt = objdata.RptEmployeeMultipleDetails(SSQL);
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            string CmpName = "";
            string Cmpaddress = "";
            if (dt_Cat.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=DOJ LIST.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
         
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='7'>");
            Response.Write("<a style=\"font-weight:bold\">"+ CmpName + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='7'>");
            Response.Write("<a style=\"font-weight:bold\">" + Cmpaddress + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='7'>");
            Response.Write("<a style=\"font-weight:bold\">DATE OF JOINING REPORT FOR THE MONTH OF " + Month + " - " + FinYearCode + "</a>");

            Response.Write("</td>");
            
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true'>");
            Response.Write("<td colspan='5'>");
            Response.Write("<a style=\"font-weight:bold\">MONTH - " + Month + "</a>");

            Response.Write("</td>");
            Response.Write("<td align='right' colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">FINANCIAL YEAR - " + FinYearVal + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }
}