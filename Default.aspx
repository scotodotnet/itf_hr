﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Welcome! SCOTO :: HR Module</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/simple-line-icons/css/simple-line-icons.css"/>
    <link rel="stylesheet"  href="assets/vendors/flag-icon-css/css/flag-icon.min.css"/>
    <link rel="stylesheet"  href="assets/vendors/css/vendor.bundle.base.css"/>
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="assets/css/style.css" <!-- End layout styles -->
    <link rel="shortcut icon" href="assets/img/Attendance/fingerprint.png" />
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-center p-5">
                <div class="brand-logo">
                  <img src="assets/img/ITFLA-LOGO.png">
                </div>
                <h6 class="font-weight-light">Sign in to continue.</h6>
                <form runat="server" class="pt-3">
                      <div class="form-group" runat="server" visible="false" style="visibility:hidden">
                    <asp:DropDownList runat="server" ID="ddlCode" class="form-control input-lg" AutoPostBack="true" OnSelectedIndexChanged="ddlCode_SelectedIndexChanged" >
                 </asp:DropDownList>
                  </div>
                     <div class="form-group" runat="server" visible="false" style="visibility:hidden">
                    <asp:DropDownList runat="server" ID="ddlLocation" class="form-control input-lg">
                 </asp:DropDownList>
                  </div>
                  <div class="form-group">
                       <asp:TextBox class="form-control input-lg"  placeholder="User ID" id="txtusername" required runat="server"/>
                  </div>
                  <div class="form-group">
                       <asp:TextBox TextMode="Password" ID="txtpassword" class="form-control input-lg" placeholder="Password" required runat="server"/>
                  </div>
                     <small runat="server" id="ErrorDisplay">User ID and Password Incorrect...</small>
                  <div class="mt-3">
                       <asp:Button type="submit" class="btn btn-success btn-block btn-lg" runat="server" Text="Sign me in" id="btnLogin" OnClick="btnLogin_Click" />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="assets/js/off-canvas.js"></script>
    <script src="assets/js/misc.js"></script>
    <!-- endinject -->
  </body>
</html>
