﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DownLoadClear.aspx.cs" Inherits="DownLoadClear" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
    <%--<script src="assets/plugins/select2/dist/js/select2.min.js"></script>--%>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
    <script>
        $(document).ready(function () {
            //alert('hi');
            $('#example').dataTable();
            $('#example1').dataTable();
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",

                autoclose: true
            });

        });
    </script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
    </script>

    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
    </script>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
    </script>

    <asp:UpdatePanel runat="server" EnableViewState="true">
        <ContentTemplate>
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Download Clear </h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Bio Metrimachine</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Download Clear</li>
                        </ol>
                    </nav>
                </div>
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="forms-sample">
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>IP Address</label>
                                                <asp:DropDownList runat="server" ID="ddlIPAddress" class="js-example-basic-single select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlIPAddress" Display="Dynamic" InitialValue="-Select-" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btndownload" Text="Download" class="btn btn-success" OnClick="btndownload_Click" OnClientClick="ProgressBarShow();" />
                                                <asp:Button runat="server" ID="btndownloadClear" Text="Download/Clear" class="btn btn-danger" OnClick="btndownloadClear_Click" OnClientClick="ProgressBarShow();" />

                                                <asp:Button runat="server" Visible="false" ID="btnDocumnet" Text="Document" class="btn btn-danger" OnClick="btnDocumnet_Click" />
                                                <asp:Button ID="btnConversion" class="btn btn-success" runat="server"
                                                    Text="Convert" ValidationGroup="Validate_Field" OnClick="btnConversion_Click"
                                                    OnClientClick="ProgressBarShow();" Visible="false" />
                                                <asp:Button ID="Button1" class="btn btn-success" runat="server"
                                                    Text="IDConvert" OnClick="Button1_Click" Visible="true" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">
                                        <asp:Label ID="lblDwnCmpltd" runat="server" Font-Bold="True" Font-Italic="True"
                                            Font-Size="Medium" ForeColor="Red"></asp:Label>
                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row" runat="server" visible="false">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Token Number</label>
                                                <asp:TextBox runat="server" ID="txtTokenNumber" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4" runat="server" visible="false" id="Div_Wages">
                                            <div class="form-group">
                                                <label>Wages Type</label>
                                                <asp:DropDownList runat="server" ID="ddlWages" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row" runat="server" visible="false">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>From Date</label>
                                                <asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>To Date</label>
                                                <asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Transfer Date</label>
                                                <asp:TextBox runat="server" ID="txtTransferDate" class="form-control datepicker"></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <br />
                                                <asp:Button runat="server" ID="btnTransfer" Text="Transfer to App" class="btn btn-success" OnClick="btnTransfer_Click" OnClientClick="ProgressBarShow();" />

                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <div class="row">

                                        <div class="col-md-4"></div>

                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->
                                    <div id="Download_loader" />
                                </div>

                            </div>
                            <!-- end row -->
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
            
            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
                </div>
            </footer>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

