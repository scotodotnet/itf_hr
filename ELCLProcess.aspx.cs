﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using Payroll.Configuration;
using Payroll.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
public partial class ELCLProcess : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string CmpName;
    static string Cmpaddress;
    string SessionUserType;
    string SessionEpay;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string Query = "";
    string SessionPayroll;

    static string All1 = "0";
    static string All2 = "0";
    static string All3 = "0";
    static string Al4 = "0";
    static string Ded1 = "0";
    static string ded2 = "0";
    static string ded3 = "0";
    static string ded4 = "0";
    static string ded5 = "0";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        Load_DB();
        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            ddlCategory_SelectedIndexChanged1(sender, e);
            ddlcategory_Rpt_SelectedIndexChanged(sender, e);
            int currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                //txtBonusYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));

                currentYear = currentYear - 1;
            }
           
        }
    }

    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Ramalinga_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }

    protected void ddlCategory_SelectedIndexChanged1(object sender, EventArgs e)
    {
        DataTable dtemp = new DataTable();
        Query = "Select 0 EmpTypeCd,'-Select-' EmpType union ";
        Query = Query + "Select EmpTypeCd,EmpType from mstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        if (SessionAdmin == "2")
        {
            Query = Query + " And EmpType <> 'SUB-STAFF' And EmpType <> 'CIVIL' And EmpType <> 'OTHERS'";
        }
        dtemp = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtemp;
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
    }

    protected void ddlcategory_Rpt_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DataTable dtemp = new DataTable();
        //Query = "Select 0 EmpTypeCd,'-Select-' EmpType union ";
        //Query = Query + "Select EmpTypeCd,EmpType from mstEmployeeType where EmpCategory='" + ddlcategory_Rpt.SelectedValue + "'";
        //if (SessionAdmin == "2")
        //{
        //    Query = Query + " And EmpType <> 'SUB-STAFF' And EmpType <> 'CIVIL' And EmpType <> 'OTHERS'";
        //}
        //dtemp = objdata.RptEmployeeMultipleDetails(Query);
        //ddlRptEmpType.DataSource = dtemp;
        //ddlRptEmpType.DataTextField = "EmpType";
        //ddlRptEmpType.DataValueField = "EmpTypeCd";
        //ddlRptEmpType.DataBind();
    }

    protected void btnSalDown_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string query2 = "";
        DataTable dt = new DataTable();


        string bonus_Period_From = "";
        string bonus_Period_To = "";

        bonus_Period_From = "01-01-" + (Convert.ToDecimal(ddlFinance.SelectedValue.ToString())).ToString();
        bonus_Period_To = "31-12-" + ddlFinance.SelectedValue.ToString();
        DateTime Bonus_From_Date = DateTime.ParseExact(bonus_Period_From, "dd-MM-yyyy", null);
        DateTime Bonus_To_Date = DateTime.ParseExact(bonus_Period_To, "dd-MM-yyyy", null);


        if (ddlCategory.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category.');", true);
            ErrFlag = true;
        }
        else if (ddlCategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly.');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddlEmployeeType.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly.');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddlEmployeeType.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly.');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }

        //Download Start
        if (!ErrFlag)
        {
            query2 = "Select ED.EmpNo,ED.ExistingCode ExisistingCode,ED.FirstName as EmpName,";
            query2 = query2 + "'0' as Deduction1,'0' as Deduction2,'0' as Deduction3,'0' as Deduction4";
            query2 = query2 + " from Employee_Mst ED";
            query2 = query2 + " inner join [" + SessionPayroll + "]..AttenanceDetails AD on ED.EmpNo=AD.EmpNo And AD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT";
            query2 = query2 + " inner join MstEmployeeType MT on MT.EmpType=ED.Wages";
            query2 = query2 + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And MT.EmpTypeCd='" + ddlEmployeeType.SelectedValue + "'";
            query2 = query2 + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
            query2 = query2 + " And AD.FromDate >=convert(datetime,'" + Bonus_From_Date + "', 105) And AD.FromDate <=convert(datetime,'" + Bonus_To_Date + "', 105)";

            query2 = query2 + " group by ED.EmpNo,ED.MachineID,ED.ExistingCode,ED.FirstName,ED.FamilyDetails,";
            query2 = query2 + " ED.DeptName,ED.DeptCode,ED.Designation,ED.DOJ,ED.IsActive,ED.BaseSalary,ED.DOR";
            query2 = query2 + " Order by ED.ExistingCode Asc";

            dt = objdata.RptEmployeeMultipleDetails(query2);


            GridExcelView.DataSource = dt;
            GridExcelView.DataBind();

            string attachment = "attachment;filename=ELCLDedDownLoad.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            GridExcelView.RenderControl(htextw);

            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);

        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        if (ddlCategory.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category.');", true);
            ErrFlag = true;
        }
        else if (ddlCategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly.');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddlEmployeeType.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly.');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (ddlEmployeeType.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly.');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }

        if (FileUpload.HasFile)
        {
            FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

        }

        if (!ErrFlag)
        {
            string bonus_Period_From = "";
            string bonus_Period_To = "";

            bonus_Period_From = "01-01-" + (Convert.ToDecimal(ddlFinance.SelectedValue.ToString())).ToString();
            bonus_Period_To = "31-12-" + ddlFinance.SelectedValue.ToString();
            DateTime Bonus_From_Date = DateTime.ParseExact(bonus_Period_From, "dd-MM-yyyy", null);
            DateTime Bonus_To_Date = DateTime.ParseExact(bonus_Period_To, "dd-MM-yyyy", null);

            string query = "";
            string query2 = "";

                string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                DataTable dts = new DataTable();
                using (sSourceConnection)
                {
                    sSourceConnection.Open();

                    OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                    sSourceConnection.Close();

                    using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                    {
                        command.CommandText = "Select * FROM [Sheet1$];";
                        sSourceConnection.Open();
                    }
                    using (OleDbDataReader dr = command.ExecuteReader())
                    {

                        if (dr.HasRows)
                        {

                        }

                    }
                    OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                    objDataAdapter.SelectCommand = command;
                    DataSet ds = new DataSet();

                    objDataAdapter.Fill(ds);
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                    }
                    string constr = ConfigurationManager.AppSettings["ConnectionString"];

                    if (dt.Rows.Count != 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {

                            string EmpNo = "";
                            All1 = "0.00";
                            All2 = "0.00";
                            All3 = "0.00";
                            Al4 = "0.00";
                            Ded1 = "0.00";
                            ded2 = "0.00";
                            ded3 = "0.00";
                            ded4 = "0.00";

                            EmpNo = dt.Rows[i][0].ToString();
                            Ded1 = dt.Rows[i][3].ToString();
                            ded2 = dt.Rows[i][4].ToString();
                            ded3 = dt.Rows[i][5].ToString();
                            ded4 = dt.Rows[i][6].ToString();


                            query2 = "Select ED.EmpNo,ED.MachineID as  BiometricID,ED.ExistingCode ExisistingCode,ED.FirstName as EmpName,ED.FamilyDetails as  FatherName,ED.DeptName as Department,ED.DeptCode as DepartmentNm,";
                            query2 = query2 + " ED.Designation,convert(varchar,ED.DOJ,105) as Dateofjoining, ";
                            query2 = query2 + " Sum(AD.Days) as W_Days,";
                            query2 = query2 + " ED.ELRate as Base";
                            query2 = query2 + " from Employee_Mst ED";
                            query2 = query2 + " inner join [" + SessionPayroll + "]..AttenanceDetails AD on ED.EmpNo=AD.EmpNo And AD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT";
                            query2 = query2 + " inner join MstEmployeeType MT on MT.EmpType=ED.Wages";
                            query2 = query2 + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And MT.EmpTypeCd='" + ddlEmployeeType.SelectedValue + "'";
                            query2 = query2 + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                            query2 = query2 + " And AD.FromDate >=convert(datetime,'" + Bonus_From_Date + "', 105) And AD.FromDate <=convert(datetime,'" + Bonus_To_Date + "', 105)";
                            query2 = query2 + " And ED.EmpNo='" + EmpNo + "'";
                            query2 = query2 + " group by ED.EmpNo,ED.MachineID,ED.ExistingCode,ED.FirstName,ED.FamilyDetails,";
                            query2 = query2 + " ED.DeptName,ED.DeptCode,ED.Designation,ED.DOJ,ED.ELRate,ED.DOR";
                            query2 = query2 + " Order by ED.ExistingCode Asc";


                        }
                    }
                }


        }
    }
}
