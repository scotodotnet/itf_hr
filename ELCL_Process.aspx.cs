﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.SqlClient;
using System.IO;
using System.Data.OleDb;

public partial class ELCL_Process : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string CmpName;
    static string Cmpaddress;
    string SessionUserType;
    string SessionEpay;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SSQL = "";
    string SessionPayroll;
    bool ErrFlag = false;
    string ss = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        con = new SqlConnection(constr);
        if (!IsPostBack)
        {

            int currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                //txtBonusYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));

                currentYear = currentYear - 1;
            }

        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ddlCategory.ClearSelection();
        ddlEmployeeType.ClearSelection();

    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_EmpType(ddlCategory.SelectedValue);
    }

    private void Load_EmpType(string selectedValue)
    {
        SSQL = "";
        SSQL = "Select EmpType,EmpTypeCd from MstEmployeeType where EmpCategory='" + selectedValue + "'";
        ddlEmployeeType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType.Items.Insert(0, new ListItem("-Select-", "0"));

    }

    protected void btnELCLDown_Click(object sender, EventArgs e)
    {
        if (ddlCategory.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
        }
        else if (ddlCategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
        }

        else if (ddlEmployeeType.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
        }
        else if (ddlEmployeeType.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
        }
        else
        {
            SSQL = "";
            SSQL = "Select EM.MachineID,EM.FirstName,EM.OT_Salary,'0' as ELDays,'0' as CLDays,isnull(sum(LD.Present),'0') as WorkedDays";
            SSQL = SSQL + " from Employee_Mst EM inner join LogTime_Days LD on LD.MachineID=EM.MachineID";
            SSQL = SSQL + " and LD.CompCode=EM.CompCode and LD.LocCode=EM.LocCode";
            SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' and EM.CatName='" + ddlCategory.SelectedItem.Text + "' and EM.Wages='" + ddlEmployeeType.SelectedItem.Text + "'";
            SSQL = SSQL + " and Convert(datetime,Attn_Date_Str,103)>=Convert(datetime,'01/01/" + ddlFinance.SelectedValue + "',103) and Convert(datetime,Attn_Date_Str,103)<=Convert(datetime,'31/12/" + ddlFinance.SelectedValue + "',103)";
            SSQL = SSQL + " group by EM.MachineID,EM.FirstName,EM.OT_Salary order by EM.MachineID";
            DataTable Emp_Dt = new DataTable();
            DataTable EL_DT = new DataTable();
            Emp_Dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Emp_Dt != null && Emp_Dt.Rows.Count > 0)
            {
                for (int i = 0; i < Emp_Dt.Rows.Count; i++)
                {
                    Emp_Dt.Rows[i]["MachineID"] = "~." + Emp_Dt.Rows[i]["MachineID"];
                    SSQL = "";
                    SSQL = "Select isnull(ELDays,0) as ELDays,isnull(CLDays,0) as CLDays from [" + SessionEpay + "]..MstELCL where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " and '" + Emp_Dt.Rows[i]["WorkedDays"] + "'>=DaysFrom and '" + Emp_Dt.Rows[i]["WorkedDays"] + "'<=DaysTo";
                    SSQL = SSQL + " and CateCode='" + ddlCategory.SelectedValue + "' and EmpTypeCode='" + ddlEmployeeType.SelectedValue + "'";
                    SSQL = SSQL + " and Year='" + ddlFinance.SelectedValue + "'";
                    EL_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (EL_DT != null && EL_DT.Rows.Count > 0)
                    {
                        Emp_Dt.Rows[i]["ELDays"] = EL_DT.Rows[0]["ELDays"];
                        Emp_Dt.Rows[i]["CLDays"] = EL_DT.Rows[0]["CLDays"];
                    }
                    else
                    {
                        Emp_Dt.Rows[i]["ELDays"] = "0";
                        Emp_Dt.Rows[i]["CLDays"] = "0";
                    }
                }
            }
            gvEmp.DataSource = Emp_Dt;
            gvEmp.DataBind();

            string attachment = "attachment;filename=ELCL_List.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";

            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            gvEmp.RenderControl(htextw);
            //gvDownload.RenderControl(htextw);
            //Response.Write("Contract Details");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlCategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }
            else if (ddlCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }

            else if (ddlEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else if (ddlEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }

            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

            }
            string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
            OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
            DataTable dts = new DataTable();
            using (sSourceConnection)
            {
                sSourceConnection.Open();

                OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                sSourceConnection.Close();

                using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                {
                    command.CommandText = "Select * FROM [Sheet1$];";
                    sSourceConnection.Open();
                }
                using (OleDbDataReader dr = command.ExecuteReader())
                {

                    if (dr.HasRows)
                    {

                    }

                }
                OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                objDataAdapter.SelectCommand = command;
                DataSet ds = new DataSet();

                objDataAdapter.Fill(ds);
                DataTable dt = new DataTable();
                dt = ds.Tables[0];
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                }
                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    SqlConnection cn = new SqlConnection(constr);

                    //Get Employee Master Details

                    DataTable dt_Emp_Mst = new DataTable();

                    string MachineIDNew = dt.Rows[j][1].ToString();
                    string[] ArrMachineID = MachineIDNew.Split('.');

                    string MachineID = ArrMachineID[1];

                    string Query_Emp_mst = "";
                    Query_Emp_mst = "Select ED.FirstName as EmpName,ED.LastName,ED.IsActive,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,MD.DeptCode,MD.DeptName as DepartmentNm, '2' as Wagestype,";
                    Query_Emp_mst = Query_Emp_mst + " ED.MachineID as BiometricID,ED.DOJ,ED.Designation from Employee_Mst ED inner Join Department_Mst MD on MD.DeptCode=ED.DeptCode";
                    Query_Emp_mst = Query_Emp_mst + " where ED.CompCode='" + SessionCcode + "'";
                    Query_Emp_mst = Query_Emp_mst + " and ED.LocCode='" + SessionLcode + "' and ED.IsActive='Yes'";
                    Query_Emp_mst = Query_Emp_mst + " and ED.MachineID='" + MachineID + "'";
                    dt_Emp_Mst = objdata.RptEmployeeMultipleDetails(Query_Emp_mst);

                    if (dt_Emp_Mst.Rows.Count == 0)
                    {
                        j = j + 2;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Token No Not Found in Employee Master. The Employee No is " + MachineID + "');", true);
                        //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag = true;
                    }

                    if (!ErrFlag)
                    {
                        string Name = dt_Emp_Mst.Rows[0]["EmpName"].ToString();
                        string lastName = dt_Emp_Mst.Rows[0]["LastName"].ToString();
                        decimal WorkDays = Convert.ToDecimal(dt.Rows[j][3].ToString());
                        decimal EL_Days = Convert.ToDecimal(dt.Rows[j][4].ToString());
                        decimal CL_Days = Convert.ToDecimal(dt.Rows[j][5].ToString());
                        decimal OneDay_Rate = Convert.ToDecimal(dt.Rows[j][6].ToString());
                        decimal A1 = Convert.ToDecimal(dt.Rows[j][7].ToString());
                        decimal A2 = Convert.ToDecimal(dt.Rows[j][8].ToString());
                        decimal A3 = Convert.ToDecimal(dt.Rows[j][9].ToString());
                        decimal D1 = Convert.ToDecimal(dt.Rows[j][10].ToString());
                        decimal D2 = Convert.ToDecimal(dt.Rows[j][11].ToString());
                        decimal D3 = Convert.ToDecimal(dt.Rows[j][12].ToString());
                        decimal D4 = Convert.ToDecimal(dt.Rows[j][13].ToString());
                        string ExistingCode = dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString();
                        string DeptCode = dt_Emp_Mst.Rows[0]["DeptCode"].ToString();
                        string DeptName = dt_Emp_Mst.Rows[0]["DepartmentNm"].ToString();
                        string Designation = dt_Emp_Mst.Rows[0]["Designation"].ToString();
                        string DOJ = dt_Emp_Mst.Rows[0]["DOJ"].ToString();
                        string IsActive = dt_Emp_Mst.Rows[0]["IsActive"].ToString();
                        string RoundOffNetPay = "";
                        string Bonus_Amt = "0";
                        string RoundOFF = "0";
                        string Final_Amt = "0";


                        decimal netAmt = 0;

                        //Total Allowance
                        decimal Total_Allow = (A1 + A2 + A3);

                        //Total Deduction
                        decimal Total_Deduc = (D1 + D2 + D3 + D4);

                        //net total Calculation
                        netAmt = (OneDay_Rate * (EL_Days + CL_Days));
                       

                        if (Convert.ToDecimal(netAmt) > 0)
                        {
                            double d1 = Convert.ToDouble(netAmt.ToString());
                            int rounded1 = ((int)((d1 + 5) / 5)) * 5;
                            RoundOffNetPay = rounded1.ToString();
                        }
                        else
                        {
                            RoundOffNetPay = "0";
                        }

                        Bonus_Amt = Math.Round(Convert.ToDecimal(RoundOffNetPay), 0, MidpointRounding.AwayFromZero).ToString();

                        netAmt = Convert.ToDecimal(Bonus_Amt) - Total_Deduc;
                        netAmt = Math.Round(netAmt, 0, MidpointRounding.AwayFromZero);

                        netAmt = Convert.ToDecimal(netAmt) + Total_Allow;
                        netAmt = Math.Round(netAmt, 0, MidpointRounding.AwayFromZero);

                        RoundOFF = netAmt.ToString();

                        //if (Convert.ToDecimal(RoundOFF) > 0)
                        //{
                        //    double d1 = Convert.ToDouble(RoundOFF.ToString());
                        //    int rounded1 = ((int)((d1 + 5) / 5)) * 5;
                        //    RoundOffNetPay = rounded1.ToString();
                        //}
                        Final_Amt = RoundOFF.ToString();
                        
                        SSQL = "";
                        SSQL = "Select * from [" + SessionEpay + "]..ELCL_Details where MachineID='" + MachineID + "' and Bonus_Year ='" + ddlFinance.SelectedValue + "'";
                        SSQL = SSQL + " and Employee_Type='" + ddlEmployeeType.SelectedItem.Value + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                        DataTable check_dt = new DataTable();
                        check_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (check_dt.Rows.Count > 0)
                        {
                            SSQL = "";
                            SSQL = "delete from [" + SessionEpay + "]..ELCL_Details where MachineID='" + MachineID + "' and Bonus_Year ='" + ddlFinance.SelectedValue + "'";
                            SSQL = SSQL + " and Employee_Type='" + ddlEmployeeType.SelectedItem.Value + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                            objdata.RptEmployeeMultipleDetails(SSQL);
                        }

                        SSQL = "";
                        SSQL = "Insert into [" + SessionEpay + "]..ELCL_Details(Ccode,Lcode,EmpNo,MachineID,ExisistingCode,EmpName,FatherName,Department,DepartmentName,Designation,DOJ";
                        SSQL = SSQL + ", Worked_Days,ELDays,CLDays,Bonus_Amt,Round_Bonus,Final_Bonus_Amt,Bonus_Year,ActivateMode,ELRate,Employee_Type,Deduction1,Deduction2,Deduction3";
                        SSQL = SSQL + ",Deduction4,Allowance1,Allowance2,Allowance3)";
                        SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + MachineID + "','" + MachineID + "','" + ExistingCode + "','" + Name + "','" + lastName + "','" + DeptCode + "','" + DeptName + "'";
                        SSQL = SSQL + ",'" + Designation + "','" + DOJ + "','" + WorkDays + "','" + EL_Days + "','" + CL_Days + "','" + Bonus_Amt + "','" + RoundOFF + "','" + Final_Amt + "','" + ddlFinance.SelectedValue + "','" + IsActive + "','" + OneDay_Rate + "'";
                        SSQL = SSQL + ",'" + ddlEmployeeType.SelectedItem.Value + "','" + D1 + "','" + D2 + "','" + D3 + "','" + D4 + "','" + A1 + "','" + A2 + "','" + A3 + "')";
                        objdata.RptEmployeeMultipleDetails(SSQL);


                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Calculation Completed Successfully!!!');", true);
            }
        }
        catch (Exception ex)
        {
            //btnSalCalc.Enabled = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
}