﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="EmpPromotion.aspx.cs" Inherits="EmpPromotion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
    </script>

    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
    </script>
    <style>
        .textmode {
            mso-number-format: \@;
        }
    </style>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
    </script>


    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Employee Details</a></li>
            <li class="active">Employee Promotion</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Employee Promotion</h1>
        <!-- end page-header -->

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Employee Promotion</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>MachineID</label>
                                                <asp:DropDownList runat="server" ID="ddlMachineID" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlMachineID_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Emp Name</label>
                                                <asp:TextBox runat="server" Enabled="false" ID="txtName" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->

                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>DeptName</label>
                                                <asp:TextBox runat="server" Enabled="false" ID="txtDeptName" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Designation</label>
                                                <asp:TextBox runat="server" Enabled="false" ID="txtDesignation" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <br />
                                    <br />
                                    <fieldset>
                                        <h4><b>Promotion From</b></h4>
                                    </fieldset>
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>ExistingCode</label>
                                                <asp:TextBox runat="server" Enabled="false" ID="txtExistingCodeFrom" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div id="Div1" class="col-md-4" runat="server" visible="true">
                                            <div class="form-group">
                                                <label>Category</label>
                                                <asp:DropDownList runat="server" Enabled="false" ID="ddlCategoryFrom" class="form-control select2" AutoPostBack="true"
                                                    Style="width: 100%;" OnSelectedIndexChanged="ddlCategoryFrom_SelectedIndexChanged">
                                                    <asp:ListItem Value="0">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="1">Staff</asp:ListItem>
                                                    <asp:ListItem Value="2">Labour</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div id="Div2" class="col-md-4" runat="server" visible="true">
                                            <div class="form-group">
                                                <label>Employee Type</label>
                                                <asp:DropDownList runat="server" Enabled="false" ID="ddlEmployeeTypeFrom" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <br />
                                    <br />
                                    <fieldset>
                                        <h4><b>Promotion To</b></h4>
                                    </fieldset>
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>ExistingCode</label>
                                              <%--  <asp:TextBox runat="server" ID="txtExistingCodeTo" AutoPostBack="true" OnTextChanged="txtExistingCodeTo_TextChanged" class="form-control"></asp:TextBox>--%>
                                                  <asp:TextBox runat="server" ID="txtExistingCodeTo" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div id="Div3" class="col-md-4" runat="server" visible="true">
                                            <div class="form-group">
                                                <label>Category</label>
                                                <asp:DropDownList runat="server" ID="ddlCategoryTo" class="form-control select2" AutoPostBack="true"
                                                    Style="width: 100%;" OnSelectedIndexChanged="ddlCategoryTo_SelectedIndexChanged">
                                                    <asp:ListItem Value="0">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="1">Staff</asp:ListItem>
                                                    <asp:ListItem Value="2">Labour</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div id="Div4" class="col-md-4" runat="server" visible="true">
                                            <div class="form-group">
                                                <label>Employee Type</label>
                                                <asp:DropDownList runat="server" ID="ddlEmployeeTypeTo" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2" runat="server" visible="false">
                                            <div class="form-group">
                                                <label>Promotion Date</label>
                                                <asp:TextBox runat="server" ID="txtPromotionTo" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <div class="row">
                                        <div class="txtcenter">
                                            <div class="form-group row">
                                                <asp:Button ID="btnSave" class="btn btn-success" runat="server" Text="Save"
                                                    OnClick="btnSave_Click" />
                                                <asp:Button ID="btnClr" class="btn btn-danger" runat="server" Text="Clear"
                                                    OnClick="btnClr_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <!-- end col-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end #content -->
</asp:Content>



