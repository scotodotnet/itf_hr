﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="EmpWeekOffUpdate.aspx.cs" Inherits="EmpWeekOffUpdate" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
    </script>

    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
    </script>
    <style>
        .textmode {
            mso-number-format: \@;
        }
    </style>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
    </script>


    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Employee Slat Change</a></li>
            <li class="active">Employee Week-Off Update</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Employee Week-Off Update</h1>
        <!-- end page-header -->

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Employee Week-Off Update</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">

                                    <!-- begin row -->
                                    <div class="row">
                                         <!-- begin col-4 -->
                                        <div id="Div3" class="col-md-4" runat="server" visible="true">
                                            <div class="form-group">
                                                <label>Employee Type</label>
                                                  <asp:DropDownList ID="ddlWagesType" runat="server" class="form-control  BorderStyle select2" >
                                                        </asp:DropDownList>
                                                </div>
                                           </div> 
                                        <!-- begin col-4 -->
                                        <div id="Div1" class="col-md-4" runat="server" visible="true">
                                            <div class="form-group">
                                                <label>Weekly Off From</label>
                                                <asp:DropDownList ID="ddlFromWeekOff" runat="server" class="form-control BorderStyle select2" Width="100%">
                                                    <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                    <asp:ListItem Value="Sunday" Text="Sunday"></asp:ListItem>
                                                    <asp:ListItem Value="Monday" Text="Monday"></asp:ListItem>
                                                    <asp:ListItem Value="Tuesday" Text="Tuesday"></asp:ListItem>
                                                    <asp:ListItem Value="Wednesday" Text="Wednesday"></asp:ListItem>
                                                    <asp:ListItem Value="Thursday" Text="Thursday"></asp:ListItem>
                                                    <asp:ListItem Value="Friday" Text="Friday"></asp:ListItem>
                                                    <asp:ListItem Value="Saturday" Text="Saturday"></asp:ListItem>
                                                    <asp:ListItem Value="NONE" Text="NONE"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div id="Div2" class="col-md-4" runat="server" visible="true">
                                            <div class="form-group">
                                                <label>Weekly Off To</label>
                                                <asp:DropDownList ID="ddlToWeekOff" runat="server" class="form-control BorderStyle select2" Width="100%">
                                                    <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                    <asp:ListItem Value="Sunday" Text="Sunday"></asp:ListItem>
                                                    <asp:ListItem Value="Monday" Text="Monday"></asp:ListItem>
                                                    <asp:ListItem Value="Tuesday" Text="Tuesday"></asp:ListItem>
                                                    <asp:ListItem Value="Wednesday" Text="Wednesday"></asp:ListItem>
                                                    <asp:ListItem Value="Thursday" Text="Thursday"></asp:ListItem>
                                                    <asp:ListItem Value="Friday" Text="Friday"></asp:ListItem>
                                                    <asp:ListItem Value="Saturday" Text="Saturday"></asp:ListItem>
                                                    <asp:ListItem Value="NONE" Text="NONE"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" class="btn btn-success" />

                                                <asp:Button ID="btnClear" runat="server" Text="Clear" class="btn btn-warning"
                                                    OnClick="btnClear_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->
                                     <!-- begin row -->
                                      <div class="row">
                               <div class="form-group">
                               <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
					                    <HeaderTemplate>
                                            <table id="example" class="display table">
                                                <thead>
                                                    <tr>
                                                       <th>S.No</th>
                                                        <th>Type</th>
                                                        <th>From </th>
                                                        <th>TO</th>
                                                        <th>Changed On</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex+1%></td>
                                                <td><%# Eval("EmpType")%></td>
                                                <td><%# Eval("From")%></td>
                                                 <td><%# Eval("To")%></td>
                                                 <td><%# Eval("ChangedOn")%></td>
                                            
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
					                </asp:Repeater>
                               </div>
                        </div>
                                </div>
                            </div>
                            <div id="Download_loader" style="display: none" />
                        </div>
                    </div>
                    <!-- end panel -->
                    </div>
                 
                </ContentTemplate>
                
            </asp:UpdatePanel>

            <!-- end col-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end #content -->
</asp:Content>


