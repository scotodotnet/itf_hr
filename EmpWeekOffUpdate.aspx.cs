﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;
public partial class EmpWeekOffUpdate : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlg = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Employee Change Slat";
            Load_Type();
            Load_Data();
        }

    }

    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from WeekOffUpdate where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Load_Type()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpType";
        ddlWagesType.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (ddlFromWeekOff.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the From Week OFF');", true);
            return;
        }

        if (ddlToWeekOff.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the To Week OFF');", true);
            return;
        }
        if (!ErrFlg)
        {

            SSQL = "";
            SSQL = "Insert Into WeekOffUpdate(Ccode,Lcode,EmpType,[From],[To],ChangeBy,ChangedOn) values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWagesType.SelectedItem.Text + "','" + ddlFromWeekOff.SelectedItem.Text + "','" + ddlToWeekOff.Text + "'";
            SSQL = SSQL + ",'" + SessionUserName + "',getDate())";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Update Employee_Mst set WeekOff='" + ddlToWeekOff.SelectedItem.Text + "' ";
            SSQL = SSQL + " where Weekoff='" + ddlFromWeekOff.SelectedItem.Text + "'";
            SSQL = SSQL + " and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            if (ddlWagesType.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " and Wages='" + ddlWagesType.SelectedItem.Text + "'";
            }
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Transfer Successfully');", true);
            btnClear_Click(sender, e);
            Load_Data();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlWagesType.ClearSelection();
        ddlFromWeekOff.ClearSelection();
        ddlToWeekOff.ClearSelection();
    }
}