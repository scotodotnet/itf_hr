﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EmployeeApproval.aspx.cs" Inherits="EmployeeApproval" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">Employee Approval </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Employee Profile</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Employee Approval</li>
                </ol>
            </nav>
        </div>
        <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">

			        <!-- begin panel -->
                    <div class="card">
                         <div class="card-body">
                            <%--<div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>--%>
                           <%-- <h4 class="panel-title">Employee Approval</h4>--%>
                       
                       <div class="forms-sample">
                       
                        <!-- begin row -->
			            <div class="row"> 
			            <div class="form-group col-lg-4">
			            <div class="form-group">
                          <%--<label>Division</label>--%>
                          <asp:DropDownList ID="ddlDivision" runat="server" Visible="false" class="form-control select2" style="width:100%">
                          </asp:DropDownList>
                        </div>
			            </div>
                        <!-- begin col-4 -->
                        <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSearch" Visible="false" Text="Search" class="btn btn-success" onclick="btnSearch_Click"/>
									
								 </div>
                               </div>
                              <!-- end col-4 -->
                        </div>
                        <!-- end row -->
                        
                          <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                          <label>Emp No</label>
                          <asp:Label ID="lblEmpNo" runat="server" CssClass="form-control" BackColor="#F3F3F3"></asp:Label>
                        </div>
			            </div>
                       <!-- end col-3 -->
                        <!-- begin col-3 -->
			            <div class="col-md-3">
			            <div class="form-group">
                          <label>Emp Name</label>
                          <asp:Label ID="lblEmpName" runat="server" CssClass="form-control"  BackColor="#F3F3F3"></asp:Label>
                        </div>
			            </div>
                       <!-- end col-3 -->
                        <!-- begin col-3 -->
			            <div class="col-md-4">
			            <div class="form-group">
                          <label>Reason</label>
                          <asp:TextBox ID="txtReason" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                        </div>
			            </div>
                       <!-- end col-3 -->
                        <!-- begin col-4 -->
                                <div class="col-md-2">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnCancel" Text="Cancel" class="btn btn-danger" 
                                         onclick="btnCancel_Click"/>
								 </div>
                               </div>
                              <!-- end col-4 -->
                        </div>
                        <!-- end row -->
                        
                         <!-- table start -->
					<div class="col-md-12">
					   <div class="form-group">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>EmpNo</th>
                                                <th>FirstName</th>
                                                <th>Designation</th>
                                                <th>Status</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                    <td><%# Eval("EmpNo")%></td>
                                    <td><%# Eval("FirstName")%></td>
                                    <td><%# Eval("Designation")%></td>
                                    <td><%# Eval("Emp_Status")%></td>
                                    <td>
                                    <asp:LinkButton ID="btnViewEnquiry_Grid" class="btn-sm btn-success btn-icon icon-printer"  runat="server" 
                                           Text="" OnCommand="GridViewEnquiryClick" CommandArgument='<%# Eval("FirstName")%>' CommandName='<%# Eval("EmpNo")%>'>
                                     </asp:LinkButton>
                                     
                                     <asp:LinkButton ID="btnCancelEnquiry_Grid" class="btn-sm btn-danger btn-icon icon-ban"  runat="server" 
                                        Text="" OnCommand="GridCancelEnquiryClick" CommandArgument='<%# Eval("FirstName")+","+ Eval("Cancel_Reson")%>' CommandName='<%# Eval("EmpNo")%>' 
                                       CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this Employee details?');">
                                     </asp:LinkButton>
                                     <asp:LinkButton ID="btnApprvEnquiry_Grid" class="btn-sm btn-success btn-icon icon-check"  runat="server" 
                                           Text="" OnCommand="GridApproveEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("EmpNo")%>'
                                           CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this Employee details?');">
                                     </asp:LinkButton>
                                     <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn-sm btn-primary btn-icon icon-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEntryClick" CommandArgument="Edit" CommandName='<%# Eval("MachineID")%>'>
                                                    </asp:LinkButton>
                                    </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                      
                        </div>
                    </div>
                 
                </div>
                  </div>
            </div>
         
        </div>
  <!-- content-wrapper ends -->
      <footer class="footer">
                        <div class="d-sm-flex justify-content-center justify-content-sm-between">
                            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
                            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
                        </div>
                    </footer>

</asp:Content>


