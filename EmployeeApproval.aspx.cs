﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class EmployeeApproval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Employee Approval";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Session.Remove("MachineID");

            Load_Division();
        }
        Load_Data();
    }
    private void Load_Division()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlDivision.Items.Clear();
        query = "Select *from Division_Master where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDivision.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Division"] = "-Select-";
        dr["Division"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDivision.DataTextField = "Division";
        ddlDivision.DataValueField = "Division";
        ddlDivision.DataBind();

    }

    private void Load_Data()
    {
            string query = "";
        DataTable DT = new DataTable();
        query = "Select ENJ.EmpNo,ES.MachineID,ES.Token_No,ENJ.FirstName,ENJ.DeptName,ENJ.Designation,ES.Emp_Status,ES.Cancel_Reson";
        query = query + " from Employee_Mst_New_Emp ENJ inner join Employee_Mst_Status ES on ES.CompCode=ENJ.CompCode";
        query = query + " And ES.LocCode=ENJ.LocCode And ES.Token_No=ENJ.ExistingCode And ES.MachineID=ENJ.MachineID where ENJ.CompCode='" + SessionCcode + "'";
        query = query + " And ENJ.LocCode='" + SessionLcode + "' And ES.CompCode='" + SessionCcode + "' AND ES.LocCode='" + SessionLcode + "' And ENJ.IsActive='Yes'";
        query = query + " And (ES.Emp_Status='Pending' Or ES.Emp_Status='Cancel')"; 
        if (ddlDivision.SelectedValue != "-Select-")
        {
            query = query + " And ENJ.Division='" + ddlDivision.SelectedItem.Text + "'";
        }
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater1.DataSource = DT;
        Repeater1.DataBind();

    }

    protected void GridApproveEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT=new DataTable();
        DataTable DT_Check=new DataTable();
        SSQL = "Select * from Employee_Mst_New_Emp Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + e.CommandName.ToString() + "'";
        DT=objdata.RptEmployeeMultipleDetails(SSQL);
        if(DT.Rows.Count!=0)
        {
            DataTable Cancel_DT = new DataTable();
            SSQL = "Select * from Employee_Mst_Status Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + e.CommandName.ToString() + "'";
            SSQL = SSQL + " And Emp_Status='Cancel'";
            Cancel_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (Cancel_DT.Rows.Count == 0)
            {
                //Insert Employee Master Table
                SSQL = "Select * from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + e.CommandName.ToString() + "'";
                DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Check.Rows.Count != 0)
                {
                    SSQL = "Delete from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

                SSQL = "INSERT INTO Employee_Mst Select * from Employee_Mst_New_Emp";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);



                //Employee Level
                DataTable DT_Chk = new DataTable();
                SSQL = "Select * from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + e.CommandName.ToString() + "'";
                DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                string EmpLevel = ""; string ExistingCode = "";
                if (DT_Check.Rows.Count != 0)
                {
                    EmpLevel = DT_Check.Rows[0]["EmpLevel"].ToString();
                    ExistingCode = DT_Check.Rows[0]["ExistingCode"].ToString();
                    if (EmpLevel == "Semi-Exp")
                    {
                        DataTable DT_Level = new DataTable();

                        SSQL = "Select *from Training_Level_Change where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        SSQL = SSQL + " And EmpNo='" + e.CommandName.ToString() + "'";
                        DT_Level = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (DT_Level.Rows.Count == 0)
                        {
                            SSQL = "insert into Training_Level_Change(Ccode,Lcode,EmpNo,MachineID,ExistingCode,";
                            SSQL = SSQL + "Training_Level,Level_Date)values('" + SessionCcode + "','" + SessionLcode + "',";
                            SSQL = SSQL + "'" + e.CommandName.ToString() + "','" + e.CommandName.ToString() + "',";
                            SSQL = SSQL + "'" + ExistingCode + "','Semi-Exp',convert(varchar,GETDATE(),103))";
                            objdata.RptEmployeeMultipleDetails(SSQL);
                        }
                    }
                }


                //Update Employee Status
                SSQL = "Update Employee_Mst_Status set Emp_Status='Completed',Cancel_Reson=''";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "Delete from Employee_Mst_New_Emp Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Approved Successfully..!');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee get Cancelled.. Cannot Approve..!');", true);
            }
        }
        Load_Data();
    }

    protected void GridCancelEnquiryClick(object sender, CommandEventArgs e)
    {
        lblEmpNo.Text = e.CommandName.ToString();
        string[] Split_Str = e.CommandArgument.ToString().Split(',');

        lblEmpName.Text = Split_Str[0].ToString();
        txtReason.Text = Split_Str[1].ToString();
    }

    protected void GridViewEnquiryClick(object sender, CommandEventArgs e)
    {
        string empNo = e.CommandArgument.ToString() + "->" + e.CommandName.ToString();
        string Employee_Status = "";
        string query = "Select * from Employee_Mst_Status where MachineID='" + empNo + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DataTable DT_Q = new DataTable();
        DT_Q = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Q.Rows.Count != 0)
        {
            Employee_Status = DT_Q.Rows[0]["Emp_Status"].ToString();
        }
        ResponseHelper.Redirect("EmployeeFull.aspx?Division=" + ddlDivision.SelectedItem.Text + "&status=" + Employee_Status + "&Empcode=" + empNo.ToString(), "_blank", "");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Load_Data();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        if (lblEmpNo.Text != "")
        {
            if (txtReason.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the reason to cancel..!');", true);
            }
        }
        else
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Employee to cancel..!');", true);
        }

        if (!ErrFlag)
        {
            //Update Employee Status
             SSQL = "Update Employee_Mst_Status set Emp_Status='Cancel',Cancel_Reson='" + txtReason.Text + "'";
             SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + lblEmpNo.Text + "'";
             objdata.RptEmployeeMultipleDetails(SSQL);

             ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Cancelled Successfully..!');", true);

             lblEmpNo.Text = "";
             lblEmpName.Text = "";
             txtReason.Text = "";
        }
        Load_Data();
    }
    protected void GridEditEntryClick(object sender, CommandEventArgs e)
    {
        Session.Remove("MachineID");
        Session["MachineID_Apprv"] = e.CommandName.ToString();
        Response.Redirect("EmployeeDetails.aspx");
    }
}
