﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EmployeeDetails.aspx.cs" Inherits="EmployeeDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

     <link href="assets/plugins/ModalPopup/CSS/Popup.css" rel="stylesheet" type="text/css"/>
 <script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
        <script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
   <script>
    $(document).ready(function() {
        //alert('hi');
    $('#example').dataTable();
    $('#example1').dataTable();
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            
            autoclose: true
        });
        
    });
</script>
    <script type="text/javascript">
     function showimagepreview(input) {
         if (input.files && input.files[0]) {
             var filerdr = new FileReader();
             filerdr.onload = function(e) {
             $('#img1').attr('src', e.target.result);
             }
             filerdr.readAsDataURL(input.files[0]);
         }
     }
     </script>

 <script type="text/javascript">
     function showimagepreview2(input) {
         if (input.files && input.files[0]) {
             var filerdr = new FileReader();
             filerdr.onload = function(e) {
                 $('#img2').attr('src', e.target.result);
             }
             filerdr.readAsDataURL(input.files[0]);
         }
     }
     </script>
<%--<script type="text/javascript">
    function UploadFile(fileUpload) {
        if (fileUpload.value != '') {
            document.getElementById("<%=btnUpload.ClientID %>").click();
        }
    }
</script>--%>


<script type="text/javascript">
  
        function loadm(url) {
            var img = new Image();
            var bcgDiv = document.getElementById("divBackground");
            var imgDiv = document.getElementById("divImage");
            var imgFull = document.getElementById("imgFull");
            var imgLoader = document.getElementById("imgLoader");
            imgLoader.style.display = "block";
            img.onload = function() {
                imgFull.src = img.src;
                imgFull.style.display = "block";
                imgLoader.style.display = "none";
            };
            img.src = url;
            var width = document.body.clientWidth;
            if (document.body.clientHeight > document.body.scrollHeight) {
                bcgDiv.style.height = document.body.clientHeight + "px";
            }
            else {
                bcgDiv.style.height = document.body.scrollHeight + "px";
            }
            imgDiv.style.left = (width - 650) / 2 + "px";
            imgDiv.style.top = "100px";
            bcgDiv.style.width = "100%";

            bcgDiv.style.display = "block";
            imgDiv.style.display = "block";
            return false;
        }
        function HideDiv() {
            var bcgDiv = document.getElementById("divBackground");
            var imgDiv = document.getElementById("divImage");
            var imgFull = document.getElementById("imgFull");
            if (bcgDiv != null) {
                bcgDiv.style.display = "none";
                imgDiv.style.display = "none";
                imgFull.style.display = "none";
            }
        }
  
</script>
<script type="text/javascript">
//    function uploadStarted() {
//        $get("imgDisplay").style.display = "none";
//    }
//    function uploadComplete(sender, args) {
//        var imgDisplay = $get("imgDisplay");
//        imgDisplay.src = "images/loader.gif";
//        imgDisplay.style.cssText = "";
//        var img = new Image();
//        img.onload = function() {
//            imgDisplay.style.cssText = "height:100px;width:100px";
//            imgDisplay.src = img.src;
//        };
//       
//    }
</script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <asp:UpdatePanel ID="UpdatePanel1" runat="server" EnableViewState="true">
        <ContentTemplate>
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">Employee Details </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Employee Profile</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Employee Details</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="forms-sample">
                            <div class="row">
                                <div class="form-group col-lg-2">
                                    <label for="exampleInputConfirmPassword1">Mill Code</label><span class="asterisk_input"></span>
                                    <asp:TextBox ID="txtMillCode" class="form-control" AutoPostBack="true" OnTextChanged="txtMillCode_OnTextChange" runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ControlToValidate="txtMillCode"   Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                     </asp:RequiredFieldValidator>
                                </div>
                                 <div class="form-group col-lg-2">
                                    <label for="exampleInputConfirmPassword1">Mill Name</label><span class="asterisk_input"></span>
                                     <asp:TextBox ID="txtMillName"  class="form-control"  runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtMillName"  Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                                 <div class="form-group col-lg-2">
                                    <label for="exampleInputConfirmPassword1">Unit Name</label><span class="asterisk_input"></span>
                                    <asp:DropDownList ID="ddlunit" class="js-example-basic-single select2"  AutoPostBack=true OnSelectedIndexChanged="ddlUnits_onselectedindexchanged" style="width:100%;" runat="server"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ControlToValidate="ddlunit" InitialValue="-Select-"  Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                                 <div class="form-group col-lg-2">
                                    <label for="exampleInputConfirmPassword1">Unit Type</label><span class="asterisk_input"></span>
                                    <asp:TextBox ID="txtunitType" class="form-control"  runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ControlToValidate="txtunitType"   Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                </div>
                                <div class="col-md-3">
                                             <div class="media" style="margin-top: -19px;">
                                                    <a class="media-right" href="javascript:;" style="float: right;">
                                                        <asp:Image ID="Image3" runat="server" class="media-object" AlternateText="ProfilePhoto" style="width: 158px;height: 161px;" ImageUrl="~/assets/img/login-bg/man-user-50.png" />
                                                   <img id="img1" alt="" height="100%" width="100%" /> 
                                                    </a></br>
                                                    <asp:FileUpload ID="FileUpload1" runat="server" onchange="showimagepreview(this)" />
                                                    <%--<asp:Button ID="btnUpload" Text="Upload" runat="server" OnClick="Upload" Style="display: none" />--%>
                                                </div>
                              </div>
                            </div>
                            <div class="row">
                             <div class="form-group col-lg-4">
                                    <label for="exampleInputEmail1">ITF No (Machine ID)</label><span class="asterisk_input"></span>
                                    <asp:TextBox ID="txtMachineID" class="form-control" runat="server" AutoPostBack="true" OnTextChanged="txtMachineID_TextChanged"></asp:TextBox>
                                     <asp:RequiredFieldValidator ControlToValidate="txtMachineID"  Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator15" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                     </asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label for="exampleInputUsername1">Employee No</label><span class="asterisk_input"></span>
                                    <asp:TextBox ID="txtEmployeeNo" runat="server" class="form-control"></asp:TextBox>
                                     <asp:RequiredFieldValidator ControlToValidate="txtEmployeeNo"  Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                     </asp:RequiredFieldValidator>
                                </div>
                                  <div class="form-group  col-lg-4">
                                    <label for="exampleInputPassword1">Participant Name</label><span class="asterisk_input"/></span>
                                    <asp:TextBox ID="txtEmployeeName" class="form-control" runat="server"></asp:TextBox>
                                      <asp:RequiredFieldValidator ControlToValidate="txtEmployeeName"  Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                </div>
                                 <div class="form-group col-lg-4">
                                    <label for="exampleInputConfirmPassword1">Date Of birth</label><span class="asterisk_input"></span>
                                    <asp:TextBox ID="txtDateofBirth" class="form-control datepicker" runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ControlToValidate="txtDateofBirth"  Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                     </asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label for="exampleInputConfirmPassword1">Department Name</label>
                                    <asp:DropDownList ID="ddlDept" class="js-example-basic-single select2" AutoPostBack="true" OnSelectedIndexChanged="ddlDept_OnSelectedIndexChanged" style="width:100%;" runat="server"></asp:DropDownList>
                                  <%--  <asp:RequiredFieldValidator ControlToValidate="ddlDept" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>--%>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label for="exampleInputConfirmPassword1">Designation Name</label><span class="asterisk_input"></span>
                                    <asp:DropDownList ID="ddlDesignation" class="js-example-basic-single select2" style="width:100%;" runat="server"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ControlToValidate="ddlDesignation" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label for="exampleInputConfirmPassword1">Program Name</label><span class="asterisk_input"></span>
                                    <asp:DropDownList ID="ddlCourseName" class="js-example-basic-single select2" AutoPostBack="true" OnSelectedIndexChanged="ddlCourseName_OnSelectedIndexChanged" style="width:100%;" runat="server"></asp:DropDownList>
                                     <asp:RequiredFieldValidator ControlToValidate="ddlCourseName" InitialValue="-Select-"  Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                </div>
                                
                                <div class="form-group col-lg-4">
                                    <label for="exampleInputConfirmPassword1">Program Level</label><span class="asterisk_input"></span>
                                     <asp:DropDownList ID="ddlcourselevel" class="js-example-basic-single select2" AutoPostBack="true"  style="width:100%;" runat="server"></asp:DropDownList>
                                  <%-- <asp:TextBox ID="txtCourseLevel" class="form-control" runat="server"></asp:TextBox>--%>
                                    <asp:RequiredFieldValidator ControlToValidate="ddlcourselevel"   Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                               </div>
                                  <div class="form-group col-lg-4">
                                    <label for="exampleInputConfirmPassword1">Date Of Joining</label>
                                    <asp:TextBox ID="txtDateofjoin" class="form-control datepicker" placeholder="dd/MM/YYYY" runat="server"></asp:TextBox>
                                       <asp:RequiredFieldValidator ControlToValidate="txtDateofjoin"  Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                       </asp:RequiredFieldValidator>
                                </div>
                                </div>
                                <div class="row">
                                 <div class="form-group col-lg-3">
                                 <label class="LabelColor">Gender</label><span class="asterisk_input"></span>
                                                        <asp:DropDownList ID="ddlGender" runat="server" class="js-example-basic-single select2" style="width:100%;">
                                                        <asp:ListItem Value="-Select-" Text="-Select-"></asp:ListItem>
                                                         <asp:ListItem Value="MALE" Text="MALE"></asp:ListItem>
                                                         <asp:ListItem Value="FEMALE" Text="FEMALE"></asp:ListItem>
                                                        </asp:DropDownList>
                                 </div>
                               
                                 <div class="form-group col-lg-3">
                                    <label for="exampleInputConfirmPassword1">Active Mode</label>
                                    
                                                            <asp:RadioButtonList ID="dbtnActive" runat="server" class="form-control  BorderStyle" 
                                                                RepeatDirection="Horizontal" AutoPostBack="true" 
                                                            onselectedindexchanged="dbtnActive_SelectedIndexChanged">
                                                              <asp:ListItem Value="1" Text="Yes" style="padding-right:40px" Selected="True"></asp:ListItem>
                                                              <asp:ListItem Value="2" Text="No"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                    </div>
                                     <div class="form-group col-lg-3">
                                                         <label class="LabelColor">Relieved Date</label>
                                                        <asp:TextBox ID="txtReliveDate" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                        
                                                    </div>
                                    <!-- begin col-4 -->
                                                <div class="form-group col-md-3">
                                                        <label class="LabelColor">Reason for Relieving</label>
                                                        <asp:TextBox ID="txtReason" runat="server" class="form-control BorderStyle" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                                                   
                                                </div>
                                                <!-- end col-4 -->
                            </div>
                               <asp:Panel ID="Approve_Cancel_panel" runat="server" Visible="false">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="LabelColor">Employee Cancel Reason</label>
                                        <asp:TextBox ID="txtCanecel_Reason_Approve" runat="server" TextMode="MultiLine" class="form-control BorderStyle"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                            <div align="center">
                                <asp:Button ID="btnSave" class="btn btn-success mr-2" ValidationGroup="Validate_Field" runat="server" Text="Save" OnClick="btnSave_Click" />
                                <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" />
                                  <asp:Button runat="server" id="btnCancel_Approve" Text="Cancel" class="btn btn-danger" 
                                                    onclick="btnCancel_Approve_Click" Visible="false"/>
                            
                                <asp:Button runat="server" ID="btnBack" Text="Back" class="btn btn-primary" OnClick="btnBack_Click" Visible="false" />
                                <asp:Button runat="server" ID="btnApprove" Text="Approve" class="btn btn-success" OnClick="btnApprove_Click" Visible="false" />
                            </div>
                        </div>
                    </div>
                    <div id="divBackground" class="modal">
                                    </div>
                                    <div id="divImage">
                                    <table style="height: 100%; width: 100%">
                                        <tr>
                                            <td valign="middle" align="center">
                                                <img id="imgLoader" alt="" src="images/loader.gif" />
                                                <img id="imgFull" alt="" src="" style="display: none; height: 500px; width: 590px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="bottom">
                                                <input id="btnClose" type="button" value="close" onclick="HideDiv()" />
                                            </td>
                                        </tr>
                                    </table>
                                    </div>
                    <style>.hideupload #ctl00_ContentPlaceHolder1_filUpload_ctl04{display:none}</style>   
                </div>
            </div>
        </div>
    </div>
    <!-- content-wrapper ends -->
    <!-- partial:../../partials/_footer.html -->
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
        </div>
    </footer>
    </ContentTemplate>
    </asp:UpdatePanel>
  <%--  <script type="text/javascript">
        //On UpdatePanel Refresh
       
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function(sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.select2').select2();
                }
            });
        };
</script>--%>
</asp:Content>

