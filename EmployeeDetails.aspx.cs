﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;


public partial class EmployeeDetails : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlg = false;
    string CourseCode;
    protected void Page_Load(object sender, EventArgs e)
    {
        ddlDept.Enabled = false;
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Load_Designation();
            Load_mill();
           Load_Course();
            if (Session["MachineID"] != null)
            {
                txtMachineID.Text = Session["MachineID"].ToString();
                txtMachineID.Enabled = false;
                btnSearch_Click(sender, e);
            }

            if (Session["MachineID_Apprv"] != null)
            {
                txtMachineID.Text = Session["MachineID_Apprv"].ToString();
                txtMachineID.Enabled = false;
                btnSearch_Click(sender, e);
                btnSave.Enabled = false;
                btnBack.Visible = true;
                btnApprove.Visible = true;
                btnCancel_Approve.Visible = true;
                Approve_Cancel_panel.Visible = true;
            
                btnCancel.Visible = false;
            }
        }
    }
    protected void Load_Designation()
    {
        SSQL = "";
        SSQL = "Select Distinct DesignName from Designation_Mst order by DesignName asc";
        ddlDesignation.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlDesignation.DataTextField = "DesignName";
        ddlDesignation.DataValueField = "DesignName";
        ddlDesignation.DataBind();
        ddlDesignation.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void Load_mill()
    {
    //    SSQL = "";
    //    SSQL = "Select * from MstMill";
    //    ddlMillName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
    //    ddlMillName.DataTextField = "MillName";
    //    ddlMillName.DataValueField = "MillCode";
    //    ddlMillName.DataBind();
    //    ddlMillName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void Load_Course()
    {

        SSQL = "";
        SSQL = "Select * from MstCourse";
        ddlCourseName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlCourseName.DataTextField = "CourseName";
        ddlCourseName.DataValueField = "CourseCode";
        ddlCourseName.DataBind();
        ddlCourseName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataTable DT_Check = new DataTable();
        string path_3 = "";
        btnSave.Text="Update";
       
        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT_Check.Rows.Count == 0)
        {
            SSQL = "Select * from Employee_Mst_New_Emp where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        }

        if (DT_Check.Rows.Count != 0)
        {

            txtEmployeeNo.Text = DT_Check.Rows[0]["ExistingCode"].ToString();
            txtMachineID.Text = DT_Check.Rows[0]["EmpNo"].ToString();
            txtEmployeeName.Text= DT_Check.Rows[0]["FirstName"].ToString();
            ddlDesignation.SelectedValue= DT_Check.Rows[0]["Designation"].ToString();
            txtDateofBirth.Text = Convert.ToDateTime(DT_Check.Rows[0]["BirthDate"]).ToString("dd/MM/yyyy");
            txtDateofjoin.Text = Convert.ToDateTime(DT_Check.Rows[0]["DOJ"]).ToString("dd/MM/yyyy");
            txtMillName.Text = DT_Check.Rows[0]["MillName"].ToString();
            txtMillCode.Text= DT_Check.Rows[0]["MillCode"].ToString();
            Load_Units(DT_Check.Rows[0]["MillCode"].ToString());
            ddlunit.SelectedValue = DT_Check.Rows[0]["UnitCode"].ToString();
            txtunitType.Text = DT_Check.Rows[0]["UnitType"].ToString();
            Load_DeptName(DT_Check.Rows[0]["MillCode"].ToString(), DT_Check.Rows[0]["UnitCode"].ToString());
            ddlDept.SelectedValue = DT_Check.Rows[0]["DeptName"].ToString();
            ddlCourseName.SelectedValue= DT_Check.Rows[0]["CourseCode"].ToString();
             CourseCode = DT_Check.Rows[0]["CourseCode"].ToString();
            ddlcourselevel.SelectedValue= DT_Check.Rows[0]["CourseLevel"].ToString();

          
          
            string courselvl;

            courselvl = DT_Check.Rows[0]["CourseLevel"].ToString();

            string[] Test = courselvl.Split(' ');
            string S1 = Test[0].ToString();
            string S2 = Test[1].ToString();

            int Courselevel_Int = Convert.ToInt32(S2);
            ddlCourseName_OnSelectedIndexChanged(CourseCode, e);
            ddlcourselevel.SelectedValue = S2;
       
        


            ddlGender.SelectedValue = DT_Check.Rows[0]["Gender"].ToString();


            if (DT_Check.Rows[0]["DOR"].ToString() != null && DT_Check.Rows[0]["DOR"].ToString() != "")
            {
                txtReliveDate.Text = Convert.ToDateTime(DT_Check.Rows[0]["DOR"].ToString()).ToString("dd/MM/yyyy");
            }
            txtReason.Text =DT_Check.Rows[0]["Reason"].ToString();
            DataTable DT_Photo = new DataTable();
            string SS = "Select * from Photo_Path_Det";
            DT_Photo = objdata.RptEmployeeMultipleDetails(SS);
            string UNIT_Folder = "";
            string PhotoDet1 = "";
            if (DT_Photo.Rows.Count != 0)
            {
                PhotoDet1 = DT_Photo.Rows[0]["Photo_Path"].ToString();
            }

            //if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I"; }
            //if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II"; }
            //if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III"; }
            //if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "Emp_Scan/UNIT_IV"; }

            if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet1 + "/UNIT_I/"; }
            if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = PhotoDet1 + "/UNIT_II/"; }
            if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = PhotoDet1 + "/UNIT_III/"; }
            if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = PhotoDet1 + "/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }
            
            //string path_1 = "~/" + UNIT_Folder + "/Photos/" + txtTokenID.Text + ".jpg";
            string path_1 = UNIT_Folder + "/Photos/" + txtMachineID.Text + ".jpg";
            DirectoryInfo di = new DirectoryInfo(path_1);
            byte[] imageBytes = new byte[0];
            //File Check
            //if (File.Exists(Server.MapPath(path_1)))
            if (File.Exists((path_1)))
            {
                //txtExistingCode.Text = path_1;
                Image3.Visible = true;
                //Image3.ImageUrl = (@"" + path_1);

                Image3.ImageUrl = "Handler.ashx?f=" + path_1 + "";
                //imageBytes = File.ReadAllBytes(di);
                //imageBytes = File.ReadAllBytes(@"" + path_1);
                //PictureBox imageControl = new PictureBox();

                //imageControl.Width = 400;

                //imageControl.Height = 400;
                //Bitmap image = new Bitmap(path_1);
                //imageControl.Image = (System.Drawing.Image)image;
                //Image3.ImageUrl = ResolveUrl(path_1);
                //img1.Src = (path_1);
            }
            else
            {
                //txtExistingCode.Text = path_1 + " Not Match";
                Image3.Visible = true;
                Image3.ImageUrl = "~/assets/images/No_Image.jpg";
                //img1.Src = "~/assets/images/No_Image.jpg";
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

        string Table_Name = "";

        if (btnSave.Text == "Save")
        {
            Table_Name = "Employee_Mst";
        }
        else
        {
            Table_Name = "Employee_Mst_New_Emp";
        }

        if (txtMachineID.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter the ITF No (MachineID)');", true);
            ErrFlg = true;
            return;
        }
        if(txtEmployeeNo.Text=="")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter the Employee No');", true);
            ErrFlg = true;
            return;
        }
        if (txtEmployeeName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter the Employee Name');", true);
            ErrFlg = true;
            return;
        }
        if (ddlDesignation.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Select the Designation');", true);
            ErrFlg = true;
            return;
        }
        if (txtDateofBirth.Text=="")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date of Birth');", true);
            ErrFlg = true;
            return;
        }
        if (txtDateofjoin.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date of Joining');", true);
            ErrFlg = true;
            return;
        }
        if (txtunitType.Text=="")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter the Mill Name');", true);
            ErrFlg = true;
            return;
        }
        if (txtMillCode.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter the Mill Code');", true);
            ErrFlg = true;
            return;
        }
        if (ddlCourseName.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose the Program ');", true);
            ErrFlg = true;
            return;
        }
        if (ddlcourselevel.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose the Program Level ');", true);
            ErrFlg = true;
            return;
        }
        DataTable Desig = new DataTable();
        string DesigValue="";
        SSQL = "";
        SSQL = "Select  Distinct DesignCode from Designation_Mst where DesignName ='" +ddlDesignation.SelectedItem.Text +"'";
        Desig = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Desig != null && Desig.Rows.Count > 0)
        {
            DesigValue = Desig.Rows[0]["DesignCode"].ToString();
        }
            if (!ErrFlg && btnSave.Text!="Update")
        {
            SSQL = "";
            SSQL = "select * from Employee_Mst where EmpNo='" + txtMachineID.Text + "' and CompCode='" + SessionCcode + "' and Loccode='" + SessionLcode + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt == null || dt.Rows.Count <= 0)
            {
                SSQL = "";
                SSQL = "Select * from Employee_Mst_New_Emp where EmpNo='" + txtMachineID.Text + "' and CompCode='" + SessionCcode + "' and loccode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Employee Number Already Taken');", true);
                    ErrFlg = true;
                    return;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Employee Number Already Taken');", true);
                ErrFlg = true;
                return;
            }
        }
        if (!ErrFlg)
        {
            SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
            DataTable DT_Check = new DataTable();
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);


            if (DT_Check.Rows.Count != 0)
             //   if (DT_Check.Rows.Count == 0)
                {
                Table_Name = "Employee_Mst";
            }
            else
            {
                Table_Name = "Employee_Mst_New_Emp";
            }
        }
        if (!ErrFlg)
        {
            DataTable DT_Photo = new DataTable();
            string SS = "Select *from Photo_Path_Det";
            DT_Photo = objdata.RptEmployeeMultipleDetails(SS);
            string UNIT_Folder = "";
            string Doc_Folder = "";
            string PhotoDet = "";
            if (DT_Photo.Rows.Count != 0)
            {
                PhotoDet = DT_Photo.Rows[0]["Photo_Path"].ToString();
            }

            if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet + "/UNIT_I/"; }
            if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = PhotoDet + "/UNIT_II/"; }
            if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = PhotoDet + "/UNIT_III/"; }
            if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = PhotoDet + "/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }


            string token_Name = txtEmployeeNo.Text;

            string path_1 = UNIT_Folder;
           
            if (FileUpload1.HasFile)
            {
                Doc_Folder = "Photos/";
                string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                string Exten = Path.GetExtension(FileUpload1.PostedFile.FileName);

                //FileUpload1.SaveAs(Server.MapPath("~/" + path_1 + Doc_Folder + token_Name + Exten));
                FileUpload1.SaveAs((path_1 + Doc_Folder + token_Name + Exten));
            }
            string MachineID_Encrypt = UTF8Encryption(txtMachineID.Text);

            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Update " + Table_Name + " set ExistingCode='" + txtEmployeeNo.Text + "',Firstname='" + txtEmployeeName.Text + "',MachineID_Encrypt='" + MachineID_Encrypt + "',";
                SSQL = SSQL + "Designation='" + ddlDesignation.SelectedItem.Text + "',BirthDate=Convert(datetime,'" + txtDateofBirth.Text + "',105),DOJ=Convert(datetime,'" + txtDateofjoin.Text + "',105),MillName='" + txtMillName.Text + "',";
                SSQL = SSQL + "MillCode='" + txtMillCode.Text + "',DesignCode='" + ddlDesignation.SelectedValue + "',Reason='" + txtReason.Text + "',DOR='" + txtReliveDate.Text + "',";
                SSQL = SSQL + "Coursename='" + ddlCourseName.SelectedItem.Text + "',CourseCode='" + ddlCourseName.SelectedValue + "',CourseLevel='" + ddlcourselevel.SelectedValue + "',";
                SSQL = SSQL + "DeptCode='" + ddlDept.SelectedValue + "',DeptName='" + ddlDept.SelectedItem.Text + "',UnitCode='" + ddlunit.SelectedValue + "',UnitName='" + ddlunit.SelectedItem.Text + "',Unittype='" + txtunitType.Text + "',IsActive='" + dbtnActive.SelectedItem.Text + "',EmpStatus='ON ROLL',Gender='" + ddlGender.SelectedItem.Text + "' where EmpNo='" + txtMachineID.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Details UpDated Successfully');", true);
                Clear_All_Field();
            }
            else
            {
                
                //Emp Status Update
                string Emp_Status = "";
                if (Table_Name == "Employee_Mst_New_Emp")
                {
                    Emp_Status = "Pending";
                }
                else
                {
                    Emp_Status = "Completed";
                }
              

                SSQL = "";
                SSQL = "Insert into " + Table_Name + " (CompCode,LocCode,TypeName,EmpPrefix,isActive,Gender,EmpStatus,MachineID,EmpNo,ExistingCode,FirstName,Designation,BirthDate,DOJ,MillCode,MillName,UnitCode,UnitName,DeptCode,DeptName,DesignCode,Reason,DOR,UnitType,MachineID_Encrypt,CourseName,CourseCode,CourseLevel)";
                SSQL = SSQL + "Values('" + SessionCcode + "','" + SessionLcode + "','0','A','" + dbtnActive.SelectedItem.Text + "','" + ddlGender.SelectedItem.Text + "','ON ROLL','" + txtMachineID.Text + "','" + txtMachineID.Text + "','" + txtEmployeeNo.Text + "','" + txtEmployeeName.Text + "','" + ddlDesignation.SelectedItem.Text + "',";
                SSQL = SSQL + "Convert(datetime,'" + txtDateofBirth.Text + "',105),convert(datetime,'" + txtDateofjoin.Text + "',105),'" + txtMillCode.Text + "','" + txtMillName.Text + "',";
                //SSQL = SSQL + "'" + ddlunit.SelectedValue + "','" + ddlunit.SelectedItem.Text + "','" + ddlDept.SelectedValue + "','" + ddlDept.SelectedItem.Text + "','" + ddlDesignation.SelectedValue + "','" + txtReason.Text + "','" + txtReliveDate.Text + "','" + txtunitType.Text + "','" + MachineID_Encrypt + "')";
                SSQL = SSQL + "'" + ddlunit.SelectedValue + "','" + ddlunit.SelectedItem.Text + "','" + ddlDept.SelectedValue + "','" + ddlDept.SelectedItem.Text + "','" + DesigValue + "','" + txtReason.Text + "','" + txtReliveDate.Text + "','" + txtunitType.Text + "','" + MachineID_Encrypt + "',";
                SSQL = SSQL + "'" + ddlCourseName.SelectedItem.Text + "','" + ddlCourseName.SelectedValue + "','" + ddlcourselevel.SelectedValue + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                DataTable EmpStatus = new DataTable();
                SSQL = "Select * from Employee_Mst_Status where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And MachineID='" + txtMachineID.Text + "'";
                EmpStatus = objdata.RptEmployeeMultipleDetails(SSQL);
                if (EmpStatus.Rows.Count != 0)
                {
                    //Update
                    SSQL = "Update Employee_Mst_Status set Token_No='" + txtEmployeeNo.Text + "',Emp_Status='" + Emp_Status + "',Cancel_Reson='' where";
                    SSQL = SSQL + " CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                }
                else
                {
                    //Insert
                    SSQL = "Insert Into Employee_Mst_Status(CompCode,LocCode,Token_No,MachineID,Emp_Status,Cancel_Reson)";
                    SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtEmployeeNo.Text + "','" + txtMachineID.Text + "','" + Emp_Status + "','')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    //InsertDeleteUpdate_Lunch_Server(SSQL)
                }

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Details Saved Successfully');", true);
                Session.Remove("MachineID");
                Clear_All_Field();
                Response.Redirect("Employee_Main.aspx");
            }
        }
    }
    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }

    protected void dbtnActive_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dbtnActive.SelectedValue == "2")
        {
            txtReason.Enabled = true;
        }
        else
        {
            txtReason.Enabled = false;
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("MachineID");
        Response.Redirect("EmployeDetails_Main.aspx");
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        DataTable DT = new DataTable();
        DataTable DT_Check = new DataTable();
        SSQL = "Select * from Employee_Mst_New_Emp Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            DataTable Cancel_DT = new DataTable();
            SSQL = "Select * from Employee_Mst_Status Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
            SSQL = SSQL + " And Emp_Status='Cancel'";
            Cancel_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (Cancel_DT.Rows.Count == 0)
            {
                //Insert Employee Master Table
                SSQL = "Select * from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
                DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Check.Rows.Count != 0)
                {
                    SSQL = "Delete from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

                SSQL = "INSERT INTO Employee_Mst Select * from Employee_Mst_New_Emp";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);


                //Update Employee Status
                SSQL = "Update Employee_Mst_Status set Emp_Status='Completed',Cancel_Reson=''";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "Delete from Employee_Mst_New_Emp Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Approved Successfully..!');", true);

                Clear_All_Field();
                Response.Redirect("EmployeeApproval.aspx");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee get Cancelled.. Cannot Approve..!');", true);
            }
        }
    }

    protected void btnCancel_Approve_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtCanecel_Reason_Approve.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Reason To Cancel..!');", true);
            txtCanecel_Reason_Approve.Focus();
        }

        if (!ErrFlag)
        {
            //Update Employee Status
            SSQL = "Update Employee_Mst_Status set Emp_Status='Cancel',Cancel_Reson='" + txtCanecel_Reason_Approve.Text + "'";
            SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Cancelled Successfully..!');", true);

            Clear_All_Field();
            Response.Redirect("EmployeeApproval.aspx");
        }
    }
    private void Clear_All_Field()
    {
        txtMillName.Text = "";
        txtReliveDate.Text = "";
        ddlGender.ClearSelection();
        txtReason.Text = "";
        txtMachineID.Text = "";
        txtEmployeeName.Text = "";
        txtEmployeeNo.Text = "";
        txtDateofBirth.Text = "";
        txtDateofjoin.Text = "";
        ddlDesignation.ClearSelection();
       // ddlCourseName.ClearSelection();
        txtunitType.Text = "";
        txtMillCode.Text = "";
       // txtCourseLevel.Text = "";
        ddlunit.ClearSelection();
        ddlDept.ClearSelection();
        txtunitType.Text = "";
       
        txtMachineID.Enabled = true;
        Session.Remove("MachineID");
        Session.Remove("MachineID_Apprv");
        btnSave.Text = "Save";
        btnSave.Enabled = true;
        btnBack.Visible = false;
        
        Image3.ImageUrl = "~/assets/img/login-bg/man-user-50.png";
    }
    protected void ddlDept_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from Designation_Mst where DeptCode='" + ddlDept.SelectedValue + "' and UnitCode='" + ddlunit.SelectedValue + "' and MillCode='" + txtMillCode.Text + "'";
        ddlDesignation.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlDesignation.DataTextField = "DesignName";
        ddlDesignation.DataValueField = "DesignCode";
        ddlDesignation.DataBind();
        ddlDesignation.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void txtMachineID_TextChanged(object sender, EventArgs e)
    {
        if (txtMachineID.Text != "")
        {
            SSQL = "";
            SSQL = "select * from Employee_Mst where MachineID='" + txtMachineID.Text + "' and CompCode='" + SessionCcode + "' and Loccode='" + SessionLcode + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt == null || dt.Rows.Count <= 0)
            {
                SSQL = "";
                SSQL = "Select * from Employee_Mst_New_Emp where MachineID='" + txtMachineID.Text + "' and CompCode='" + SessionCcode + "' and loccode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This ITF Number Already Taken');", true);
                    txtMachineID.Text = "";
                    ErrFlg = true;
                    return;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This ITF Number Already Taken');", true);
                txtMachineID.Text = "";
                ErrFlg = true;
                return;
            }
        }
    }
    protected void ddlCourseName_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        //SSQL = "";
        //SSQL = "Select * from MstCourse where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and CourseCode='" + ddlCourseName.SelectedValue + "'";
        //DataTable dt = new DataTable();
        //dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (dt != null && dt.Rows.Count > 0)
        //{
        //    txtCourseLevel.Text = dt.Rows[0]["Level"].ToString();
        //}
       
        SSQL = "";
        SSQL = "Select Level from MstCourse where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and CourseCode='" + ddlCourseName.SelectedValue + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt != null && dt.Rows.Count > 0)
        {
          //  txtTotalLevel.Text = dt.Rows[0]["Level"].ToString();
            load_LastLevel(dt.Rows[0]["Level"].ToString());
        }
    }
    protected void load_LastLevel(string Courrselevel)
    {

      
            int Courselevel_Int = Convert.ToInt32(Courrselevel);

            DataTable dt = new DataTable();
            dt.Columns.Add("Courselevel");
            DataRow Dr;
            for (int i = 1; i <= Courselevel_Int; i++)
            {
                Dr = dt.NewRow();
                Dr["Courselevel"] = "MODULE " + i;
                dt.Rows.Add(Dr);
            }
        
        ddlcourselevel.DataSource = dt;
        ddlcourselevel.DataTextField = "Courselevel";
        ddlcourselevel.DataValueField = "Courselevel";
        ddlcourselevel.DataBind();
        ddlcourselevel.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

       

    }
    protected void txtMillCode_OnTextChange(object sender, EventArgs e)
    {
        if (txtMillCode.Text != "")
        {
            SSQL = "";
            SSQL = "Select * from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + txtMillCode.Text + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt != null && dt.Rows.Count > 0)
            {
                txtMillName.Text = dt.Rows[0]["MillName"].ToString();
                Load_Units(dt.Rows[0]["MillCode"].ToString());
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Mill Code Does not Exists!!!');", true);
                txtMillCode.Focus();

            }
        }
    }
   

    private void Load_DeptName(string MillCode, string UnitCode)
    {
        SSQL = "";
        SSQL = "Select * from  Department_Mst where MillCode='" + MillCode + "' and UnitCode='" + UnitCode + "'";
        ddlDept.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlDept.DataValueField = "DeptCode";
        ddlDept.DataTextField = "DeptName";
        ddlDept.DataBind();
        ddlDept.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Units(string MillCode)
    {
        SSQL = "";
        SSQL = "Select * from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + MillCode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count <= 0)
        {
            SSQL = "";
            SSQL = "select Units from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Millcode='" + MillCode + "'";
            DataTable Dt_get = new DataTable();
            Dt_get = objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable Unit_dt = new DataTable();

            Unit_dt.Columns.Add("UnitCode");
            Unit_dt.Columns.Add("UnitName");

            int Unitcode = 0;
            if (Dt_get != null && Dt_get.Rows.Count > 0)
            {

                Unitcode = Convert.ToInt32(Dt_get.Rows[0]["Units"]);
                DataRow dr;

                for (int i = 1; i <= Unitcode; i++)
                {
                    dr = Unit_dt.NewRow();
                    dr["UnitCode"] = "UNIT " + i;
                    dr["UnitName"] = "UNIT " + i;
                    Unit_dt.Rows.Add(dr);
                }

                if (Unit_dt.Rows.Count > 0)
                {
                    ddlunit.DataSource = Unit_dt;
                    ddlunit.DataTextField = "UnitName";
                    ddlunit.DataValueField = "UnitCode";
                    ddlunit.DataBind();
                    ddlunit.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
                }
            }
        }
        else
        {
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlunit.DataSource = dt;
            ddlunit.DataTextField = "UnitName";
            ddlunit.DataValueField = "UnitCode";
            ddlunit.DataBind();
            ddlunit.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }
    protected void ddlUnits_onselectedindexchanged(object sender, EventArgs e)
    {
        if (ddlunit.SelectedItem.Text != "-Select-")
        {
            SSQL = "";
            //    SSQL = "Select (UnitTypeCode+'-'+UnitTypeName) as Units ,MillCode,UnitCode from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + txtMillCode.Text + "'";
                SSQL = "Select (UnitTypeName) as Units ,MillCode,UnitCode from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + txtMillCode.Text + "'";
            SSQL = SSQL + " and UnitCode='" + ddlunit.SelectedValue + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt != null && dt.Rows.Count > 0)
            {
                txtunitType.Text = dt.Rows[0]["Units"].ToString();
                Load_DeptName(dt.Rows[0]["MillCode"].ToString(), dt.Rows[0]["UnitCode"].ToString());
            }
        }
    }
    protected void ddlCourse( string CourseCode)
    {
        if (btnSave.Text == "UPDATE")
        {
            SSQL = "";
            SSQL = "Select Level from MstCourse where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and CourseCode='" + ddlCourseName.SelectedValue + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt != null && dt.Rows.Count > 0)
            {
                //  txtTotalLevel.Text = dt.Rows[0]["Level"].ToString();
                load_LastLevel(dt.Rows[0]["Level"].ToString());
            }
        }
    }

    protected void split()
    {
       
    }
}