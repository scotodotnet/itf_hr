﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Employee_Main.aspx.cs" Inherits="Employee_Main" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>
<asp:UpdatePanel runat="server" EnableViewState="true">
        <ContentTemplate>
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Employee Details</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Master</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Employee Details</li>
                           
                        </ol>
                    </nav>
                </div>
                     <asp:Button runat="server" ID="btnSave" Text="Add New Employee" class="btn btn-success"  OnClick="lbtnAdd_Click" />
                <br />
                <br />  
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="forms-sample">
                                    <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <label>Employee ActiveMode</label>
                        <asp:RadioButtonList ID="rbtnIsActive" runat="server" AutoPostBack="true"
                          RepeatDirection="Horizontal" 
                              onselectedindexchanged="rbtnIsActive_SelectedIndexChanged">
                        <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Pending" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Cancel" Value="4"></asp:ListItem>
                        </asp:RadioButtonList>
                   </div>
                </div>
                       
                             <!-- table start -->
                                    <div class="col-lg-12">  
                               <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
					                    <HeaderTemplate>
                                            <table id="example" class="display table">
                                                <thead>
                                                    <tr>
                                                       <th>ITF ID</th>
                                                        <th>Name</th>
                                                       
                                                        
                                                        <th>Mill Name</th>
                                                        <th>Designation</th>
                                                        <th>Date Of Join</th>
                                                        <th>Edit</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("MachineID")%></td>
                                                <td><%# Eval("FirstName")%></td>
                                                
                                                <td><%# Eval("MillName")%></td>
                                                <td><%# Eval("Designation")%></td>
                                                <td><%# Eval("DOB")%></td>
                                                <td>
                                                    <asp:LinkButton ID="btnEditIssueEntry_Grid"  class="btn-sm btn-success btn-icon icon-pencil"   runat="server" 
                                                        Text="" OnCommand="GridEditEntryClick" CommandArgument="Edit" CommandName='<%# Eval("MachineID")%>'>
                                                    </asp:LinkButton>
                                                    </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
					                </asp:Repeater>
                             </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
                </div>
            </footer>
        </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>

