﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;

public partial class Employee_Update : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Employee Update";
            Load_WagesType();
        }
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpType";
        ddlEmployeeType.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and WeekOff='" + ddlWeekoffFrom.SelectedValue + "'";
        DataTable WeekOff_from_Dt = new DataTable();
        WeekOff_from_Dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (WeekOff_from_Dt != null && WeekOff_from_Dt.Rows.Count > 0)
        {
            SSQL = "";
            SSQL = "Update Employee_Mst set Weekoff='" + ddlWeekOffTo.SelectedValue + "' where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and Weekoff='" + ddlWeekoffFrom.SelectedValue + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('" + WeekOff_from_Dt.Rows.Count + " Employees Transfered from " + ddlWeekoffFrom.SelectedValue + " To " + ddlWeekOffTo.SelectedValue + "');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employees Not Present in this Week off Choose Another Days');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ddlEmployeeType.ClearSelection();
        ddlWeekoffFrom.ClearSelection();
        ddlWeekOffTo.ClearSelection();
    }
}