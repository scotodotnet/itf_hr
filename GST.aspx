﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GST.aspx.cs" Inherits="GST" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  <link href="assets/plugins/ModalPopup/CSS/Popup.css" rel="stylesheet" type="text/css" />
    <script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
    <script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

       <asp:UpdatePanel runat="server" EnableViewState="true">
        <ContentTemplate>
            <br>
            <br></br>
           <%-- <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>
                        GST TYPE</label>
                        <asp:DropDownList ID="ddlGSTType" runat="server" AutoPostBack="true" CssClass="js-example-basic-single select2" OnSelectedIndexChanged="ddlGSTType_SelectedIndexChanged" Style="width: 100%">
                            <asp:ListItem Selected="True" Text="GST/CGST" Value="0"></asp:ListItem>
                            <asp:ListItem Text="IGST" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" class="form_error" ControlToValidate="ddlGSTType" EnableClientScript="true" ErrorMessage="This field is required." ValidationGroup="Validate_Field">
                              </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>--%>
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">GST</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Employee Profile</a></li>
                            <li aria-current="page" class="breadcrumb-item active">GST</li>
                        </ol>
                    </nav>
                </div>
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="forms-sample">
                                     <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>
                       TYPE</label>
                        <asp:DropDownList ID="ddlGSTType" runat="server" AutoPostBack="true" CssClass="js-example-basic-single select2" OnSelectedIndexChanged="ddlGSTType_SelectedIndexChanged" Style="width: 100%">
                            <asp:ListItem Selected="True" Text="GST/CGST" Value="0"></asp:ListItem>
                            <asp:ListItem Text="IGST" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" class="form_error" ControlToValidate="ddlGSTType" EnableClientScript="true" ErrorMessage="This field is required." ValidationGroup="Validate_Field">
                              </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
                                 <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>
                                                CGST</label>
                                                <asp:TextBox ID="txtCGST" runat="server" class="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ControlToValidate="txtCGST" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>
                                                SGST</label>
                                                <asp:TextBox ID="txtSGST" runat="server" class="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ControlToValidate="txtSGST" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>
                                                IGST</label>
                                                <asp:TextBox ID="txtIGST" runat="server" class="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ControlToValidate="txtGST" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                        <!-- end col-4 --><%-- </div>--%>
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>
                                                TAX Description</label>
                                                <asp:TextBox ID="txtDescrip" runat="server" class="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ControlToValidate="txtGST" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                        <div class="row">

                                            
                                           <%-- <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>
                                                    Choose</label>
                                                    <asp:DropDownList ID="ddlChoose" runat="server" AutoPostBack="true" CssClass="js-example-basic-single select2" Style="width: 100%">
                                                        <asp:ListItem Selected="True" Text="Selected" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="Unselected" Value="1"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="validate1" runat="server" class="form_error" ControlToValidate="ddlChoose" EnableClientScript="true" ErrorMessage="This field is required." ValidationGroup="Validate_Field">
                                            </asp:RequiredFieldValidator>
                                                </div>
                                            </div>--%>
                                            <!-- end col-4 -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                        </div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button ID="btnSave" runat="server" class="btn btn-success" OnClick="btnSave_Click" Text="Save" ValidationGroup="Validate_Field" />
                                                <asp:Button ID="btnCancel" runat="server" class="btn btn-danger" OnClick="btnCancel_Click" Text="Cancel" />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                    <div class="col-lg-12">
                                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="True">
                                            <HeaderTemplate>
                                                <table id="example" class="display table">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Bill ID</th>
                                                            <%-- <th>Name</th>--%>
                                                            <th>Mill Name</th>
                                                            <th>Unit Name</th>
                                                            <th>Designation</th>
                                                            <th>Total Amount</th>
                                                            <th>Status</th>
                                                            <th>Edit</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%--<tr>
                                               <td><%# Container.ItemIndex+1 %></td>
                                                <td><%# Eval("BillCode")%></td>
                                               <td><%# Eval("EmpName")%></td>
                                                <td><%# Eval("MillName")%></td>
                                             <td><%# Eval("UnitCode")%></td>
                                                <td><%# Eval("Designation")%></td>
                                                 <td><%# Eval("NetAmount")%></td>
                                                    <td><%# Eval("Status")%></td>                                               
                                                <td>
                                                    <asp:LinkButton ID="btnEditIssueEntry_Grid"  class="btn-sm btn-success btn-icon icon-pencil"   runat="server" 
                                                        Text="" OnCommand="btnEditIssueEntry_Grid_Command" CommandArgument="Edit" CommandName='<%# Eval("BillCode")%>'>
                                                    </asp:LinkButton>
                                                    </td>
                                            </tr>--%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content-wrapper ends -->
                <!-- partial:../../partials/_footer.html -->
                <footer class="footer">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span> <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted &amp; made with <i class="icon-heart text-danger"></i></span>
                    </div>
                </footer>
            </div>
            </br>
        </ContentTemplate>
    </asp:UpdatePanel>

  <script>
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
            
        });
        
    });
</script>
      <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable();
            $('.js-example-basic-single').select2();
        });

    </script>
     <script type="text/javascript">
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        $('#example').dataTable();
                        $('.js-example-basic-single').select2();
                        $('.datepicker').datepicker({
                            format: "dd/mm/yyyy",
                            autoclose: true
                        });
                    }
                });
            };
        </script>
</asp:Content>

