﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;

public partial class GST : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlg = false;
    string Coursecode;
    string units;
    protected void Page_Load(object sender, EventArgs e)

    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
           
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            GSTLoad();
            Load();
            if ((string)Session["TaxDescription"] != null)
            {
               // ddlGSTType.Text = Session["TaxDescription"].ToString();
                btnSearch_Click(sender, e);
               
                
            }
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        btnSave.Text = "Update";
        if (btnSave.Text == "Update")
        {
            SSQL = "";
            SSQL = "Select CGST,SGST,IGST,TaxDescription,GSTType from Mst_GST";
            SSQL = SSQL + " Where TaxDescription ='" + Session["TaxDescription"].ToString() + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt != null && dt.Rows.Count > 0)
            {

                txtCGST.Text = dt.Rows[0]["CGST"].ToString();
                txtSGST.Text = dt.Rows[0]["SGST"].ToString();
                txtIGST.Text = dt.Rows[0]["IGST"].ToString();
                txtDescrip.Text = dt.Rows[0]["TaxDescription"].ToString();
                ddlGSTType.SelectedItem.Text = dt.Rows[0]["GSTType"].ToString();
                txtDescrip.Enabled = false;
                if (ddlGSTType.SelectedItem.Text == "GST/CGST")
                {
                    txtIGST.Enabled = false;
                    txtCGST.Enabled = true;
                    txtSGST.Enabled = true;
                    ddlGSTType.Enabled = false;
                }
                if (ddlGSTType.SelectedItem.Text == "IGST")
                {
                    txtIGST.Enabled = true;
                    txtCGST.Enabled = false;
                    txtSGST.Enabled = false;
                    ddlGSTType.Enabled = false;

                }
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable Log_DT = new DataTable();
        SSQL = "";
        SSQL = "select * from Mst_GST Where TaxDescription='" + txtDescrip.Text + "'";
        Log_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Log_DT != null && Log_DT.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Program Details is Already Present');", true);
            ErrFlg = true;
            return;
        }
        if (ddlGSTType.SelectedItem.Text == "GST/CGST")
        {
            if (!ErrFlg)
            {
                if (txtCGST.Text == "") 
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter The CGST');", true);
                ErrFlg = true;
                return;
            }
            if (txtSGST.Text == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter The SGST');", true);
                ErrFlg = true;
                return;
            }
            else
            {
               
                

                    SSQL = "";
                    SSQL = "Insert Into Mst_GST(Ccode,Lcode,CGST,SGST,IGST,TaxDescription,CreatedDate,CreatedBy,GSTType)";
                    SSQL = SSQL + " Values ('" + SessionCcode + "','" + SessionLcode + "','" + txtCGST.Text + "',";
                    SSQL = SSQL + "'" + txtSGST.Text + "','" + txtIGST.Text + "','" + txtDescrip.Text + "',GetDate(),'" + SessionUserName + "','" + ddlGSTType.SelectedItem.Text + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GST  Added Succesfully');", true);
                    btnCancel_Click(sender, e);

                }
            }
        }
        if (ddlGSTType.SelectedItem.Text == "IGST")
        {
            if (txtIGST.Text == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter The IGST');", true);
                ErrFlg = true;
                return;
            }
            else
            {
                if (!ErrFlg)
                {
                    SSQL = "";
                    SSQL = "Insert Into MSt_GST(Ccode,Lcode,CGST,SGST,IGST,TaxDescription,CreatedDate,CreatedBy,GSTTYpe)";
                    SSQL = SSQL + " Values ('" + SessionCcode + "','" + SessionLcode + "','" + txtCGST.Text + "',";
                    SSQL = SSQL + "'" + txtSGST.Text + "','" + txtIGST.Text + "','" + txtDescrip.Text + "',GetDate(),'" + SessionUserName + "','" + ddlGSTType.SelectedItem.Text + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GST TYPE  Added Succesfully');", true);
                    btnCancel_Click(sender, e);

                }

            }
        }
         if(btnSave.Text=="Update")
        {
            SSQL = "";
            SSQL = "update MSt_GST Set CGST='" + txtCGST.Text + "',SGST='" + txtSGST.Text + "',IGST='" + txtIGST.Text + "',GStType ='"+ ddlGSTType.SelectedItem.Text+"' where TaxDescription='" + txtDescrip.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GST Updated Succesfully');", true);
            btnCancel_Click(sender, e);
            Response.Redirect("GST_Main.aspx");
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtCGST.Text = "";
        txtDescrip.Text = "";
        txtIGST.Text = "";
        txtSGST.Text = "";
    }

    protected void ddlGSTType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load();
        if(ddlGSTType.SelectedItem.Text == "GST/CGST")
        {
            txtIGST.Enabled = false;
            txtCGST.Enabled = true;
            txtSGST.Enabled = true;
        }
        else if(ddlGSTType.SelectedItem.Text =="IGST")
        {
            txtIGST.Enabled = true;
            txtCGST.Enabled = false;
            txtSGST.Enabled = false;
                
        }
    }
    private void GSTLoad()
    {
        if (ddlGSTType.SelectedItem.Text == "GST/CGST")
        {
            txtIGST.Enabled = false;
        }
    }
    private void Load()
    {
        txtCGST.Text = "0";
        txtIGST.Text = "0";
        txtSGST.Text = "0";
        txtDescrip.Text = "";
        txtDescrip.Enabled = true;
      

    }
}