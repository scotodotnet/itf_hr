﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using System.Collections.Generic;

public partial class KYCWiseEmpList : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    string Month;
    string Status;
    string TokenNo = "";
    string FinYearVal = "";
    string FinYearCode = "";
    string Wages = "";
    string Department = "";
    string Fields = "";

    DataTable dt_Cat = new DataTable();

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
   

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Employee List";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();

            //FinYearCode = Request.QueryString["FinYearCode"].ToString();
            //FinYearVal = Request.QueryString["FinYearVal"].ToString();
            //Month = Request.QueryString["Month"].ToString();
            Fields = Request.QueryString["Fields"].ToString();

            Wages = Request.QueryString["Wages"].ToString();

            if (Request.QueryString["DeptName"].ToString() != "-Select-")
            {
                Department = Request.QueryString["DeptName"].ToString();
            }

            // TokenNo = Request.QueryString["TokenNo"].ToString();

            string SessionUserType_1 = SessionUserType;

            if (SessionUserType_1 == SessionUserType)
            {
                Report();
            }
        }
    }

    private void Report()
    {
        
        string[] ArrAy;
        DataTable AutoDTable = new DataTable();

        //AutoDTable.Columns.Add("EmpNo");
        //AutoDTable.Columns.Add("ExistingCode");
        //AutoDTable.Columns.Add("FirstName");
        //AutoDTable.Columns.Add("Wages");
        //AutoDTable.Columns.Add("DepartmentName");
        //AutoDTable.Columns.Add("Aadhar_No");
        //AutoDTable.Columns.Add("PF_No");
        //AutoDTable.Columns.Add("ESI_No");
        //AutoDTable.Columns.Add("LIC_NoS");

        //SSQL = "";
        //SSQL = "SELECT  isnull(EM.EmpNo,'') as [EmpNo],isnull(EM.ExistingCode,'') as [ExistingCode], isnull(EM.FirstName,'') as [EmpName] ,isnull (EM.Wages,'') as [Wages],";
        //SSQL = SSQL + "isnull(EM.DeptName,'') as [DepartmentName],DocMas.DocNo as[Aadhar_No],isnull(EM.PFNo,'') as [PF_No],isnull(EM.ESINo,'')as [ESI_No],";
        //SSQL = SSQL + "isnull(EM.LicNo,'')as[LIC_No] from Employee_Mst EM ";
        //SSQL = SSQL + "inner join Employee_Doc_Mst DocMas on DocMas.EmpNo=EM.EmpNo ";
        //SSQL = SSQL + "Where EM.CompCode = '" + SessionCcode + "' and EM.LocCode = '" + SessionLcode + "' And ";
        //SSQL = SSQL + "EM.IsActive = 'Yes'"; //And Wages= '" + Wages + "' ";



        //if (Department != "")
        //{
        //    SSQL = SSQL + "And DeptName ='" + Department + "' ";
        //}


       
        string Field_str = "";
        string FldName = "";
        string TblName_Str = "";
        string tblName = "";
      
        string where = "";

        if (Fields != "")
        {
            ArrAy = Fields.ToString().Split('^');

            for (int ArrInd = 0; ArrInd < ArrAy.Length - 1; ArrInd++)
            {
                Field_str = ArrAy[ArrInd].ToString().Split('~').FirstOrDefault().ToString();
                if (FldName == "")
                {
                    FldName = Field_str.ToString().Split('|').FirstOrDefault().ToString();
                }
                else
                {
                    FldName = FldName + "," + Field_str.ToString().Split('|').FirstOrDefault().ToString();
                }


                TblName_Str = ArrAy[ArrInd].ToString().Split('~').LastOrDefault().ToString();
                if (tblName == "")
                {
                    if (!TblName_Str.Contains("Employee_Mst"))
                    {
                        tblName = " from Employee_Mst EM inner join " + TblName_Str + " on " + TblName_Str.ToString().Split(' ').LastOrDefault().ToString() + ".ExistingCode=EM.ExistingCode";
                    }
                    else
                    {
                        tblName = " from Employee_Mst EM ";
                    }
                   
                }
                else
                {
                    if (!tblName.Contains(TblName_Str))
                    {
                        tblName = tblName + " inner join  " + TblName_Str + " on " + TblName_Str.ToString().Split(' ').LastOrDefault().ToString() + ".ExistingCode=EM.ExistingCode";
                    }
                }

                if (Field_str.ToString().Split('|').Length > 1)
                {
                    if (where == "")
                    {
                        where = " and " + Field_str.ToString().Split('|').LastOrDefault();
                    }
                    else
                    {
                        if (!where.Contains(Field_str.ToString().Split('|').LastOrDefault()))
                        {
                            where = where + " and " + Field_str.ToString().Split('|').LastOrDefault();
                        }
                    }
                }
            }
            

            SSQL = "";
            SSQL = "Select isnull(EM.EmpNo,'') as [Code],isnull(EM.ExistingCode,'') as [ExistingCode], isnull(EM.FirstName,'') as [EmpName],";
            SSQL = SSQL + FldName + tblName + " where EM.CompCode='" + SessionCcode + "' and EM.Loccode='" + SessionLcode + "'" + where;
            if (Department != "")
            {
                SSQL = SSQL + "And EM.DeptName ='" + Department + "' ";
            }
            if (Wages != "-Select-")
            {
                SSQL = SSQL + " and EM.Wages='" + Wages + "'";
            }
        }


        SSQL = SSQL + " Order By EM.EmpNo";
       
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);
        
        if (AutoDTable.Rows.Count > 0)
        {
            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionEpay + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            //dt = objdata.RptEmployeeMultipleDetails(SSQL);
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            string CmpName = "";
            string Cmpaddress = "";
            if (dt_Cat.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=KYC WISE EMP LIST.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
         
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan="+AutoDTable.Columns.Count+">");
            Response.Write("<a style=\"font-weight:bold\">"+ CmpName + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan=" + AutoDTable.Columns.Count + ">");
            Response.Write("<a style=\"font-weight:bold\">" + Cmpaddress + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan=" + AutoDTable.Columns.Count + ">");
            Response.Write("<a style=\"font-weight:bold\">KYC WISE EMPLOYEE REPORT</a>");

            Response.Write("</td>");
            
            Response.Write("</tr>");
           

            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Data Found')", true);
        }
    }
}