﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;


public partial class LeftNewJoiningReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    string SSQL;
   
    string mvarUserType;
    DataTable mDataSet = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    string iEmpDet = "";
    

  
    string Empcode;
    DataSet ds = new DataSet();
    string Division;
   
    int ik = 0;
    DataTable dt1 = new DataTable();

    System.Web.UI.WebControls.DataGrid grid =
                        new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Employee Left Joining Report";
                Load_Location();
                ddlunit.SelectedValue = SessionLcode;
            }
        }
    }



    public void Load_Location()
    {
        string SSQL = "";
        DataTable dtempty = new DataTable();
        ddlunit.DataSource = dtempty;
        ddlunit.DataBind();
        DataTable dt = new DataTable();
        SSQL = "Select LocCode as LCode from Location_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlunit.DataSource = dt;
        ddlunit.DataTextField = "LCode";
        ddlunit.DataValueField = "LCode";
        ddlunit.DataBind();


    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        if (RdbCashBank.SelectedValue=="0")
        {
            AutoDataTable.Columns.Add("S.No");
            AutoDataTable.Columns.Add("MachineID");
            AutoDataTable.Columns.Add("ExistingCode");
            AutoDataTable.Columns.Add("FirstName");
            
            AutoDataTable.Columns.Add("DeptName");
            AutoDataTable.Columns.Add("Designation");
            AutoDataTable.Columns.Add("DOJ");
            AutoDataTable.Columns.Add("WagesType");
            AutoDataTable.Columns.Add("CatName");
            AutoDataTable.Columns.Add("Qualification");
            AutoDataTable.Columns.Add("Address1");


            // EmpCatCode='" + Division + "' and
            SSQL = "";

            SSQL = " Select * from Employee_Mst";
            SSQL += " WHERE  CompCode='" + SessionCcode + "' and LocCode='" + ddlunit.SelectedItem.Text + "' and IsActive='Yes' ";
            if (txtFrmdate.Text != "" && txtTodate.Text != "")
            {
                SSQL += "And CONVERT(DATETIME,DOJ, 103)>=CONVERT(Datetime,'" + txtFrmdate.Text + "',103) And CONVERT(DATETIME,DOJ, 103)<=CONVERT(Datetime,'" + txtTodate.Text + "',103)  ";

            }
            SSQL += " order by ExistingCode ";
            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

            if (mDataSet.Rows.Count > 0)
            {

                for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
                {


                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();
                    string dojoining = mDataSet.Rows[iRow]["DOJ"].ToString();
                    DateTime dt = Convert.ToDateTime(dojoining.ToString());
                    DateTime dayy = Convert.ToDateTime(dt.ToShortDateString());

                    string joining = Convert.ToString(dayy.ToShortDateString());


                    ik += 1;
                    AutoDataTable.Rows[iRow]["S.No"] = ik;
                    AutoDataTable.Rows[iRow]["MachineID"] = mDataSet.Rows[iRow]["MachineID"];
                    AutoDataTable.Rows[iRow]["ExistingCode"] = mDataSet.Rows[iRow]["ExistingCode"];
                    AutoDataTable.Rows[iRow]["FirstName"] = mDataSet.Rows[iRow]["FirstName"];
                    if (mDataSet.Rows[iRow]["DOJ"].ToString() != "" && mDataSet.Rows[iRow]["DOJ"].ToString() != null)
                    {
                        AutoDataTable.Rows[iRow]["DOJ"] = Convert.ToDateTime(mDataSet.Rows[iRow]["DOJ"]).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        AutoDataTable.Rows[iRow]["DOJ"] = "";
                    }
                    AutoDataTable.Rows[iRow]["CatName"] = mDataSet.Rows[iRow]["CatName"];
                    AutoDataTable.Rows[iRow]["WagesType"] = mDataSet.Rows[iRow]["Wages"];

                    AutoDataTable.Rows[iRow]["DeptName"] = mDataSet.Rows[iRow]["DeptName"].ToString();
                    AutoDataTable.Rows[iRow]["Designation"] = mDataSet.Rows[iRow]["Designation"];

                    AutoDataTable.Rows[iRow]["Qualification"] = mDataSet.Rows[iRow]["Qualification"].ToString();

                    AutoDataTable.Rows[iRow]["Address1"] = mDataSet.Rows[iRow]["Address1"];



                }
                SSQL = "Select * from Company_Mst where CompCode='"+SessionCcode +"' ";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt1.Rows[0]["CompName"].ToString();



                grid.DataSource = AutoDataTable;
                grid.DataBind();
                string attachment = "attachment;filename=EmployeeWiseJoining.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table border='1'>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='11'>");
                Response.Write("<a style=\"font-weight:bold\">" + name + "</a>");
                Response.Write(" &nbsp;&nbsp;");
                Response.Write("<a style=\"font-weight:bold\">" + ddlunit.SelectedItem.Text + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='11'>");
                Response.Write("<a style=\"font-weight:bold\">New Employee Joining</a>");
                Response.Write(" &nbsp;&nbsp;&nbsp; ");
                Response.Write("</td>");
                Response.Write("</tr>");


                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='11'>");

                if (txtFrmdate.Text != "" && txtTodate.Text != "")
                {
                    Response.Write("<a style=\"font-weight:bold\">From:" + txtFrmdate.Text + "</a>");
                    Response.Write(" &nbsp;-- &nbsp;");
                    Response.Write("<a style=\"font-weight:bold\">To:" + txtTodate.Text + "</a>");
                }
                else
                {
                    Response.Write("<a></a>");
                }
                Response.Write("</td>");
                Response.Write("</tr>");

               
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();

            }
        }
        else if (RdbCashBank.SelectedValue=="1")
        {
            AutoDataTable.Columns.Add("S.No");
            AutoDataTable.Columns.Add("MachineID");
            AutoDataTable.Columns.Add("ExistingCode");
            AutoDataTable.Columns.Add("FirstName");
            AutoDataTable.Columns.Add("DeptName");
            AutoDataTable.Columns.Add("Designation");
            AutoDataTable.Columns.Add("DOJ");
            AutoDataTable.Columns.Add("WagesType");
            AutoDataTable.Columns.Add("CatName");
            AutoDataTable.Columns.Add("Qualification");
            AutoDataTable.Columns.Add("Address1");
            AutoDataTable.Columns.Add("Resign");

            // EmpCatCode='" + Division + "' and
            SSQL = "";

            SSQL = " Select * from Employee_Mst";
            SSQL += " WHERE  CompCode='" + SessionCcode + "' and LocCode='" + ddlunit.SelectedItem.Text + "' and IsActive='No'";
            if (txtFrmdate.Text != "" && txtTodate.Text != "")
            {
                SSQL += "And CONVERT(DATETIME,DOR, 103)>=CONVERT(Datetime,'" + txtFrmdate.Text + "',103) And CONVERT(DATETIME,DOR, 103)<=CONVERT(Datetime,'" + txtTodate.Text + "',103)  ";
            }
            SSQL += " order by ExistingCode ";
                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

            if (mDataSet.Rows.Count > 0)
            {

                for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
                {


                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();
                    string dojoining = mDataSet.Rows[iRow]["DOJ"].ToString();
                    DateTime dt = Convert.ToDateTime(dojoining.ToString());
                    DateTime dayy = Convert.ToDateTime(dt.ToShortDateString());

                    string joining = Convert.ToString(dayy.ToShortDateString());


                    ik += 1;
                    AutoDataTable.Rows[iRow]["S.No"] = ik;
                    AutoDataTable.Rows[iRow]["MachineID"] = mDataSet.Rows[iRow]["MachineID"];
                    AutoDataTable.Rows[iRow]["ExistingCode"] = mDataSet.Rows[iRow]["ExistingCode"];
                    AutoDataTable.Rows[iRow]["FirstName"] = mDataSet.Rows[iRow]["FirstName"];
                    if (mDataSet.Rows[iRow]["DOJ"].ToString() != "" && mDataSet.Rows[iRow]["DOJ"].ToString() != null)
                    {
                        AutoDataTable.Rows[iRow]["DOJ"] = Convert.ToDateTime(mDataSet.Rows[iRow]["DOJ"]).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        AutoDataTable.Rows[iRow]["DOJ"] = "";
                    }
                    AutoDataTable.Rows[iRow]["CatName"] = mDataSet.Rows[iRow]["CatName"];
                    AutoDataTable.Rows[iRow]["WagesType"] = mDataSet.Rows[iRow]["Wages"];

                    AutoDataTable.Rows[iRow]["DeptName"] = mDataSet.Rows[iRow]["DeptName"].ToString();
                    AutoDataTable.Rows[iRow]["Designation"] = mDataSet.Rows[iRow]["Designation"];

                    AutoDataTable.Rows[iRow]["Qualification"] = mDataSet.Rows[iRow]["Qualification"].ToString();

                    AutoDataTable.Rows[iRow]["Address1"] = mDataSet.Rows[iRow]["Address1"];
                    if (mDataSet.Rows[iRow]["DOR"].ToString() != "" && mDataSet.Rows[iRow]["DOR"].ToString() != null)
                    {
                        AutoDataTable.Rows[iRow]["Resign"] = Convert.ToDateTime(mDataSet.Rows[iRow]["DOR"]).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        AutoDataTable.Rows[iRow]["Resign"] = "";
                    }

                   
                }
                SSQL = "Select * from Company_Mst where CompCode='"+SessionCcode +"' ";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt1.Rows[0]["CompName"].ToString();

                grid.DataSource = AutoDataTable;
                grid.DataBind();
                string attachment = "attachment;filename=EmployeeResigning.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table border='1'>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='12'>");
                Response.Write("<a style=\"font-weight:bold\">" + name + "</a>");
                Response.Write(" &nbsp;&nbsp;");
                Response.Write("<a style=\"font-weight:bold\">" + ddlunit.SelectedItem.Text + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='12'>");
                Response.Write("<a style=\"font-weight:bold\">Employee Resigning</a>");
                Response.Write(" &nbsp;&nbsp;&nbsp; ");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='12'>");
                if (txtFrmdate.Text != "" && txtTodate.Text != "")
                {
                    Response.Write("<a style=\"font-weight:bold\">From:" + txtFrmdate.Text + "</a>");
                    Response.Write(" &nbsp;-- &nbsp;");
                    Response.Write("<a style=\"font-weight:bold\">To:" + txtTodate.Text + "</a>");
                }
                else
                {
                    Response.Write("<a></a>");
                }
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("</tr>");

                Response.Write("</table>");


                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();

            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data's Found...');", true);
        }
    }

  

    
}
