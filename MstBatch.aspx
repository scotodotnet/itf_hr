﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstBatch.aspx.cs" Inherits="MstBatch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
           <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" />

     <script  type="text/javascript">
     $(document).ready(function() {
     $('#example').dataTable();
     $('.select2').select2();
     });
	</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
            }
        });
    };</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:UpdatePanel runat="server" EnableViewState="true">
        <ContentTemplate>
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Batch Master</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Master</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Batch Master</li>
                        </ol>
                    </nav>
                </div>
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="forms-sample">
                                    <div class="row">
                                      <%--  <div class="form-group col-lg-4">
                                            <label>Program Code</label><span class="asterisk_input"></span>
                                            <asp:TextBox ID="txtCourseCode" Enabled="false" MaxLength="30" class="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtCourseCode" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                        </div>--%>

                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Program Name</label><span class="asterisk_input"></span>
                                                  <asp:DropDownList ID="ddlCourseName" class="js-example-basic-single select2 form-control" AutoPostBack="true"  style="width:100%;" OnSelectedIndexChanged="ddlCourseName_SelectedIndexChanged" runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlCourseName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                         <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Module Name</label>
                                                 <asp:TextBox ID="txtmodule" Enabled="false" MaxLength="30" class="form-control" runat="server"></asp:TextBox>
                                               <%-- <asp:RequiredFieldValidator ControlToValidate="txtPeriod" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">--%>
                                              <%--  </asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                          <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Batch Name</label><span class="asterisk_input"></span>
                                                <asp:DropDownList ID="ddlBatch" class="js-example-basic-single select2 form-control" style="width:100%;" runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlBatch" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                          <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Date</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtDate" class="form-control datepicker" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                         <!-- begin col-4 -->
                                       <%-- <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Program Level</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtCourseLevel" class="form-control" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtCourseLevel" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>--%>
                                        <!-- end col-4 -->
                                        <%-- <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Batch</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="TxtBatch" class="form-control" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtBatch" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>--%>
                                    </div>

                                    <!-- end row -->
                                  
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                                <asp:Button runat="server" ID="btnCancel" Text="Cancel" class="btn btn-danger" OnClick="btnCancel_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->

                                    
                                    <!-- table start -->
                                    <div class="col-lg-12">
                                      
                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="display table">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No</th>
                                                                <th>Program Code</th>
                                                                <th>Program Name</th>
                                                                 <th>Module Name</th> 
                                                                <th>Batch Name</th>
                                                                 <th>Date</th>
                                                                <%--<th>Level</th>
                                                                <th>Batch</th>--%>
                                                               <%-- <th>Mode</th>--%>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("CourseCode")%></td>
                                                        <td><%# Eval("CourseName")%></td>
                                                          <td><%# Eval("ModuleName")%></td>
                                                        <td><%# Eval("Batch")%></td>
                                                         <td><%# Eval("Date")%></td>
                                                       <%-- <td><%# Eval("Level")%></td>
                                                        <td><%# Eval("Batch")%></td>--%>
                                                     <%--   <td>
                                                            <asp:LinkButton ID="btnEditGrid" class="btn-sm btn-success btn-icon icon-pencil" runat="server"
                                                                Text="" OnCommand="btnEditGrid_Command" CommandArgument="Edit" CommandName='<%# Eval("CourseCode") +","+Eval("Date")%>'>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn-sm btn-danger btn-icon icon-trash" runat="server"
                                                                Text="" OnCommand="btnDeleteGrid_Command" CommandArgument="Delete"  CommandName='<%# Eval("CourseCode") +","+Eval("Date")%>'>
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this new user details?');">
                                                            </asp:LinkButton>
                                                        </td>--%>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        
                                    </div>
                                    <!-- table End -->
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
                </div>
            </footer>
        </ContentTemplate>
    </asp:UpdatePanel>

      <script src=" https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src=" https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
</asp:Content>



