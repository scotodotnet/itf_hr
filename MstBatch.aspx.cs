﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Security.Cryptography;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstBatch : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    bool ErrFLg = false;
    string MstLvl;
    int level;
    string levelupdate;
    bool ErrFlg = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your Session Expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Load_Course();
            Load_Data();
           }

    }
    protected void Load_Data()
    {
        SSQL = "";
        SSQL = "Select CONVERT(varchar(10),Date,103) as Date, * from MstBatch where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();

    }
    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstCourse where CourseCode='" + e.CommandName + "' and Lcode='" + SessionLcode + "' and ccode='" + SessionCcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt != null && dt.Rows.Count > 0)
        {
            //txtCourseCode.Text = dt.Rows[0]["CourseCode"].ToString();
            //txtCoursename.Text = dt.Rows[0]["CourseName"].ToString();
            //txtCourseLevel.Text = dt.Rows[0]["Level"].ToString();
            //txtFrom.Text = dt.Rows[0]["FromDate"].ToString();
            //txtToDate.Text = dt.Rows[0]["ToDate"].ToString();
            //txtPeriod.Text = dt.Rows[0]["Period"].ToString();
            //TxtBatch.Text = dt.Rows[0]["Batch"].ToString();

            btnSave.Text = "Update";
        }

        Load_Data();
    }
    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from MstCourse where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and CourseCode='" + e.CommandName + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Program Deleted Successfully')", true);
       // getlast_Code();
        Load_Data();

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ddlCourseName.SelectedItem.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Choose the Course Name')", true);
            ErrFLg = true;
            return;
        }
        if (ddlBatch.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Choose the Batch No')", true);
            ErrFLg = true;
            return;
        }
        if (txtDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Date')", true);
            ErrFLg = true;
            return;
        }
        if (txtmodule.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Module Name')", true);
            ErrFLg = true;
            return;
        }
        if(!ErrFLg)
        {
            SSQL="Select * from MstBatch where CourseCode='" +ddlCourseName.SelectedValue +"' and ModuleName='" + txtmodule.Text +"' and Batch='" + ddlBatch.SelectedItem.Text +"' and Date=  Convert(datetime, '" + txtDate.Text + "', 103)";
            DataTable DT_chk = new DataTable();
            DT_chk=objdata.RptEmployeeMultipleDetails(SSQL);
            if(DT_chk.Rows.Count != 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('This Batch And Date Already Entered')", true);
                ErrFLg = true;
                return;

            }
            else
            {
                if(!ErrFlg)
                {
                    SSQL = "Insert Into MstBatch(Ccode,Lcode,CourseCode,CourseName,ModuleName,Batch,Date) Values (";
                    SSQL = SSQL + " '" + SessionCcode + "','" + SessionLcode + "','" + ddlCourseName.SelectedValue + "',";
                    SSQL =SSQL + "'" +ddlCourseName.SelectedItem.Text +"','" + txtmodule.Text +"','"+ddlBatch.SelectedItem.Text+"', Convert(datetime, '" + txtDate.Text + "', 103))";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Batch Details Entered Successfully')", true);
                    ErrFLg = true;
                    btnCancel_Click(sender ,e);
                    Load_Course();
                    return;

                }
            }
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ddlCourseName_SelectedIndexChanged(sender, e);
        txtDate.Text = "";
        txtmodule.Text = "";

    }
    protected void Load_Course()
    {
        SSQL = "";
        SSQL = "Select * from MstCourse where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        ddlCourseName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlCourseName.DataTextField = "CourseName";
        ddlCourseName.DataValueField = "CourseCode";
        ddlCourseName.DataBind();
        ddlCourseName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void ddlCourseName_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "select max(CourseLevel) as Module from Employee_Mst where CourseCode='" + ddlCourseName.SelectedValue + "'";
        DataTable Module = new DataTable();
        Module = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Module.Rows.Count != 0)
        {
            txtmodule.Text = Module.Rows[0]["Module"].ToString();

        }
        SSQL = "";
        SSQL = "select * from  MstCourse where CourseCode='" + ddlCourseName.SelectedValue + "'";
        DataTable DT_MstCourse = new DataTable();
        DT_MstCourse = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_MstCourse.Rows.Count > 0)
        {
            MstLvl = DT_MstCourse.Rows[0]["Batch"].ToString();
        }
        //level = 1;
        //if (Convert.ToInt32(MstLvl) >= level)
        //{
        //    levelupdate = "BATCH " + level;
        //    level = +1;

        //}
        DataTable dt = new DataTable();
        dt.Columns.Add("Courselevel");
        DataRow Dr;
        for (int i = 1; i <= Convert.ToInt32(MstLvl); i++)
        {
            Dr = dt.NewRow();
            Dr["Courselevel"] = "BATCH " + i;
            dt.Rows.Add(Dr);
        }

        ddlBatch.DataSource = dt;
        ddlBatch.DataTextField = "Courselevel";
        ddlBatch.DataValueField = "Courselevel";
        ddlBatch.DataBind();
        ddlBatch.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        Load_Data();



    }
    }
