﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Security.Cryptography;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstCategory : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    bool ErrFLg = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        Load_Data();
        if (!IsPostBack)
        {
            getlast_Code();
        }
    }
    protected void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from MstCategory where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        txtCategoryCode.Text = "";
        txtCategoryName.Text = "";
        getlast_Code();
        btnSave.Text = "Save";
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (txtCategoryCode.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Category Code')", true);
            ErrFLg = true;
            return;
        }
        if (txtCategoryName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Category Name')", true);
            ErrFLg = true;
            return;
        }
        if (!ErrFLg)
        {
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from MstCategory where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and CategoryName='" + txtCategoryName.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('This Category Name is Already Present')", true);
                    ErrFLg = true;
                    return;
                }
            }
            else
            {
                SSQL = "";
                SSQL = "Delete from MstCategory where CategoryName='" + txtCategoryName.Text + "' and CategoryCode='" + txtCategoryCode.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (!ErrFLg)
            {
                SSQL = "";
                SSQL = "Insert into MstCategory(Ccode,Lcode,CategoryCode,CategoryName)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtCategoryCode.Text + "','" + txtCategoryName.Text + "')";

                objdata.RptEmployeeMultipleDetails(SSQL);

                btnCancel_Click(sender, e);
            }
        }
    }

    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstCategory where CategoryCode='" + e.CommandName + "' and Lcode='" + SessionLcode + "' and ccode='" + SessionCcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt != null && dt.Rows.Count > 0)
        {
            txtCategoryCode.Text = dt.Rows[0]["CategoryCode"].ToString();
            txtCategoryName.Text = dt.Rows[0]["CategoryName"].ToString();
            btnSave.Text = "Update";
        }
        Load_Data();
    }

    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from MstCategory where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and CategoryCode='" + e.CommandName + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
    }
    protected void getlast_Code()
    {
        int no = 0;
        SSQL = "";
        SSQL = "select isnull(Max(CategoryCode),0) as cnt from MstCategory where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt != null && dt.Rows.Count > 0)
        {
            no = Convert.ToInt32(dt.Rows[0]["cnt"]);
            no = no + 1;
        }
        else
        {
            no = 1;
        }

        txtCategoryCode.Text = no.ToString();
    }
}