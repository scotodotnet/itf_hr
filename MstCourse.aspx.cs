﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Security.Cryptography;

using Altius.BusinessAccessLayer.BALDataAccess;
public partial class MstCourse : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    bool ErrFLg = false;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your Session Expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        Load_Data();

        if (!IsPostBack)
        {
            getlast_Code();
        }
    }
    protected void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from MstCourse where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();

    }
    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstCourse where CourseCode='" + e.CommandName + "' and Lcode='" + SessionLcode + "' and ccode='" + SessionCcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt != null && dt.Rows.Count > 0)
        {
            txtCourseCode.Text = dt.Rows[0]["CourseCode"].ToString();
            txtCoursename.Text = dt.Rows[0]["CourseName"].ToString();
            txtCourseLevel.Text = dt.Rows[0]["Level"].ToString();
            txtFrom.Text = dt.Rows[0]["FromDate"].ToString();
            txtToDate.Text = dt.Rows[0]["ToDate"].ToString();
            txtPeriod.Text = dt.Rows[0]["Period"].ToString();
            TxtBatch.Text = dt.Rows[0]["Batch"].ToString();

            btnSave.Text = "Update";
        }

        Load_Data();
    }

    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from MstCourse where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and CourseCode='" + e.CommandName + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Program Deleted Successfully')", true);
        getlast_Code();
        Load_Data();

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtCourseCode.Text = "";
        txtCoursename.Text = "";
        txtFrom.Text = "";
        txtPeriod.Text = "";
        txtToDate.Text = "";
        txtCourseLevel.Text = "";
        TxtBatch.Text = "";
        btnSave.Text = "Save";
        getlast_Code();
        Load_Data();

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtCourseCode.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Course Code')", true);
            ErrFLg = true;
            return;
        }
        if (txtCoursename.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Course Name')", true);
            ErrFLg = true;
            return;
        }
        if (txtFrom.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the From Date')", true);
            ErrFLg = true;
            return;
        }
        if (txtToDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the To Date')", true);
            ErrFLg = true;
            return;
        }
        if (txtPeriod.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Period')", true);
            ErrFLg = true;
            return;
        }
        if (txtCourseLevel.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Program Level')", true);
            ErrFLg = true;
            return;
        }
        if (!ErrFLg)
        {
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from MstCourse where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and CourseName='" + txtCoursename.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('This Program Name is Already Present')", true);
                    ErrFLg = true;
                    return;
                }
            }
            if(btnSave.Text =="Update")
            {
                if (!ErrFLg)
                {
                    SSQL = "";
                    SSQL = "Update MstCourse Set Coursename='" + txtCoursename.Text + "' , Period='" + txtPeriod.Text + "' , FromDate='" + txtFrom.Text + "' , ToDate ='" + txtToDate.Text + "' , Level='" + txtCourseLevel.Text + "' , Batch ='" + TxtBatch.Text + "' where CourseCode='"+ txtCourseCode.Text + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Program Updated Successfully')", true);
                   

                }
            }


            else
            {
                SSQL = "";
                SSQL = "Delete from MstCourse where Coursename='" + txtCoursename.Text + "' and CourseCode='" + txtCourseCode.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (!ErrFLg)
            {
                if (btnSave.Text == "Save")
                {
                    SSQL = "";
                    SSQL = "Insert into MstCourse(Ccode,Lcode,CourseCode,Coursename,Period,FromDate,toDate,Level,Batch)";
                    SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtCourseCode.Text + "','" + txtCoursename.Text + "','" + txtPeriod.Text + "','" + txtFrom.Text + "','" + txtToDate.Text + "','" + txtCourseLevel.Text + "','" + TxtBatch.Text + "')";

                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Program Saved Successfully')", true);
                    btnCancel_Click(sender, e);
                }
                btnCancel_Click(sender, e);
            }
        }
    }
    protected void getlast_Code()
    {
        int no = 0;
        SSQL = "";
        SSQL = "select isnull(Max(CourseCode),0) as cnt from MstCourse where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt != null && dt.Rows.Count > 0)
        {
            no = Convert.ToInt32(dt.Rows[0]["cnt"]);
            no = no + 1;
        }
        else
        {
            no = 1;
        }

        txtCourseCode.Text = no.ToString();
    }
}