﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class MstDACalc : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_WagesType();
            Load_DaScore();
            Load_DAPint_All();
        }
        
    }
    public void Clear_ALL()
    { 
    
    }
    public void Load_DAPint_All()
    {
        DataTable dt = new DataTable();
        Query = "";
        Query = Query + "select * from [" + SessionPayroll + "]..MstDaCalc where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' order by cast(DAscore as int) desc";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtDaScore.Text = dt.Rows[0]["DAscore"].ToString();
            txtFromDate.Text = dt.Rows[0]["FromDate"].ToString();
            txtToDate.Text = dt.Rows[0]["ToDate"].ToString();
            txtTypeIFrom.Text = dt.Rows[0]["TypeIfrom"].ToString();
            txtTypeITo.Text = dt.Rows[0]["TypeIto"].ToString();
            txtMin1Point.Text = dt.Rows[0]["TypeIpoint"].ToString();
            txtTypeIIFrom.Text = dt.Rows[0]["TypeIIfrom"].ToString();
            txtTypeIITo.Text = dt.Rows[0]["TypeIIto"].ToString();
            txtMin2Point.Text = dt.Rows[0]["TypeIIpoint"].ToString();
            txtTypeIIFixedDA.Text = dt.Rows[0]["TypeIIfixed"].ToString();
            txtTypeIIIFrom.Text = dt.Rows[0]["TypeIIIfrom"].ToString();
            txtTypeIIITo.Text = dt.Rows[0]["TypeIIIto"].ToString();
            txtMin3Point.Text = dt.Rows[0]["TypeIIIpoint"].ToString();
            txtTypeIIIFixedDA.Text = dt.Rows[0]["TypeIIIFixed"].ToString();
            txtDaRate1.Text = dt.Rows[0]["AddIDaPoint"].ToString();
            Add1DaPoint.Text = dt.Rows[0]["AddIDaMin"].ToString();
            txtDaRate2.Text = dt.Rows[0]["AddIIDaPoint"].ToString();
            Add2DaPoint.Text = dt.Rows[0]["AddIIDaMin"].ToString();

        }


    }
    public void Load_DaScore()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        query = "select distinct DAscore from [" + SessionPayroll + "]..DaCalculation where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDascore.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DAscore"] = "0";
        dr["DAscore"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDascore.DataTextField = "DAscore";
        ddlDascore.DataValueField = "DAscore";
        ddlDascore.DataBind();
    }
    private void Load_WagesType()
    {
        //string query = "";
        //DataTable dtdsupp = new DataTable();
        //txtEmployeeType.Items.Clear();
        //string Category_Str = "0";
        //if (ddlcategory.SelectedItem.Text == "STAFF")
        //{
        //    Category_Str = "1";
        //}
        //else if (ddlcategory.SelectedItem.Text == "LABOUR")
        //{
        //    Category_Str = "2";
        //}
        //query = "select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + Category_Str + "'";

        //dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        //txtEmployeeType.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpTypeCd"] = "0";
        //dr["EmpType"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        //txtEmployeeType.DataTextField = "EmpType";
        //txtEmployeeType.DataValueField = "EmpTypeCd";
        //txtEmployeeType.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Ramalinga_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    


    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {

        //string query;
        //string EmpType = "";
        //DataTable DT = new DataTable();
        //DataTable dt_Type = new DataTable();

        //query = "";
        //query = "select EmpTypeCd from MstEmployeeType where EmpType='" + e.CommandArgument.ToString() + "'";
        //dt_Type = objdata.RptEmployeeMultipleDetails(query);
        //if (dt_Type.Rows.Count > 0)
        //{
        //    query = "select * from [" + SessionPayroll + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'  And EmpType='" + dt_Type.Rows[0]["EmpTypeCd"].ToString() + "'";
        //    DT = objdata.RptEmployeeMultipleDetails(query);

        //    if (DT.Rows.Count > 0)
        //    {
        //        query = "delete from [" + SessionPayroll + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpType='" + dt_Type.Rows[0]["EmpTypeCd"].ToString() + "'";
        //        objdata.RptEmployeeMultipleDetails(query);

        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Incentive Deleted Successfully...!');", true);
        //    }
        //    else
        //    {
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        //    }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Type Not Match..!');", true);

        //}





        //Load_Data_Incentive();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string MessFlag = "Insert";
            decimal TypeI_From = 0;
            decimal TypeI_To = 0;

            decimal TypeII_From = 0;
            decimal TypeII_To = 0;

            decimal TypeIII_From = 0;
            decimal TypeIII_To = 0;

            string DaRate = "";
            string DaCalcs = "";

            DataTable AutoDT = new DataTable();

            AutoDT.Columns.Add("DaType");
            AutoDT.Columns.Add("PaisePer");
            AutoDT.Columns.Add("DaRate");
            

            if ((txtDaScore.Text.Trim() == "") || (txtDaScore.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter The DA Score');", true);
                ErrFlag = true;
            }
            else if ((txtFromDate.Text.Trim() == "") || (txtFromDate.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter The From Date');", true);
                ErrFlag = true;
            }
            else if ((txtToDate.Text.Trim() == "") || (txtToDate.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter The To Date');", true);
                ErrFlag = true;
            }
            else if ((txtTypeIFrom.Text.Trim() == "") || (txtTypeIFrom.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter The Type I From ');", true);
                ErrFlag = true;
            }
            else if ((txtTypeITo.Text.Trim() == "") || (txtTypeITo.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter The Type I To');", true);
                ErrFlag = true;
            }
            else if ((txtMin1Point.Text.Trim() == "") || (txtMin1Point.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter The Type I Point');", true);
                ErrFlag = true;
            }
            else if ((txtTypeIIFrom.Text.Trim() == "") || (txtTypeIIFrom.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter The Type II From');", true);
                ErrFlag = true;
            }
            else if ((txtTypeIITo.Text.Trim() == "") || (txtTypeIITo.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter The Type II To');", true);
                ErrFlag = true;
            }
            else if ((txtMin2Point.Text.Trim() == "") || (txtMin2Point.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter The Type II Point');", true);
                ErrFlag = true;
            }
            else if ((txtTypeIIFixedDA.Text.Trim() == "") || (txtTypeIIFixedDA.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter The Type II Fixed DA');", true);
                ErrFlag = true;
            }

            else if ((txtTypeIIIFrom.Text.Trim() == "") || (txtTypeIIIFrom.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter The Type III From');", true);
                ErrFlag = true;
            }
            else if ((txtTypeIIITo.Text.Trim() == "") || (txtTypeIIITo.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter The Type III To');", true);
                ErrFlag = true;
            }
            else if ((txtMin3Point.Text.Trim() == "") || (txtMin3Point.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter The Type III Point');", true);
                ErrFlag = true;
            }
            else if ((txtTypeIIIFixedDA.Text.Trim() == "") || (txtTypeIIIFixedDA.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ther Type III Fixed DA');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                string Paisa = "0";
                TypeI_From = Convert.ToDecimal(txtTypeIFrom.Text);
                TypeI_To = Convert.ToDecimal(txtTypeITo.Text);

                TypeII_From = Convert.ToDecimal(txtTypeIIFrom.Text);
                TypeII_To = Convert.ToDecimal(txtTypeIITo.Text);

                TypeIII_From = Convert.ToDecimal(txtTypeIIIFrom.Text);
                TypeIII_To = Convert.ToDecimal(txtTypeIIITo.Text);


                DataTable dt = new DataTable();
                Query = "Select * from [" + SessionPayroll + "]..MstDaCalc where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and  DAscore='" + txtDaScore.Text + "' and FromDate='" + txtFromDate.Text + "' and ToDate='" + txtToDate.Text + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "Delete from [" + SessionPayroll + "]..MstDaCalc where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and DAscore='" + txtDaScore.Text + "' and FromDate='" + txtFromDate.Text + "' and ToDate='" + txtToDate.Text + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);

                    Query = "Delete from [" + SessionPayroll + "]..DaCalculation where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and DAscore='" + txtDaScore.Text + "' and FromDate='" + txtFromDate.Text + "' and ToDate='" + txtToDate.Text + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);

                    MessFlag = "Update";
                }

                Query = "Insert Into [" + SessionPayroll + "]..MstDaCalc (Ccode,Lcode,DAscore,FromDate,ToDate,TypeIfrom,TypeIto,TypeIpoint,";
                Query = Query + "TypeIIfrom,TypeIIto,TypeIIpoint,TypeIIfixed,TypeIIIfrom,TypeIIIto,TypeIIIpoint,TypeIIIFixed,";
                Query = Query + "AddIDaPoint,AddIDaMin,AddIIDaPoint,AddIIDaMin) Values('" + SessionCcode + "','" + SessionLcode + "',";
                Query = Query + "'" + txtDaScore.Text + "','" + txtFromDate.Text + "','" + txtToDate.Text + "','" + txtTypeIFrom.Text + "','" + txtTypeITo.Text + "','" + txtMin1Point.Text + "',";
                Query = Query + "'" + txtTypeIIFrom.Text + "','" + txtTypeIITo.Text + "','" + txtMin2Point.Text + "','" + txtTypeIIFixedDA.Text + "',";
                Query = Query + "'" + txtTypeIIIFrom.Text + "','" + txtTypeIIITo.Text + "','" + txtMin3Point.Text + "','" + txtTypeIIIFixedDA.Text + "',";
                Query = Query + "'" + txtDaRate1.Text + "','" + Add1DaPoint.Text + "','" + txtDaRate2.Text + "','" + Add2DaPoint.Text + "')";
                objdata.RptEmployeeMultipleDetails(Query);


                //Start Type I
                for (int i = Convert.ToInt32(TypeI_From.ToString().Split('.').FirstOrDefault()); i <= Convert.ToInt32(TypeI_To.ToString().Split('.').FirstOrDefault()); i++)
                {
                    AutoDT.NewRow();
                    AutoDT.Rows.Add();
                    if (i.ToString().Length == 1)
                    {
                        Paisa = "0.0" + i.ToString();
                    }
                    else
                    {
                        Paisa = "0." + i.ToString();
                    }

                    DaCalcs = (Convert.ToDecimal(txtDaScore.Text) - Convert.ToDecimal(txtMin1Point.Text)).ToString();
                    DaRate = (Convert.ToDecimal(DaCalcs) * Convert.ToDecimal(Paisa)).ToString();
                    DaRate = (Math.Round(Convert.ToDecimal(DaRate), 2, MidpointRounding.AwayFromZero)).ToString();

                    AutoDT.Rows[AutoDT.Rows.Count - 1]["DaType"] = "DA";
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["PaisePer"] = i.ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["DaRate"] = DaRate;


                }
                //End Type I

                //Start Type II
                for (int j = Convert.ToInt32(TypeII_From.ToString().Split('.').FirstOrDefault()); j <= Convert.ToInt32(TypeII_To.ToString().Split('.').FirstOrDefault()); j++)
                {
                    AutoDT.NewRow();
                    AutoDT.Rows.Add();
                    if (j.ToString().Length == 1)
                    {
                        Paisa = "0.0" + j.ToString();
                    }
                    else
                    {
                        Paisa = "0." + j.ToString();
                    }

                    DaCalcs = (Convert.ToDecimal(txtDaScore.Text) - Convert.ToDecimal(txtMin2Point.Text)).ToString();
                    DaRate = (Convert.ToDecimal(DaCalcs) * Convert.ToDecimal(Paisa)).ToString();
                    DaRate = (Math.Round(Convert.ToDecimal(DaRate), 2, MidpointRounding.AwayFromZero)).ToString();
                    DaRate = (Convert.ToDecimal(DaRate) + Convert.ToDecimal(txtTypeIIFixedDA.Text)).ToString();

                    AutoDT.Rows[AutoDT.Rows.Count - 1]["DaType"] = "DA";
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["PaisePer"] = j.ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["DaRate"] = DaRate;
                }

                //End Type II

                //Start Type III
                for (int k = Convert.ToInt32(TypeIII_From.ToString().Split('.').FirstOrDefault());k  <= Convert.ToInt32(TypeIII_To.ToString().Split('.').FirstOrDefault()); k++)
                {
                    AutoDT.NewRow();
                    AutoDT.Rows.Add();
                    if (k.ToString().Length == 1)
                    {
                        Paisa = "0.0" + k.ToString();
                    }
                    else
                    {
                        Paisa = "0." + k.ToString();
                    }

                    DaCalcs = (Convert.ToDecimal(txtDaScore.Text) - Convert.ToDecimal(txtMin3Point.Text)).ToString();
                    DaRate = (Convert.ToDecimal(DaCalcs) * Convert.ToDecimal(Paisa)).ToString();
                    DaRate = (Math.Round(Convert.ToDecimal(DaRate), 2, MidpointRounding.AwayFromZero)).ToString();
                    DaRate = (Convert.ToDecimal(DaRate) + Convert.ToDecimal(txtTypeIIIFixedDA.Text)).ToString();

                    AutoDT.Rows[AutoDT.Rows.Count - 1]["DaType"] = "DA";
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["PaisePer"] = k.ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["DaRate"] = DaRate;
                }

                for (int l = 0; l <= 1; l++)
                {
                    AutoDT.NewRow();
                    AutoDT.Rows.Add();

                    if (l == 0)
                    {
                        DaCalcs = (Convert.ToDecimal(txtDaScore.Text) - Convert.ToDecimal(txtDaRate1.Text)).ToString();
                        DaRate = (Convert.ToDecimal(DaCalcs) * Convert.ToDecimal(Add1DaPoint.Text)).ToString();
                        DaRate = (Math.Round(Convert.ToDecimal(DaRate), 2, MidpointRounding.AwayFromZero)).ToString();

                        AutoDT.Rows[AutoDT.Rows.Count - 1]["DaType"] = "DA Rate";
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["PaisePer"] = "ADDITIONAL DA RATE 1";
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["DaRate"] = DaRate;
                    }
                    else
                    {
                        DaCalcs = (Convert.ToDecimal(txtDaScore.Text) - Convert.ToDecimal(txtDaRate2.Text)).ToString();
                        DaRate = (Convert.ToDecimal(DaCalcs) * Convert.ToDecimal(Add2DaPoint.Text)).ToString();
                        DaRate = (Math.Round(Convert.ToDecimal(DaRate), 2, MidpointRounding.AwayFromZero)).ToString();

                        AutoDT.Rows[AutoDT.Rows.Count - 1]["DaType"] = "DA Rate";
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["PaisePer"] = "ADDITIONAL DA RATE 2";
                        AutoDT.Rows[AutoDT.Rows.Count - 1]["DaRate"] = DaRate;
                    
                    }

                    

                }

                string Process = "Completed";

                for (int m = 0; m < AutoDT.Rows.Count; m++)
                {
                    Query = "Insert Into [" + SessionPayroll + "]..DaCalculation (Ccode,Lcode,DAscore,FromDate,ToDate,DaType,PaisePer,DaRate) ";
                    Query = Query + "Values('" + SessionCcode + "','" + SessionLcode + "','" + txtDaScore.Text + "','" + txtFromDate.Text + "','" + txtToDate.Text + "',";
                    Query = Query + "'" + AutoDT.Rows[m]["DaType"].ToString() + "','" + AutoDT.Rows[m]["PaisePer"].ToString() + "','" + AutoDT.Rows[m]["DaRate"].ToString() + "')";
                    objdata.RptEmployeeMultipleDetails(Query);

                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                
            }
            //else if ((txtMinDaysWorked.Text.Trim() == "") || (txtMinDaysWorked.Text.Trim() == "0"))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Minimum Days Worked');", true);
            //    ErrFlag = true;
            //}
            //else if ((txtIncent_Amount.Text.Trim() == "") || (txtIncent_Amount.Text.Trim() == "0"))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Incentive Amount');", true);
            //    ErrFlag = true;
            //}
            //else if (ddlcategory.SelectedValue == "-Select-")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
            //    ErrFlag = true;
            //}
            //else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
            //    ErrFlag = true;
            //}
            //if (!ErrFlag)
            //{
            //    string Category_Str = "0";
            //    if (ddlcategory.SelectedItem.Text == "STAFF")
            //    {
            //        Category_Str = "1";
            //    }
            //    else if (ddlcategory.SelectedItem.Text == "LABOUR")
            //    {
            //        Category_Str = "2";
            //    }

            //    DataTable dt = new DataTable();
            //    Query = "Select * from [" + SessionPayroll + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Catname='" + Category_Str + "' and EmpType='" + txtEmployeeType.SelectedValue + "' and MonthDays='" + txtDaysOfMonth.Text + "'";
            //    dt = objdata.RptEmployeeMultipleDetails(Query);
            //    if (dt.Rows.Count > 0)
            //    {
            //        Query = "delete from [" + SessionPayroll + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  and MonthDays='" + txtDaysOfMonth.Text + "' and Catname='" + Category_Str + "' and EmpType='" + txtEmployeeType.SelectedValue + "'";
            //        dt = objdata.RptEmployeeMultipleDetails(Query);
            //        MessFlag = "Update";
            //    }
            //    Query = "Insert Into [" + SessionPayroll + "]..WorkerIncentive_mst (Ccode,Lcode,WorkerDays,WorkerAmt,MonthDays,Catname,EmpType)";
            //    Query = Query + "values ('" + SessionCcode + "','" + SessionLcode + "','" + txtMinDaysWorked.Text + "','" + txtIncent_Amount.Text + "','" + txtDaysOfMonth.Text + "','" + Category_Str + "','" + txtEmployeeType.SelectedValue + "')";
            //    objdata.RptEmployeeMultipleDetails(Query);


            //    if (MessFlag == "Insert")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
            //    }
            //    Load_Data_Incentive();
            //    txtMinDaysWorked.Text = "0";
            //    txtDaysOfMonth.Text = "0";
            //    txtIncent_Amount.Text = "0";
            //}
        }
        catch (Exception Ex)
        {

        }
    }
    //protected void btnIncen_Full_Amt_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        bool ErrFlag = false;
    //        string MessFlag = "Insert";

    //        if ((txtIncent_Month_Full_Amount.Text.Trim() == "") || (txtIncent_Month_Full_Amount.Text.Trim() == "0"))
    //        {
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Hostel Amount....!');", true);
    //            ErrFlag = true;
    //        }

    //        if (!ErrFlag)
    //        {
    //            DataTable dt = new DataTable();
    //            Query = "Select Amt from [" + SessionPayroll + "]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Amt='" + txtIncent_Month_Full_Amount.Text + "'";
    //            dt = objdata.RptEmployeeMultipleDetails(Query);
    //            if (dt.Rows.Count > 0)
    //            {
    //                Query = "delete from [" + SessionPayroll + "]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Amt='" + txtIncent_Month_Full_Amount.Text + "'";
    //                dt = objdata.RptEmployeeMultipleDetails(Query);
    //                MessFlag = "Update";
    //            }

    //            Query = "Insert Into [" + SessionPayroll + "]..HostelIncentive (Ccode,Lcode,Amt,WDays,HAllowed,OTDays,NFHDays,NFHWorkDays,CalWDays,CalHAllowed,CalOTDays,CalNFHDays,CalNFHWorkDays)";
    //            Query = Query + "values ('" + SessionCcode + "','" + SessionLcode + "','" + txtIncent_Month_Full_Amount.Text + "','" + WorkDays.Checked.ToString() + "','" + Hallowed.Checked.ToString() + "',";
    //            Query = Query + "'" + OTdays.Checked.ToString() + "','" + NFHdays.Checked.ToString() + "','" + NFHwork.Checked.ToString() + "','" + CalWorkdays.Checked.ToString() + "',";
    //            Query = Query + "'" + CalHallowed.Checked.ToString() + "','" + CalOTdays.Checked.ToString() + "','" + CalNFHdays.Checked.ToString() + "','" + CalNFHwork.Checked.ToString() + "')";
    //            objdata.RptEmployeeMultipleDetails(Query);

    //            if (MessFlag == "Insert")
    //            {
    //                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
    //            }
    //            else
    //            {
    //                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
    //            }
    //            txtMinDaysWorked.Text = "0";
    //            txtDaysOfMonth.Text = "0";
    //            txtIncent_Amount.Text = "0";
    //        }
    //    }
    //    catch (Exception Ex)
    //    {

    //    }
    //}
    //public void Load_MonthIncentive()
    //{
    //    DataTable dt = new DataTable();
    //    Query = "Select Amt,WDays,HAllowed,OTDays,NFHDays,NFHWorkDays,CalWDays,CalHAllowed,CalOTDays,CalNFHDays,CalNFHWorkDays from [" + SessionPayroll + "]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
    //    dt = objdata.RptEmployeeMultipleDetails(Query);
    //    if (dt.Rows.Count > 0)
    //    {
    //        txtIncent_Month_Full_Amount.Text = dt.Rows[0]["Amt"].ToString();
    //        if (dt.Rows[0]["WDays"].ToString().ToUpper() == "true".ToUpper()) { WorkDays.Checked = true; } else { WorkDays.Checked = false; }
    //        if (dt.Rows[0]["HAllowed"].ToString().ToUpper() == "true".ToUpper()) { Hallowed.Checked = true; } else { Hallowed.Checked = false; }
    //        if (dt.Rows[0]["OTDays"].ToString().ToUpper() == "true".ToUpper()) { OTdays.Checked = true; } else { OTdays.Checked = false; }
    //        if (dt.Rows[0]["NFHDays"].ToString().ToUpper() == "true".ToUpper()) { NFHdays.Checked = true; } else { NFHdays.Checked = false; }
    //        if (dt.Rows[0]["NFHWorkDays"].ToString().ToUpper() == "true".ToUpper()) { NFHwork.Checked = true; } else { NFHwork.Checked = false; }

    //        if (dt.Rows[0]["CalWDays"].ToString().ToUpper() == "true".ToUpper()) { CalWorkdays.Checked = true; } else { CalWorkdays.Checked = false; }
    //        if (dt.Rows[0]["CalHAllowed"].ToString().ToUpper() == "true".ToUpper()) { CalHallowed.Checked = true; } else { CalHallowed.Checked = false; }
    //        if (dt.Rows[0]["CalOTDays"].ToString().ToUpper() == "true".ToUpper()) { CalOTdays.Checked = true; } else { CalOTdays.Checked = false; }
    //        if (dt.Rows[0]["CalNFHDays"].ToString().ToUpper() == "true".ToUpper()) { CalNFHdays.Checked = true; } else { CalNFHdays.Checked = false; }
    //        if (dt.Rows[0]["CalNFHWorkDays"].ToString().ToUpper() == "true".ToUpper()) { CalNFHwork.Checked = true; } else { CalNFHwork.Checked = false; }
    //    }
    //    else
    //    {
    //        txtIncent_Month_Full_Amount.Text = "0.00";
    //        WorkDays.Checked = false; Hallowed.Checked = false; OTdays.Checked = false; NFHdays.Checked = false;
    //        NFHwork.Checked = false; CalWorkdays.Checked = false; CalHallowed.Checked = false; CalOTdays.Checked = false;
    //        CalNFHdays.Checked = false; CalNFHwork.Checked = false;
    //    }

    //}
    //protected void btnCivil_Incent_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        bool ErrFlag = false;
    //        bool SaveFlag = true;
    //        DataTable dt = new DataTable();
    //        if (txtEligible_Days.Text.Trim() == "")
    //        {
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Eligible Days....! ');", true);
    //            ErrFlag = true;
    //        }
    //        if (txtCivil_Incen_Amt.Text.Trim() == "")
    //        {
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Incentive Amount....! ');", true);
    //            ErrFlag = true;
    //        }
    //        if (!ErrFlag)
    //        {
    //            Query = "";
    //            Query = Query + "select * from [" + SessionPayroll + "]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
    //            dt = objdata.RptEmployeeMultipleDetails(Query);
    //            if (dt.Rows.Count > 0)
    //            {
    //                Query = Query + "delete from [" + SessionPayroll + "]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
    //                dt = objdata.RptEmployeeMultipleDetails(Query);
    //                SaveFlag = false;
    //            }
    //            Query = "Insert into [" + SessionPayroll + "]..CivilIncentive(Ccode,Lcode,Amt,ElbDays) values(";
    //            Query = Query + "'" + SessionCcode + "','" + SessionLcode + "','" + txtEligible_Days.Text + "','" + txtCivil_Incen_Amt.Text + "')";
    //            objdata.RptEmployeeMultipleDetails(Query);

    //            if (SaveFlag == true)
    //            {
    //                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....! ');", true);

    //            }
    //            else 
    //            {
    //                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully....! ');", true);
    //            }
    //            txtEligible_Days.Text = "0";
    //            txtCivil_Incen_Amt.Text = "0";
    //            Load_CivilIncentive();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}
    //public void Load_CivilIncentive()
    //{
    //    DataTable dt = new DataTable();
    //    Query = "";
    //    Query = Query + "select * from [" + SessionPayroll + "]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
    //    dt = objdata.RptEmployeeMultipleDetails(Query);
    //    if (dt.Rows.Count > 0)
    //    {
    //        txtEligible_Days.Text = dt.Rows[0]["ElbDays"].ToString();
    //        txtCivil_Incen_Amt.Text = dt.Rows[0]["Amt"].ToString();
    //    }
    //}

    protected void btnReport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string ReportName = "DA Point";


        if ((ddlDascore.SelectedValue == "0") || (ddlDascore.SelectedItem.Text == "-Select-"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the DA Point.');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            ResponseHelper.Redirect("RptOthers.aspx?DaSore=" + ddlDascore.SelectedValue + "&ReportName=" + ReportName, "_blank", "");
        }
        
        //ResponseHelper.Redirect("NewViewReport.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&AgentName=" + AgentName + "&ReportFormat=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + Str_PFType + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&PayslipType=" + Payslip_Format_Type.ToString() + "&PFTypePost=" + Str_PFType.ToString() + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Report_Type=Payslip" + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");
    }
}
