﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Security.Cryptography;

using Altius.BusinessAccessLayer.BALDataAccess;
public partial class MstDept : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    bool ErrFLg = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        Load_Data();
      
        if (!IsPostBack)
        {
            Load_Mill();
            txtMillCode.Focus();
        }
    }
    protected void Load_Mill()
    {
        //SSQL = "";
        //SSQL = "Select * from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        //ddlMillCode.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        //ddlMillCode.DataTextField = "MillCode";
        //ddlMillCode.DataValueField = "MillCode";
        //ddlMillCode.DataBind();
        //ddlMillCode.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    //protected void getlast_Code()
    //{
    //    int no = 0;
    //    SSQL = "";
    //    SSQL = "select isnull(Max(DeptCode),0) as cnt from Department_Mst ";
    //    DataTable dt = new DataTable();
    //    dt = objdata.RptEmployeeMultipleDetails(SSQL);
    //    if (dt != null && dt.Rows.Count > 0)
    //    {
    //        no = Convert.ToInt32(dt.Rows[0]["cnt"]);
    //        no = no + 1;
    //    }
    //    else
    //    {
    //        no = 1;
    //    }

    //    txtDeptCode.Text = no.ToString();
    //}
    protected void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from Department_Mst order by MillCode,UnitCode,DeptCode asc";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }
    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {

        SSQL = "";
        SSQL = "Select * from Department_Mst where DeptCode='" + e.CommandName + "' and MillCode='"+e.CommandArgument.ToString().Split(',').First()+"'";
        SSQL = SSQL + " and UnitCode='" + e.CommandArgument.ToString().Split(',').Last() + "'";
        txtDeptCode.Enabled = false;

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt != null && dt.Rows.Count > 0)
        {
            txtDeptCode.Text = dt.Rows[0]["DeptCode"].ToString();
            txtDeptname.Text = dt.Rows[0]["DeptName"].ToString();
            txtMillCode.Text = dt.Rows[0]["MillCode"].ToString();
            txtMillName.Text = dt.Rows[0]["MillName"].ToString();
            Load_Units(dt.Rows[0]["MillCode"].ToString());
            ddlUnits.SelectedValue = dt.Rows[0]["UnitCode"].ToString();
            txtUnitsType.Text = dt.Rows[0]["UnitTypeCode"].ToString() + "-" + dt.Rows[0]["UnitTypeName"].ToString();
            btnSave.Text = "Update";
        }
        Load_Data();
    }

    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from Department_Mst where DeptCode='" + e.CommandName + "' and MillCode='" + e.CommandArgument.ToString().Split(',').First() + "'";
        SSQL = SSQL + " and UnitCode='" + e.CommandArgument.ToString().Split(',').Last() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtMillCode.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please enter Mill Code');", true);
            ErrFLg = true;
            return;
        }
        if (txtMillName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please enter Mill Name');", true);
            ErrFLg = true;
            return;
        }
        
        if (ddlUnits.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose Unit Name');", true);
            ErrFLg = true;
            return;
        }
        if (txtUnitsType.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose Unit Type');", true);
            ErrFLg = true;
            return;
        }
       
        if (!ErrFLg)
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "select * from Department_Mst where MillCode='" + txtMillCode.Text + "' and UnitName='" + ddlUnits.SelectedItem.Text + "' and DeptName='" + txtDeptname.Text + "'";
                DataTable Unit_DT = new DataTable();
                Unit_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (Unit_DT != null && Unit_DT.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('The Unit Already Have This Department Code or Department Name');", true);
                    ErrFLg = true;
                    return;
                }
                else if (!ErrFLg)
                {
                    SSQL = "";
                    SSQL = "Update Department_Mst set MillName='" + txtMillName.Text + "',UnitTypeName='" + txtUnitsType.Text.ToString().Trim().Split('-').Last() + "',UnitTypeCode='" + txtUnitsType.Text.ToString().Trim().Split('-').First() + "',";
                    SSQL = SSQL + "DeptCode='" + txtDeptCode.Text + "' , DeptName='" + txtDeptname.Text + "'";
                    SSQL = SSQL + " where MillCode='" + txtMillCode.Text + "' and UnitCode='" + ddlUnits.SelectedValue + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Details UpDated Successfully');", true);
                    btnCancel_Click(sender, e);
                }
            }
            else if (!ErrFLg)
            {
                SSQL = "";
                SSQL = "select * from Department_Mst where  MillCode='" + txtMillCode.Text + "' and UnitName='" + ddlUnits.SelectedItem.Text + "' and (DeptName='" + txtDeptname.Text + "' or DeptCode='" + txtDeptCode.Text + "')";
                DataTable Unit_DT = new DataTable();
                Unit_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (Unit_DT != null && Unit_DT.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('The Unit Already Have This Department Code or Department Name');", true);
                    ErrFLg = true;
                    return;
                }
                if (!ErrFLg)
                {
                    SSQL = "";
                    SSQL = "Insert into Department_Mst(DeptName,DeptCode,MillCode,MillName,UnitCode,UnitName,UnitTypeName,UnitTypeCode) ";
                    SSQL = SSQL + "values('" + txtDeptname.Text + "','" + txtDeptCode.Text + "','" + txtMillCode.Text + "','" + txtMillName.Text + "','" + ddlUnits.SelectedValue + "','" + ddlUnits.SelectedItem.Text + "','" + txtUnitsType.Text.ToString().Trim().Split('-').Last() + "','" + txtUnitsType.Text.ToString().Trim().Split('-').First() + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Details Saved Successfully');", true);
                    btnCancel_Click(sender, e);
                }
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtMillCode.Text = "";
        txtMillName.Text = "";
        ddlUnits.ClearSelection();
        txtUnitsType.Text = "";
        txtDeptCode.Text = "";
        txtDeptname.Text = "";
        Load_Data();
        txtDeptCode.Enabled = true;
        btnSave.Text = "Save";
        txtMillCode.Focus();
    }
    protected void txtMillCode_onTextChanged(object sender, EventArgs e)
    {
        if (txtMillCode.Text != "")
        {
            SSQL = "";
            SSQL = "Select * from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + txtMillCode.Text + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt != null && dt.Rows.Count > 0)
            {
                txtMillName.Text = dt.Rows[0]["MillName"].ToString();
                Load_Units(dt.Rows[0]["MillCode"].ToString());
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Mill Code Does not Exists!!!');", true);
                txtMillCode.Focus();
                
            }
        }
    }
    private void Load_Units(string MillCode)
    {
        SSQL = "";
        SSQL = "Select * from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + MillCode + "'";
        DataTable dt = new DataTable();

        if (dt.Rows.Count <= 0)
        {
            SSQL = "";
            SSQL = "select Units from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Millcode='" + MillCode + "'";
            DataTable Dt_get = new DataTable();
            Dt_get = objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable Unit_dt = new DataTable();

            Unit_dt.Columns.Add("UnitCode");
            Unit_dt.Columns.Add("UnitName");

            int Unitcode = 0;
            if (Dt_get != null && Dt_get.Rows.Count > 0)
            {

                Unitcode = Convert.ToInt32(Dt_get.Rows[0]["Units"]);
                DataRow dr;

                for (int i = 1; i <= Unitcode; i++)
                {
                    dr = Unit_dt.NewRow();
                    dr["UnitCode"] = "UNIT " + i;
                    dr["UnitName"] = "UNIT " + i;
                    Unit_dt.Rows.Add(dr);
                }

                if (Unit_dt.Rows.Count > 0)
                {
                    ddlUnits.DataSource = Unit_dt;
                    ddlUnits.DataTextField = "UnitName";
                    ddlUnits.DataValueField = "UnitCode";
                    ddlUnits.DataBind();
                    ddlUnits.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
                }
            }
        }
        else
        {

            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlUnits.DataSource = dt;
            ddlUnits.DataTextField = "UnitName";
            ddlUnits.DataValueField = "UnitCode";
            ddlUnits.DataBind();
            ddlUnits.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }
    protected void ddlUnits_onselectedindexchanged(object sender, EventArgs e)
    {
        if (ddlUnits.SelectedItem.Text != "-Select-")
        {
            SSQL = "";
            SSQL = "Select (UnitTypeCode+'-'+UnitTypeName) as Units from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + txtMillCode.Text + "'";
            SSQL = SSQL + " and UnitCode='" + ddlUnits.SelectedValue + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt != null && dt.Rows.Count > 0)
            {
                txtUnitsType.Text = dt.Rows[0]["Units"].ToString();
            }
        }
    }
}