﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Security.Cryptography;

using Altius.BusinessAccessLayer.BALDataAccess;
public partial class MstDesignation : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    bool ErrFLg = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        Load_Data();
        if (!IsPostBack)
        {
            getlast_Code();
            txtMillCode.Focus();
        }
    }
    protected void getlast_Code()
    {
        //int no = 0;
        //SSQL = "";
        //SSQL = "select isnull(Max(DesignCode),0) as cnt from Designation_Mst ";
        //DataTable dt = new DataTable();
        //dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (dt != null && dt.Rows.Count > 0)
        //{
        //    no = Convert.ToInt32(dt.Rows[0]["cnt"]);
        //    no = no + 1;
        //}
        //else
        //{
        //    no = 1;
        //}

        //txtDesignationCode.Text = no.ToString();
    }
    protected void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from Designation_Mst";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from Designation_Mst where DeptCode='" + e.CommandName.ToString().Split(',').First() + "' and DesignCode='" + e.CommandName.ToString().Split(',').Last() + "'";
        SSQL = SSQL + " and MillCode='" + e.CommandArgument.ToString().Split(',').First() + "' and UnitCode='" + e.CommandArgument.ToString().Split(',').Last() + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        txtDesignationCode.Enabled = false;
        if (dt != null && dt.Rows.Count > 0)
        {
            txtDesignationCode.Text = dt.Rows[0]["DesignCode"].ToString();
            txtMillCode.Text = dt.Rows[0]["MillCode"].ToString();
            txtMillName.Text = dt.Rows[0]["MillName"].ToString();
            Load_Units(dt.Rows[0]["MillCode"].ToString());
            ddlUnits.SelectedValue = dt.Rows[0]["UnitCode"].ToString();
            load_Dept(dt.Rows[0]["MillCode"].ToString(), dt.Rows[0]["UnitCode"].ToString());
            ddlDeptName.SelectedValue = dt.Rows[0]["DeptCode"].ToString();
            txtDesignationName.Text = dt.Rows[0]["DesignName"].ToString();
            btnSave.Text = "Update";
        }
        Load_Data();
    }

    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from Designation_Mst where DeptCode='" + e.CommandName.ToString().Split(',').First() + "' and DesignCode='" + e.CommandName.ToString().Split(',').Last() + "'";
        SSQL = SSQL + " and MillCode='" + e.CommandArgument.ToString().Split(',').First() + "' and UnitCode='" + e.CommandArgument.ToString().Split(',').Last() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtDesignationCode.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Designation Code')", true);
            ErrFLg = true;
            return;
        }
        if (txtDesignationName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Designation Name')", true);
            ErrFLg = true;
            return;
        }

        if (txtMillCode.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Mill Code')", true);
            ErrFLg = true;
            return;
        }
        if (txtMillName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Mill Name')", true);
            ErrFLg = true;
            return;
        }
        if (ddlUnits.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Choose the Unit')", true);
            ErrFLg = true;
            return;
        }
        if (ddlDeptName.SelectedItem.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Choose the Department Name')", true);
            ErrFLg = true;
            return;
        }
        if (!ErrFLg)
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Select * from Designation_Mst where Millcode='" + txtMillCode.Text + "' and UnitCode='" + ddlUnits.SelectedValue + "' and DeptCode='" + ddlDeptName.SelectedValue + "' and DesignName='" + txtDesignationCode.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('This Designation Name is Already Present')", true);
                    ErrFLg = true;
                    return;
                }
                else if (!ErrFLg)
                {
                    SSQL = "";
                    SSQL = "Update Designation_Mst set ";
                    SSQL = SSQL + "DesignCode='" + ddlDeptName.SelectedValue + "',DesignName='" + ddlDeptName.SelectedItem.Text + "'";
                    SSQL = SSQL + " where MillCode='" + txtMillCode.Text + "' and UnitCode='" + ddlUnits.SelectedValue + "' and DeptCode='"+ddlDeptName.SelectedValue+"'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details UpDated Successfully');", true);
                    btnCancel_Click(sender, e);
                }
            }
            else if (!ErrFLg)
            {
                SSQL = "";
                SSQL = "Select * from Designation_Mst where Millcode='" + txtMillCode.Text + "' and UnitCode='" + ddlUnits.SelectedValue + "' and DeptCode='" + ddlDeptName.SelectedValue + "' and DesignName='" + txtDesignationCode.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('This Designation Name is Already Present')", true);
                    ErrFLg = true;
                    return;
                }
                if (!ErrFLg)
                {

                    SSQL = "";
                    SSQL = "Insert into Designation_Mst(DesignCode,DesignName,DeptCode,Deptname,MillCode,MillName,UnitCode,UnitName)";
                    SSQL = SSQL + " values('" + txtDesignationCode.Text + "','" + txtDesignationName.Text + "',";
                    SSQL = SSQL + "'" + ddlDeptName.SelectedValue + "','" + ddlDeptName.SelectedItem.Text + "','" + txtMillCode.Text + "','" + txtMillName.Text + "',";
                    SSQL = SSQL + "'" + ddlUnits.SelectedValue + "','" + ddlUnits.SelectedItem.Text + "')";

                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Saved Successfully');", true);
                    btnCancel_Click(sender, e);
                }
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtDesignationCode.Text = "";
        txtDesignationName.Text = "";
        txtMillName.Text = "";
        txtMillCode.Text = "";
        ddlUnits.ClearSelection();
        ddlDeptName.ClearSelection();
        getlast_Code();
        txtDesignationCode.Enabled = true;
        Load_Data();
        btnSave.Text = "Save";
    }
    protected void txtMillCode_onTextChanged(object sender, EventArgs e)
    {
        if (txtMillCode.Text != "")
        {
            SSQL = "";
            SSQL = "Select * from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + txtMillCode.Text + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt != null && dt.Rows.Count > 0)
            {
                txtMillName.Text = dt.Rows[0]["MillName"].ToString();
                Load_Units(dt.Rows[0]["MillCode"].ToString());
                
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Mill Code Does not Exists!!!');", true);
                txtMillCode.Focus();
            }
        }
    }
    
    protected void ddlUnits_onselectedindexchanged(object sender, EventArgs e)
    {
        if (ddlUnits.SelectedItem.Text != "-Select-")
        {
            SSQL = "";
            SSQL = "Select * from Department_Mst where  MillCode='" + txtMillCode.Text + "' and UnitCode='" + ddlUnits.SelectedValue + "'";
            SSQL = SSQL + " and UnitCode='" + ddlUnits.SelectedValue + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt != null && dt.Rows.Count > 0)
            {
                load_Dept(dt.Rows[0]["MillCode"].ToString(), dt.Rows[0]["UnitCode"].ToString());
            }
        }
    }
    private void load_Dept(string MillCode, string UnitCode)
    {
        SSQL = "";
        SSQL = "Select * from Department_Mst where MillCode='" + MillCode + "' and UnitCode='" + UnitCode + "'";
        ddlDeptName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlDeptName.DataTextField = "DeptName";
        ddlDeptName.DataValueField = "DeptCode";
        ddlDeptName.DataBind();
        ddlDeptName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Units(string MillCode)
    {
        SSQL = "";
        SSQL = "Select * from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + MillCode + "'";
        DataTable dt = new DataTable();

        if (dt.Rows.Count <= 0)
        {
            SSQL = "";
            SSQL = "select Units from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Millcode='" + MillCode + "'";
            DataTable Dt_get = new DataTable();
            Dt_get = objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable Unit_dt = new DataTable();

            Unit_dt.Columns.Add("UnitCode");
            Unit_dt.Columns.Add("UnitName");

            int Unitcode = 0;
            if (Dt_get != null && Dt_get.Rows.Count > 0)
            {

                Unitcode = Convert.ToInt32(Dt_get.Rows[0]["Units"]);
                DataRow dr;

                for (int i = 1; i <= Unitcode; i++)
                {
                    dr = Unit_dt.NewRow();
                    dr["UnitCode"] = "UNIT " + i;
                    dr["UnitName"] = "UNIT " + i;
                    Unit_dt.Rows.Add(dr);
                }

                if (Unit_dt.Rows.Count > 0)
                {
                    ddlUnits.DataSource = Unit_dt;
                    ddlUnits.DataTextField = "UnitName";
                    ddlUnits.DataValueField = "UnitCode";
                    ddlUnits.DataBind();
                    ddlUnits.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
                }
            }
        }
        else
        {

            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlUnits.DataSource = dt;
            ddlUnits.DataTextField = "UnitName";
            ddlUnits.DataValueField = "UnitCode";
            ddlUnits.DataBind();
            ddlUnits.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }
}