﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="MstESIPF.aspx.cs" Inherits="MstESIPF" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     });
 </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>
<asp:UpdatePanel ID="ESI" runat="server">
    <ContentTemplate>
    <!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">ESI/PF</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">ESI/PF</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">ESI/PF</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>PF % *</label>
								<asp:TextBox ID="txtPF" runat="server" class="form-control"></asp:TextBox>
								</div>
                               </div>
                           <!-- end col-4 -->
                             <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>ESI % *</label>
								 <asp:TextBox ID="txtESI" runat="server" class="form-control"></asp:TextBox>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>PF Salary *</label>
								 <asp:TextBox ID="txtPFsalary" runat="server" class="form-control"></asp:TextBox>
								</div>
                               </div>
                           <!-- end col-4 -->
                          
                         </div>
                        <!-- end row -->
                        
                        <div class="row">
                            <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
								 <label>Employeer ESI *</label>
									<asp:TextBox ID="txtEmpESI" runat="server" class="form-control"></asp:TextBox>
									
								 </div>
                               </div>
                              <!-- end col-4 -->
                                 <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
								 <label>Employeer PF A/c1 *</label>
									<asp:TextBox ID="txtEmpPF1" runat="server" class="form-control"></asp:TextBox>
									
								 </div>
                               </div>
                              <!-- end col-4 -->
                                 <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
								 <label>Employeer PF A/c2 *</label>
									<asp:TextBox ID="txtEmpPF2" runat="server" class="form-control"></asp:TextBox>
									
								 </div>
                               </div>
                              <!-- end col-4 -->
                        </div>

                            <div class="row">
                                <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>ESI Salary *</label>
                                        <asp:TextBox ID="txtESISalary" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <!-- end col-4 -->
                            </div>
            
                       
                      <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         onclick="btnSave_Click" />
									
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        <h5>Hostel Canteen Per Day Amount</h5>
                        <div class="row">
                             <div class="col-md-4">
								<div class="form-group">
								    <label>Canteen Amount *</label>
									<asp:TextBox ID="txtHostelCanteenAmt" Text="0" runat="server" class="form-control"></asp:TextBox>
									<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtHostelCanteenAmt" ValidChars="0123456789.">
                                    </cc1:FilteredTextBoxExtender>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group">
								    <label>.</label><br />
									<asp:Button runat="server" id="btnHostelCanteenSave" Text="Canteen Save" class="btn btn-success" 
                                         onclick="btnHostelCanteenSave_Click" />
									
								</div>
                            </div>
                        </div>
                    <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                
                                                <th>Machine ID</th>
                                                <th>Token No</th>
                                                <th>Emp Name</th>
                                                <th>Department</th>
                                                <th>DOJ</th>
                                                <th>Type</th>
                                                <th>OLD Salary</th>
                                                <th>New Salary</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>1001</td>
                                        <td>1001</td>
                                        <td>Raja</td>
                                        <td>Spinning</td>
                                        <td>12/12/2015</td>
                                        <td>New</td>
                                        <td>450</td>
                                        <td>500</td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
    </ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

