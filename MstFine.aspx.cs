﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MstFine : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Fine Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Load_Data();
            
            
        }
    }

    public void Load_Data()
    {
        SSQL = "select * from Mst_LateFine where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count != 0)
        {
            txtImproperDays.Text = dt.Rows[0]["ImproperDays"].ToString();
            txtImproperAmt.Text = dt.Rows[0]["ImproperAmt"].ToString();
            txtLateINDays.Text = dt.Rows[0]["LateInDays"].ToString();
            txtLateINAmt.Text = dt.Rows[0]["LateInAmt"].ToString();
        }
        else
        {
            txtImproperDays.Text = "";
            txtImproperAmt.Text = "";
            txtLateINDays.Text = "";
            txtLateINAmt.Text = "";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        SSQL = "Delete from Mst_LateFine where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Insert into Mst_LateFine(CompCode,LocCode,ImproperDays,ImproperAmt,LateInDays,LateInAmt)";
        SSQL = SSQL + "values('" + SessionCcode + "','" + SessionLcode + "','" + txtImproperDays.Text + "',";
        SSQL = SSQL + "'" + txtImproperAmt.Text + "','" + txtLateINDays.Text + "','" + txtLateINAmt.Text + "'";
        SSQL = SSQL + ")";
        objdata.RptEmployeeMultipleDetails(SSQL);

        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Fine Details Saved Successfully...!');", true);

        Load_Data();
    }


    protected void btnClear_Click(object sender, EventArgs e)
    {
        Load_Data();
    }
}
