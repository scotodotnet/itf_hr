﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

public partial class MstIncrementNew : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    string SSQL = "";

    bool ErrFlg = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                //ddlFinYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //currentYear = currentYear - 1;
            }
            load_wages();
        }
        Load_OLD_Data();
    }

    private void load_wages()
    {
        DataTable dt = new DataTable();

        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpTypeCd";
        ddlWagesType.DataBind();
    }

    private void Load_OLD_Data()
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstDayIncProcess where Compcode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtFromDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter From Date');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlg = true;
            return;
        }
        if (txtToDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter To Date');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlg = true;
            return;
        }
        if (ddlWagesType.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Choose Wages');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlg = true;
            return;
        }

        if (!ErrFlg)
        {
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from [" + SessionEpay + "]..MstDayIncProcess where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and WagesCode='" + ddlWagesType.SelectedValue + "'";
                SSQL = SSQL + " And FromDate='" + txtFromDate.Text + "' And ToDate='" + txtToDate.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Already increment Process Present in this dates Please try Another dates');", true);
                    //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlg = true;
                    return;
                }
            }
            if (!ErrFlg)
            {
                SSQL = "";
                SSQL = "Delete from [" + SessionEpay + "]..MstDayIncProcess where Compcode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and WagesCode='" + ddlWagesType.SelectedValue + "'";
                SSQL = SSQL + " And FromDate='" + txtFromDate.Text + "' And ToDate='" + txtToDate.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "";
                SSQL = "Insert into [" + SessionEpay + "]..MstDayIncProcess (CompCode,LocCode,FromDate,ToDate,Wages,WagesCode,Status,IncrementDate) ";
                SSQL = SSQL + "Values('" + SessionCcode + "','" + SessionLcode + "','" + txtFromDate.Text + "','" + txtToDate.Text + "','" + ddlWagesType.SelectedItem.Text + "','" + ddlWagesType.SelectedValue + "','0','" + txtIncDate.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SalaryIncrement();

                if (btnSave.Text == "Save")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Process Details Saved Successfully');", true);

                }
                if (btnSave.Text == "Update")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Process Details Updated Successfully');", true);
                    //btnCancel_Click(sender, e);
                }
                btnCancel_Click(sender, e);
            }
        }
        Load_OLD_Data();
    }

   
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtIncDate.Text = "";
        btnSave.Text="Save";
        ddlWagesType.ClearSelection();
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstDayIncProcess where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' WagesCode='" + e.CommandName + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtFromDate.Text = dt.Rows[0]["FromDate"].ToString();
            txtToDate.Text = dt.Rows[0]["ToDate"].ToString();
            ddlWagesType.SelectedValue= dt.Rows[0]["WagesCode"].ToString();
            txtIncDate.Text = dt.Rows[0]["IncrementDate"].ToString();

            btnSave.Text = "Update";
        }
        Load_OLD_Data();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstDayIncProcess where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and FromDate='" + e.CommandArgument.ToString().Split(',').First() + "'";
        SSQL = SSQL + " and ToDate='" + e.CommandArgument.ToString().Split(',').Last() + "' and WagesCode='" + e.CommandName.ToString() + "' and Status='1'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Process Already Approved');", true);
        }
        else
        {
            SSQL = "";
            SSQL = "Delete from [" + SessionEpay + "]..MstDayIncProcess where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and FromDate='" + e.CommandArgument.ToString().Split(',').First() + "'";
            SSQL = SSQL + " and ToDate='" + e.CommandArgument.ToString().Split(',').Last() + "' and WagesCode='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Delete from [" + SessionEpay + "]..EmpDaysIncrement where cCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and FromDate='" + e.CommandArgument.ToString().Split(',').First() + "'";
            SSQL = SSQL + " and ToDate='" + e.CommandArgument.ToString().Split(',').Last() + "' and WagesCode='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Process Details Deleted Successfully');", true);
        }
        Load_OLD_Data();
    }
    protected void btnApproveEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstDayIncProcess where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and FromDate='" + e.CommandArgument.ToString().Split(',').First() + "'";
        SSQL = SSQL + " and ToDate='" + e.CommandArgument.ToString().Split(',').Last() + "' and WagesCode='" + e.CommandName.ToString() + "' and Status='1'";
        DataTable dt = new DataTable();
        dt=objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Process Already Approved');", true);
        }
        else
        {
            SSQL = "";
            SSQL = "Update [" + SessionEpay + "]..MstDayIncProcess set Status='1' where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and FromDate='" + e.CommandArgument.ToString().Split(',').First() + "'";
            SSQL = SSQL + " and ToDate='" + e.CommandArgument.ToString().Split(',').Last() + "' and WagesCode='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable DT = new DataTable();

            SSQL = "Select EmpNo,Increment,SplIncrement from [" + SessionEpay + "]..EmpDaysIncrement where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " and FromDate='" + e.CommandArgument.ToString().Split(',').First() + "'";
            SSQL = SSQL + " and ToDate='" + e.CommandArgument.ToString().Split(',').Last() + "' and WagesCode='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                SSQL = "";
                SSQL = "Update [" + SessionEpay + "]..EmpDaysIncrement set Status='1' where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FromDate='" + e.CommandArgument.ToString().Split(',').First() + "'";
                SSQL = SSQL + " and ToDate='" + e.CommandArgument.ToString().Split(',').Last() + "' and WagesCode='" + e.CommandName.ToString() + "'";
                SSQL = SSQL + " and EmpNo='" + DT.Rows[i]["EmpNo"].ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);



                SSQL = "Update Employee_Mst set BaseSalary=BaseSalary + " + (Convert.ToDecimal(DT.Rows[i]["Increment"].ToString()) + Convert.ToDecimal(DT.Rows[i]["SplIncrement"].ToString())) + "";
                SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And Eligible_Increment='1' And EmpNo='" + DT.Rows[i]["EmpNo"].ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            


            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Increment Process Approved Successfully');", true);
        }
    }

    public void SalaryIncrement()
    {
        string Query="";
        string MachineID = "";

        //Salary Increment Start
        DataTable DT_Emp = new DataTable();
        DataTable DT_Chk = new DataTable();
        DataTable DT = new DataTable();
        DataTable DT_Sal = new DataTable();
        DataTable DT_Inc = new DataTable();
        DataTable DT_Elg = new DataTable();
        string IncAmt = "0";
        string SplInc = "0";

        Query = "Select EmpNo,BaseSalary from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        Query = Query + " And Wages='" + ddlWagesType.SelectedItem.Text + "' And IsActive='Yes' And Eligible_Increment='1'";
        DT_Elg = objdata.RptEmployeeMultipleDetails(Query);

        for (int i = 0; i < DT_Elg.Rows.Count; i++)
        {
            IncAmt = "0";
            SplInc = "0";
            MachineID = DT_Elg.Rows[i]["EmpNo"].ToString();

            string SSQL = "Select * from [" + SessionEpay + "]..MstDayIncProcess where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And IncrementDate='" + txtIncDate.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count != 0)
            {
                SSQL = "Select *From [" + SessionEpay + "]..MstDaysSplIncrement where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                DT_Inc = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Inc.Rows.Count != 0)
                {
                    SplInc = DT_Inc.Rows[0]["SplIncrement"].ToString();
                }

                Query = "Select isnull(sum(Days),0) as EmpDays from [" + SessionEpay + "]..AttenanceDetails where EmpNo='" + MachineID + "' and ";
                Query = Query + "Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(datetime, FromDate, 105 ) >= Convert(datetime, '" + DT.Rows[0]["FromDate"].ToString() + "',105) ";
                Query = Query + "and convert(datetime, ToDate, 105) <= convert(Datetime, '" + DT.Rows[0]["ToDate"].ToString() + "', 105) ";
                DT_Sal = objdata.RptEmployeeMultipleDetails(Query);

                if (DT_Sal.Rows.Count != 0)
                {
                    if (DT_Sal.Rows[0]["EmpDays"].ToString() != "" && DT_Sal.Rows[0]["EmpDays"].ToString() != "0")
                    {
                        Query = "Select isnull(IncrementAmt,'0') as IncrementAmt from [" + SessionEpay + "]..MstDaysIncrement where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        Query = Query + " And PresentDays='" + DT_Sal.Rows[0]["EmpDays"].ToString() + "'";
                        DT_Inc = objdata.RptEmployeeMultipleDetails(Query);

                        if (DT_Inc.Rows.Count != 0)
                        {
                            IncAmt = DT_Inc.Rows[0]["IncrementAmt"].ToString();
                        }
                    }
                }
                Query = "Select *from [" + SessionEpay + "]..EmpDaysIncrement where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                Query = Query + " And FromDate='" + txtFromDate.Text + "' And ToDate='" + txtToDate.Text + "' And IncrementDate='" + txtIncDate.Text + "'";
                Query = Query + " And EmpNo='" + MachineID + "' And Status='1'";
                DT_Chk = objdata.RptEmployeeMultipleDetails(Query);

                decimal TotalInc = 0;
                decimal TotalBasic = 0;

                TotalInc = Convert.ToDecimal(SplInc) + Convert.ToDecimal(IncAmt);
                TotalInc = Math.Round(Convert.ToDecimal(TotalInc), 2, MidpointRounding.AwayFromZero);

                TotalBasic = Convert.ToDecimal(DT_Elg.Rows[i]["BaseSalary"].ToString()) + TotalInc;
                TotalBasic = Math.Round(Convert.ToDecimal(TotalBasic), 2, MidpointRounding.AwayFromZero);

                if (DT_Chk.Rows.Count == 0)
                {
                    Query = "Delete from [" + SessionEpay + "]..EmpDaysIncrement where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    Query = Query + " And FromDate='" + txtFromDate.Text + "' And ToDate='" + txtToDate.Text + "' And IncrementDate='" + txtIncDate.Text + "'";
                    Query = Query + " And EmpNo='" + MachineID + "'";
                    objdata.RptEmployeeMultipleDetails(Query);


                    Query = "Insert into [" + SessionEpay + "]..EmpDaysIncrement (Ccode,Lcode,Days,FromDate,ToDate,";
                    Query = Query + "Increment,SplIncrement,TotalIncrement,TotalBasic,FinYear,Basic,EmpNo,Wages,WagesCode,IncrementDate)values('" + SessionCcode + "',";
                    Query = Query + "'" + SessionLcode + "','" + DT_Sal.Rows[0]["EmpDays"].ToString() + "',";
                    Query = Query + "'" + txtFromDate.Text + "','" + txtToDate.Text + "',";
                    Query = Query + "'" + IncAmt + "','" + SplInc + "','" + TotalInc + "','" + TotalBasic + "','" + Convert.ToInt32(Utility.GetFinancialYear) + "-" + Convert.ToInt32(Utility.GetFinancialYear+1)  + "','" + DT_Elg.Rows[i]["BaseSalary"].ToString() + "','" + MachineID + "',";
                    Query = Query + "'" + ddlWagesType.SelectedItem.Text + "','" + ddlWagesType.SelectedValue + "',";
                    Query = Query + "'" + txtIncDate.Text + "')";
                    objdata.RptEmployeeMultipleDetails(Query);

                    //Query = "Update Employee_Mst set BaseSalary=BaseSalary + " + Convert.ToDecimal(DT_Inc.Rows[0]["IncrementAmt"].ToString()) + "";
                    //Query = Query + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //Query = Query + " And Eligible_Increment='1' And EmpNo='" + MachineID + "'";
                    //objdata.RptEmployeeMultipleDetails(Query);
                }

            }
        }
        //Salary Increment End

    }

}