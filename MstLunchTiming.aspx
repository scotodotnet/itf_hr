﻿<%@ Page Language="C#" MasterPageFile ="~/MainPage.master" AutoEventWireup="true" CodeFile="MstLunchTiming.aspx.cs" Inherits="MstLunchTiming" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>

<asp:UpdatePanel ID="UpdatePanel5" runat="server">
    <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">Lunch Timing</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Lunch Timing</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Lunch Timing</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row" runat="server" visible="false">
                           <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Shift</label>
								  <asp:DropDownList runat="server" ID="ddlShift" class="form-control"></asp:DropDownList>
								 <%-- <asp:RequiredFieldValidator ControlToValidate="ddlShift" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>--%>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Start Time</label>
								  <asp:TextBox runat="server" ID="txtStartTime" class="form-control" MaxLength="10"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtStartTime" Display="Dynamic"   ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>End Time</label>
								  <asp:TextBox runat="server" ID="txtEndTime" class="form-control" MaxLength="10"></asp:TextBox>
								<%--  <asp:RequiredFieldValidator ControlToValidate="txtEndTime" Display="Dynamic"   ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>--%>
								</div>
                               </div>
                              <!-- end col-4 -->
                              
                                <div class="col-md-3">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnlunchSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="Validate_Field" onclick="btnlunchSave_Click"  />
									<asp:Button runat="server" id="btnlunchClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnlunchClear_Click"/>
								 </div>
                               </div>
                              
                              </div>
                        <!-- end row -->
                      <div class="row" runat="server" visible="false">
                       <div class="col-md-3">
								<div class="form-group">
								  <label>Late IN Days</label>
								  <asp:TextBox runat="server" ID="txtLateIN" class="form-control" MaxLength="10"></asp:TextBox>
								<%--  <asp:RequiredFieldValidator ControlToValidate="txtLateIN" Display="Dynamic"   ValidationGroup="Validate_Field1" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>--%>
								</div>
                               </div>
                      
                      
                       <div class="row">
                       <div class="col-md-3">
								<div class="form-group">
								  <label>ImproperPunch Days</label>
								  <asp:TextBox runat="server" ID="txtimproper" class="form-control" MaxLength="10"></asp:TextBox>
								 <%-- <asp:RequiredFieldValidator ControlToValidate="txtimproper" Display="Dynamic"   ValidationGroup="Validate_Field1" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>--%>
								</div>
                               </div>
                    
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-3">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="Validate_Field1" onclick="btnSave_Click"  />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click"/>
								 </div>
                               </div>
                              <!-- end col-4 -->
                             
                         </div>
                        <!-- end row -->
                        
                            <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Shift</th>
                                                <th>StartTime</th>
                                                <th>EndTime</th>
                                                 
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("Shift")%></td>
                                        <td><%# Eval("StartTime")%></td>
                                        <td><%# Eval("EndTime")%></td>
                                        
                                         
                                       
                                        
                                        <td>
                                                   <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument='<%# Eval("Shift")%>' >
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%# Eval("Shift")%>'  
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Recruitment Officer details?');">
                                                    </asp:LinkButton>
                                                </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        
                        </div>
                       
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Wages Type</label>
								 <asp:DropDownList runat="server" ID="ddlWages" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" onselectedindexchanged="ddlWages_SelectedIndexChanged">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlWages" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                               
                               
                              
                         <!-- begin col-4 -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Department</label><span class="mandatory">*</span>
                                    <asp:DropDownList ID="ddlDepartment" runat="server" 
                                        class="form-control select2" style="width:100%;" 
                                        onselectedindexchanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ControlToValidate="ddlDepartment" Display="Dynamic" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                               
                            </div>
                         <!-- end col-4 -->
                         <!-- begin col-4 -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Designation</label><span class="mandatory">*</span>
                                    <asp:DropDownList ID="ddlDesignation" runat="server" class="form-control select2" style="width:100%;" >
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ControlToValidate="ddlDesignation" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                               
                               <div class="col-md-2">
								<div class="form-group">
								  <label>Lunch Timing</label>
								  <asp:TextBox runat="server" ID="txtLunchTiming" class="form-control" Text="0"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtLunchTiming" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtLunchTiming" ValidChars="0123456789.">
                                   </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                               
                           <!-- end col-4 -->
                     
                    </div>
                     <div class="row">
                      <div class="col-md-2">
								 <div class="form-group">
									
									<asp:Button runat="server" id="btnLunchTimingSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="Validate_Field" onclick="btnLunchTimingSave_Click" />
									<asp:Button runat="server" id="btnLunchTimingClear" Text="Clear" class="btn btn-danger" onclick="btnLunchTimingClear_Click" 
                                         />
								 </div>
                               </div>
                               </div> 
                    <!-- end panel -->
                    
                    
                      <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                             <th>Wages Type</th>
                                                <th>DeptName</th>
                                                <th>Designation</th>
                                                <th>LunchTiming</th>
                                                
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                             <td><%# Eval("Wages")%></td>
                                        <td><%# Eval("DeptName")%></td>
                                        <td><%# Eval("Designation")%></td>
                                        <td><%# Eval("LunchTiming")%></td>
                                       
                                        <td>
                                                   <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument='<%# Eval("Wages")%>' CommandName='<%# Eval("DeptCode")+","+Eval("Designation")%>'>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%# Eval("Wages")%>' CommandName='<%# Eval("DeptCode")+","+Eval("Designation")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Incentive details?');">
                                                    </asp:LinkButton>
                                                </td>
                                      
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                    
                    
                    
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>
<!-- end #content -->

    </ContentTemplate>
</asp:UpdatePanel>



</asp:Content>

