﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstMessTiming.aspx.cs" Inherits="MstMessTiming" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script type="text/javascript">
     $(document).ready(function() {
     $('#example').dataTable();
     $('#example1').dataTable();

     });
 </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
                $('#example1').dataTable();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">Mess Deduction</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Mess Deduction</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Mess Deduction</h4>
                        </div>
                        <div class="panel-body">
                      <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label> Mess Agent</label>
								  <asp:TextBox runat="server" ID="txtMessAgent" class="form-control"></asp:TextBox>
							   </div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Mess Amount</label>
								  <asp:TextBox runat="server" ID="txtMessAmt" class="form-control"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-4 -->
                              
                              <!-- begin col-4 -->
                                <div class="col-md-3">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="Validate_Field" onclick="btnSave_Click"  />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click"/>
								 </div>
                               </div>
                              <!-- end col-4 -->
                          </div>
                      <!-- end row -->
                      <div class="row">
                         <div class="col-md-8">
                         <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                
                                                <th>Agent Name</th>
                                                <th>Mess Amt</th>
                                                
                                                
                                                
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>  
                                    
                                    <td><%# Eval("MessAgent")%></td>
                                    <td><%# Eval("MessAmt")%></td>
                                        
                                      
                                       
                                        <td>
                                                     <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%# Eval("MessAgent")%>'
                                                                                                          CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Mess details?');">
                                                    </asp:LinkButton>

                                                </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
                         </div>
					        
					    </div>
                      
                      
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
                                 <!-- table start -->
					<div class="col-md-12">
					  <div class="panel panel-inverse">
					    <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Mess Days</h4>
                        </div>
                        <div class="panel-body">
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Mess Agent</label>
								 <asp:DropDownList runat="server" ID="ddlMessAgentName" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Month Work</label>
								  <asp:TextBox runat="server" ID="txtMonthDays" class="form-control"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-4 -->
                              
                              <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Worked Days</label>
								  <asp:TextBox runat="server" ID="txtWorkedDays" class="form-control"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-5">
                              <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnDaysSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="Validate_Field" onclick="btnDaysSave_Click"   />
									<asp:Button runat="server" id="btnDaysClear" Text="Clear" 
                                        class="btn btn-danger" onclick="btnDaysClear_Click" 
                                         />
								 </div>
                              </div>
                              <!-- end col-4 -->
                      </div>
                      <div class="row">
                      <div class="col-md-12">
                         <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example1" class="display table">
                                        <thead>
                                            <tr>
                                                
                                                <th>Agent Name</th>
                                                <th>Month Work</th>
                                                <th>Worked Days</th>
                                                
                                                
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>  
                                    
                                    <td><%# Eval("MessAgent")%></td>
                                    <td><%# Eval("MessMonth")%></td>
                                    <td><%# Eval("MessWroked")%></td>    
                                      
                                       
                                        <td>
                                                     <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridViewDeleteEnquiryClick" CommandArgument='<%# Eval("MessAgent")+","+Eval("MessMonth")%>'
                                                                                                          CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Mess Days?');">
                                                    </asp:LinkButton>

                                                </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
                         </div>
                      </div>
                        </div>
                        
					  </div>
					 
					    
					    
					</div>
					<!-- table End -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>




</asp:Content>

