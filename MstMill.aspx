﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstMill.aspx.cs" Inherits="MstMill" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
     <asp:UpdatePanel runat="server" EnableViewState="true">
        <ContentTemplate>
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Mill Details</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Master</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Mill Details</li>
                        </ol>
                    </nav>
                </div>
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="forms-sample">
                                    <div class="row">
                                        <div class="col-md  -3">
                                         <div class="form-group">
                                            <label>Mill Code</label><span class="asterisk_input"></span>
                                            <asp:TextBox ID="txtMillCode" MaxLength="30" class="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtMillCode" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Mill Name</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtMillName" class="form-control" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtMillName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                             <div class="form-group">
                                                <label>Type</label><span class="asterisk_input"></span>
                                                <asp:DropDownList ID="ddlMillType" runat="server"  class="js-example-basic-single select2" Style="width: 100%">
                                                </asp:DropDownList>
                                                </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Total Units</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtUnits" class="form-control" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtUnits" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>GST No</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtGStNo" class="form-control" runat="server" ></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtGStNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                       
                                        <!-- begin col-2 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Contact Person1</label>
                                               <asp:TextBox ID="txtContactPerson1" class="form-control"  runat="server"  ></asp:TextBox>
                                               <%-- <asp:RequiredFieldValidator ControlToValidate="txtContactPerson1" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                         <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Contact Person1 Phone</label>
                                               <asp:TextBox ID="txtContactPerson1Phone" class="form-control"  runat="server" ></asp:TextBox>
                                               <%-- <asp:RequiredFieldValidator ControlToValidate="txtContactPerson1Phone" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                         <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Contact Person2</label>
                                               <asp:TextBox ID="txtContactPerson2" class="form-control"  runat="server"  ></asp:TextBox>
                                                
                                            </div>
                                        </div>
                                         <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Contact Person2 Phone</label>
                                               <asp:TextBox ID="txtContactPerson2Phone" class="form-control"  runat="server" ></asp:TextBox>
                                               
                                            </div>
                                        </div>
                                         <!-- begin col-4 -->
                                        <div class="col-md-4">
                                             <div class="form-group">
                                                <label>Address</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtAddress" class="form-control" style="resize:none"  runat="server" TextMode="MultiLine" ></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtAddress" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                         <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Remarks</label>
                                               <asp:TextBox ID="txtRemarks" class="form-control" Style="resize:none" TextMode="MultiLine"  runat="server" ></asp:TextBox>
                                              
                                           </div>
                                        </div>
                                        <!-- end col-2 -->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                                <asp:Button runat="server" ID="btnCancel" Text="Cancel" class="btn btn-danger" OnClick="btnCancel_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
                </div>
            </footer>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

      <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
    
</asp:Content>

