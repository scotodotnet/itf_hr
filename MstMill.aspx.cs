﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Security.Cryptography;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstMill : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    bool ErrFLg = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

       
        if (!IsPostBack)
        {
            getlast_Code();
            Load_Type();

            if ((string)Session["MillCode"] != "")
            {
                btn_Search(sender, e);
            }
        }
    }
    protected void Load_Type()
    {
        SSQL = "";
        SSQL = "Select * from MstMillType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable Dt_Type = new DataTable();
        Dt_Type = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Dt_Type != null && Dt_Type.Rows.Count > 0)
        {
            ddlMillType.DataSource = Dt_Type;
            ddlMillType.DataTextField = "MillTypeName";
            ddlMillType.DataValueField = "MillTypeCode";
            ddlMillType.DataBind();
            ddlMillType.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtMillCode.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the MillCode')", true);
            ErrFLg = true;
            return;
        }
        if (txtMillName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page,this.GetType(),"alert", "SaveMsgAlert('Please Enter the MillName')", true);
            ErrFLg = true;
            return;
        }
        if (ddlMillType.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Select the Mill Tyep')", true);
            ErrFLg = true;
            return;
        }
        if (txtGStNo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the GST Number')", true);
            ErrFLg = true;
            return;
        }
        if (txtAddress.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Address')", true);
            ErrFLg = true;
            return;
        }
        if (txtUnits.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Total Number of Units')", true);
            ErrFLg = true;
            return;
        }
        //if (txtContactPerson1.Text=="")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Contact Person1 name')", true);
        //    ErrFLg = true;
        //    return;
        //}
        //if (txtContactPerson1Phone.Text=="")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Contact Person1 Phone Number')", true);
        //    ErrFLg = true;
        //    return;
        //}
        if (!ErrFLg)
        {
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillName='" + txtMillName.Text + "' or MillCode='" + txtMillCode.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('This Mill Name or Mill Code is Already Present')", true);
                    ErrFLg = true;
                    return;
                }
            }
            else
            {
                SSQL="";
                SSQL = "Delete from MstMill where MillName='" + txtMillName.Text + "' and Millcode='" + txtMillCode.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (!ErrFLg)
            {
                SSQL = "";
                SSQL = "Insert into MstMill(Ccode,Lcode,MilltypeCode,MillTypeName,MillCode,MillName,GstNo,Address,Contact_Person1,Contact_person1_ph,Contact_person2,Contact_person2_Ph,Remarks,Units)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + ddlMillType.SelectedValue + "','" + ddlMillType.SelectedItem.Text + "','" + txtMillCode.Text + "','" + txtMillName.Text + "',";
                SSQL = SSQL + "'" + txtGStNo.Text + "','" + txtAddress.Text + "','" + txtContactPerson1.Text + "','" + txtContactPerson1Phone.Text + "','" + txtContactPerson2.Text + "','" + txtContactPerson2Phone.Text + "','" + txtRemarks.Text + "','" + txtUnits.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Mill Saved Successfully')", true);
                btnCancel_Click(sender,e);   
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtMillCode.Text = "";
        txtMillName.Text = "";
        ddlMillType.ClearSelection();
        txtGStNo.Text = "";
        txtAddress.Text = "";
        txtContactPerson1.Text = "";
        txtContactPerson1Phone.Text = "";
        txtContactPerson2.Text = "";
        txtContactPerson2Phone.Text = "";
        txtRemarks.Text = "";
        txtUnits.Text = "";

        btnSave.Text = "Save";
        getlast_Code();
    }
    protected void btn_Search(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + Session["MillCode"] + "'";
        DataTable Dt_Get = new DataTable();
        Dt_Get = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Dt_Get != null && Dt_Get.Rows.Count > 0)
        {
            
            txtMillCode.Text = Dt_Get.Rows[0]["MillCode"].ToString();
            txtMillName.Text = Dt_Get.Rows[0]["MillName"].ToString();
            ddlMillType.SelectedValue = Dt_Get.Rows[0]["MillTypeCode"].ToString();
            txtGStNo.Text = Dt_Get.Rows[0]["GstNo"].ToString();
            txtAddress.Text = Dt_Get.Rows[0]["Address"].ToString();
            txtContactPerson1.Text = Dt_Get.Rows[0]["Contact_Person1"].ToString();
            txtContactPerson1Phone.Text = Dt_Get.Rows[0]["Contact_person1_ph"].ToString();
            txtContactPerson2.Text = Dt_Get.Rows[0]["Contact_person2"].ToString();
            txtContactPerson2Phone.Text = Dt_Get.Rows[0]["Contact_person2_ph"].ToString();
            txtRemarks.Text = Dt_Get.Rows[0]["Remarks"].ToString();
            txtUnits.Text= Dt_Get.Rows[0]["Units"].ToString();
            btnSave.Text = "Update";
        }
    }
    protected void getlast_Code()
    {
        //    int no = 0;
        //    SSQL = "";
        //    SSQL = "select isnull(Max(MillCode),0) as cnt from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        //    DataTable dt = new DataTable();
        //    dt = objdata.RptEmployeeMultipleDetails(SSQL);
        //    if (dt != null && dt.Rows.Count > 0)
        //    {
        //        no = Convert.ToInt32(dt.Rows[0]["cnt"]);
        //        no = no + 1;
        //    }
        //    else
        //    {
        //        no = 1;
        //    }

        //    txtMillCode.Text = no.ToString() ;
        //}
    }
}