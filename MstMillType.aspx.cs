﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Security.Cryptography;

using Altius.BusinessAccessLayer.BALDataAccess;
public partial class MstMillType : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    bool ErrFLg = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        Load_Data();
        if (!IsPostBack)
        {
            getlast_Code();
        }
    }
    protected void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from MstMillType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }
    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstMilltype where MillTypeCode='" + e.CommandName + "' and Lcode='" + SessionLcode + "' and ccode='" + SessionCcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt != null && dt.Rows.Count > 0)
        {
            txtTypeMillCode.Text = dt.Rows[0]["MillTypeCode"].ToString();
            txtTypeMillName.Text = dt.Rows[0]["MillTypeName"].ToString();
            btnSave.Text = "Update";
        }
        Load_Data();
    }

    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from MstMillType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillTypeCode='" + e.CommandName + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
        getlast_Code();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtTypeMillCode.Text = "";
        txtTypeMillName.Text = "";
        getlast_Code();
        btnSave.Text = "Save";
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtTypeMillCode.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Mill Type Code')", true);
            ErrFLg = true;
            return;
        }
        if (txtTypeMillName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Enter the Mill Type Name')", true);
            ErrFLg = true;
            return;
        }
        if (!ErrFLg)
        {
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from MstMillType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillTypeName='" + txtTypeMillName.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('This Mill Type Name is Already Present')", true);
                    ErrFLg = true;
                    return;
                }
            }
            else
            {
                SSQL = "";
                SSQL = "Delete from MstMillType where MillTypeName='" + txtTypeMillName.Text + "' or MillTypeCode='" + txtTypeMillCode.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (!ErrFLg)
            {
                SSQL = "";
                SSQL = "Insert into MstMillType(Ccode,Lcode,MilltypeCode,MillTypeName)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtTypeMillCode.Text + "','" + txtTypeMillName.Text + "')";
               
                objdata.RptEmployeeMultipleDetails(SSQL);
                if(btnSave.Text=="Save")
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Mill Types Saved Successfully')", true);
                else
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Mill Types Updated Successfully')", true);
                Load_Data();
                btnCancel_Click(sender, e);
            }
        }
    }
    protected void getlast_Code()
    {
        int no = 0;
        SSQL = "";
        SSQL = "select isnull(Max(MillTypeCode),0) as cnt from MstMillType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt != null && dt.Rows.Count > 0)
        {
            no = Convert.ToInt32(dt.Rows[0]["cnt"]);
            no = no + 1;
        }
        else
        {
            no = 1;
        }

        txtTypeMillCode.Text = no.ToString();
    }
}