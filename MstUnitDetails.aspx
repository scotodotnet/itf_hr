﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstUnitDetails.aspx.cs" Inherits="MstUnitDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
       <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
     <script  type="text/javascript">
     $(document).ready(function() {
     $('#example').dataTable();
     $('.select2').select2();
     });
	</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
            }
        });
    };
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel runat="server" EnableViewState="true">
        <ContentTemplate>
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Units Details</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Master</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Units Details</li>
                        </ol>
                    </nav>
                </div>
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="forms-sample">
                                    <div class="row">
                                        <div class="form-group col-lg-3">
                                            <label>Mill Code</label><span class="asterisk_input"></span>
                                            <asp:TextBox ID="txtMillCode" AutoPostBack="true" OnTextChanged="txtMillCode_TextChanged" MaxLength="30" class="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtMillCode" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Mill Name</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtMillName" class="form-control" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtMillName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Mill Type</label><span class="asterisk_input"></span>
                                                <asp:DropDownList ID="ddlMillType" runat="server" class="form-control" Style="width: 100%">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Units</label><span class="asterisk_input"></span>
                                                <asp:DropDownList ID="ddlUnits" runat="server" class="form-control" Style="width: 100%"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlUnits" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Units Type</label><span class="asterisk_input"></span>
                                                <asp:DropDownList ID="ddlUnitTypes" runat="server" class="form-control" Style="width: 100%"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlUnitTypes" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>GST No</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtGStNo" class="form-control" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtGStNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                       
                                        <!-- begin col-2 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Unit Contact Person</label>
                                                <asp:TextBox ID="txtUnitPerson" class="form-control" runat="server"></asp:TextBox>
                                               <%-- <asp:RequiredFieldValidator ControlToValidate="txtUnitPerson" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Contact Person Mobile</label>
                                                <asp:TextBox ID="txtUnitPersonPhone" class="form-control" runat="server"></asp:TextBox>
                                              <%--  <asp:RequiredFieldValidator ControlToValidate="txtUnitPersonPhone" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Contact Person DeptName</label>
                                                <asp:DropDownList ID="ddldeptName" runat="server" class="form-control" OnSelectedIndexChanged="ddlDept_OnSelectChanged" Style="width: 100%"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Contact Person Designation</label>
                                                <asp:DropDownList ID="ddldesignation" runat="server" class="form-control" Style="width: 100%"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                         <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Unit Email ID</label>
                                                <asp:TextBox ID="txtEmail" class="form-control" Style="resize: none" runat="server"></asp:TextBox>
                                               
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                         <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Unit Address</label><span class="asterisk_input"></span>
                                                <asp:TextBox ID="txtAddress" class="form-control" Style="resize: none" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtAddress" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Remarks</label>
                                                <asp:TextBox ID="txtRemarks" class="form-control" Style="resize: none" TextMode="MultiLine" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end col-2 -->
                                </div>
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <!-- begin col-4 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <br />
                                            <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                            <asp:Button runat="server" ID="btnCancel" Text="Cancel" class="btn btn-danger" OnClick="btnCancel_Click" />
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <div class="col-md-4"></div>
                                </div>
                                <!-- end row -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
                </div>
            </footer>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script src=" https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src=" https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
</asp:Content>

