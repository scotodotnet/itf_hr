﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Security.Cryptography;

using Altius.BusinessAccessLayer.BALDataAccess;
public partial class MstUnitDetails : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    bool ErrFLg = false;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();


        if (!IsPostBack)
        {
            //getlast_Code();
            Load_Type();
           
            txtMillCode.Focus();
            Load_DeptName("","");
            if ((string)Session["MillCode"] != "" && (string)Session["UnitCode"] != "") 
            {
                btn_Search(sender, e);
            }
        }
    }

    private void Load_DeptName(string MillCode,string UnitCode)
    {
        SSQL = "";
        if (MillCode != null||UnitCode!=null)
        {
            SSQL = "Select * from Department_Mst where MillCode='" + MillCode + "' and UnitCode='"+UnitCode+"'";
        }
        else
        {
            SSQL = "Select * from Department_Mst";
        }
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddldeptName.DataSource = dt;
        ddldeptName.DataTextField = "DeptName";
        ddldeptName.DataValueField = "DeptCode";
        ddldeptName.DataBind();
        ddldeptName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

    }
    protected void Load_Type()
    {
        SSQL = "";
        SSQL = "Select * from MstMillType where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable Dt_Type = new DataTable();
        Dt_Type = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Dt_Type != null && Dt_Type.Rows.Count > 0)
        {
            ddlMillType.DataSource = Dt_Type;
            ddlMillType.DataTextField = "MillTypeName";
            ddlMillType.DataValueField = "MillTypeCode";
            ddlMillType.DataBind();
            ddlMillType.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

            ddlUnitTypes.DataSource = Dt_Type;
            ddlUnitTypes.DataTextField = "MillTypeName";
            ddlUnitTypes.DataValueField = "MillTypeCode";
            ddlUnitTypes.DataBind();
            ddlUnitTypes.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }
    protected void btn_Search(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + Session["MillCode"] + "' and UnitCode='" + Session["UnitCode"] + "'";
        DataTable Dt_Get = new DataTable();
        Dt_Get = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Dt_Get != null && Dt_Get.Rows.Count > 0)
        {

            txtMillCode.Text = Dt_Get.Rows[0]["MillCode"].ToString();
            txtMillName.Text = Dt_Get.Rows[0]["MillName"].ToString();
            ddlMillType.SelectedValue = Dt_Get.Rows[0]["MillTypeCode"].ToString();
            ddlUnitTypes.SelectedValue = Dt_Get.Rows[0]["UnitTypeCode"].ToString();
            txtGStNo.Text = Dt_Get.Rows[0]["GstNo"].ToString();
            ddldeptName.SelectedValue = Dt_Get.Rows[0]["ContactPersonDeptCode"].ToString();
            ddldesignation.SelectedValue = Dt_Get.Rows[0]["ContactPersonDesigName"].ToString();
            txtAddress.Text = Dt_Get.Rows[0]["Address"].ToString();
            txtUnitPerson.Text = Dt_Get.Rows[0]["ContactPersonName"].ToString();
            txtUnitPersonPhone.Text = Dt_Get.Rows[0]["ContactPersonMobile"].ToString();
            txtRemarks.Text = Dt_Get.Rows[0]["Remarks"].ToString();
            Load_Units(Dt_Get.Rows[0]["MillCode"].ToString());
            ddlUnits.SelectedValue = Dt_Get.Rows[0]["UnitCode"].ToString();
            txtEmail.Text = Dt_Get.Rows[0]["Email"].ToString();
            btnSave.Text = "Update";
        }
    }
    private void Load_Units(string MillCode)
    {
        SSQL = "";
        SSQL = "select Units from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Millcode='" + MillCode + "'";
        DataTable dt = new DataTable();
        DataTable Unit_dt = new DataTable();

        Unit_dt.Columns.Add("UnitCode");
        Unit_dt.Columns.Add("UnitName");

        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        int Unitcode = 0;
        if (dt != null && dt.Rows.Count > 0)
        {
            
            Unitcode = Convert.ToInt32(dt.Rows[0]["Units"]);
            DataRow dr;

            for(int i = 1; i <= Unitcode; i++)
            {
                dr = Unit_dt.NewRow();
                //dr["UnitCode"] = "UNIT " + i;
                dr["UnitCode"] = i;
                dr["UnitName"] = "UNIT " + i;
                Unit_dt.Rows.Add(dr);
            }

            if (Unit_dt.Rows.Count > 0)
            {
                ddlUnits.DataSource = Unit_dt;
                ddlUnits.DataTextField = "UnitName";
                ddlUnits.DataValueField = "UnitCode";
                ddlUnits.DataBind();
                ddlUnits.Items.Insert(0,new ListItem("-Select-","-Select-",true));
            }

        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtMillCode.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please enter Mill Code');", true);
            ErrFLg = true;
            return;
        }
        if (txtMillName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please enter Mill Name');", true);
            ErrFLg = true;
            return;
        }
        if (ddlMillType.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose Mill Type');", true);
            ErrFLg = true;
            return;
        }
        if (ddlUnits.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose Unit Name');", true);
            ErrFLg = true;
            return;
        }
        if (ddlUnitTypes.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose Unit Type');", true);
            ErrFLg = true;
            return;
        }
        if (txtGStNo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter Unit GSTNo');", true);
            ErrFLg = true;
            return;
        }
        //if (txtUnitPerson.Text == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter Unit Contact Person Name');", true);
        //    ErrFLg = true;
        //    return;
        //}
        //if (txtUnitPersonPhone.Text == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter Unit Contact Person Mobile Number');", true);
        //    ErrFLg = true;
        //    return;
        //}
        //if (txtUnitPersonPhone.Text == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter Unit Contact Person Mobile Number');", true);
        //    ErrFLg = true;
        //    return;
        //}
        if (txtAddress.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Enter Unit Address');", true);
            ErrFLg = true;
            return;
        }
        if (!ErrFLg)
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "select * from  MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + txtMillCode.Text + "' and UnitName='" + ddlUnits.SelectedItem.Text + "'";
                DataTable Unit_DT = new DataTable();
                Unit_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (Unit_DT != null && Unit_DT.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Units Already Saved');", true);
                    ErrFLg = true;
                    return;
                }
                else if (!ErrFLg)
                {
                    SSQL = "";
                    SSQL = "Update MstMillUnits set MillName='" + txtMillName.Text + "',UnitCode='" + ddlUnits.SelectedValue + "',";
                    SSQL = SSQL + "UnitName='" + ddlUnits.SelectedItem.Text + "',UnitTypeName='" + ddlUnitTypes.SelectedItem.Text + "',UnitTypeCode='" + ddlUnitTypes.SelectedValue + "',";
                    SSQL = SSQL + "MilltypeCode='" + ddlMillType.SelectedValue + "',MillTypeName='" + ddlMillType.SelectedItem.Text + "',GSTNo='" + txtGStNo.Text + "',Address='" + txtAddress.Text + "',";
                    SSQL = SSQL + "ContactpersonName='" + txtUnitPerson.Text + "',ContactPersonDeptName='" + ddldeptName.SelectedValue + "',ContactPersonDeptCode='" + ddldeptName.SelectedValue + "',ContactPersonDesigName='" + ddldesignation.SelectedItem.Text + "'";
                    SSQL = SSQL + "ContactPersonMobile='" + txtUnitPersonPhone.Text + "',Email='" + txtEmail.Text + "',Remarks='"+txtRemarks.Text+"' where MillCode='" + txtMillCode.Text + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Unit Details UpDated Successfully');", true);
                    btnCancel_Click(sender, e);
                }
            }
            else if(!ErrFLg)
            {
                SSQL = "";
                SSQL = "select * from  MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + txtMillCode.Text + "' and UnitName='" + ddlUnits.SelectedItem.Text + "'";
                DataTable Unit_DT = new DataTable();
                Unit_DT= objdata.RptEmployeeMultipleDetails(SSQL);
                if (Unit_DT != null && Unit_DT.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Units Already Saved');", true);
                    ErrFLg = true;
                    return;
                }
                if (!ErrFLg)
                {
                    SSQL = "";
                    SSQL = "Insert into MstMillUnits(Ccode,Lcode,MillCode,MillName,UnitCode,UnitName,UnitTypeName,UnitTypeCode,MilltypeCode,MillTypeName,GSTNo,Address,ContactpersonName,ContactPersonDeptName,ContactPersonDeptCode,ContactPersonDesigName,Email,ContactPersonMobile,Remarks) ";
                    SSQL = SSQL + "values('" + SessionCcode + "','" + SessionLcode + "','" + txtMillCode.Text + "','" + txtMillName.Text + "','" + ddlUnits.SelectedValue + "','" + ddlUnits.SelectedItem.Text + "','" + ddlUnitTypes.SelectedItem.Text + "','" + ddlUnitTypes.SelectedValue + "',";
                    SSQL = SSQL + "'" + ddlMillType.SelectedValue + "','" + ddlMillType.SelectedItem.Text + "','" + txtGStNo.Text + "','" + txtAddress.Text + "','" + txtUnitPerson.Text + "','" + ddldeptName.SelectedItem.Text + "','" + ddldeptName.SelectedValue + "','" + ddldesignation.SelectedItem + "',";
                    SSQL = SSQL + "'" + txtEmail.Text + "','" + txtUnitPersonPhone.Text + "','" + txtRemarks.Text + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Unit Details Saved Successfully');", true);
                    btnCancel_Click(sender, e);
                }
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtMillCode.Text = "";
        txtMillName.Text = "";
        txtAddress.Text = "";
        txtGStNo.Text = "";
        txtEmail.Text = "";
        ddlUnits.ClearSelection();
        txtRemarks.Text = "";
        txtUnitPerson.Text = "";
        ddlUnits.DataSource = null;
        ddlMillType.ClearSelection();
        ddlUnitTypes.ClearSelection();
        ddldeptName.ClearSelection();
        ddldesignation.ClearSelection();
        btnSave.Text = "Save";
        ddldesignation.ClearSelection();
        ddldeptName.ClearSelection();
        txtUnitPersonPhone.Text = "";
    }
    protected void ddlDept_OnSelectChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from Designation_Mst where DeptName='" + ddldeptName.SelectedItem.Text + "' and MillCode='" + txtMillCode.Text + "'";
        ddldesignation.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddldesignation.DataTextField = "DesignName";
        ddldesignation.DataValueField = "DesignCode";
        ddldesignation.DataBind();
        ddldesignation.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void txtMillCode_TextChanged(object sender, EventArgs e)
    {
        if (txtMillCode.Text != "")
        {
            SSQL = "";
            SSQL = "Select * from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + txtMillCode.Text + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt != null && dt.Rows.Count > 0)
            {
                
               // ddlMillType.SelectedValue = dt.Rows[0]["MillCode"].ToString();
                ddlMillType.SelectedValue = dt.Rows[0]["MillTypeCode"].ToString();
                txtMillName.Enabled = false;
                txtMillName.Text = dt.Rows[0]["MillName"].ToString();
                Load_Units(dt.Rows[0]["MillCode"].ToString());
                txtGStNo.Text = dt.Rows[0]["GstNo"].ToString();

            }else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Mill Code Does not Exists!!!');", true);

                txtMillName.Text = "";
                txtAddress.Text = "";
                txtGStNo.Text = "";
                txtRemarks.Text = "";
                txtUnitPerson.Text = "";
                ddlUnits.DataSource = null;
                ddlUnits.DataBind();
                ddlMillType.ClearSelection();
                ddlUnitTypes.ClearSelection();
                ddldesignation.ClearSelection();
                ddldeptName.ClearSelection();
            }
        }
    }
}