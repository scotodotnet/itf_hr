﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstUnitDetailsMain.aspx.cs" Inherits="MstUnitDetailsMain" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" />

      <script  type="text/javascript">
     $(document).ready(function() {
     $('#example').dataTable();
     $('.select2').select2();
     });
	</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
            }
        });
    };</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UpdatePanel1" runat="server" EnableViewState="true">
        <ContentTemplate>
            <div class="content-wrapper">
                <div class="page-header" >
                    <h3 align="center" class="page-title">Unit Details</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Master</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Unit Details</li>
                           
                        </ol>
                    </nav>
                </div>
                     <asp:Button runat="server" ID="btnSave" Text="Add Unit Details" class="btn btn-success"  OnClick="btnSave_Click" />
                <br />
                <br />  
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="forms-sample">
                                    <div class="row">
                                  
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                               
                                               
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->

                                    
                                    <!-- table start -->
                                    <div class="col-lg-12">
                                      
                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="display table">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No</th>
                                                                <th>Mill Code</th>
                                                                <th>Mill Name</th>
                                                                <th>Mill Type</th>
                                                                 <th>Unit Code</th>
                                                                <th>Unit Name</th>
                                                                 <th>Unit Type</th>
                                                               <%--  <th>Contact Name</th>
                                                                <th>Contact Mobile</th>--%>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("MillCode")%></td>
                                                        <td><%# Eval("MillName")%></td>
                                                        <td><%# Eval("MillTypeName")%></td>
                                                         <td><%# Eval("UnitCode")%></td>
                                                          <td><%# Eval("UnitName")%></td>
                                                           <td><%# Eval("UnitTypeName")%></td>
                                                        <%--<td><%# Eval("ContactPersonName")%></td>
                                                        <td><%# Eval("ContactpersonMobile")%></td>--%>
                                                     
                                                        <td>
                                                            <asp:LinkButton ID="btnEditGrid" class="btn-sm btn-success btn-icon icon-pencil" runat="server"
                                                                Text="" OnCommand="btnEditGrid_Command" CommandArgument="Edit" CommandName='<%# Eval("MillCode")+","+Eval("UnitCode")%>'>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn-sm btn-danger btn-icon icon-trash" runat="server"
                                                                Text="" OnCommand="btnDeleteGrid_Command" CommandArgument="Delete" CommandName='<%# Eval("MillCode")+","+Eval("UnitCode")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this new user details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        
                                    </div>
                                    <!-- table End -->
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
                </div>
            </footer>
        </ContentTemplate>
    </asp:UpdatePanel>


    <script src=" https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src=" https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
</asp:Content>

