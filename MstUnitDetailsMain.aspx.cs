﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Security.Cryptography;

using Altius.BusinessAccessLayer.BALDataAccess;
public partial class MstUnitDetailsMain : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    bool ErrFLg = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        Session.Remove("MillCode");
        Session.Remove("UnitCode");

        Load_Data();

    }
    protected void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Session.Remove("MillCode");
        Session.Remove("UnitCode");
        Response.Redirect("MstUnitDetails.aspx");
    }

    protected void btnEditGrid_Command(object sender, CommandEventArgs e)
    {
        Session["MillCode"] = e.CommandName.ToString().Split(',').First();
        Session["UnitCode"] = e.CommandName.ToString().Split(',').Last();
        Response.Redirect("MstUnitDetails.aspx");
    }

    protected void btnDeleteGrid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + e.CommandName.ToString().Split(',').First() + "' and UnitCode='" + e.CommandName.ToString().Split(',').Last() +"'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }
}
