﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MstUserCreation.aspx.cs" Inherits="MstUserCreation" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
         
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/dataTables.bootstrap4.min.css" rel="stylesheet" />

  

   


    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.select2').select2();
                }
            });
        };
    </script>

    <asp:updatepanel runat="server" enableviewstate="true">
        <ContentTemplate>
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">User Creation </h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Master</a></li>
                            <li class="breadcrumb-item active" aria-current="page">User Creations</li>
                        </ol>
                    </nav>
                </div>
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="forms-sample">
                                    <div class="row">
                                        <div class="form-group col-lg-4">
                                            <label>User ID</label>
                                            <asp:TextBox ID="txtUserID" MaxLength="30" class="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtUserID" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                        </div>

                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Full Name</label>
                                                <asp:TextBox ID="txtUserFullName" class="form-control" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtUserFullName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->

                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>User Type</label>
                                                <asp:DropDownList ID="txtUserType" runat="server" class="js-example-basic-single select2" Style="width: 100%">
                                                    <asp:ListItem Value="1" Text="Admin"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="IF User"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="Normal User"></asp:ListItem>
                                                    <%--<asp:ListItem Value="4" Text="MD"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="Hospital"></asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>New Password</label>
                                                <asp:TextBox ID="txtNewPassword" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtNewPassword" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Conform Password</label>
                                                <asp:TextBox ID="txtConformPassword" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtConformPassword" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-2 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Location</label>
                                                <asp:DropDownList ID="ddlUnit" runat="server" class="js-example-basic-single select2" Style="width: 100%">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-2 -->
                                        <!-- begin col-2 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnUnitChange" Text="Load" class="btn btn-warning" OnClick="btnUnitChange_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-2 -->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                                <asp:Button runat="server" ID="btnCancel" Text="Cancel" class="btn btn-danger" OnClick="btnCancel_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->

                                    <!-- table start -->
                                    <div class="col-lg-12">
                                      
                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="display table">
                                                        <thead>
                                                            <tr>
                                                                <th>User ID</th>
                                                                <th>Name</th>
                                                                <th>Type</th>
                                                                <%--<th>Password</th>--%>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("UserCode")%></td>
                                                        <td><%# Eval("UserName")%></td>
                                                        <td><%# Eval("IsAdmin")%></td>
                                                        <%--<td><%# Eval("NewPassword")%></td>--%>
                                                        <td>
                                                            <asp:LinkButton ID="btnEditGrid" class="btn-sm btn-success btn-icon icon-pencil" runat="server"
                                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("UserCode")%>'>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn-sm btn-danger btn-icon icon-trash" runat="server"
                                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("UserCode")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this new user details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        
                                    </div>
                                    <!-- table End -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
                </div>
            </footer>
        </ContentTemplate>
    </asp:updatepanel>


</asp:Content>

