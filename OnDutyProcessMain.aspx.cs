﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class OnDutyProcessMain : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR | ON DUTY";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Menu_Commision"));
            //li.Attributes.Add("class", "has-sub active open");
        }
        ondutyDetails();
    }


    public void ondutyDetails()
    {
          
        DataTable dtDisplay = new DataTable();
        string Query = "select TransID,TokenNo,EmpName,ONDutyFromDate,ONDutyToDate,ONDutyReturnDate,TravelCharge from OnDuty_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
        dtDisplay = objdata.RptEmployeeMultipleDetails(Query);

        
        Repeater1.DataSource = dtDisplay;
        Repeater1.DataBind();
        
    }
    protected void GridEditEntryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();

        string Query = "Select *from OnDuty_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and TransID='" + e.CommandName.ToString() + "' And ONDutyReturnDate=''";
        DT = objdata.RptEmployeeMultipleDetails(Query);

        if (DT.Rows.Count != 0)
        {
            Session["TransID"] = e.CommandName.ToString();
            Response.Redirect("OnDutyProcessAdd.aspx");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('ON Duty Details get Completed. Cannot Edit.');", true);
        }
    }

    protected void lbtnAdd_Click(object sender, EventArgs e)
    {
        Session.Remove("TransID");
        Response.Redirect("OnDutyProcessAdd.aspx");
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();

        string Query = "Select *from OnDuty_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and TransID='" + e.CommandName.ToString() + "' And ONDutyReturnDate=''";
        DT = objdata.RptEmployeeMultipleDetails(Query);

        if (DT.Rows.Count != 0)
        {
            Query = "Delete from OnDuty_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and TransID='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(Query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('ON Duty Details get Deleted..');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('ON Duty Details get Completed. Cannot Delete.');", true);
        }
        ondutyDetails();
    }
}
