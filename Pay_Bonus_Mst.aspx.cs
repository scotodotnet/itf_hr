﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Pay_Bonus_Mst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    string SSQL = "";
    DataTable dt = new DataTable();
    String CurrentYear1;
    static int CurrentYear;
    string SessionPayroll;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_EmpType();
            ELYear_Load();
           // Load_Data();
        }
        Load_data1();
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Ramalinga_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }

    public void Load_EmpType()
    {
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpTypeCd"] = "0";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        //ddlEmployeeType_SelectedIndexChanged(sender, e);

    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_EmpType();
    }
    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Load_data1();
    }

    public void Load_data1()
    {
        Query = "Select BM.Ccode,BM.Lcode,BM.Category,(case BM.Category when '1' then 'STAFF' else 'LABOUR' end) as CategoryName,";
        Query = Query + "BM.EmployeeType,EM.EmpType,BM.BelowOneYear,BM.AboveOneYear,BM.Gross_Sal_Percent,";
        Query = Query + " BM.BelowDaysPercent,BM.AboveDaysPercent,BM.Min_Amount,BM.Bonus_Percent from[" + SessionPayroll + "].. MstBonusMaster BM ";
        Query = Query + " inner join MstEmployeeType EM on BM.EmployeeType=EM.EmpTypeCd";
        Query = Query + " where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        //And Category='" + ddlCategory.SelectedValue.ToString() + "' ";
        //Query = Query + " and EmployeeType='" + ddlEmployeeType.SelectedValue.ToString() + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        //if (dt.Rows.Count != 0)
        //{
        //    txtbelowoneYear.Text = dt.Rows[0]["BelowOneYear"].ToString();
        //    txtBelowDays.Text = dt.Rows[0]["BelowDaysPercent"].ToString();
        //    txtAboveDays.Text = dt.Rows[0]["AboveDaysPercent"].ToString();
        //    txtAboveOneYear.Text = dt.Rows[0]["AboveOneYear"].ToString();
        //    txtGrossSalPercent.Text = dt.Rows[0]["Gross_Sal_Percent"].ToString();
        //    txtMinAmount.Text = dt.Rows[0]["Min_Amount"].ToString();
        //    txtBonusPercent.Text = dt.Rows[0]["Bonus_Percent"].ToString();
        //}
    }
    protected void btnSave1_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool SaveFlag = false;

            if (ddlCategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if (ddlCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if (ddlEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }
            else if (ddlEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {

                SSQL = "Select * from [" + SessionPayroll + "]..MstBonusMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  ";
                SSQL = SSQL + " and Category='" + ddlCategory.SelectedValue.ToString() + "' And EmployeeType='" + ddlEmployeeType.SelectedValue.ToString() + "' ";
                SSQL = SSQL + " and BelowOneYear='" + txtbelowoneYear.Text + "' and AboveOneYear='" + txtAboveOneYear.Text + "'";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count != 0)
                {
                    SSQL = "Delete from [" + SessionPayroll + "]..MstBonusMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  ";
                    SSQL = SSQL + " and Category='" + ddlCategory.SelectedValue.ToString() + "' And EmployeeType='" + ddlEmployeeType.SelectedValue.ToString() + "' ";
                    SSQL = SSQL + " and BelowOneYear='" + txtbelowoneYear.Text + "' and AboveOneYear='" + txtAboveOneYear.Text + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                }


                SSQL = "Insert Into [" + SessionPayroll + "]..MstBonusMaster (Ccode,Lcode,Category,EmployeeType,BelowOneYear,";
                SSQL = SSQL + " AboveOneYear,Gross_Sal_Percent,Bonus_Percent,Type)";
                SSQL = SSQL + " values ('" + SessionCcode + "','" + SessionLcode + "','" + ddlCategory.SelectedValue.ToString() + "','" + ddlEmployeeType.SelectedValue.ToString() + "','" + txtbelowoneYear.Text + "',";
                SSQL = SSQL + "'" + txtAboveOneYear.Text + "','" + txtGrossSalPercent.Text + "','" + txtBonusPercent.Text + "','" + RdbType.SelectedValue + "') ";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SaveFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                clr1();
            }
        }
        catch (Exception ex)
        {
        }
    }
    private void clr1()
    {
        ddlCategory.SelectedValue = "0";
        ddlEmployeeType.SelectedValue = "0";
        txtbelowoneYear.Text = "0.00";
        txtAboveDays.Text = "0.00";
        txtBelowDays.Text = "0.00";
        txtAboveOneYear.Text = "0.00";
        txtGrossSalPercent.Text = "100.00";
        txtMinAmount.Text = "0.00";
        txtBonusPercent.Text = "0.00";

        RdbType.SelectedValue = "1";

        Load_data1();
    }
    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string[] CommArg = e.CommandArgument.ToString().Split(',');
        string[] CommName = e.CommandName.ToString().Split(',');

        string Category = CommName[0].ToString();
        string EmployeeType = CommName[1].ToString();

        string FromYear = CommArg[0].ToString();
        string ToYear = CommArg[1].ToString();

        SSQL = "Select * from [" + SessionPayroll + "]..MstBonusMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  ";
        SSQL = SSQL + " and Category='" + Category + "' And EmployeeType='" + EmployeeType + "' ";
        SSQL = SSQL + " and BelowOneYear='" + FromYear + "' and AboveOneYear='" + ToYear + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        if (dt.Rows.Count != 0)
        {
            ddlCategory.SelectedValue = dt.Rows[0]["Category"].ToString();
            Load_EmpType();
            ddlEmployeeType.SelectedValue = dt.Rows[0]["EmployeeType"].ToString();
            txtbelowoneYear.Text = dt.Rows[0]["BelowOneYear"].ToString();
            txtAboveOneYear.Text = dt.Rows[0]["AboveOneYear"].ToString();
            txtGrossSalPercent.Text = dt.Rows[0]["Gross_Sal_Percent"].ToString();
            txtBonusPercent.Text = dt.Rows[0]["Bonus_Percent"].ToString();
            RdbType.SelectedValue = dt.Rows[0]["Type"].ToString();
        }



    }
    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string[] CommArg = e.CommandArgument.ToString().Split(',');
        string[] CommName = e.CommandName.ToString().Split(',');

        string Category = CommName[0].ToString();
        string EmployeeType = CommName[1].ToString();

        string FromYear = CommArg[0].ToString();
        string ToYear = CommArg[1].ToString();

        SSQL = "Select * from [" + SessionPayroll + "]..MstBonusMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  ";
        SSQL = SSQL + " and Category='" + Category + "' And EmployeeType='" + EmployeeType + "' ";
        SSQL = SSQL + " and BelowOneYear='" + FromYear + "' and AboveOneYear='" + ToYear + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        if (dt.Rows.Count != 0)
        {
            SSQL = "delete from [" + SessionPayroll + "]..MstBonusMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  ";
            SSQL = SSQL + " and Category='" + Category + "' And EmployeeType='" + EmployeeType + "' ";
            SSQL = SSQL + " and BelowOneYear='" + FromYear + "' and AboveOneYear='" + ToYear + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);


            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully');", true);

        }

        Load_data1();
    }
    protected void btnClear1_Click(object sender, EventArgs e)
    {
        clr1();
    }
    private void ELYear_Load()
    {
        //Financial Year Add


        CurrentYear1 = DateTime.Now.Year.ToString();
        CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());

        txtELYear.Items.Add("----Select----");
        for (int i = 0; i < 11; i++)
        {

            string tt = CurrentYear1;
            txtELYear.Items.Add(tt.ToString());

            CurrentYear = CurrentYear - 1;
            string cy = Convert.ToString(CurrentYear);
            CurrentYear1 = cy;
        }
       
    }
    protected void txtELYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Q = new DataTable();
        query = "Select * from [" + SessionPayroll + "]..MstBasic_EL_Percentage where Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And Attn_Year='" + txtELYear.SelectedValue + "'";
        DT_Q = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Q.Rows.Count != 0)
        {
            txtEL_Percentage.Text = DT_Q.Rows[0]["EL_Percentage"].ToString();
        }
        else
        {
            txtEL_Percentage.Text = "0.00";
        }

    }

    protected void btnELPercentSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Q = new DataTable();
        query = "Select * from [" + SessionPayroll + "]..MstBasic_EL_Percentage where Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And Attn_Year='" + txtELYear.SelectedValue + "'";
        DT_Q = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Q.Rows.Count != 0)
        {
            query = "Update [" + SessionPayroll + "]..MstBasic_EL_Percentage set EL_Percentage='" + txtEL_Percentage.Text + "'";
            query = query + " where Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "'";
            query = query + " And Attn_Year='" + txtELYear.SelectedValue + "'";
            objdata.RptEmployeeMultipleDetails(query);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
        }
        else
        {
            query = "insert into[" + SessionPayroll + "].. MstBasic_EL_Percentage(Ccode,LCode,Attn_Year,EL_Percentage) Values(";
            query = query + " '" + SessionCcode + "','" + SessionLcode + "','" + txtELYear.SelectedValue + "',";
            query = query + " '" + txtEL_Percentage.Text + "')";
            objdata.RptEmployeeMultipleDetails(query);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
        }
    }

   
}
