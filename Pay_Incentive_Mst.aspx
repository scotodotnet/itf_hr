﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="Pay_Incentive_Mst.aspx.cs" Inherits="Pay_Incentive_Mst" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>


<!-- begin #content -->
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
	    <li class="active">Incentive Master</li>
	</ol>
	<h1 class="page-header">INCENTIVE MASTER</h1>
	<div class="row">
        <div class="col-md-12">
		    <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Incentive Master</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <h5>Days Incentive</h5>
                        <div class="row">
                        
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlcategory" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlcategory_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="STAFF">STAFF</asp:ListItem>
								 <asp:ListItem Value="LABOUR">LABOUR</asp:ListItem>
								</asp:DropDownList>
								
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="txtEmployeeType" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								
								</div>
                               </div>
                           <!-- end col-4 -->
                           
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Days of Month</label>
								    <asp:TextBox runat="server" ID="txtDaysOfMonth" class="form-control" Text="0" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            
                        </div>
                        <div class="row">
                        <div class="col-md-3">
								<div class="form-group">
								    <label>Minimum Days Worked</label>
								    <asp:TextBox runat="server" ID="txtMinDaysWorked" class="form-control" Text="0" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
								<div class="form-group">
								    <label>Incentive Amount</label>
								    <asp:TextBox runat="server" ID="txtIncent_Amount" class="form-control" Text="0.00" ></asp:TextBox>
								</div>
                            </div>
                            
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" />
						    	</div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-4"></div>
                            
                            
                            
                            
                              <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                
                                                <th>Emp Type</th>
                                                <th>MonthDays</th>
                                                <th>WorkerDays</th>   
                                                <th>WorkerAmt</th>
                                                
                                                
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>  
                                    
                                    <td><%# Eval("EmpType")%></td>
                                    <td><%# Eval("MonthDays")%></td>
                                        <td><%# Eval("WorkerDays")%></td>
                                        <td><%# Eval("WorkerAmt")%></td>
                                      
                                       
                                        <td>
                                                     <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%# Eval("EmpType")%>'
                                                                                                          CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Incentive details?');">
                                                    </asp:LinkButton>

                                                </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                            
                            <div class="col-md-4"></div>
                        </div>
                        
                        
                      <%-- <div class="panel">
                         
                        <h5>Hostel / Regular Basic Incentive Text</h5>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:RadioButtonList ID="txtRdpHos_Rg_Incent_Txt" runat="server" RepeatColumns="3" class="form-control" RepeatDirection="Horizontal">
                                        <asp:ListItem Value="1" Text="Attendance" style="padding:5px;"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Production" style="padding:5px;"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Punctuality" style="padding:5px;"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group">
								    <br />
									<asp:Button runat="server" id="BtnHos_Rg_Incen" Text="Save" class="btn btn-success" />
						    	 </div>
                            </div>
                        </div>
                       
                       
                         <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Department Incentive Amount</label>
								    <asp:TextBox runat="server" ID="txtDeptIncAmt" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                             <div class="col-md-4">
								<div class="form-group">
								    <br />
									<asp:Button runat="server" id="BtnDeptIncAmt" Text="Save" class="btn btn-success" />
						    	 </div>
                            </div>
                        </div>
                        </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</asp:Content>

