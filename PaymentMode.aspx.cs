﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;

public partial class PaymentMode : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlg = false;
    string GstAmount;
    string units;
    string Total;
    string pay;
    int Payment;
    string presentCount;
    string millcode;
    string desigAmount;
    DataTable DT_DAmount;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
      
        txtTotal.Enabled = false;
        txtGSTAmount.Enabled = false;
        txtTotalAmt.Enabled = false;
        txtRoundOff.Enabled = false;
        txtNetAmount.Enabled = false;
        txtDesignationAmount.Enabled = false;
        if (!IsPostBack)
        {
            btnCancel_Click(sender, e);
           
            Load_Mill();
            Load_Designation();
            if ((string)Session["BillCode"] != null)
            {
                btnSearch_Click(sender, e);
            }

        }
       
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

      
        SSQL = "";
        SSQL = "Select Distinct MD.MillName,(MM.MillCode +' - '+ MM.MillName) as MillCode,PM.BillCode,PM.Designation,PM.UnitCode,PM.TotalAmount,PM.Status,PM.FromDate,PM.ToDate,PM.PresentEmployees,PM.DesignationAmount,PM.MillCode as MillUnit,PM.Total,";
        SSQL = SSQL + "PM.GSTAmount,PM.Status,PM.Month,PM.GSTPercentage,PM.RoundOff,PM.NetAmount";
       SSQL = SSQL + " from Paymentmode_Mst PM inner join Employee_Mst EM on EM.MillCode=PM.MillCode inner join MstMill MM on MM.Millcode=PM.MillCode";

        SSQL = SSQL + " Inner join MstMill MD on MD.MillCode=PM.MillCode where PM.Ccode='" + SessionCcode + "' and PM.Lcode='" + SessionLcode + "' and BillCode='" + Session["BillCode"].ToString() + "'";

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt != null && dt.Rows.Count > 0)

        {
            pay = dt.Rows[0]["Status"].ToString();
            if (pay =="Paid")
            {
                Payment = 0;
            }
            else
            {
                Payment = 1;
            }
            btnSave.Text = "Update";

            units = dt.Rows[0]["MillUnit"].ToString();
            ddlMillCode.SelectedItem.Text = dt.Rows[0]["Millcode"].ToString();
            ddlDesignationName.Text = dt.Rows[0]["Designation"].ToString();
            //ddlCourseName.SelectedItem.Text = dt.Rows[0]["CourseName"].ToString();
           txtFromDate.Text = dt.Rows[0]["FromDate"].ToString();
            txtToDate.Text = dt.Rows[0]["ToDate"].ToString();
            ddlmonth.SelectedValue = dt.Rows[0]["Month"].ToString();
            txtPresentEmp.Text = dt.Rows[0]["PresentEmployees"].ToString();
            txtDesignationAmount.Text = dt.Rows[0]["DesignationAmount"].ToString();
            txtTotalAmt.Text = dt.Rows[0]["TotalAmount"].ToString();
            txtGSTAmount.Text = dt.Rows[0]["GSTAmount"].ToString();
            //ddlStatus.SelectedItem.Text = dt.Rows[0]["Status"].ToString();
            ddlStatus.SelectedIndex = Payment;
            txtTotal.Text = dt.Rows[0]["Total"].ToString();
            txtGstPercentage.Text = dt.Rows[0]["GSTPercentage"].ToString();
            txtRoundOff.Text = dt.Rows[0]["RoundOff"].ToString();
            txtNetAmount.Text = dt.Rows[0]["NetAmount"].ToString();
            ddlMillCode_SelectedIndexChanged(sender, e);
            ddlDesignationName.Enabled = false;
            ddlMillCode.Enabled =false ;
            ddlUnits.Enabled = false;
            txtFromDate.Enabled = false;
            txtToDate.Enabled = false;
            ddlmonth.Enabled = false;
            txtPresentEmp.Enabled = false;
            txtDesignationAmount.Enabled = false;
            txtTotalAmt.Enabled = false;
            txtGSTAmount.Enabled = false;
            txtGstPercentage.Enabled = false;

        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (ddlMillCode.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Mill Code not Found');", true);
            ErrFlg = true;
            return;
        }
   
        if (!ErrFlg)
        {
            if (btnSave.Text == "Save")
            {
                if (ddlMillCode.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose The Mill Code');", true);
                    ErrFlg = true;
                    return;
                }
                if (ddlUnits.Text == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose The Units');", true);
                    ErrFlg = true;
                    return;
                }
                if (ddlDesignationName.Text == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Choose The Designation Code');", true);
                    ErrFlg = true;
                    return;
                }
               

                SSQL = "";
                SSQL = "Select * from Paymentmode_Mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and Designation='" + ddlDesignationName.SelectedItem.Text + "'and TotalAmount='" + txtTotalAmt.Text + "'";

                DataTable Chk_Dt = new DataTable();
                Chk_Dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (Chk_Dt != null && Chk_Dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Program Details is Already Present');", true);
                    ErrFlg = true;
                    return;
                }
                
                if (!ErrFlg)
                {
                    SSQL = "";
                    SSQL = "insert into Paymentmode_Mst (Ccode,Lcode,MillCode,UnitCode,Designation,FromDate,ToDate,DesignationAmount,TotalAmount, ";
                    SSQL = SSQL + " GSTAmount,Status,CreatedDate,PresentEmployees,Month,GSTpercentage,Total,RoundOff,NetAmount)";
                    SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + ddlMillCode.SelectedValue + "','" + ddlUnits.SelectedItem.Text + "',";
                    SSQL = SSQL + "'" + ddlDesignationName.SelectedItem.Text + "','" + txtFromDate.Text + "','" + txtToDate.Text + "','" + txtDesignationAmount.Text + "', '" + txtTotalAmt.Text + "',";
                    SSQL = SSQL + "'" + txtGSTAmount.Text+ "','" + ddlStatus.SelectedItem.Text +"',Getdate(),'"+ txtPresentEmp.Text +"','" +ddlmonth.SelectedItem.Text +"',";
                    SSQL = SSQL + "'" + txtGstPercentage.Text + "%','" + txtTotal.Text + "','" + txtRoundOff.Text + "','" + txtNetAmount.Text + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Payment Added Succesfully');", true);
                    btnCancel_Click(sender, e);
                    Response.Redirect("Paymentmode_Main.aspx");
                }
            }
            else
            {
                SSQL = "";
                SSQL = "Update Paymentmode_Mst set Status= '" + ddlStatus.SelectedItem.Text + "' where BillCode='" + Session["BillCode"].ToString() + "'";
                 objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Payment Updated Succesfully');", true);
                btnCancel_Click(sender, e);
                Response.Redirect("Paymentmode_Main.aspx");
            }
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

           ddlUnits.ClearSelection();
           ddlDesignationName.ClearSelection();
           ddlMillCode.ClearSelection();
           ddlmonth.ClearSelection();
           ddlStatus.ClearSelection();
        
          txtFromDate.Text = "";
          txtToDate.Text = "";
          txtPresentEmp.Text = "";
          txtDesignationAmount.Text = "";
          txtTotalAmt.Text = "";
          txtGstPercentage.Text = "";
          btnSave.Text = "Save";
          txtGSTAmount.Text = "";
          txtRoundOff.Text = "";
          txtTotal.Text = "";
          txtNetAmount.Text = "";

    }
    protected void ddlMillCode_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (btnSave.Text == "Update")
        {
            Load_unit(units);
        }
        else
        {
            Load_unit(ddlMillCode.SelectedValue);
        }
    }

    protected void Load_Mill()
    {
        SSQL = "";
        SSQL = "Select (MillCode +' - '+ MillName) as MillName,MillCode from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlMillCode.DataSource = dt;
        ddlMillCode.DataTextField = "MillName";
        ddlMillCode.DataValueField = "MillCode";
        millcode = dt.Rows[0]["Millcode"].ToString();
        ddlMillCode.DataBind();
        ddlMillCode.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void Load_Designation()
    {
        SSQL = "";
        SSQL = "Select Distinct DesignName,DesignCode from  Designation_Mst";
        ddlDesignationName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        //  ddlDesignationName.DataValueField = "DesignCode";
        ddlDesignationName.DataTextField = "DesignName";
        ddlDesignationName.DataBind();
        ddlDesignationName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

    }
    protected void Load_unit(string MillCode)
    {
        millcode = MillCode;
        SSQL = "";
        SSQL = "Select (UnitName+'-'+UnitTypeName) as UnitName,UnitCode from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + MillCode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count <= 0)
        {
            SSQL = "";
            SSQL = "select Units from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Millcode='" + MillCode + "'";
            DataTable Dt_get = new DataTable();
            Dt_get = objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable Unit_dt = new DataTable();

            Unit_dt.Columns.Add("UnitCode");
            Unit_dt.Columns.Add("UnitName");

            int Unitcode = 0;
            if (Dt_get != null && Dt_get.Rows.Count > 0)
            {

                Unitcode = Convert.ToInt32(Dt_get.Rows[0]["Units"]);
                DataRow dr;

                for (int i = 1; i <= Unitcode; i++)
                {
                    dr = Unit_dt.NewRow();
                    dr["UnitCode"] = "UNIT " + i;
                    dr["UnitName"] = "UNIT " + i;
                    Unit_dt.Rows.Add(dr);
                }

                if (Unit_dt.Rows.Count > 0)
                {
                    ddlUnits.DataSource = Unit_dt;
                    ddlUnits.DataTextField = "UnitName";
                    ddlUnits.DataValueField = "UnitCode";
                    ddlUnits.DataBind();
                    ddlUnits.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
                }
            }
        }
        else
        {
            ddlUnits.DataSource = dt;
            ddlUnits.DataTextField = "UnitName";
            ddlUnits.DataValueField = "UnitCode";
            ddlUnits.DataBind();
            //  ddlUnits.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }


    protected void txtDesignationAmount_TextChanged(object sender, EventArgs e)
    {
       
        Total = (Convert.ToDecimal(txtPresentEmp.Text) * Convert.ToDecimal(txtDesignationAmount.Text)).ToString();
        txtTotal.Text = Total;
        
    }





protected void txtGstPercentage_TextChanged(object sender, EventArgs e)
    {
        string tot;
        string round;
        string rounded;
        string net;
        GstAmount = (Convert.ToDecimal(txtTotal.Text) * Convert.ToDecimal(txtGstPercentage.Text) / 100).ToString();

        txtGSTAmount.Text = GstAmount;
       // lopdays = (Convert.ToDecimal(Fixed_Work_Days) - Convert.ToDecimal(Staff_Work_Days)).ToString();
        tot = (Convert.ToDecimal(txtTotal.Text) + Convert.ToDecimal(txtGSTAmount.Text)).ToString();
        txtTotalAmt.Text = tot;
    
        round = (Math.Round(Convert.ToDecimal(tot), 0, MidpointRounding.AwayFromZero)).ToString();
        rounded = (Convert.ToDecimal(round) - Convert.ToDecimal(tot)).ToString();
        txtRoundOff.Text = rounded;
        round = (Math.Round(Convert.ToDecimal(tot), 0, MidpointRounding.AwayFromZero)).ToString();
        txtNetAmount.Text = round;
    }

    protected void txtToDate_TextChanged(object sender, EventArgs e)
    {
        DataTable present_DT = new DataTable();

        SSQL = "";
        SSQL = " Select Count(TimeIN) as PresentCount from  Employee_Mst EM Inner join LogTime_IN LT on EM.MachineID_Encrypt =LT.MachineID ";
        SSQL = SSQL +" where LT.Compcode='" + SessionCcode + "' and LT.Loccode='" + SessionLcode + "' and";
        SSQL = SSQL + " Convert(datetime,LT.TimeIN,103)>=Convert(datetime,'" + txtFromDate.Text + "',103) and Convert(datetime,LT.TimeIN,103)<=Convert(datetime,'" + txtToDate.Text + "',103)";
        SSQL = SSQL + " and EM.MillCode= '" + millcode + "'";
        present_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (present_DT != null && present_DT.Rows.Count > 0)
        {
            presentCount = present_DT.Rows[0]["PresentCount"].ToString();
        }
        txtPresentEmp.Text = presentCount;
    }

    protected void ddlDesignationName_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select Amount From Course_Costing where Designation= '" + ddlDesignationName.SelectedValue + "'";
         DT_DAmount = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_DAmount != null && DT_DAmount.Rows.Count > 0)
        {
            desigAmount = DT_DAmount.Rows[0]["Amount"].ToString();
        }
        txtDesignationAmount.Text = desigAmount;
    } 
}