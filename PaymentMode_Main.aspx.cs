﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;


public partial class PaymentMode_Main : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlg = false;
    string Coursecode;
    string status;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        Session.Remove("BillCode");
        Load_Data();
    }
    protected void Load_Data()
    {
        if (rbtnIsActive.SelectedValue == "0" || rbtnIsActive.SelectedValue == "1" || rbtnIsActive.SelectedValue == "2")
        {
            if (rbtnIsActive.SelectedValue == "0")
            {
                status = "All";
            }
            else if (rbtnIsActive.SelectedValue == "1")
            {
                status = "Paid";
            }
            else if (rbtnIsActive.SelectedValue == "2")
            {
                status = "Unpaid";
            }
            if (status == "Paid")
            {
                SSQL = "";
                SSQL = "Select Distinct MD.MillName,PM.BillCode,PM.Designation,PM.UnitCode,PM.NetAmount,PM.Status from Paymentmode_Mst PM inner join Employee_Mst EM on EM.MillCode=PM.MillCode";
                SSQL = SSQL + " Inner join MstMill MD on MD.MillCode=PM.MillCode where PM.Ccode='" + SessionCcode + "' and PM.Lcode='" + SessionLcode + "' and PM.Status='Paid'";

            }
            if (status == "Unpaid")
            {
                SSQL = "";
                SSQL = "Select Distinct MD.MillName,PM.BillCode,PM.Designation,PM.UnitCode,PM.NetAmount,PM.Status from Paymentmode_Mst PM inner join Employee_Mst EM on EM.MillCode=PM.MillCode";
                SSQL = SSQL + " Inner join MstMill MD on MD.MillCode=PM.MillCode where PM.Ccode='" + SessionCcode + "' and PM.Lcode='" + SessionLcode + "'and PM.Status='UnPaid'";

            }
            if (status == "All")
            {
                SSQL = "";
                SSQL = "Select Distinct MD.MillName,PM.BillCode,PM.Designation,PM.UnitCode,PM.NetAmount,PM.Status from Paymentmode_Mst PM inner join Employee_Mst EM on EM.MillCode=PM.MillCode";
                SSQL = SSQL + " Inner join MstMill MD on MD.MillCode=PM.MillCode where PM.Ccode='" + SessionCcode + "' and PM.Lcode='" + SessionLcode + "'";
            }
            Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
            Repeater1.DataBind();
        }
    }
    protected void rbtnIsActive_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data();

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

        Response.Redirect("PaymentMode.aspx");
    }
    protected void btnEditIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        Session["BillCode"] = e.CommandName.ToString();
        Response.Redirect("PaymentMode.aspx");

    }
}