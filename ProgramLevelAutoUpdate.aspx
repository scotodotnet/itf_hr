﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProgramLevelAutoUpdate.aspx.cs" Inherits="ProgramLevelAutoUpdate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  <link href="assets/plugins/ModalPopup/CSS/Popup.css" rel="stylesheet" type="text/css" />
    <script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
    <script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



    <script type="text/javascript">
    function ProgressBarShow() {
       /// alert('BARSHOW')
           $('#<%=LblNotify.ClientID%>').html('Please Wait...');
        
    }


</script>

<script type="text/javascript">
    function ProgressBarHide() {
        alert('hide');
        //$('#Download_loader').hide();
         $('#<%=LblNotify.ClientID%>').html(' ');
    }
</script>


   

<script type="text/javascript">
    $(document).ready(function() {
        //    bindButton();
     //   alert('hi');
 $('#<%=btnSave.ClientID%>').on('click', function () {
        alert("Bind In1");
            $('#<%=LblNotify.ClientID%>').html('Please Wait...');
        });
    });
    function bindButton() {
    $('#<%=btnSave.ClientID%>').on('click', function () {
        alert("Bind In2");
            $('#<%=LblNotify.ClientID%>').html('Please Wait...');
        });
    }
</script>

<script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function f() {
        $('#<%=LblNotify.ClientID%>').html("DOWNLOAD PROCESSING....");
    }
        </script>    





    <asp:UpdatePanel runat="server" EnableViewState="true">
        <ContentTemplate>
            <br>
          
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Program Level Master</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Master</a></li>
                            <li aria-current="page" class="breadcrumb-item active">Program Level Master</li>
                        </ol>
                    </nav>
                </div>
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="forms-sample">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="exampleInputName">From Date</label>
                               <asp:TextBox runat="server" ID="txtFrmdate" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                    </div>
                                   
                                             <div class="form-group col-md-4">
                                            <label for="exampleInputName">To Date</label>
                                            <asp:TextBox runat="server" ID="txtTodate" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                            
                                        </div>
                          <div class="col-md-3">
                                            <div class="form-group">
                                               <label>Program Name</label><span class="asterisk_input"></span>
                                                <asp:DropDownList ID="ddlCourseName" CssClass="form-control" Style="width: 100%" AutoPostBack="true" onselectedindexchanged="ddlCourseName_SelectedIndexChanged" runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlCourseName" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        </div>
                                 </div>
                        </div>
                             <div class="row">
                                 <div class="form-group col-md-4">
                                 <asp:Label ID="LblNotify" runat="server" Visible="true" Text=""></asp:Label>

                                        </div>
                                 </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                        </div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <%--<asp:Button ID="btnSave" runat="server" class="btn btn-success" OnClick="btnSave_Click" OnClientClick="javascript:ProgressBarShow()" Text="Convert" ValidationGroup="Validate_Field" />--%>
                                                 <asp:Button ID="btnSave" runat="server" class="btn btn-success" OnClick="btnSave_Click" OnClientClick="javascript: return ProgressBarShow();" Text="Convert" ValidationGroup="Validate_Field" />
                                              <asp:Button ID="btnCancel" runat="server" class="btn btn-danger" OnClick="btnCancel_Click" Text="Cancel" />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                            <!-- table start -->
                                    <div class="col-lg-12">
                                      
                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="display table">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No</th>
                                                                <th>Program Code</th>
                                                                 <th>Program Name</th>
                                                                 <th>Program From</th>
                                                                 <th>Program To</th>
                                                                <th>From Date</th>
                                                                <th>To Date</th>
                                                                <%--<th>Mode</th>--%>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("CourseCode")%></td>
                                                        <td><%# Eval("CourseName")%></td>
                                                        <td><%# Eval("CourseLevelFROM")%></td>
                                                        <td><%# Eval("CourseLevelTO")%></td>
                                                        <td><%# Eval("FromDate")%></td>

                                                        <td><%# Eval("ToDate")%></td>
                                                        <%--<td><%# Eval("NewPassword")%></td>--%>
                                                       <%-- <td>
                                                            <asp:LinkButton ID="btnEditGrid" class="btn-sm btn-success btn-icon icon-pencil" runat="server"
                                                                Text="" OnCommand="btnEditGrid_Command" CommandArgument="Edit" CommandName='<%# Eval("MillTypeCode")%>'>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn-sm btn-danger btn-icon icon-trash" runat="server"
                                                                Text="" OnCommand="btnDeleteGrid_Command" CommandArgument="Delete" CommandName='<%# Eval("MillTypeCode")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this new user details?');">
                                                            </asp:LinkButton>
                                                        </td>--%>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        
                                    </div>
                                    <!-- table End -->
                                  
                                </div>
                            </div>
                    <%--    </div>
                    </div>
                </div--%>
                <!-- content-wrapper ends -->
                <!-- partial:../../partials/_footer.html -->
                <footer class="footer">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span> <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted &amp; made with <i class="icon-heart text-danger"></i></span>
                    </div>
                </footer>
            </div>
            </br>
        </ContentTemplate>
    </asp:UpdatePanel>

  <script>
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
            
        });
        
    });
</script>
      <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable();
            $('.js-example-basic-single').select2();
        });

    </script>
     <script type="text/javascript">
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        $('#example').dataTable();
                        $('.js-example-basic-single').select2();
                        $('.datepicker').datepicker({
                            format: "dd/mm/yyyy",
                            autoclose: true
                        });
                    }
                });
            };
        </script>
</asp:Content>

