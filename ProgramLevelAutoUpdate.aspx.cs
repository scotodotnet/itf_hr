﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;


public partial class ProgramLevelAutoUpdate : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();


    string SSQL;
    string CurrentDate;
    DateTime date1 = new DateTime();
    DateTime date2 = new DateTime();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
   
    bool ErrFlg = false;
    string CourseName;
    string Coursecode;
    string units;
    string EmpNo;
    string CourseLevel;
    int level;
    string levelupdate;
    string MstLvl;
    string ChkDate;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        LblNotify.Text = "";

        if (!IsPostBack)
        {
            Load_update();
            btnCancel_Click(sender, e);
            Load_Course();
            


        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        //btnSave.Enabled = false;

        try
        {
         
            if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
                Load_update();
                LoadBtn();
            }
            if (txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter To Date');", true);
                Load_update();
                LoadBtn();
            }
            if ((ddlCourseName.SelectedItem.Text == "") && (ddlCourseName.SelectedItem.Text == null))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Program Name');", true);
                Load_update();
                LoadBtn();
            }

            date1 = Convert.ToDateTime(txtFrmdate.Text);
            SSQL = "";
            SSQL = "select max(ToDate) as Last from MstProgramlevel where  CourseCode='" + ddlCourseName.SelectedValue + "'";
            DataTable DT_Chk = new DataTable();
            DT_Chk = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Chk.Rows.Count != 0)
            {
                ChkDate = DT_Chk.Rows[0]["Last"].ToString().Trim();
            }
            if ((ChkDate == ""))
            {
                ChkDate = "01/01/1900";
            }
            date2 = Convert.ToDateTime(ChkDate);

            if (date2 > date1)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Valid Date');", true);
                ErrFlg = true;
                Load_update();
                LoadBtn();
                return;


            }






            //SSQL = "";
            ////   SSQL = "Select * from MstProgramlevel where CourseCode='" + ddlCourseName.SelectedValue + "' and FromDate='" + txtFrmdate.Text + "'";
            //SSQL = "Select * from MstProgramlevel where CourseCode='" + ddlCourseName.SelectedValue + "' and FromDate >='" + txtFrmdate.Text + "'";
            //SSQL = SSQL + " and FromDate<='" + txtFrmdate.Text + "'";

            //LblNotify.Text = "UPDATING IN PROGRESS!!!";
            ////SSQL = SSQL + " and ToDate='" + txtTodate.Text + "'";
            //DataTable DT_Pro = new DataTable();
            //DT_Pro = objdata.RptEmployeeMultipleDetails(SSQL);
            //if (DT_Pro.Rows.Count != 0)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert(' Program Level Already updated Check The From Date And ToDate and Program');", true);
            //    ErrFlg = true;
            //    Load_update();

            //    return;

            //}

            else
            {

                ddlCourseName_SelectedIndexChanged(this, EventArgs.Empty);
                date1 = Convert.ToDateTime(txtFrmdate.Text);
                date2 = Convert.ToDateTime(txtTodate.Text);
                if (date1 > date2)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Valid Date');", true);
                    ErrFlg = true;
                    Load_update();
                    LoadBtn();
                    return;
                }
                SSQL = "";
                SSQL = "Select Getdate() as CurrentDate";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt != null && dt.Rows.Count > 0)
                {
                    CurrentDate = dt.Rows[0]["CurrentDate"].ToString().Trim();
                    date1 = Convert.ToDateTime(CurrentDate);
                    date2 = Convert.ToDateTime(txtTodate.Text);
                    if (date1 < date2)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Valid Date');", true);
                        ErrFlg = true;
                        LblNotify.Visible = false;
                        Load_update();
                        LoadBtn();
                        return;

                    }
                }
                if (!ErrFlg)
                {

                    SSQL = "";
                    SSQL = "Select * from Employee_Mst where CourseCode='" + Coursecode + "'";
                    DataTable DT_Emp = new DataTable();
                    DT_Emp = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DT_Emp != null && DT_Emp.Rows.Count > 0)
                    {
                        for (int i = 0; i < DT_Emp.Rows.Count; i++)
                        {
                            EmpNo = DT_Emp.Rows[i]["EmpNo"].ToString();

                            SSQL = "";
                            SSQL = "select * from Employee_Mst where EmpNo='" + EmpNo + "'";
                            DataTable DT_EmpNo = new DataTable();
                            DT_EmpNo = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (DT_EmpNo.Rows.Count > 0)
                            {
                                CourseLevel = DT_EmpNo.Rows[0]["CourseLevel"].ToString();
                            }
                            string[] Test = CourseLevel.Split(' ');
                            string S1 = Test[0].ToString();
                            string S2 = Test[1].ToString();

                            int Courselevel_Int = Convert.ToInt32(S2);



                            level = Courselevel_Int + 1;

                            SSQL = "";
                            SSQL = "select * from  MstCourse where CourseCode='" + Coursecode + "'";
                            DataTable DT_MstCourse = new DataTable();
                            DT_MstCourse = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (DT_MstCourse.Rows.Count > 0)
                            {
                                MstLvl = DT_MstCourse.Rows[0]["Level"].ToString();
                            }
                            if (Convert.ToInt32(MstLvl) >= level)
                            {
                                levelupdate = "MODULE " + level;

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Check Course Level');", true);
                                ErrFlg = true;
                                LblNotify.Visible = false;
                                Load_update();
                                LoadBtn();
                                return;
                               

                            }
                            if (!ErrFlg)
                            {
                                SSQL = "";
                                SSQL = "Update Employee_Mst Set CourseLevel='" + levelupdate + "' where EmpNo='" + EmpNo + "'";
                                objdata.RptEmployeeMultipleDetails(SSQL);
                                LblNotify.Text = "";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Program Level Updated...');", true);
                                Load_update();
                                LoadBtn();
                            }
                        }
                        insertData();
                        Load_Course();
                        Load_update();

                        txtTodate.Text = "";
                        txtFrmdate.Text = "";

                        if (!ErrFlg)
                        {

                            //LblNotify.Text = "DOWNLOAD COMPLETED....";
                            ////ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                            //ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                            //ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "SaveMsgAlert('UPDATE COMPLETED....');", true);
                            //LoadBtn();
                            ////ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('DOWNLOAD COMPLETED....');", true);
                            //Load_update();
                            //ErrFlg = true;
                        }


                    }
                }
          
        }
        }
        catch (Exception EX)
        {

        }
                   

    }




    protected void  insertData()
    {
        if (!ErrFlg)
        {


            date1 = Convert.ToDateTime(txtFrmdate.Text);
            date2 = Convert.ToDateTime(txtTodate.Text);
           
                SSQL = "";
         //   SSQL = "Select * from MstProgramlevel where CourseCode='" + ddlCourseName.SelectedValue + "' and FromDate='" + txtFrmdate.Text + "'";
            SSQL = "Select * from MstProgramlevel where CourseCode='" + ddlCourseName.SelectedValue + "' and FromDate >=Convert(datetime,'" + txtFrmdate.Text + "',103)";
            SSQL = SSQL + " and ToDate<=Convert(datetime,'" + txtFrmdate.Text + "',103) and CourseCode ='" + ddlCourseName.SelectedValue +"'";


            //SSQL = SSQL + " and ToDate='" + txtTodate.Text + "'";
            DataTable DT_Pro = new DataTable();
            DT_Pro = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Pro.Rows.Count != 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert(' Program Level Already updated Check The From Date And ToDate and Program');", true);
                ErrFlg = true;
                Load_update();
                LoadBtn();
                return;
            }
            else
            {
                SSQL = "";
                SSQL = "insert Into MstProgramlevel(Ccode,Lcode,CourseCode,CourseName,LastUpdate,FromDate,ToDate";
                SSQL = SSQL + ",CourseLevelFROM,CourseLevelTo)";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + Coursecode + "','" + CourseName + "',";
                SSQL = SSQL + " GetDate(),Convert(datetime,'" + txtFrmdate.Text + "',103),Convert(datetime,'" + txtTodate.Text + "',103),";
                SSQL = SSQL + "'" + CourseLevel + "','" + levelupdate + "')";
               objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Program Level Updated...');", true);

                Response.Redirect("ProgramLevelAutoUpdate.aspx");
            }

        }
    }
    protected void Load_Course()
    {
        SSQL = "";
        SSQL = "Select * from MstCourse where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        ddlCourseName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlCourseName.DataTextField = "CourseName";
        ddlCourseName.DataValueField = "CourseCode";
        ddlCourseName.DataBind();
        ddlCourseName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    { 
        Load_Course();
        txtTodate.Text = "";
        txtFrmdate.Text = "";
        Load_update();

    }

    protected void ddlCourseName_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "select * from MstCourse where CourseCode='" + ddlCourseName.SelectedIndex + "'";
        DataTable DT_course = new DataTable();
        DT_course = objdata.RptEmployeeMultipleDetails(SSQL);

        if(DT_course.Rows.Count !=0)
        {
            Coursecode = DT_course.Rows[0]["CourseCode"].ToString();
            CourseName = DT_course.Rows[0]["CourseName"].ToString();
        }
        Load_update();

    }
    // private void LoadLbl()
    //{
    //    LblNotify.Text = "UPDATING IN PROGRESS";
    //}

    private void Load_update()
    {
        SSQL = "";
        SSQL = "Select CONVERT(varchar(10),FromDate,103) as FromDate,CONVERT(varchar(10),ToDate,103) as ToDate,* from MstProgramlevel";
        DataTable DT_Rptr;
        DT_Rptr = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = DT_Rptr;
        Repeater1.DataBind();

    }
    public void refresh1()
    {
        Response.Redirect("ProgramLevelAutoUpdate.aspx");
    }

    public void LoadBtn()
    {
      //  btnSave.Enabled = true;
    }
}