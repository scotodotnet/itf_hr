﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Drawing;

public partial class ReelingWorkloadEntry : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    AttenanceClass objatt = new AttenanceClass();
    string SessionCcode;
    string SessionLcode;
    string WagesType;
    string SessionUserType;
    string SessionEpay;
    string SessionUserID;
    string SessionUserName;
    string Query = "";

    string SSQL = "";

    string TempNFH = "0";
    string TempCL = "0";
    string TempThreeSide = "0";
    string TempFull = "0";
    string TempHome = "0";
    string TempOTNew = "";
    string TempWH_Work_Days = "0";
    string TempFixed_Days = "0";
    string TempNFH_Work_Days = "0";
    string TempNFH_Work_Manual = "0";
    string TempNFH_Work_Station = "0";
    string TempNFH_Count_Days = "0";
    string TempLBH_Count_Days = "0";
    bool ErrFlg = false;

    DataTable AutoDT = new DataTable();

    SqlConnection con;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];

    System.Web.UI.WebControls.DataGrid grid =
                         new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserID = ss;
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

        }
    }
    public void Load_TwoDates()
    {
        if (ddlMonths.SelectedValue != "-Select-" && ddlfinance.SelectedValue != "-Select-")
        {

            decimal Month_Total_days = 0;
            string Month_Last = "";
            string Year_Last = "0";
            string Temp_Years = "";
            string[] Years;
            string FromDate = "";
            string ToDate = "";

            Temp_Years = ddlfinance.SelectedItem.Text;
            Years = Temp_Years.Split('-');
            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
            {
                Month_Total_days = 31;
            }
            else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
            {
                Month_Total_days = 30;
            }

            else if (ddlMonths.SelectedValue == "February")
            {
                int yrs = (Convert.ToInt32(Years[0]) + 1);
                if ((yrs % 4) == 0)
                {
                    Month_Total_days = 29;
                }
                else
                {
                    Month_Total_days = 28;
                }
            }
            switch (ddlMonths.SelectedItem.Text)
            {
                case "January":
                    Month_Last = "01";
                    break;
                case "February":
                    Month_Last = "02";
                    break;
                case "March":
                    Month_Last = "03";
                    break;
                case "April":
                    Month_Last = "04";
                    break;
                case "May":
                    Month_Last = "05";
                    break;
                case "June":
                    Month_Last = "06";
                    break;
                case "July":
                    Month_Last = "07";
                    break;
                case "August":
                    Month_Last = "08";
                    break;
                case "September":
                    Month_Last = "09";
                    break;
                case "October":
                    Month_Last = "10";
                    break;
                case "November":
                    Month_Last = "11";
                    break;
                case "December":
                    Month_Last = "12";
                    break;
                default:
                    break;
            }

            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "February") || (ddlMonths.SelectedValue == "March"))
            {
                Year_Last = Years[1];
            }
            else
            {
                Year_Last = Years[0];
            }
            FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
            ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

            txtFrom.Text = FromDate.ToString();
            txtTo.Text = ToDate.ToString();

        }
        else
        {
            txtFrom.Text = "";
            txtTo.Text = "";
        }


    }
    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void ddlfinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {

            if (ddlMonths.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlg = true;
            }
            //else if ((txtdays.Text.Trim() == "") || (txtdays.Text.Trim() == null))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Working days in a Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            //else if (Convert.ToInt32(txtdays.Text) == 0)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days Properly.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            else if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlg = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlg = true;
            }

            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

            }
            if (!ErrFlg)
            {

                if (FileUpload.HasFile)
                {
                    FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

                }


                if (!ErrFlg)
                {
                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();
                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();
                        }

                        for (int ck = 0; ck < dt.Rows.Count; ck++)
                        {
                            for (int j = 4; j < dt.Columns.Count; j++)
                                if (dt.Rows[ck][j].ToString() == string.Empty || dt.Rows[ck][j].ToString() == "" || dt.Rows[ck][j].ToString() == null)
                                {
                                    dt.Rows[ck][j] = "0";
                                    dt.AcceptChanges();
                                }
                        }


                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string DeptCode = dt.Rows[i][0].ToString();
                            string EmpNo = dt.Rows[i][1].ToString();
                            string EmpName = dt.Rows[i][2].ToString();
                            string NoOfdays = dt.Rows[i][3].ToString();
                            string ExistingCode = "";
                            decimal BaseSalary = 0;
                            decimal Total_DayPay = 0;
                            decimal DaySalaryWithOneDay = 0;
                            decimal Total_WorkLoadPay = 0;
                            decimal Sum_WorkloadPay = 0;
                            decimal Sum_WorkloadOTPay = 0;
                            decimal Total_OTWorkloadPay = 0;
                            decimal PriceRate = 0;
                            decimal OTPriceRate = 0;
                            decimal Deduc1 = 0;
                            decimal Dedu2 = 0;
                            decimal All1 = 0;
                            decimal All2 = 0;
                            decimal OneDayPay = 0;
                            decimal WorkLoad = 0;
                            string Workload_Name = "";
                            string PFEligible = "";
                            string PFS = "0";
                            string EmployeerPFOne = "0";
                            string EmployeerPFTwo = "0";
                            decimal ToTalAllowance = 0;
                            decimal TotalDeduction = 0;
                            decimal NetDayPay = 0;
                            decimal TotalELPay = 0;
                            decimal OtherGrassWages = 0;
                            decimal NetAmt = 0;
                            decimal OtherNetWages = 0;
                            decimal PayTotal = 0;

                            SSQL = "";
                            SSQL = "Select ExistingCode,BaseSalary,OT_Salary,Eligible_PF from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " and MachineID='" + EmpNo + "'";
                            DataTable EmpDt = new DataTable();
                            EmpDt = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (EmpDt != null && EmpDt.Rows.Count > 0)
                            {
                                ExistingCode = EmpDt.Rows[0]["ExistingCode"].ToString();
                                BaseSalary = Convert.ToDecimal(EmpDt.Rows[0]["BaseSalary"].ToString());
                                OneDayPay = Convert.ToDecimal(EmpDt.Rows[0]["OT_Salary"].ToString());
                                PFEligible = EmpDt.Rows[0]["Eligible_PF"].ToString();
                            }

                            //Day Salary Calculation
                            Total_DayPay = Convert.ToDecimal(NoOfdays) * BaseSalary;
                            Total_DayPay = Math.Round(Total_DayPay, 2, MidpointRounding.AwayFromZero);

                            DaySalaryWithOneDay = Convert.ToDecimal(NoOfdays) * OneDayPay;
                            DaySalaryWithOneDay = Math.Round(DaySalaryWithOneDay, 2, MidpointRounding.AwayFromZero);

                            for (int j = 4; j < dt.Columns.Count; j++)
                            {
                                if (dt.Rows[i][j].ToString() != "")
                                {
                                    Workload_Name = dt.Columns[j].ColumnName.ToString().Replace("'", "");
                                    Workload_Name= dt.Columns[j].ColumnName.ToString().Replace("OT", "");
                                    WorkLoad = Convert.ToDecimal(dt.Rows[i][j].ToString());
                                }

                                if (Workload_Name.ToString() == dt.Columns[j].ColumnName.ToString()&& Workload_Name.ToString()!="Al1" && Workload_Name.ToString() != "Al2" && Workload_Name.ToString() != "Ded1" && Workload_Name.ToString() != "Ded2")
                                {
                                    SSQL = "";
                                    SSQL = "Select isnull(PriceRate,'0') as PiceRate from [" + SessionEpay + "]..MstReelingWagesSlab";
                                    SSQL = SSQL + " where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FinYearVal='" + ddlfinance.SelectedValue + "'";
                                    SSQL = SSQL + " and Workload='" + Workload_Name.ToString().Replace("'","") + "'";
                                    DataTable Workload_DT = new DataTable();
                                    Workload_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                                    if (Workload_DT != null && Workload_DT.Rows.Count > 0)
                                    {
                                        PriceRate = Convert.ToDecimal(Workload_DT.Rows[0]["PiceRate"]);
                                    }

                                    Total_WorkLoadPay = WorkLoad * PriceRate;
                                    Total_WorkLoadPay = Math.Round(Total_WorkLoadPay, 2, MidpointRounding.AwayFromZero);

                                    Sum_WorkloadPay = Sum_WorkloadPay + Total_WorkLoadPay;

                                }
                                else if (Workload_Name.ToString() == dt.Columns[j].ColumnName.ToString().Replace("OT", "" )&& Workload_Name.ToString() != "Al1" && Workload_Name.ToString() != "Al2" && Workload_Name.ToString() != "Ded1" && Workload_Name.ToString() != "Ded2")
                                {
                                    SSQL = "";
                                    SSQL = "Select isnull(OTPriceRate,'0') as OTPiceRate from [" + SessionEpay + "]..MstReelingWagesSlab";
                                    SSQL = SSQL + " where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FinYearVal='" + ddlfinance.SelectedValue + "'";
                                    SSQL = SSQL + " and Workload='" + Workload_Name.ToString().Replace("OT", "") + "'";
                                    DataTable Workload_DT = new DataTable();
                                    Workload_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                                    if (Workload_DT != null && Workload_DT.Rows.Count > 0)
                                    {
                                        OTPriceRate = Convert.ToDecimal(Workload_DT.Rows[0]["OTPiceRate"]);
                                    }

                                    Total_OTWorkloadPay = WorkLoad * OTPriceRate;
                                    Total_OTWorkloadPay = Math.Round(Total_OTWorkloadPay, 2, MidpointRounding.AwayFromZero);
                                    Sum_WorkloadOTPay = Sum_WorkloadOTPay + Total_OTWorkloadPay;
                                }

                                if (Workload_Name.ToString() == "Al1")
                                {
                                    All1 = Convert.ToDecimal(WorkLoad);
                                    ToTalAllowance = ToTalAllowance + All1;
                                }
                                if (Workload_Name.ToString() == "Al2")
                                {
                                    All2 = Convert.ToDecimal(WorkLoad);
                                    ToTalAllowance = ToTalAllowance + All2;
                                }
                                if (Workload_Name.ToString() == "Ded1")
                                {
                                    Deduc1 = Convert.ToDecimal(WorkLoad);
                                    TotalDeduction = TotalDeduction + Deduc1;
                                }
                                if (Workload_Name.ToString() == "Ded2")
                                {
                                    Dedu2 = Convert.ToDecimal(WorkLoad);
                                    TotalDeduction = TotalDeduction + Dedu2;
                                }
                            }

                            if (PFEligible.ToString().ToUpper() == "1")
                            {
                                //PF Calculation
                                DataTable dtpf = new DataTable();
                                Query = "";
                                Query = Query + "Select PF_per,StaffSalary,StampCg,VDA1,VDA2,Union_val,Spinning,DayIncentive,";
                                Query = Query + "halfNight,FullNight,ThreeSideAmt,EmployeerPFone,EmployeerPFTwo,EmployeerESI from [" + SessionEpay + "]..MstESIPF where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                dtpf = objdata.RptEmployeeMultipleDetails(Query);

                                if (Convert.ToDecimal(Total_DayPay) >= Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()))
                                {
                                    PFS = ((Convert.ToDecimal(Total_DayPay) * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                    PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                    //Employeer PF
                                    EmployeerPFOne = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFone"].ToString())) / 100).ToString();
                                    EmployeerPFOne = (Math.Round(Convert.ToDecimal(EmployeerPFOne), 0, MidpointRounding.AwayFromZero)).ToString();

                                    //EmployeerPFOne = ((Convert.ToDecimal(dtpf.Rows[0]["StaffSalary"].ToString()) * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFone"].ToString())) / 100).ToString();
                                    EmployeerPFTwo = (Convert.ToDecimal(PFS) - Convert.ToDecimal(EmployeerPFOne)).ToString();
                                    EmployeerPFTwo = (Math.Round(Convert.ToDecimal(EmployeerPFTwo), 0, MidpointRounding.AwayFromZero)).ToString();
                                }
                                else
                                {
                                    decimal PF_val = (Convert.ToDecimal(Total_DayPay));
                                    PFS = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["PF_per"].ToString())) / 100).ToString();
                                    PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                    //Employeer PF
                                    EmployeerPFOne = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFone"].ToString())) / 100).ToString();
                                    EmployeerPFOne = (Math.Round(Convert.ToDecimal(EmployeerPFOne), 0, MidpointRounding.AwayFromZero)).ToString();

                                    //EmployeerPFOne = ((PF_val * Convert.ToDecimal(dtpf.Rows[0]["EmployeerPFone"].ToString())) / 100).ToString();
                                    EmployeerPFTwo = (Convert.ToDecimal(PFS) - Convert.ToDecimal(EmployeerPFOne)).ToString();
                                    EmployeerPFTwo = (Math.Round(Convert.ToDecimal(EmployeerPFTwo), 0, MidpointRounding.AwayFromZero)).ToString();
                                }
                            }

                            //WorkDay Deduction 
                            NetDayPay = Total_DayPay - Convert.ToDecimal(PFS);
                            //NetDayPay = (NetDayPay - TotalDeduction);
                            //NetDayPay = NetDayPay + ToTalAllowance;

                            TotalELPay = Sum_WorkloadPay + Sum_WorkloadOTPay + DaySalaryWithOneDay;

                            OtherGrassWages = Total_DayPay;

                            NetAmt = TotalELPay - OtherGrassWages;

                            OtherNetWages = OtherGrassWages - Convert.ToDecimal(PFS);

                            PayTotal = TotalELPay - Convert.ToDecimal(PFS);

                            PayTotal = (PayTotal - TotalDeduction);

                            PayTotal = PayTotal + ToTalAllowance;

                            PayTotal = Math.Round(PayTotal, 2, MidpointRounding.AwayFromZero);



                            SSQL = "";
                            SSQL = "Select * from [" + SessionEpay + "]..ReelingWagesProcess where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and EmpNo='" + EmpNo + "'";
                            SSQL = SSQL + " and Convert(datetime,FromDate,105)=Convert(datetime,'" + txtFrom.Text + "',105) and Convert(datetime,ToDate,105)=Convert(datetime,'" + txtTo.Text + "',105) and Month='"+ddlMonths.SelectedItem.Text+"'";
                            DataTable Chk_Duplicate = new DataTable();
                            Chk_Duplicate = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (Chk_Duplicate != null && Chk_Duplicate.Rows.Count > 0)
                            {
                                SSQL = "";
                                SSQL = "delete from [" + SessionEpay + "]..ReelingWagesProcess where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and EmpNo='" + EmpNo + "'";
                                SSQL = SSQL + " and Convert(datetime,FromDate,105)=Convert(datetime,'" + txtFrom.Text + "',105) and Convert(datetime,ToDate,105)=Convert(datetime,'" + txtTo.Text + "',105) and Month='" + ddlMonths.SelectedItem.Text + "'";
                                objdata.RptEmployeeMultipleDetails(SSQL);
                            }

                            SSQL = "";
                            SSQL = "Insert into [" + SessionEpay + "]..ReelingWagesProcess(Ccode,Lcode,DeptCode,EmpNo,ExistingCode,EmpName,Days,BaseSalary,OneDayPay,ToTalDayPay,NetDayPay,PFS,PFone,PFTwo,";
                            SSQL = SSQL + "OneDayPaySalary,TotalWorkloadPay,ToTalWorkloadOTPay,TotalELPay,OtherGrassWages,NetAmt,OtherNetWages,PayTotal,All1,All2,TotalAll,Dec1,Dec2,TotalDec,Month,FromDate,ToDate,CreatedBy,CreatedOn,FinYearCode,FinYearVal)";
                            SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + DeptCode + "','" + EmpNo + "','" + ExistingCode + "','" + EmpName + "','" + NoOfdays + "','" + BaseSalary + "','" + OneDayPay + "',";
                            SSQL = SSQL + "'" + Total_DayPay + "','" + NetDayPay + "','" + PFS + "','" + EmployeerPFOne + "','" + EmployeerPFTwo + "','" + DaySalaryWithOneDay + "','" + Sum_WorkloadPay + "','" + Sum_WorkloadOTPay + "',";
                            SSQL = SSQL + "'" + TotalELPay + "','" + OtherGrassWages + "','" + NetAmt + "','" + OtherNetWages + "','" + PayTotal + "','" + All1 + "','" + All2 + "','" + ToTalAllowance + "','" + Deduc1 + "','" + Dedu2 + "',";
                            SSQL = SSQL + "'" + TotalDeduction + "','" + ddlMonths.SelectedItem.Text + "', Convert(datetime,'" + txtFrom.Text + "',105) , Convert(datetime,'" + txtTo.Text + "',105),'" + SessionUserID + "',getDate(),'" + ddlfinance.SelectedItem.Text + "','" + ddlfinance.SelectedValue + "')";
                            objdata.RptEmployeeMultipleDetails(SSQL);

                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Calaculated Successfully');", true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    
    protected void btnDownload_Click(object sender, EventArgs e)
    {
        if (txtFrom.Text == "" || txtTo.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page,this.GetType(),"alert", "SaveMsgAlert(Please Select the fromData and Todate properly);", true);
            return;
        }
        if (!ErrFlg)
        {
            SSQL = "";
            SSQL = "Select EM.DeptCode,EM.MachineID as EmpCode,EM.FirstName as EmpName,isnull(Sum(LD.Present),'0') as [No Of Days]";
            SSQL = SSQL + " from Employee_Mst EM inner join LogTime_Days LD on LD.MachineID=EM.MachineID";
            SSQL = SSQL + " where convert(Datetime,Attn_Date,103)>=Convert(datetime,'" + txtFrom.Text + "',103) and convert(Datetime,Attn_Date,103)<=Convert(datetime,'" + txtTo.Text + "',103)";
            SSQL = SSQL + " and EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' and LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "' and EM.DeptCode='41'";
            SSQL = SSQL + " group by EM.DeptCode,EM.MachineID,EM.FirstName order by EM.MachineID";
            DataTable EmpDay_DT = new DataTable();
            EmpDay_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (EmpDay_DT != null && EmpDay_DT.Rows.Count > 0)
            {
                SSQL = "";
                SSQL = "Select WorkLoad from [" + SessionEpay + "]..MstReelingWagesSlab where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and FinYearVal='" + ddlfinance.SelectedValue + "'";
                DataTable Slab_DT = new DataTable();

                Slab_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (Slab_DT != null && Slab_DT.Rows.Count > 0)
                {
                    for (int SlabRow = 0; SlabRow < Slab_DT.Rows.Count; SlabRow++)
                    {
                        EmpDay_DT.Columns.Add("'"+Slab_DT.Rows[SlabRow]["WorkLoad"].ToString());
                        EmpDay_DT.AcceptChanges();
                    }

                    for (int SlabRow = 0; SlabRow < Slab_DT.Rows.Count; SlabRow++)
                    {
                        EmpDay_DT.Columns.Add("OT"+Slab_DT.Rows[SlabRow]["WorkLoad"].ToString());
                        EmpDay_DT.AcceptChanges();
                    }
                    EmpDay_DT.Columns.Add("Al1");
                    EmpDay_DT.Columns.Add("Al2");
                    EmpDay_DT.Columns.Add("Ded1");
                    EmpDay_DT.Columns.Add("Ded2");
                }
            }

            if (EmpDay_DT.Rows.Count > 0)
            {
                grid.DataSource = EmpDay_DT;
                grid.DataBind();
                string attachment = "attachment;filename=Reeling WorkLoad.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
        }
    }
}