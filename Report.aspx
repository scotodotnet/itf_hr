﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Report.aspx.cs" Inherits="Report" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="assets/js/master_list_jquery.min.js"></script>
     <script src="assets/js/master_list_jquery-ui.min.js"></script>
    <link href="assets/plugins/ModalPopup/CSS/Popup.css" rel="stylesheet" type="text/css"/>
     <script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
        <script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
   <script>
    $(document).ready(function() {
        //alert('hi');
    $('#example').dataTable();
    $('#example1').dataTable();
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            
            autoclose: true
        });
        
    });
</script>
   

    <script  type="text/javascript">
     $(document).ready(function() {
     $('#example').dataTable();
     $('.select2').select2();
     });
	</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            //alert('hi');
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
            }
        });
    };</script>

  <%--  <script type="text/javascript" >
        $(document).ready(function () {
            $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
            $('.js-states').select2();
        });
    </script>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel runat="server" EnableViewState="true">
        <ContentTemplate>
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Report</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Report</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Report</li>
                        </ol>
                    </nav>
                </div>
                <div class="row">
                    <div class="col-lg-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="forms-sample">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputName">REPORT NAME</label>
                                            <asp:DropDownList ID="ddlRptName" runat="server"
                                                class="form-control select2" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlRptName_SelectedIndexChanged">
                                                <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="ALL"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="SPECIFIC"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="ABSTRACT"></asp:ListItem>
                                                <asp:ListItem Value="5" Text="MIS"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <asp:ListBox ID="ListRptName" runat="server" Width="360px" Height="409px"
                                            Font-Bold="True" Font-Names="Times New Roman" Font-Size="Medium"
                                            OnSelectedIndexChanged="ListRptName_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem></asp:ListItem>
                                        </asp:ListBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="col-lg-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="forms-sample">

                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <asp:Label ID="RptLabel" runat="server" Font-Bold="True" Height="100%"
                                                Width="100%" align="center">Option</asp:Label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputName">Mill Code</label>
                                            <asp:DropDownList ID="ddlMillCode" AutoPostBack="true" OnSelectedIndexChanged="ddlMillCode_TextChanged" runat="server" class="form-control select2">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputName">Unit Code</label>
                                            <asp:DropDownList ID="ddlIUnitCode" runat="server" CssClass="form-control select2" AutoPostBack="true" OnSelectedIndexChanged="ddlIUnitCode_TextChanged" >
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputName">Department</label>
                                            <asp:DropDownList ID="ddlDepartment" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_TextChanged" runat="server" class="form-control select2">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputName">Designation</label>
                                            <asp:DropDownList ID="ddldesignation" AutoPostBack="true" OnSelectedIndexChanged="ddldesignation_SelectedIndexChanged" runat="server" class="form-control select2">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputName">ITF No</label>
                                            <asp:DropDownList ID="ddlITFNo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlITFNo_SelectedIndexChanged" class="form-control select2">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputName">Participant Name</label>
                                            <asp:TextBox ID="txtEmpName" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputName">From Date</label>
                                            <asp:TextBox runat="server" ID="txtFrmdate" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputName">To Date</label>
                                            <asp:TextBox runat="server" ID="txtTodate" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                            
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputName">Program Code</label>
                                            <asp:DropDownList ID="ddlCourse" AutoPostBack="true" OnSelectedIndexChanged="ddlCourse_SelectedIndexChanged" runat="server" class="form-control select2">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputName">Program Level</label>
                                            <asp:TextBox runat="server" ID="txtCourseLevel" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                             <label>Status</label>
                                                <asp:DropDownList ID="ddlStatus" CssClass="form-control select2" Style="width: 100%"  runat="server">
                                                    <asp:ListItem Text="On Roll" Value="On Roll"></asp:ListItem>
                                                    <asp:ListItem Text="Complete" Value="Complete"></asp:ListItem>
                                                    <asp:ListItem Text="-Select-" Value="-Select-" Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                       
                                    <!-- begin col-4 -->
                                       <div class="col-md-6">
                                            <div class="form-group">
                                               <%-- <label>GST</label>--%>
                                                <asp:DropDownList ID="ddlGST" CssClass="js-example-basic-single select2" Style="width: 100%" Visible="false"  runat="server">
                                                        
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlGST" Visible="false" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <asp:CheckBox ID="ckbTimeIN" Checked="true" Text="Time IN" runat="server" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-12" align="center">
                                            <asp:Button ID="btnExcel" class="btn btn-info" runat="server" Text="Excel"
                                                OnClick="btnExcel_Click" />
                                            <asp:Button ID="btnReport" class="btn btn-success" runat="server" Text="Report"
                                                OnClick="btnReport_Click" />
                                            <asp:Button ID="btnClear" class="btn btn-danger" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
                </div>
            </footer>
        </ContentTemplate>
    </asp:UpdatePanel>
     <script src=" https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src=" https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
</asp:Content>

