﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Drawing;
using Payroll;

public partial class Report : System.Web.UI.Page
{
    System.Web.UI.WebControls.DataGrid grid =
                     new System.Web.UI.WebControls.DataGrid();
    String CurrentYear1;
    static int CurrentYear;
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode; string SessionUser;
    string SessionDivision; string SessionRights;
    BALDataAccess objdata = new BALDataAccess();
    //string SSQL = "";
    string ss1 = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionUserType;
    string Date1_str;
    string Date2_str;
    string Wages_Type;
    bool Errflag = false;
    string SSQL = "";
    bool Errflag1 = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            SessionRights = Session["Rights"].ToString();
            SessionUser = Session["Usernmdisplay"].ToString();

            con = new SqlConnection(constr);

            if (!IsPostBack)
            {
                Load_Mill();
                Load_Course();
                Load_ITFNo();
                Load_GST();
                ddlIUnitCode.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
                ddldesignation.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
                btnClear_Click(sender, e);

            }
        }
    }
    protected void Load_ITFNo()
    {
        SSQL = "";
        SSQL = "Select (MachineID+'-'+FirstName) as ITFName,MachineID as ITFID from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
      //  SSQL = SSQL + " and UnitCode='" + ddlIUnitCode.SelectedValue + "' and DeptCode='" + ddlDepartment.SelectedValue + "' and DesignCode='" + ddldesignation.SelectedValue + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlITFNo.DataSource = dt;
        ddlITFNo.DataTextField = "ITFName";
        ddlITFNo.DataValueField = "ITFID";
        ddlITFNo.DataBind();
        ddlITFNo.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void Load_Mill()
    {
        SSQL = "";
        SSQL = "Select (MillCode +' - '+ MillName) as MillName,MillCode from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlMillCode.DataSource = dt;
        ddlMillCode.DataTextField = "MillName";
        ddlMillCode.DataValueField = "MillCode";
        ddlMillCode.DataBind();
        ddlMillCode.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void Load_Course()
    {
        SSQL = "";
        SSQL = "Select (CourseCode+'-'+CourseName) as CourseName , CourseCode from MstCourse where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlCourse.DataSource = dt;
        ddlCourse.DataTextField = "CourseName";
        ddlCourse.DataValueField = "CourseCode";
        ddlCourse.DataBind();
        ddlCourse.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void Load_Designation()
    {

        SSQL = "";
      //  SSQL = "Select * from Designation_Mst where  UnitCode='" + ddlIUnitCode.SelectedValue + "' and MillCode='" + ddlMillCode.SelectedValue + "'";
        SSQL = "Select Distinct DesignName,DesignCode from Designation_Mst ";
        ddldesignation.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddldesignation.DataTextField = "DesignName";
        ddldesignation.DataValueField = "DesignCode";
        ddldesignation.DataBind();
       ddldesignation.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

    }
    protected void ddlRptName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlRptName.SelectedItem.Text == "ALL")
        {
            ListRptName.Items.Clear();
            ListRptName.Items.Add("DAY WISE-INDIVIDUAL");
            ListRptName.Items.Add("INDIVIDUAL ATTENDANCE - BETWEEN DATES");
          //  ListRptName.Items.Add("PROGRAM LEVEL REPORT");
            ListRptName.Items.Add("MILL WISE REPORT");
            ListRptName.Items.Add("MILLWISE INDIVIDUAL REPORT");
            ListRptName.Items.Add("PROGRAM WISE REPORT");
            ListRptName.Items.Add("BETWEEN DATES REPORT");
            ListRptName.Items.Add("DAILY BATCH WISE REPORT");
            ListRptName.Items.Add("ABSENT REPORT BETWEEN DATES REPORT");
            //ListRptName.Items.Add("BILL");




        }
        if (ddlRptName.SelectedItem.Text == "SPECIFIC")
        {
            ListRptName.Items.Clear();
          //  ListRptName.Items.Add("PROGRAM COSTING REPORT");
           // ListRptName.Items.Add("PAYMENT REPORT");
         


        }

        if (ddlRptName.SelectedItem.Text == "ABSTRACT")
        {
            ListRptName.Items.Clear();

          
                       
        }
    }

    protected void ListRptName_SelectedIndexChanged(object sender, EventArgs e)
    {
        RptLabel.Text = ListRptName.SelectedItem.Text;

        //if(RptLabel.Text== "DAY WISE-INDIVIDUAL")
        //{
        //    ckbTimeIN.Checked = false;
        //    ckbTimeIN.Enabled = false;

        //}
    }

    protected void ddlITFNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlITFNo.SelectedValue != "-Select-")
        {
            txtEmpName.Text = ddlITFNo.SelectedItem.Text.ToString().Split('-').Last();
        }
        else
        {
            txtEmpName.Text = "";
        }
    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {

    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        string status;
        if (ckbTimeIN.Checked == true)
        {
            status = "TimeIN";
        }
        else
        {
            status = "";
        }
        if (RptLabel.Text == "DAY WISE-INDIVIDUAL")
        {
            
            if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else
            {
                
                ResponseHelper.Redirect("RptWeekoff.aspx?FromDate=" + txtFrmdate.Text + "&MachineId=" + ddlITFNo.SelectedValue + "&status=" +status  +"&Date=" + txtFrmdate.Text, "_blank", "");
                
            }
        }
        if (RptLabel.Text == "PROGRAM LEVEL REPORT")
        {

            ResponseHelper.Redirect("RptCourseLevel.aspx?CourseCode=" + ddlCourse.SelectedValue + "&CourseLevel=" + txtCourseLevel.Text + "&MillCode=" + ddlMillCode.SelectedValue + "&UnitCode=" + ddlIUnitCode.SelectedValue + "&MachineId=" + ddlITFNo.SelectedValue + "&DeptCode=" + ddlDepartment.SelectedValue + "&DesignationCode=" + ddldesignation.SelectedValue + "&CourseStatus=" + ddlStatus.SelectedItem.Text, "_blank", "");


        }

        if (RptLabel.Text == "INDIVIDUAL ATTENDANCE - BETWEEN DATES")
        {
            if (ddlITFNo.SelectedItem.Text == "-Select-" || ddlITFNo.SelectedItem==null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select ITF No');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }

            else if (txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
            }
            else
            {
                string s = ddlITFNo.SelectedItem.Text;
                string[] delimiters = new string[] { "-" };
                string[] items = s.Split(delimiters, StringSplitOptions.None);
                string ss = items[0];
                ss1 = items[1];

                ResponseHelper.Redirect("Individual_Between_Days.aspx?EmpCode=" + ss + "&status=" + status + "&Date1=" + txtFrmdate.Text + "&Date2=" + txtTodate.Text, "_blank", "");
            }
        }
        if (RptLabel.Text == "PROGRAM COSTING REPORT")
        {
            ResponseHelper.Redirect("RptCourse_Costing.aspx?CourseCode=" + ddlCourse.SelectedValue + "&MillCode=" + ddlMillCode.SelectedValue + "&UnitCode=" + ddlIUnitCode.SelectedItem.Text +  "&DesignationCode=" + ddldesignation.SelectedItem.Text, "_blank", "");
            btnClear_Click(sender ,e);
        }
        if (RptLabel.Text == "PAYMENT REPORT")
        {
            ResponseHelper.Redirect("RptPayment.aspx?MillCode=" + ddlMillCode.SelectedValue + "&UnitCode=" + ddlIUnitCode.SelectedItem.Text + "&DesignationCode=" + ddldesignation.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text +"&ToDate=" + txtTodate.Text, "_blank", "");
            btnClear_Click(sender, e);
        }
        
        if (RptLabel.Text == "MILL WISE REPORT")
        {
            if (ddlMillCode.SelectedItem.Text == "-Select-" || ddlITFNo.SelectedItem == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Mill Code');", true);
            }

            if (ddlIUnitCode.SelectedItem.Text == "-Select-" || ddlITFNo.SelectedItem == null)
              {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Unit Code');", true);
            }
            else{
                ResponseHelper.Redirect("RptMillWise.aspx?CourseCode=" + ddlCourse.SelectedValue + "&MillCode=" + ddlMillCode.SelectedValue + "&UnitCode=" + ddlIUnitCode.SelectedValue + "&DesignationCode=" + ddldesignation.SelectedValue, "_blank", "");
                btnClear_Click(sender, e);
            }
        }
        if (RptLabel.Text == "MILLWISE INDIVIDUAL REPORT")
        {
            

            if (ddlITFNo.SelectedItem.Text == "-Select-" || ddlITFNo.SelectedItem == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select ITF No Code');", true);
            }
            if (txtFrmdate.Text == "" || txtTodate.Text == "")
            {
                Errflag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please enter the FromDate and ToDate Properly');", true);
            }
            else
            {
                ResponseHelper.Redirect("RptMillWiseIndividual.aspx?CourseCode=" + ddlCourse.SelectedValue + "&MillCode=" + ddlMillCode.SelectedValue + "&UnitCode=" + ddlIUnitCode.SelectedValue + "&DesignationCode=" + ddldesignation.SelectedValue +"&MachineID=" + ddlITFNo.SelectedValue + "&Date1=" + txtFrmdate.Text + "&Date2=" + txtTodate.Text, "_blank", "");
                btnClear_Click(sender, e);
            }
        }
        if (RptLabel.Text == "PROGRAM WISE REPORT")
        {
            if (ddlCourse.SelectedItem.Text == "-Select-" || ddlCourse.SelectedItem == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select a Program');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter From Date');", true);
            }

            else
            {
                ResponseHelper.Redirect("RptCourseWise.aspx?CourseCode=" + ddlCourse.SelectedValue +"&FromDate=" + txtFrmdate.Text + "&MillCode=" + ddlMillCode.SelectedValue + "&UnitCode=" + ddlIUnitCode.SelectedValue + "&MachineID=" + ddlITFNo.SelectedValue + "&DesignationCode=" + ddldesignation.SelectedValue, "_blank", "");
               
                btnClear_Click(sender, e);
            }
        }
       

            if (RptLabel.Text == "DAILY BATCH WISE REPORT")
        {
            if (ddlCourse.SelectedItem.Text == "-Select-" || ddlCourse.SelectedItem == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select a Program');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter From Date');", true);
            }

            else
            {
               
                ResponseHelper.Redirect("RptDailyBatchWise.aspx?CourseCode=" + ddlCourse.SelectedValue + "&FromDate=" + txtFrmdate.Text + "&MillCode=" + ddlMillCode.SelectedValue + "&UnitCode=" + ddlIUnitCode.SelectedValue + "&MachineID=" + ddlITFNo.SelectedValue + "&DesignationCode=" + ddldesignation.SelectedValue, "_blank", "");
                btnClear_Click(sender, e);
            }
        }

        if (RptLabel.Text == "BILL")
        {
            if (ddlMillCode.SelectedItem.Text == "-Select-")
            {
                Errflag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select the Mill Name');", true);

            }
            if (ddldesignation.SelectedItem.Text == "-Select-")
            {
                Errflag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select the Designation Name');", true);

            }
            else if (ddlIUnitCode.SelectedItem.Text == "-Select-")
            {
                Errflag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select the Unit');", true);
            }
            if (txtFrmdate.Text == "" || txtTodate.Text == "")
            {
                Errflag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please enter the FromDate and ToDate Properly');", true);
            }
            if (ddlGST.SelectedItem.Text == "-Select-")
            {
                Errflag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select the GST');", true);
            }
            if (txtCourseLevel.Text == "-Select-")
            {
                Errflag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select the Program Level');", true);
            }
            if (ddlCourse.SelectedItem.Text == "-Select-")
            {
                Errflag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select the Program');", true);
            }

            if (ckbTimeIN.Checked == true)
            {

            }

            if (!Errflag)
            {
                ResponseHelper.Redirect("RptPayment.aspx?MillCode=" + ddlMillCode.SelectedValue + "&UnitCode=" + ddlIUnitCode.SelectedValue + "&DesignationCode=" + ddldesignation.SelectedValue + "&CourseLevel=" + txtCourseLevel.Text + "&GST=" + ddlGST.SelectedValue + "&CourseName=" + ddlCourse.SelectedItem.Text + "&CourseCode=" + ddlCourse.SelectedValue + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
                btnClear_Click(sender, e);
            }
        }
        if (RptLabel.Text == "BETWEEN DATES REPORT")
        {

            if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            if (txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter To Date');", true);
            }
            else
            {

                ResponseHelper.Redirect("RptBetweenDates.aspx?Date1=" + txtFrmdate.Text + "&Date2=" + txtTodate.Text + "&MillCode=" + ddlMillCode.SelectedValue + "&UnitCode=" + ddlIUnitCode.SelectedValue, "_blank", "");
                

            }
        }
        if (RptLabel.Text == "ABSENT REPORT BETWEEN DATES REPORT")
        {

            if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            if (txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter To Date');", true);
            }
            if (ddlCourse.SelectedItem.Text == "-Select-")
            {
                Errflag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select the Program');", true);
            }

            else
            {

               
                ResponseHelper.Redirect("RptAbsentBetweenDays.aspx?Date1=" + txtFrmdate.Text + "&Date2=" + txtTodate.Text + "&MillCode=" + ddlMillCode.SelectedValue + "&UnitCode=" + ddlIUnitCode.SelectedValue + "&CourseCode=" + ddlCourse.SelectedValue, "_blank", "");

            }
        }


    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlCourse.ClearSelection();
        ddlITFNo.ClearSelection();
        ddlDepartment.ClearSelection();
        ddldesignation.ClearSelection();
        ddlMillCode.ClearSelection();
        ddlIUnitCode.ClearSelection();
        txtEmpName.Text = "";
        txtFrmdate.Text = "";
        txtTodate.Text = "";
        txtCourseLevel.Text = "";
        ckbTimeIN.Checked = false;
    }

    protected void ddlMillCode_TextChanged(object sender, EventArgs e)
    {
        Load_unit(ddlMillCode.SelectedValue);
        Load_Designation();
    }
    protected void Load_unit(string MillCode)
    {
        SSQL = "";
        SSQL = "Select (UnitName+'-'+UnitTypeName) as UnitName,UnitCode from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + MillCode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count <= 0)
        {
            SSQL = "";
            SSQL = "select Units from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Millcode='" + MillCode + "'";
            DataTable Dt_get = new DataTable();
            Dt_get = objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable Unit_dt = new DataTable();

            Unit_dt.Columns.Add("UnitCode");
            Unit_dt.Columns.Add("UnitName");

            int Unitcode = 0;
            if (Dt_get != null && Dt_get.Rows.Count > 0)
            {

                Unitcode = Convert.ToInt32(Dt_get.Rows[0]["Units"]);
                DataRow dr;

                for (int i = 1; i <= Unitcode; i++)
                {
                    dr = Unit_dt.NewRow();
                    dr["UnitCode"] = "UNIT " + i;
                    dr["UnitName"] = "UNIT " + i;
                    Unit_dt.Rows.Add(dr);
                }

                if (Unit_dt.Rows.Count > 0)
                {
                    ddlIUnitCode.DataSource = Unit_dt;
                    ddlIUnitCode.DataTextField = "UnitName";
                    ddlIUnitCode.DataValueField = "UnitCode";
                    ddlIUnitCode.DataBind();
                    ddlIUnitCode.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
                }
            }
        }
        else
        {
            ddlIUnitCode.DataSource = dt;
            ddlIUnitCode.DataTextField = "UnitName";
            ddlIUnitCode.DataValueField = "UnitCode";
            ddlIUnitCode.DataBind();
            ddlIUnitCode.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }
    protected void ddlIUnitCode_TextChanged(object sender, EventArgs e)
    {
        if (ddlIUnitCode.SelectedItem.Text != "-Select-")
        {
            SSQL = "";
            SSQL = "Select MillCode,UnitCode from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + ddlMillCode.SelectedValue + "'";
            SSQL = SSQL + " and UnitCode='" + ddlIUnitCode.SelectedValue + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt != null && dt.Rows.Count > 0)
            {
                
                Load_DeptName(dt.Rows[0]["MillCode"].ToString(), dt.Rows[0]["UnitCode"].ToString());
            }
        }
    }
    protected void Load_DeptName(string MillCode, string UnitCode)
    {
        SSQL = "";
        SSQL = "Select * from  Department_Mst where MillCode='" + MillCode + "' and UnitCode='" + UnitCode + "'";
        ddlDepartment.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlDepartment.DataValueField = "DeptCode";
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataBind();
        ddlDepartment.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void ddlDepartment_TextChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from Designation_Mst where DeptCode='" + ddlDepartment.SelectedValue + "' and UnitCode='" + ddlIUnitCode.SelectedValue + "' and MillCode='" + ddlMillCode.SelectedValue + "'";
        ddldesignation.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddldesignation.DataTextField = "DesignName";
        ddldesignation.DataValueField = "DesignCode";
        ddldesignation.DataBind();
        ddldesignation.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void ddldesignation_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddldesignation.SelectedValue != "-Select-")
        {
            SSQL = "";
            SSQL = "Select (MachineID+'-'+FirstName) as ITFName,MachineID as ITFID from Employee_Mst where CompCode='" + SessionCcode + "' and LocCOde='" + SessionLcode + "' and MillCode='"+ddlMillCode.SelectedValue+"'";
          //  SSQL = SSQL + " and UnitCode='" + ddlIUnitCode.SelectedValue + "' and DeptCode='" + ddlDepartment.SelectedValue + "' and DesignCode='" + ddldesignation.SelectedValue + "'";
            SSQL = SSQL + " and UnitCode='" + ddlIUnitCode.SelectedValue + "' and DesignCode='" + ddldesignation.SelectedValue + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlITFNo.DataSource = dt;
            ddlITFNo.DataTextField = "ITFName";
            ddlITFNo.DataValueField = "ITFID";
            ddlITFNo.DataBind();
            ddlITFNo.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }

    protected void ddlCourse_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstCourse where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and CourseCode='" + ddlCourse.SelectedValue + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt != null && dt.Rows.Count > 0)
        {
            txtCourseLevel.Text = dt.Rows[0]["Level"].ToString();
        }
    }
    private void Load_GST()
    {
        SSQL = "";
        SSQL = "Select * from Mst_GST";
        ddlGST.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlGST.DataValueField = "TaxDescription";
        ddlGST.DataTextField = "TaxDescription";
        ddlGST.DataBind();
        ddlGST.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
}