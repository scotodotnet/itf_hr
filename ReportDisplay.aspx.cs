﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class ReportDisplay : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";
    string SSQL = "";
    string SessionPayroll;

    BALDataAccess objdata = new BALDataAccess();
    DataTable dt = new DataTable();
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    ReportDocument rd = new ReportDocument();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    string RptYear;
    string ReportName;
    string FromDate;
    string ToDate;
    string EmpType;
    string Deptname = "";
    string MachineID = "";

    string Months;
    string FinYear;
    string Category;
    string CmpName, Cmpaddress;

    string FromMonth, ToMonth, FromYear, ToYear;

    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Payroll";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            Load_DB();

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            ReportName = Request.QueryString["Report"].ToString();

           
        }

        if (ReportName == "PF DEDUCTION")
        {
            
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            PF_Ded_Load();
        }
       
        else if (ReportName == "BANK PAYMENT")
        {

            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            Bank_Amt();
        }
        else if (ReportName == "OTHER PAYSLIP")
        {

            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            Others_Pay_Load();
        }
        else if (ReportName == "ESI DEDUCTION")
        {
            //ReportName = Request.QueryString["Report"].ToString();
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            ESI_Ded_Load();
        }
        else if (ReportName == "LIC DEDUCTION")
        {
            //ReportName = Request.QueryString["Report"].ToString();
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            LIC_Ded_Load();
        }
        else if (ReportName == "HOUSE RENT DEDUCTION")
        {
            //ReportName = Request.QueryString["Report"].ToString();
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            Home_Ded_Load();
        }
        else if (ReportName == "TEMPLE DEDUCTION")
        {
            //ReportName = Request.QueryString["Report"].ToString();
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            Temple_Ded_Load();
        }
        else if (ReportName == "STORES DEDUCTION")
        {
            //ReportName = Request.QueryString["Report"].ToString();
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            Stores_Ded_Load();
        }
        else if (ReportName == "OFFICE ADVANCE")
        {
            //ReportName = Request.QueryString["Report"].ToString();
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            Advance_Ded_Load();
        }
        else if (ReportName == "MISC2 DEDUCTION")
        {
            //ReportName = Request.QueryString["Report"].ToString();
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            MIC2_Ded_Load();
        }
        else if (ReportName == "MISC4 DEDUCTION")
        {
            //ReportName = Request.QueryString["Report"].ToString();
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            MIC4_Ded_Load();
        }
        else if (ReportName == "HOUSE LOAN DEDUCTION")
        {
            //ReportName = Request.QueryString["Report"].ToString();
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            Hloan_Ded_Load();
        }
        else if (ReportName == "COMMISSION LIST")
        {
            //ReportName = Request.QueryString["Report"].ToString();
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            Comm_Ded_Load();
        }
        else if (ReportName == "OT AMOUNT")
        {
            OT_Amt();
        }
        else if (ReportName == "MAN DAYS LIST")
        {
            //ReportName = Request.QueryString["Report"].ToString();
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            //Months = Request.QueryString["Month"].ToString();
            //FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            Man_Work_Days();
        }
        else if (ReportName == "EMPLOYEE WORK LIST")
        {
            //ReportName = Request.QueryString["Report"].ToString();
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            //Months = Request.QueryString["Month"].ToString();
            //FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();

            FromMonth = Request.QueryString["FromMonth"].ToString();
            ToMonth = Request.QueryString["ToMonth"].ToString();
            FromYear = Request.QueryString["FromYear"].ToString();
            ToYear = Request.QueryString["ToYear"].ToString();

            Emp_Man_Days();
        }
        else if (ReportName == "SALARY HISTORY")
        {
            //ReportName = Request.QueryString["Report"].ToString();
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            //Months = Request.QueryString["Month"].ToString();
            //FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();

            FromMonth = Request.QueryString["FromMonth"].ToString();
            ToMonth = Request.QueryString["ToMonth"].ToString();
            FromYear = Request.QueryString["FromYear"].ToString();
            ToYear = Request.QueryString["ToYear"].ToString();

            Emp_Salary();
        }
        else if (ReportName == "WAGES ABSTRACT")
        {
            //ReportName = Request.QueryString["Report"].ToString();
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["Year"].ToString();
            RptYear = Request.QueryString["RptYear"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            
            Wages_Abstract();
        }
        if (ReportName == "Increment")
        {
            EmpType = Request.QueryString["Wages"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            //5 Months = Request.QueryString["Month"].ToString();
            FinYear = Request.QueryString["FinYear"].ToString();


            Increment_Report();
        }

        if (ReportName == "FORM 2 PF")
        {
            EmpType = Request.QueryString["Wages"].ToString();
            Category= Request.QueryString["CatName"].ToString();
            Deptname= Request.QueryString["DeptName"].ToString();
            MachineID = Request.QueryString["MachineID"].ToString();

            Form2_PF();

        }
        if (ReportName == "FORM 2 ESI")
        {
            EmpType = Request.QueryString["Wages"].ToString();
            Category = Request.QueryString["CatName"].ToString();
            Deptname = Request.QueryString["DeptName"].ToString();
            MachineID = Request.QueryString["MachineID"].ToString();

            Form2_ESI();
        }
    }

    private void Form2_ESI()
    {
        SSQL = "";
        SSQL = "Select EM.MachineID as [Code],EM.FirstName as [Name],'ARUPPUKOTTAI' as [Dispensary],EM.BirthDate as [Dob],EM.Age as [Age],EM.Gender as [Sex],EM.PFNo as [Pfno],EM.ESINo as [Esino]";
        SSQL = SSQL + ",EM.DOJ as [Doj],EM.PFDOJ as [Dpf],EM.ESIDOJ as [Desi],EM.Designation as [Design],EM.MaritalStatus as [MaritalStatus],EM.LastName as [FatherName],EM.Address1 as [PermanentAddress]";
        SSQL = SSQL + ",EM.EmployeeMobile as [PhoneNumber],EM.Adhar_No as [AadharNo],EM.Qualification as [EduQualification],FD.Names as [F_Mem_Name],FD.Age as [F_Mem_Age],FD.Gender as [F_Mem_Sex],FD.DOB as [F_Mem_Dob]";
        SSQL = SSQL + ",FD.FamilyTypes as [F_Mem_Relation] from Employee_Mst EM inner join FamilyDetails FD on FD.MachineID=EM.MachineID";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' and FD.CompCode='" + SessionCcode + "' and FD.LocCode='" + SessionLcode + "'";
        if (EmpType != "")
        {
            SSQL = SSQL + " and EM.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and  EM.Wages!='CONTRACTCLEANING I' and EM.Wages!='CONTRACTCLEANING II'";
        }
        if (Category != "")
        {
            SSQL = SSQL + " and EM.CatName='" + Category + "'";
        }
        if (Deptname != "-Select-")
        {
            SSQL = SSQL + " and EM.DeptName='" + Deptname + "'";
        }
        if (MachineID != "0")
        {
            SSQL = SSQL + " and EM.MachineID='" + MachineID + "'";
        }

        DataTable ESI2_DT = new DataTable();
        ESI2_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (ESI2_DT != null && ESI2_DT.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_Cat.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip/EsiForm2.rpt"));
            rd.SetDataSource(ESI2_DT);
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyLocation"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyLocation"].Text = "''";
            }

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Data Found')", true);
        }
    }

    private void Form2_PF()
    {
        SSQL = "";
        SSQL = "Select EM.FirstName as [Name],EM.LastName as [Father/HusbandName],EM.BirthDate as [Dob],EM.Gender as [Sex],EM.MaritalStatus as [MaritalStatus],";
        SSQL = SSQL + "EM.AccountNo as [AccountNo],'' as [Email],EM.EmployeeMobile as [PhoneNo],EM.Address1 as [AddressPerm],EM.Address2 as [AddressTemp],";
        SSQL = SSQL + "EM.Nominee as [NameofNom],EM.Address1 as [AddressNom],EM.NomineeRelation as [Nominee'sRelation],FD.DOB as [DobNominee],'' as [TotalAmountOfShare],'' as [IfTheNomineeIsMinor]";
        SSQL = SSQL + " from Employee_Mst EM inner join FamilyDetails FD on FD.MachineID=EM.MachineID and FD.FamilyTypes=EM.NomineeRelation and FD.Names=EM.Nominee and FD.CompCode=EM.CompCode and FD.LocCode=EM.LocCode ";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "'";
        if (EmpType != "")
        {
            SSQL = SSQL + " and EM.Wages='" + EmpType + "'";
        }
        if (Category != "")
        {
            SSQL = SSQL + " and EM.CatName='" + Category + "'";
        }
        if (Deptname != "-Select-")
        {
            SSQL = SSQL + " and EM.DeptName='" + Deptname + "'";
        }
        if (MachineID != "-Select-")
        {
            SSQL = SSQL + " and EM.MachineID='" + MachineID + "'";
        }

        DataTable PF2_DT = new DataTable();
        PF2_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (PF2_DT != null && PF2_DT.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_Cat.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip/PfForm2_main.rpt"));
            rd.SetDataSource(PF2_DT);

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Data Found')", true);
        }
    }

    private void Increment_Report()
    {
        DataTable DT_Elg = new DataTable();
        DataTable DT_Inc = new DataTable();
        string SSQL = "";
        string SplInc = "0";


        SSQL = "Select ED.EmpNo,EM.FirstName,EM.Wages,SUM(ED.Days) as Days,EM.BaseSalary as Basic,SUM(ED.Increment) as Inc,Sum(ED.SplIncrement) as SplInc,'0' as TotalInc,'0' as TotalBasic,EM.DeptName";
        SSQL = SSQL + " from [" + SessionPayroll + "]..EmpDaysIncrement ED inner join Employee_Mst EM on EM.EmpNo=ED.EmpNo";
        SSQL = SSQL + " where Convert(datetime,FromDate,103)=Convert(datetime,'" + FromDate.ToString() + "',103) and Convert(datetime,ToDate,103)=Convert(datetime,'" + ToDate.ToString() + "',103)";
        SSQL = SSQL + " and ED.Ccode='" + SessionCcode + "' and EM.CompCode='" + SessionCcode + "' and ED.Lcode='" + SessionLcode + "' and EM.LocCode='" + SessionLcode + "'";

        if (EmpType != "-Select-")
        {
            SSQL = SSQL + " and ED.Wages='" + EmpType.ToString() + "' And EM.Wages='" + EmpType.ToString() + "'";
        }
        SSQL = SSQL + " group by ED.EmpNo,EM.FirstName,EM.Wages,EM.DeptName,EM.BaseSalary order by ED.EmpNo";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/EmpIncrementDays.rpt"));
            rd.SetDataSource(dt);

            rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
            rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            rd.DataDefinition.FormulaFields["EmpType"].Text = "'" + EmpType + "'";
            rd.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
            rd.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Ramalinga_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    public void Emp_Man_Days()
    {
        SSQL = "";
        SSQL = SSQL + " select EmpNo,(FirstName) as EmpName,isnull([January] ,'0') as [Jan],isnull([February] , '0') as [Feb],";
        SSQL = SSQL + " isnull([March], '0') as [Mar],isnull([April], '0') as [Apr],isnull([May], '0') as [May],";
        SSQL = SSQL + " isnull([June], '0') as [Jun],isnull([July], '0') as [Jul],isnull([August], '0') as [Aug],";
        SSQL = SSQL + " isnull([September] ,'0') as [Sep],isnull([October] ,'0') as [Oct],";
        SSQL = SSQL + " isnull([November], '0') as [Nov],ISNULL([December] ,'0') as [Dec],";
        SSQL = SSQL + " (isnull([April] , '0')  + isnull([May] , '0')  + isnull([June] , '0')  + isnull([July] , '0')  + isnull([August] , '0') + isnull([September] , '0')) as TotalOne,";
        SSQL = SSQL + " (isnull([October] , '0')  + isnull([November] , '0')  + isnull([December] , '0') + isnull([January] ,'0') + isnull([February] , '0')+ isnull([March] , '0')) as TotalTwo,";
        SSQL = SSQL + " (isnull([January] ,'0') + isnull([February] , '0') +  isnull([March] , '0')  + isnull([April] , '0')  + isnull([May] , '0')  + isnull([June] , '0')  + isnull([July] , '0')  + isnull([August] , '0') + isnull([September] , '0')  + isnull([October] , '0')  + isnull([November] , '0')  + isnull([December] , '0')) as Total";
        SSQL = SSQL + " from (";
        SSQL = SSQL + " select EM.EmpNo,EM.FirstName,SD.Month, (sum(SD.WorkedDays)) AS Male";
        SSQL = SSQL + " from [Ramalinga_Epay]..SalaryDetails SD inner join Employee_Mst EM on SD.EmpNo=EM.EmpNo ";
        SSQL = SSQL + " where EM.IsActive='Yes' ";
        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EM.CatName='" + Category + "' and EM.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EM.Wages!='CONTRACTCLEANING I' and EM.Wages!='CONTRACTCLEANING II'";
        }
        SSQL = SSQL + " and CONVERT (datetime,SD.FromDate,103)>=CONVERT(datetime,'" + FromDate + "',103) and";
        SSQL = SSQL + " CONVERT(datetime,SD.ToDate,103)<=CONVERT(datetime,'" + ToDate + "',103)";
        SSQL = SSQL + " group by  EM.FirstName,SD.Month,EM.EmpNo";
        SSQL = SSQL + " ) as PVT";
        SSQL = SSQL + " PIVOT(";
        SSQL = SSQL + " sum(Male) FOR Month in([January],[February],[March],[April],[May],[June],[July],[August],[September],[October],[November],[December])";
        SSQL = SSQL + " ) AS pt order by cast(EmpNo as int) asc";



        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/EmpWorkDays.rpt"));
            rd.SetDataSource(dt);

            
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["FromMonth"].Text = "'" + FromMonth + "'";
            rd.DataDefinition.FormulaFields["ToMonth"].Text = "'" + ToMonth + "'";
            rd.DataDefinition.FormulaFields["FromYear"].Text = "'" + FromYear + "'";
            rd.DataDefinition.FormulaFields["ToYear"].Text = "'" + ToYear + "'";
            rd.DataDefinition.FormulaFields["EmpType"].Text = "'" + EmpType + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void Emp_Salary()
    {
        SSQL = "";
        SSQL = SSQL + " select EmpNo,(FirstName) as EmpName,isnull([January] ,'0') as [Jan],isnull([February] , '0') as [Feb],";
        SSQL = SSQL + " isnull([March], '0') as [Mar],isnull([April], '0') as [Apr],isnull([May], '0') as [May],";
        SSQL = SSQL + " isnull([June], '0') as [Jun],isnull([July], '0') as [Jul],isnull([August], '0') as [Aug],";
        SSQL = SSQL + " isnull([September] ,'0') as [Sep],isnull([October] ,'0') as [Oct],";
        SSQL = SSQL + " isnull([November], '0') as [Nov],ISNULL([December] ,'0') as [Dec],";
        SSQL = SSQL + " (isnull([January] ,'0') + isnull([February] , '0') +  isnull([March] , '0')  + isnull([April] , '0')  + isnull([May] , '0')  + isnull([June] , '0')  + isnull([July] , '0')  + isnull([August] , '0') + isnull([September] , '0')  + isnull([October] , '0')  + isnull([November] , '0')  + isnull([December] , '0')) as Total";
        SSQL = SSQL + " from (";
        SSQL = SSQL + " select EM.EmpNo,EM.FirstName,SD.Month, sum(SD.GrossEarnings) AS Male";
        SSQL = SSQL + " from [Ramalinga_Epay]..SalaryDetails SD inner join Employee_Mst EM on SD.EmpNo=EM.EmpNo ";
        SSQL = SSQL + " where EM.IsActive='Yes' ";
        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EM.CatName='" + Category + "' and EM.Wages='" + EmpType + "'";
        }

        else
        {
            SSQL = SSQL + " and EM.Wages!='CONTRACTCLEANING I' and EM.Wages!='CONTRACTCLEANING II'";
        }
        SSQL = SSQL + " and CONVERT (datetime,SD.FromDate,103)>=CONVERT(datetime,'" + FromDate + "',103) and";
        SSQL = SSQL + " CONVERT(datetime,SD.ToDate,103)<=CONVERT(datetime,'" + ToDate + "',103)";
        SSQL = SSQL + " group by  EM.FirstName,SD.Month,EM.EmpNo";
        SSQL = SSQL + " ) as PVT";
        SSQL = SSQL + " PIVOT(";
        SSQL = SSQL + " sum(Male) FOR Month in([January],[February],[March],[April],[May],[June],[July],[August],[September],[October],[November],[December])";
        SSQL = SSQL + " ) AS pt order by cast(EmpNo as int) asc";



        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/EmpSalary.rpt"));
            rd.SetDataSource(dt);

            
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["FromMonth"].Text = "'" + FromMonth + "'";
            rd.DataDefinition.FormulaFields["ToMonth"].Text = "'" + ToMonth + "'";
            rd.DataDefinition.FormulaFields["FromYear"].Text = "'" + FromYear + "'";
            rd.DataDefinition.FormulaFields["ToYear"].Text = "'" + ToYear + "'";
            rd.DataDefinition.FormulaFields["EmpType"].Text = "'" + EmpType + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    
    }
    public void Man_Work_Days()
    {
        SSQL = "";
        SSQL = SSQL + " select Gender,isnull([January] ,'0') as [Jan],isnull([February] , '0') as [Feb],";
        SSQL = SSQL + " isnull([March], '0') as [Mar],isnull([April], '0') as [Apr],isnull([May], '0') as [May],";
        SSQL = SSQL + " isnull([June], '0') as [Jun],isnull([July], '0') as [Jul],isnull([August], '0') as [Aug],";
        SSQL = SSQL + " isnull([September] ,'0') as [Sep],isnull([October] ,'0') as [Oct],";
        SSQL = SSQL + " isnull([November], '0') as [Nov],ISNULL([December] ,'0') as [Dec],";
        SSQL = SSQL + " (isnull([January] ,'0') + isnull([February] , '0') +  isnull([March] , '0')  + isnull([April] , '0')  + isnull([May] , '0')  + isnull([June] , '0')  + isnull([July] , '0')  + isnull([August] , '0') + isnull([September] , '0')  + isnull([October] , '0')  + isnull([November] , '0')  + isnull([December] , '0')) as Total";
        SSQL = SSQL + " from (";
        SSQL = SSQL + " select EM.Gender,AD.Months, sum(AD.Days) AS Male";
        SSQL = SSQL + " from Employee_Mst EM inner join [Ramalinga_Epay]..AttenanceDetails AD on AD.EmpNo=EM.EmpNo ";
        SSQL = SSQL + " where EM.IsActive='Yes' And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
        //SSQL = SSQL + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EM.CatName='" + Category + "' and EM.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EM.Wages!='CONTRACTCLEANING I' and EM.Wages!='CONTRACTCLEANING II'";
        }
        SSQL = SSQL + " and CONVERT (datetime,AD.FromDate,103)>=CONVERT(datetime,'" + FromDate + "',103) and";
        SSQL = SSQL + " CONVERT(datetime,AD.ToDate,103)<=CONVERT(datetime,'" + ToDate + "',103)";
        SSQL = SSQL + " group by  EM.Gender,AD.Months";
        SSQL = SSQL + " ) as PVT";
        SSQL = SSQL + " PIVOT(";
        SSQL = SSQL + " sum(Male) FOR Months in([January],[February],[March],[April],[May],[June],[July],[August],[September],[October],[November],[December])";
        SSQL = SSQL + " ) AS pt";


    
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/ManWork.rpt"));
            rd.SetDataSource(dt);

             
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void OT_Amt()
    {
        SSQL = "";

        SSQL = SSQL + " select EmpDet.ExistingCode,EmpDet.EmpNo,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,(EmpDet.OT_Salary) as Others ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " where EmpDet.IsActive='Yes' and EmpDet.OTEligible='1' ";
        SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,EmpDet.OT_Salary ";
        SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/OTamt.rpt"));
            rd.SetDataSource(dt);

            rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
            rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void Others_Pay_Load()
    {
        SSQL = "";

        SSQL = SSQL + " select EmpDet.ExistingCode,EmpDet.EmpNo,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,(EmpDet.AccountNo) as AccountNo,SalDet.ProvidentFund,(SalDet.TotalBasicAmt - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as GrossEarningsOT, ";
        SSQL = SSQL + " SalDet.ESI,(SalDet.IncomeTax) as TotDed,(SalDet.CommsAmt) AS CommsAmt,(SalDet.TempleAmt) as Deduction2,(SalDet.HouseAmt) as Deduction1,SalDet.Hloan,";
        SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,(SalDet.Advance) as Adv,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,(SalDet.Deduction3) as Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,(EmpDet.PFNo) as PF,SalDet.WorkedDays,SalDet.Basic_SM,(SalDet.NFh) as OTHoursNew,(SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax) as TotalDeductions,";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,(SalDet.allowances5) as Advance,(SalDet.NetPay) as NetPay,(SalDet.Fbasic) as BasicandDA,SalDet.Deduction5 ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "' and ";
        SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and Salary_Through='2' ";

        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }

        SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,EmpDet.AccountNo,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo,SalDet.Basic_SM,SalDet.NFh,SalDet.Fbasic, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.NetPay,SalDet.WorkedDays ";
        SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/OthersPay.rpt"));
            rd.SetDataSource(dt);

             
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void Bank_Amt()
    {
        SSQL = "";

        SSQL = SSQL + " select ROW_NUMBER() OVER(ORDER BY EmpDet.EmpNo) as [S.No], EmpDet.EmpNo as [Code],(EmpDet.FirstName) as EmpName,(EmpDet.AccountNo) as [A/C NO], ";
        //SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,(SalDet.Advance) as Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,(SalDet.Deduction3) as Deduction3,SalDet.Deduction4,";
        //SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " (SalDet.NetPay + SalDet.HRAamt + SalDet.TAamt + SalDet.OTHoursAmtNew + SalDet.withpay) as Others ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "' and ";
        SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and Salary_Through='2' ";

        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }

        SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,EmpDet.AccountNo,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.NetPay, ";
        SSQL = SSQL + " SalDet.HRAamt,SalDet.TAamt,SalDet.OTHoursAmtNew,SalDet.withpay";
        SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";


        //For PDF
        //SSQL = SSQL + " select  EmpDet.EmpNo ,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,(EmpDet.AccountNo) as ESINo,SalDet.ProvidentFund,(SalDet.IncomeTax) as IncomeTax,(SalDet.CommsAmt) AS CommsAmt,(SalDet.TempleAmt) as TempleAmt,(SalDet.HouseAmt) as HouseAmt,SalDet.Hloan, ";
        //SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,(SalDet.Advance) as Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,(SalDet.Deduction3) as Deduction3,SalDet.Deduction4,";
        //SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        //SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,(SalDet.NetPay + SalDet.HRAamt + SalDet.TAamt + SalDet.OTHoursAmtNew + SalDet.withpay) as Others ";
        //SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        //SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        //SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "' and ";
        //SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        //SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        //SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and Salary_Through='2' ";

        //if ((Category != "") && (EmpType != ""))
        //{
        //    SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        //}
        //else
        //{
        //    SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        //}

        //SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,EmpDet.AccountNo,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        //SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        //SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        //SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.NetPay, ";
        //SSQL = SSQL + " SalDet.HRAamt,SalDet.TAamt,SalDet.OTHoursAmtNew,SalDet.withpay";
        //SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }


            grid.DataSource = dt;
            grid.DataBind();
            string attachment = "attachment;filename=BANKPAYMENTREPORT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");

            if (Category != "CONTRACTCLEANING I" && Category != "CONTRACTCLEANING II")
            {
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan=" + dt.Columns.Count + ">");
                Response.Write("<a style=\"font-weight:bold\">" + CmpName + "</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan=" + dt.Columns.Count + ">");
                Response.Write("<a style=\"font-weight:bold\">" + Cmpaddress + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
            }

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan=" + dt.Columns.Count + ">");
            Response.Write("<a style=\"font-weight:bold\">BANK PAYMENT REPORT FOR THE MONTH OF " + Months + "-" + FinYear.ToString().Split('-').Last() + "</a>");

            Response.Write("</td>");

            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='right'>");
            Response.Write("<td colspan=" + dt.Columns.Count + ">");
            Response.Write("<a style=\"font-weight:bold\">FINANCIAL YEAR - " + FinYear + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.Write("<table border=1>");
            Response.Write("<tr Font-Bold='true'>");
            Response.Write("<td colspan=4 align='Right'>");
            Response.Write("<b>TOTAL</b>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<b>=sum(E6:E" + Convert.ToInt32(dt.Rows.Count + 5) + ")</b>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.End();
            Response.Clear();




            ////FOR PDF REPORT
            //rd.Load(Server.MapPath("Payslip_New_Component/BankPayment.rpt"));
            //rd.SetDataSource(dt);


            //if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            //{
            //    rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            //    //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
            //    rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            //}
            //else
            //{
            //    rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
            //    //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
            //    rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            //}
            //rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            //rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            //rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            //CrystalReportViewer1.ReportSource = rd;
            //CrystalReportViewer1.RefreshReport();
            //CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void Comm_Ded_Load()
    {
        SSQL = "";

        SSQL = SSQL + " select EmpDet.ExistingCode,EmpDet.EmpNo,(EmpDet.FirstName) as EmpName,(EmpDet.DeptName) as DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,(SalDet.IncomeTax) as IncomeTax,(SalDet.CommsAmt) AS Others,(SalDet.TempleAmt) as TempleAmt,(SalDet.HouseAmt) as HouseAmt,SalDet.Hloan, ";
        SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,(SalDet.Advance) as Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,(SalDet.Deduction3) as Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "' and ";
        SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and SalDet.CommsAmt<>'0' ";

        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }

        SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/Comms.rpt"));
            rd.SetDataSource(dt);

             
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";
            rd.DataDefinition.FormulaFields["EmpType"].Text = "'" + EmpType + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void Advance_Ded_Load()
    {
        SSQL = "";

        SSQL = SSQL + " select EmpDet.ExistingCode,EmpDet.EmpNo,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,(SalDet.IncomeTax) as IncomeTax,SalDet.CommsAmt,(SalDet.TempleAmt) as TempleAmt,(SalDet.HouseAmt) as HouseAmt,SalDet.Hloan, ";
        SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,(SalDet.Advance) as Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,(SalDet.Advance3) as Others,(SalDet.Deduction3) as Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "' and ";
        SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and SalDet.Advance3<>'0' ";

        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }

        SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/Advance.rpt"));
            rd.SetDataSource(dt);

              
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void MIC2_Ded_Load()
    {
        SSQL = "";

        SSQL = SSQL + " select EmpDet.ExistingCode,EmpDet.EmpNo,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,(SalDet.IncomeTax) as IncomeTax,SalDet.CommsAmt,(SalDet.TempleAmt) as TempleAmt,(SalDet.HouseAmt) as HouseAmt,SalDet.Hloan, ";
        SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,(SalDet.Advance) as Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,(SalDet.Deduction3) as Deduction3,(SalDet.Deduction4) as Others,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "' and ";
        SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and SalDet.Deduction4<>'0' ";

        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }

        SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/MIC2.rpt"));
            rd.SetDataSource(dt);

           
             
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void MIC4_Ded_Load()
    {
        SSQL = "";

        SSQL = SSQL + " select EmpDet.ExistingCode,EmpDet.EmpNo,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,(SalDet.IncomeTax) as IncomeTax,SalDet.CommsAmt,(SalDet.TempleAmt) as TempleAmt,(SalDet.HouseAmt) as HouseAmt,SalDet.Hloan, ";
        SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,(SalDet.Advance) as Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,(SalDet.Deduction3) as Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " (SalDet.Deduction5) as Others,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "' and ";
        SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and SalDet.Deduction5<>'0' ";

        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }

        SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/MIC4.rpt"));
            rd.SetDataSource(dt);
            
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void Hloan_Ded_Load()
    {
        SSQL = "";

        SSQL = SSQL + " select EmpDet.ExistingCode,EmpDet.EmpNo,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,(SalDet.IncomeTax) as IncomeTax,SalDet.CommsAmt,(SalDet.TempleAmt) as TempleAmt,(SalDet.HouseAmt) as HouseAmt,(SalDet.Hloan) as Others, ";
        SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,(SalDet.Advance) as Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,(SalDet.Deduction3) as Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "' and ";
        SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and SalDet.Hloan<>'0' ";

        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }

        SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/Hlon.rpt"));
            rd.SetDataSource(dt);

             
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void Stores_Ded_Load()
    {
        SSQL = "";

        SSQL = SSQL + " select EmpDet.ExistingCode,EmpDet.EmpNo,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,(SalDet.IncomeTax) as IncomeTax,SalDet.CommsAmt,(SalDet.TempleAmt) as TempleAmt,(SalDet.HouseAmt) as HouseAmt,SalDet.Hloan, ";
        SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,(SalDet.Deduction3) as Others,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "' and ";
        SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and SalDet.Deduction3<>'0' ";

        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }

        SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/Store_Ded.rpt"));
            rd.SetDataSource(dt);

            
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void Wages_Abstract()
    {
        //SSQL = "";
        if (Category.ToString().ToUpper() != "STAFF")
        {
            SSQL = "";
            SSQL = "Select EM.DeptName";
            SSQL = SSQL + ", COUNT(NULLIF(0,SD.DaFixed)) as NOPDA,COUNT(NULLIF(0,SD.DaRateAmt)) as NOPADA,COUNT(NULLIF(0,SD.ProvidentFund)) as NOPEPF,COUNT(NULLIF(0,SD.ESI)) as NOPESI,COUNT(NULLIF(0,SD.IncomeTax)) as NOPLIC";
            SSQL = SSQL + ", COUNT(NULLIF(0,SD.DayIncentive)) as NOPHRA, COUNT(NULLIF(0,SD.CommsAmt)) as NOPCOMN,SUM(ISNULL(SD.Fbasic,0)) as BASIC, SUM(ISNULL(SD.DaFixed,0)) as DA,SUM(ISNULL(SD.DaRateAmt,0)) as ADA";
            SSQL = SSQL + ", SUM(ISNULL(SD.TotalBasicAmt,0)) as GROSS,SUM(ISNULL(SD.ProvidentFund,0)) as EPF,SUM(ISNULL(SD.ESI,0)) as ESI";
            SSQL = SSQL + ", SUM(ISNULL(SD.IncomeTax,0)) as LIC,0.00 as PRS,0.00 as ITAX, (SUM(ISNULL(SD.ProvidentFund,0))+SUM(ISNULL(SD.ESI,0))+SUM(ISNULL(SD.IncomeTax,0))) as TOTDED";
            SSQL = SSQL + ", SUM(ISNULL(SD.DayIncentive,0)) as HRA,SUM(ISNULL(SD.CommsAmt,0)) as COMN, SUM(ISNULL(SD.NetPay,0)) as NETT from Employee_Mst EM";
            SSQL = SSQL + " Inner Join [" + SessionPayroll + "]..SalaryDetails SD on SD.EmpNo=EM.EmpNo and SD.Ccode=EM.Compcode and SD.Lcode=EM.LocCode where EM.CatName='" + Category.ToUpper() + "' and EM.Wages='" + EmpType + "'";
            SSQL = SSQL + " and SD.FinancialYear='" + FinYear + "' and SD.Month='" + Months + "' and Convert(datetime,SD.FromDate,103)>=Convert(datetime,'" + FromDate + "',103)";
            SSQL = SSQL + " and Convert(datetime,ToDate,103)<=Convert(datetime,'" + ToDate + "',103) and EM.Compcode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and SD.Ccode='" + SessionCcode + "' and SD.Lcode='" + SessionLcode + "' and SD.Availabledays<>'0'";
            SSQL = SSQL + " group by EM.DeptName";
        }
        else
        {
            SSQL = "";
            SSQL = "Select EM.DeptName";
            SSQL = SSQL + ", COUNT(NULLIF(0,SD.DaFixed)) as NOPDA,COUNT(NULLIF(0,SD.DaRateAmt)) as NOPADA,COUNT(NULLIF(0,SD.ProvidentFund)) as NOPEPF,COUNT(NULLIF(0,SD.ESI)) as NOPESI,COUNT(NULLIF(0,SD.IncomeTax)) as NOPLIC";
            SSQL = SSQL + ", COUNT(NULLIF(0,SD.HRAamt)) as NOPHRA, COUNT(NULLIF(0,SD.TAamt)) as NOPCOMN,SUM(ISNULL(SD.Fbasic,0)) as BASIC, SUM(ISNULL(SD.DaFixed,0)) as DA,SUM(ISNULL(SD.DaRateAmt,0)) as ADA";
            SSQL = SSQL + ", SUM(ISNULL(SD.TotalBasicAmt,0)) as GROSS,SUM(ISNULL(SD.ProvidentFund,0)) as EPF,SUM(ISNULL(SD.ESI,0)) as ESI";
            SSQL = SSQL + ", SUM(ISNULL(SD.IncomeTax,0)) as LIC,0.00 as PRS,0.00 as ITAX, (SUM(ISNULL(SD.ProvidentFund,0))+SUM(ISNULL(SD.ESI,0))+SUM(ISNULL(SD.IncomeTax,0))) as TOTDED";
            SSQL = SSQL + ", SUM(ISNULL(SD.HRAamt,0)) as HRA,SUM(ISNULL(SD.TAamt,0)) as COMN, SUM(ISNULL(SD.NetPay,0)) as NETT from Employee_Mst EM";
            SSQL = SSQL + " Inner Join [" + SessionPayroll + "]..SalaryDetails SD on SD.EmpNo=EM.EmpNo and SD.Ccode=EM.Compcode and SD.Lcode=EM.LocCode where EM.CatName='" + Category.ToUpper() + "' ";
            if (EmpType != "")
            {
                SSQL = SSQL + "  and EM.Wages = '" + EmpType + "'";
            }
            else
            {
                SSQL = SSQL + " and EM.Wages!='CONTRACTCLEANING I' and EM.Wages!='CONTRACTCLEANING II'";
            }
            SSQL = SSQL + " and SD.FinancialYear='" + FinYear + "' and SD.Month='" + Months + "' and Convert(datetime,SD.FromDate,103)>=Convert(datetime,'" + FromDate + "',103)";
            SSQL = SSQL + " and Convert(datetime,ToDate,103)<=Convert(datetime,'" + ToDate + "',103) and EM.Compcode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and SD.Ccode='" + SessionCcode + "' and SD.Lcode='" + SessionLcode + "' and SD.Availabledays<>'0'";
            SSQL = SSQL + " group by EM.DeptName";
        }

        ////SSQL = SSQL + " select EmpDet.ExistingCode,EmpDet.EmpNo,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,(SalDet.IncomeTax) as IncomeTax,SalDet.CommsAmt,(SalDet.TempleAmt) as Others,(SalDet.HouseAmt) as HouseAmt,SalDet.Hloan, ";
        ////SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        ////SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        ////SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        ////SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        ////SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        ////SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "' and  ";
        ////SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        ////SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        ////SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and SalDet.TempleAmt<>'0' ";

        //if ((Category != "") && (EmpType != ""))
        //{
        //    SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        //}

        //SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        //SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        //SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        //SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        //SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/WagesAbs.rpt"));
            rd.SetDataSource(dt);
            
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void Temple_Ded_Load()
    {
        SSQL = "";

        SSQL = SSQL + " select (EmpDet.DoorNo) as ExisistingCode,EmpDet.EmpNo,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,(SalDet.IncomeTax) as IncomeTax,SalDet.CommsAmt,(SalDet.TempleAmt) as Others,(SalDet.HouseAmt) as HouseAmt,SalDet.Hloan, ";
        SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "' and  ";
        SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and SalDet.TempleAmt<>'0' ";

        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }
        SSQL = SSQL + " group by EmpDet.DoorNo,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " Order by cast(EmpDet.EmpNo as int) Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/Temple_Ded.rpt"));
            rd.SetDataSource(dt);

              
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void Home_Ded_Load()
    {
        SSQL = "";

        SSQL = SSQL + " select EmpDet.EmpNo,EmpDet.DoorNo as ExisistingCode,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,(SalDet.IncomeTax) as IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,(SalDet.HouseAmt) as Others,SalDet.Hloan, ";
        SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "' and ";
        SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and SalDet.HouseAmt<>'0' ";

        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }

        SSQL = SSQL + " group by EmpDet.DoorNo,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " Order by cast(EmpDet.EmpNo as int) Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/Home_Ded.rpt"));
            rd.SetDataSource(dt);

              
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    
    }
    public void LIC_Ded_Load()
    {
        SSQL = "";

        SSQL = SSQL + " select EmpDet.ExistingCode,EmpDet.EmpNo,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,(SalDet.IncomeTax) as Others,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan, ";
        SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "' and ";
        SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and SalDet.IncomeTax<>'0' ";

        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }


        SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/LIC_Ded.rpt"));
            rd.SetDataSource(dt);

            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void PF_Ded_Load()
    {

        SSQL = "";

        SSQL = SSQL + " select EmpDet.ExistingCode,EmpDet.EmpNo,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan, ";
        SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.EmployeerPFone,SalDet.EmployeerPFTwo,";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "'  and ";
        SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";
        
        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }
        
        
        SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo,SalDet.EmployeerPFone,SalDet.EmployeerPFTwo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/PF_Ded.rpt"));
            rd.SetDataSource(dt);


          
            if (EmpType != "CONTRACTCLEANING I" && EmpType != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "''";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "''";
            }
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }

    public void ESI_Ded_Load()
    {
        SSQL = "";

        SSQL = SSQL + " Select EmpDet.ExistingCode,EmpDet.EmpNo,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan, ";
        SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo,EmpDet.ESINo,SalDet.EmployeerESI, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5 ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        SSQL = SSQL + " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + FinYear + "' and  ";
        SSQL = SSQL + " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + FinYear + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_ESI='1' ";
        if ((Category != "") && (EmpType != ""))
        {
            SSQL = SSQL + " and EmpDet.CatName='" + Category + "' and EmpDet.Wages='" + EmpType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }

        SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo,SalDet.EmployeerESI, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,EmpDet.ESINo ";
        SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            rd.Load(Server.MapPath("Payslip_New_Component/ESI_Ded.rpt"));
            rd.SetDataSource(dt);

            rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
            rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            rd.DataDefinition.FormulaFields["Month"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + RptYear + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    
}
