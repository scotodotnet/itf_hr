﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportRights.aspx.cs" Inherits="ReportRights" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>
<asp:UpdatePanel runat="server">
<ContentTemplate>
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">User Creation </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Master</a></li>
                    <li class="breadcrumb-item active" aria-current="page">User Creations</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="forms-sample">
                            <div class="row">
                                <!-- begin col-4 -->
                                <div class="form-group col-md-3">
                                    <label for="exampleInputName">User Name</label>
                                    <asp:DropDownList ID="txtUserName" runat="server" class="js-example-basic-single" Style="width: 100%"
                                        AutoPostBack="true" OnSelectedIndexChanged="txtUserName_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                                <!-- end col-4 -->
                                <!-- begin col-4 -->
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName">Report Type</label>
                                    <asp:DropDownList ID="txtReportType" runat="server" class="js-example-basic-single" Style="width: 100%" AutoPostBack="true"
                                        OnSelectedIndexChanged="txtReportType_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                                <!-- end col-4 -->


                                <div class="form-group col-md-1">
                                    <br />
                                    <asp:Button ID="btnView" Width="50" Height="30" class="btn-success" runat="server" Text="View" ValidationGroup="Item_Validate_Field" OnClick="btnView_Click" />
                                </div>
                            </div>
                            <!-- end row -->
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <asp:CheckBox ID="chkAll" runat="server" Text="Select / UnSelect" Visible="true"
                                            OnCheckedChanged="chkAll_CheckedChanged" AutoPostBack="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <asp:Panel ID="GVPanel" runat="server" ScrollBars="None" Visible="true">
                                        <asp:GridView ID="GVModule" runat="server" AutoGenerateColumns="false"
                                            ClientIDMode="Static" class="gvv display table">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Report Name" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ReportID" runat="server" Text='<%# Eval("ReportID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ReportName" HeaderText="Report Name" />
                                                <asp:TemplateField HeaderText="Add">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                </div>
                            </div>
                            <!-- Button start -->
                            <div class="col-md-12">
                                <div class="row">
                                    <table align="center">
                                        <tr>
                                            <td>

                                                <asp:Button ID="btnSave" class="btn btn-success" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                                <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                                <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- Button end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- content-wrapper ends -->
    <!-- partial:../../partials/_footer.html -->
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
        </div>
    </footer>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

