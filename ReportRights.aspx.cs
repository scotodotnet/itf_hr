﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class ReportRights : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionCompanyName;
    string SessionAdmin;
    string SessionLocationName;
    string SessionRights;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionRights = Session["Rights"].ToString();
        //SessionCompanyName = Session["CompanyName"].ToString();
        //SessionLocationName = Session["LocationName"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: User Form Rights";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Module_Name_MenuName_And_UserName_Add();
            Load_Report_Details();
        }
    }

    private void Module_Name_MenuName_And_UserName_Add()
    {
        //User Name Add
        DataTable dtUser = new DataTable();
        string query = "";
        txtUserName.Items.Clear();
        query = "Select * from [" + SessionRights + "]..MstUsers where CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "' And UserCode <> 'Scoto' And IsAdmin <> '1' And IsAdmin <> '4' order by UserCode Asc";
        dtUser = objdata.RptEmployeeMultipleDetails(query);
        txtUserName.DataSource = dtUser;
        txtUserName.DataTextField = "UserCode";
        txtUserName.DataValueField = "UserCode";
        txtUserName.DataBind();
        txtUserName.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

        //Module Name Add
        DataTable dtcate = new DataTable();
        txtReportType.Items.Clear();
        query = "Select Distinct ReportType from [" + SessionRights + "]..ReportType_Name";
        dtcate = objdata.RptEmployeeMultipleDetails(query);
        txtReportType.DataSource = dtcate;
        txtReportType.DataTextField = "ReportType";
        txtReportType.DataValueField = "ReportType";
        txtReportType.DataBind();

        ////Menu Name Add
        //DataTable dtMenu = new DataTable();
        //query = "Select * from [" + SessionRights + "]..Company_Module_MenuHead_Rights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' order by MenuName Asc";
        //dtMenu = objdata.RptEmployeeMultipleDetails(query);
        //txtMenuName.DataSource = dtMenu;
        //txtMenuName.DataTextField = "MenuName";
        //txtMenuName.DataValueField = "MenuID";
        //txtMenuName.DataBind();
    }

    private void Load_Report_Details()
    {

        string query = "";
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;

        string Module_ID_Encrypt = "";
        Module_ID_Encrypt = txtReportType.SelectedValue.ToString();

        
        DataTable DT = new DataTable();
        query = "Select ReportID,ReportName from [" + SessionRights + "]..ReportType_Name where ReportType='" + Module_ID_Encrypt + "'";
        query = query + " order by ReportID Asc";
        DT = objdata.RptEmployeeMultipleDetails(query);
        GVModule.DataSource = DT;
        GVModule.DataBind();

        if (GVModule.Rows.Count != 0)
        {
            GVPanel.Visible = true;
        }
        else
        {
            GVPanel.Visible = false;
        }
        Load_Report_Rights();

    }

    private void Load_Report_Rights()
    {
        chkAll.Visible = true;
        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            string ModuleName = txtReportType.SelectedItem.ToString();
            //string ModuleID = Decrypt(txtModuleName.SelectedValue.ToString()).ToString();
            string ModuleID_Encrypt = txtReportType.SelectedValue.ToString();

            
            string UserName = txtUserName.SelectedItem.Text.ToString();

            string FormName = "";
            string FormID = "0";
            string FormID_Encrypt = "0";

            Label FormID_lbl = (Label)gvsal.FindControl("ReportID");
            FormID = FormID_lbl.Text.ToString();
            FormName = gvsal.Cells[1].Text.ToString();
            FormID_Encrypt = FormID.ToString();
            //Get Company Module Rights
            string query = "";
            DataTable DT = new DataTable();
            query = "Select * from [" + SessionRights + "]..Report_User_Rights where ReportType='" + ModuleID_Encrypt + "' And ReportID='" + FormID_Encrypt + "' And UserName='" + UserName + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //User Rights Update in Grid
                //((CheckBox)gvsal.FindControl("chkSelect")).Checked = true;

                //User Rights Update in Grid
                if (DT.Rows[0]["AddRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkSelect")).Checked = true; }
                //if (DT.Rows[0]["ModifyRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkModify")).Checked = true; }
                //if (DT.Rows[0]["DeleteRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkDelete")).Checked = true; }
                //if (DT.Rows[0]["ViewRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkView")).Checked = true; }
                //if (DT.Rows[0]["ApproveRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkApprove")).Checked = true; }
                //if (DT.Rows[0]["PrintRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkPrintout")).Checked = true; }
            }
        }
    }

    protected void txtUserName_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Menu Name Add
        if (txtReportType.SelectedValue != "")
        {
            btnView_Click(sender, e);
        }
    }
    protected void txtReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Report_Details();
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        Load_Report_Details();
    }
    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            //User Rights Update in Grid
            if (chkAll.Checked == true)
            {
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = true;
            }
            else
            {
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = false;
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        //GVModule.Rows[1].FindControl("chkAdd"). = "true";

        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "";
        bool ErrFlag = false;

        string Module_ID_Encrypt = "0";
        string Module_ID = "0";
        string Module_Name = "0";

        string Menu_ID_Encrypt = "0";
        string Menu_ID = "0";
        string Menu_Name = "0";

        string UserName = "";

        //Module_ID = Decrypt(txtModuleName.SelectedValue.ToString()).ToString();
        Module_Name = txtReportType.SelectedItem.ToString();
        Module_ID_Encrypt = txtReportType.SelectedValue.ToString();

      

        UserName = txtUserName.SelectedValue.ToString();

        query = "Delete from [" + SessionRights + "]..Report_User_Rights where ReportType='" + Module_ID_Encrypt + "' And UserName='" + UserName + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        objdata.RptEmployeeMultipleDetails(query);

        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            SaveMode = "Insert";


            string Form_ID_Encrypt = "0";
            string Form_ID = "0";
            string Form_Name = "0";

            CheckBox ChkSelect_chk = (CheckBox)gvsal.FindControl("chkSelect");
            string AddCheck = "0";
            string ModifyCheck = "0";
            string DeleteCheck = "0";
            string ViewCheck = "0";
            string ApproveCheck = "0";
            string PrintCheck = "0";


            //CheckBox ChkModify = (CheckBox)gvsal.FindControl("chkModify");
            //CheckBox ChkDelete = (CheckBox)gvsal.FindControl("chkDelete");
            //CheckBox ChkView = (CheckBox)gvsal.FindControl("chkView");
            //CheckBox ChkApprove = (CheckBox)gvsal.FindControl("chkApprove");
            //CheckBox ChkPrint = (CheckBox)gvsal.FindControl("chkPrintout");

            if (ChkSelect_chk.Checked == true) { AddCheck = "1"; }
            //if (ChkModify.Checked == true) { ModifyCheck = "1"; }
            //if (ChkDelete.Checked == true) { DeleteCheck = "1"; }
            //if (ChkView.Checked == true) { ViewCheck = "1"; }
            //if (ChkApprove.Checked == true) { ApproveCheck = "1"; }
            //if (ChkPrint.Checked == true) { PrintCheck = "1"; }

            Label FormID_lbl = (Label)gvsal.FindControl("ReportID");
            Form_ID = FormID_lbl.Text.ToString();
            Form_Name = gvsal.Cells[1].Text.ToString();
            Form_ID_Encrypt = Form_ID.ToString();

            //if (ChkSelect_chk.Checked == true || ChkModify.Checked == true || ChkDelete.Checked == true || ChkView.Checked == true || ChkApprove.Checked == true || ChkPrint.Checked == true)
            if (ChkSelect_chk.Checked == true)
            {
                //Get Menu LIST
                string Menu_LI_ID = "";
                string Form_LI_ID = "";
                DataTable dtID = new DataTable();
              

                //Insert User Rights
                query = "Insert Into [" + SessionRights + "]..Report_User_Rights(CompCode,LocCode,ReportType,ReportID,ReportName,UserName,AddRights) Values('" + SessionCcode + "','" + SessionLcode + "','" + Module_ID_Encrypt + "',";
                query = query + " '" + Form_ID_Encrypt + "','" + Form_Name + "','" + UserName + "','" + AddCheck + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
        }

        if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Report Rights Details Saved Successfully');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Report Rights Details Not Saved Properly...');", true);
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
    }

    private void Clear_All_Field()
    {
        chkAll.Checked = false;
        Load_Report_Details();
    }

}
