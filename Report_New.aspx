﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Report_New.aspx.cs" Inherits="Report_New" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
          <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
     <script  type="text/javascript">
     $(document).ready(function() {
     $('#example').dataTable();
     $('.select2').select2();
     });
	</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
            }
        });
    };</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:UpdatePanel runat="server" EnableViewState="true">
        <ContentTemplate>
            <div class="content-wrapper">
                <div class="page-header">
                    <h3 class="page-title">Report</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Report</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Report</li>
                        </ol>
                    </nav>
                </div>
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="forms-sample">
                                    <div class="row">
                                        <div class="form-group col-lg-4">
                                            <label for="exampleInputName">Report Name</label>
                                            <asp:DropDownList ID="ddlRptName" runat="server"
                                                class="js-example-basic-single select2" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlRptName_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Mill Code</label>
                                            <asp:DropDownList ID="ddlMillCode" AutoPostBack="true" OnSelectedIndexChanged="ddlMillCode_SelectedIndexChanged" runat="server" class="js-example-basic-single select2">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Unit Code</label>
                                            <asp:DropDownList ID="ddlIUnitCode" AutoPostBack="true" OnSelectedIndexChanged="ddlIUnitCode_SelectedIndexChanged" class="js-example-basic-single select2" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                          <div class="form-group col-md-3">
                                            <label for="exampleInputName">Department</label>
                                            <asp:DropDownList ID="ddlDepartment" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_TextChanged" runat="server" class="form-control select2">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Designation</label>
                                            <asp:DropDownList ID="ddldesignation" AutoPostBack="true" OnSelectedIndexChanged="ddldesignation_SelectedIndexChanged" runat="server" class="form-control select2">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                     <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">ITF No</label>
                                            <asp:DropDownList ID="ddlITFNo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlITFNo_SelectedIndexChanged" class="form-control select2">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Participant Name</label>
                                            <asp:TextBox ID="txtEmpName" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                          <div class="form-group col-md-3">
                                            <label for="exampleInputName">From Date</label>
                                            <asp:TextBox runat="server" ID="txtFrmdate" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                           
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">To Date</label>
                                            <asp:TextBox runat="server" ID="txtTodate" class="form-control datepicker " placeholder="dd/MM/YYYY"></asp:TextBox>
                                           
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
                </div>
            </footer>
        </ContentTemplate>
    </asp:UpdatePanel>
     <script src=" https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src=" https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
</asp:Content>

