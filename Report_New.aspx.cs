﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Drawing;
using Payroll;
public partial class Report_New : System.Web.UI.Page
{

    System.Web.UI.WebControls.DataGrid grid =
                     new System.Web.UI.WebControls.DataGrid();
    String CurrentYear1;
    static int CurrentYear;
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode; string SessionUser;
    string SessionDivision; string SessionRights;
    BALDataAccess objdata = new BALDataAccess();
    //string SSQL = "";
    string ss1 = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionUserType;
    string Date1_str;
    string Date2_str;
    string Wages_Type;
    bool Errflag = false;
    string SSQL = "";
    bool Errflag1 = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            SessionRights = Session["Rights"].ToString();
            SessionUser = Session["Usernmdisplay"].ToString();

            con = new SqlConnection(constr);

            if (!IsPostBack)
            {
                Load_Mill();
                //Load_Course();
            }
        }
    }
    protected void Load_Mill()
    {
        SSQL = "";
        SSQL = "Select (MillCode +' - '+ MillName) as MillName,MillCode from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlMillCode.DataSource = dt;
        ddlMillCode.DataTextField = "MillName";
        ddlMillCode.DataValueField = "MillCode";
        ddlMillCode.DataBind();
        ddlMillCode.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void ddlRptName_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ddlMillCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_unit(ddlMillCode.SelectedValue);
    }
    protected void Load_unit(string MillCode)
    {
        SSQL = "";
        SSQL = "Select (UnitName+'-'+UnitTypeName) as UnitName,UnitCode from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + MillCode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count <= 0)
        {
            SSQL = "";
            SSQL = "select Units from MstMill where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Millcode='" + MillCode + "'";
            DataTable Dt_get = new DataTable();
            Dt_get = objdata.RptEmployeeMultipleDetails(SSQL);

            DataTable Unit_dt = new DataTable();

            Unit_dt.Columns.Add("UnitCode");
            Unit_dt.Columns.Add("UnitName");

            int Unitcode = 0;
            if (Dt_get != null && Dt_get.Rows.Count > 0)
            {

                Unitcode = Convert.ToInt32(Dt_get.Rows[0]["Units"]);
                DataRow dr;

                for (int i = 1; i <= Unitcode; i++)
                {
                    dr = Unit_dt.NewRow();
                    dr["UnitCode"] = "UNIT " + i;
                    dr["UnitName"] = "UNIT " + i;
                    Unit_dt.Rows.Add(dr);
                }

                if (Unit_dt.Rows.Count > 0)
                {
                    ddlIUnitCode.DataSource = Unit_dt;
                    ddlIUnitCode.DataTextField = "UnitName";
                    ddlIUnitCode.DataValueField = "UnitCode";
                    ddlIUnitCode.DataBind();
                    //ddlIUnitCode.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
                }
            }
        }
        else
        {

            ddlIUnitCode.DataSource = dt;
            ddlIUnitCode.DataTextField = "UnitName";
            ddlIUnitCode.DataValueField = "UnitCode";
            ddlIUnitCode.DataBind();
            //ddlIUnitCode.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }

    protected void ddlIUnitCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIUnitCode.SelectedItem.Text != "-Select-")
        {
            SSQL = "";
            SSQL = "Select MillCode,UnitCode from MstMillUnits where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MillCode='" + ddlMillCode.SelectedValue + "'";
            SSQL = SSQL + " and UnitCode='" + ddlIUnitCode.SelectedValue + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt != null && dt.Rows.Count > 0)
            {

                Load_DeptName(dt.Rows[0]["MillCode"].ToString(), dt.Rows[0]["UnitCode"].ToString());
            }
        }
    }
    protected void Load_DeptName(string MillCode, string UnitCode)
    {
        SSQL = "";
        SSQL = "Select * from  Department_Mst where MillCode='" + MillCode + "' and UnitCode='" + UnitCode + "'";
        ddlDepartment.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlDepartment.DataValueField = "DeptCode";
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataBind();
        ddlDepartment.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }
    protected void ddlDepartment_TextChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from Designation_Mst where DeptCode='" + ddlDepartment.SelectedValue + "' and UnitCode='" + ddlIUnitCode.SelectedValue + "' and MillCode='" + ddlMillCode.SelectedValue + "'";
        ddldesignation.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddldesignation.DataTextField = "DesignName";
        ddldesignation.DataValueField = "DesignCode";
        ddldesignation.DataBind();
        ddldesignation.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void ddldesignation_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddldesignation.SelectedValue != "-Select-")
        {
            SSQL = "";
            SSQL = "Select (MachineID+'-'+FirstName) as ITFName,MachineID as ITFID from Employee_Mst where CompCode='" + SessionCcode + "' and LocCOde='" + SessionLcode + "' and MillCode='" + ddlMillCode.SelectedValue + "'";
            SSQL = SSQL + " and UnitCode='" + ddlIUnitCode.SelectedValue + "' and DeptCode='" + ddlDepartment.SelectedValue + "' and DesignCode='" + ddldesignation.SelectedValue + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlITFNo.DataSource = dt;
            ddlITFNo.DataTextField = "ITFName";
            ddlITFNo.DataValueField = "ITFID";
            ddlITFNo.DataBind();
            ddlITFNo.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }
    protected void ddlITFNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlITFNo.SelectedValue != "-Select-")
        {
            txtEmpName.Text = ddlITFNo.SelectedItem.Text.ToString().Split('-').Last();
        }
        else
        {
            txtEmpName.Text = "";
        }
    }
}