﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
  
public partial class RptCourse_Costing : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string Date = "";
    ReportDocument report = new ReportDocument();

    DataTable Log_DS = new DataTable();
    string SessionPayroll;
    BALDataAccess objdata = new BALDataAccess();

    DataTable AutoDTable = new DataTable();
    DataTable Course1 = new DataTable();
    string CmpName, Cmpaddress;
    string SSQL = "";
    DateTime date1;
    DateTime date2;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string status;
    string MillCode = "";
    string UnitCode = "";
    string DeptCode = "";
    string DesignationCode = "";
    string CourseCode = "";
    string CourseLevel = "";
    string Course;

    DataSet ds = new DataSet();
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | PROGRAM COSTING REPORT ";



            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            //FromDate = Request.QueryString["FromDate"].ToString();

            // DeptCode = Request.QueryString["DeptCode"].ToString();
            DesignationCode = Request.QueryString["DesignationCode"].ToString();
            MillCode = Request.QueryString["MillCode"].ToString();
            UnitCode = Request.QueryString["UnitCode"].ToString();
            CourseCode = Request.QueryString["CourseCode"].ToString();
            CourseCosting();


        }
    }
    public void CourseCosting()
    {
        AutoDTable.Columns.Add("S.No");
        //AutoDTable.Columns.Add("Millcode");
        //AutoDTable.Columns.Add("UnitCode");
        AutoDTable.Columns.Add("Designation");
        AutoDTable.Columns.Add("Amount");
        AutoDTable.Columns.Add("CourseName");
        try {

            //SSQL = "";
            //SSQL = "select Distinct CC.Designation,CC.UnitCode,CC.Amount,CC.CourseName,CC.MillCode, ";
            //SSQL = SSQL + "EM.MillName from Course_Costing CC inner join Employee_Mst EM on EM.MillCode = CC.MillCode inner Join MstMill MM on MM.Millcode=CC.MillCode";
            //SSQL = SSQL + " where CC.Ccode='" + SessionCcode + "' and CC.Lcode='" + SessionLcode + "'";

            SSQL = "";
            SSQL = "select Distinct CC.Designation,CC.UnitCode,CC.Amount,CC.CourseName,CC.MillCode from Course_Costing CC ";
            SSQL = SSQL + " where CC.Ccode='" + SessionCcode + "' and CC.Lcode='" + SessionLcode + "'";

            //if (MillCode != "-Select-")
            //{
            //    SSQL = SSQL + "  and CC.MillCode='" + MillCode + "'";
            //}

            //if ((UnitCode != "") && (UnitCode != "-Select-") )
            //    {
            //    SSQL = SSQL + " and CC.UnitCode='" + UnitCode + "'";
            //}
            if ((DesignationCode != "")  && (DesignationCode != "-Select-") )
        {
            SSQL = SSQL + "and  CC.Designation='" + DesignationCode + "'";
        }
        if (CourseCode != "-Select-")
        {
                string ss = "";
                ss = "Select CourseName from CourseLevel Where CourseCode='" + CourseCode + "'";

                Course1 = objdata.RptEmployeeMultipleDetails(ss);
                Course = Course1.Rows[0]["CourseName"].ToString();

                SSQL = SSQL + " and CC.CourseName='" + Course + "'";
        }

        Log_DS = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Log_DS.Rows.Count != 0)
        {
            for (int i = 0; i < Log_DS.Rows.Count; i++)
            {
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["S.No"] = AutoDTable.Rows.Count;
                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Millcode"] = Log_DS.Rows[i]["MillName"].ToString();
                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["UnitCode"] = Log_DS.Rows[i]["UnitCode"].ToString();

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Designation"] = Log_DS.Rows[i]["Designation"].ToString();

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Amount"] = Log_DS.Rows[i]["Amount"].ToString(); ;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["CourseName"] = Log_DS.Rows[i]["CourseName"].ToString();


            }
            ds.Tables.Add(AutoDTable);
            string CmpName = "";
            string Cmpaddress = "";
            DataTable dt_Cat = new DataTable();


            SSQL = "";
            SSQL = SSQL + "Select CompName,City from Company_Mst";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_Cat.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["CompName"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["City"].ToString());
            }
            report.Load(Server.MapPath("crystal/Course_Costing.rpt"));
            report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            report.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
           // report.DataDefinition.FormulaFields["FromDT"].Text = "'" + Date + "'";
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }
    catch (Exception ex)
        {

        }
    } 
      
          
}