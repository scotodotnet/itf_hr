﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class RptCourselevel : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string Date = "";
    ReportDocument report = new ReportDocument();

    DataTable Log_DS = new DataTable();
    string SessionPayroll;
    BALDataAccess objdata = new BALDataAccess();

    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    string CmpName, Cmpaddress;
    string SSQL = "";
    DateTime date1;
    DateTime date2;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string status;

    string MillCode = "";
    string UnitCode = "";
    string DeptCode = "";
    string DesignationCode = "";
    string CourseCode = "";
    string CourseLevel = "";
    

    string MachineId;
    DataSet ds = new DataSet();
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();

    DataTable dt_Cat = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            // FromDate = Request.QueryString["FromDate"].ToString();
            MachineId = Request.QueryString["MachineId"].ToString();
            //Date = Request.QueryString["Date"].ToString();
            //Date = Request.QueryString["Date"].ToString();
            status = Request.QueryString["CourseStatus"].ToString();

            DeptCode = Request.QueryString["DeptCode"].ToString();
            DesignationCode = Request.QueryString["DesignationCode"].ToString();
            MillCode = Request.QueryString["MillCode"].ToString();
            UnitCode = Request.QueryString["UnitCode"].ToString();
            CourseCode = Request.QueryString["CourseCode"].ToString();
            Courselevel();
        }
    }
    protected void Courselevel()
    {
        SSQL = "";
        SSQL = "select CU.ITFNo,CU.MillName,EM.UnitName,EM.DeptName,EM.Designation,EM.FirstName as Name,CD.CourseName,CU.CourseLevel";
        SSQL = SSQL + ",(DATEDIFF(dd,Convert(datetime,CU.FromDate,105),convert(Datetime,CU.ToDate,105))) as CourseDuration,CU.Status from CourseLevelUpdate CU inner join Employee_Mst EM on EM.MachineID=CU.ITFNo";
        SSQL = SSQL + " inner join MstCourse CD on CD.CourseCode=CU.CourseCode";
        SSQL = SSQL + " where CU.CCode='" + SessionCcode + "' and CU.LCode='" + SessionLcode + "'";
        if(status!="-Select-")
        {
            SSQL = SSQL + " and Cu.Status='" + status + "'";
        }
        if (MachineId != "-Select-")
        {
            SSQL = SSQL + " and CU.ITFNo='" + MachineId + "'";
        }
        if (UnitCode != "-Select-")
        {
            SSQL = SSQL + " and CU.UnitCode='" + UnitCode + "'";

        }
        if (MillCode != "-Select-")
        {
            SSQL = SSQL + " and CU.MillCode='" + MillCode + "'";
        }
        //if ((DeptCode != "-Select-") &&(DeptCode!=null))
        //{
        //    SSQL = SSQL + " and EM.DeptCode='" + DeptCode + "'";
        //}
        if (DesignationCode != "-Select-")
        {
            SSQL = SSQL + " and EM.DesignCode='" + DesignationCode + "'";
        }
        if (CourseCode != "-Select-")
        {
            SSQL = SSQL + " and CU.CourseCode='" + CourseCode.ToString().Split('-').First() + "'";
        }
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if(dt!=null && dt.Rows.Count > 0)
        {
            string CmpName = "";
            string Cmpaddress = "";
            DataTable dt_Cat = new DataTable();
            
            SSQL = "";
            SSQL = SSQL + "Select CompName,City from Company_Mst";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_Cat.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["CompName"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["City"].ToString());
            }

            report.Load(Server.MapPath("crystal/Course_Level.rpt"));
            report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            report.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            //report.DataDefinition.FormulaFields["FromDT"].Text = "'" + Date + "'";
            report.Database.Tables[0].SetDataSource(dt);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }
}