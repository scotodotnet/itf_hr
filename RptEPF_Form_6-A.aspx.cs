﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;

using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class RptEPF_Form_6_A : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string str_cate;
    string str_dept;
    string AgentName;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin; string SessionUserType;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate;
    string ToDate;
    string salaryType;
    string Emp_ESI_Code;
    string SessionPayroll;
    string EmployeeType; string EmployeeTypeCd;
    string PayslipType;
    string Report_Types;

    string PFTypeGet;

    string Left_Employee = "0";
    string Left_Date = "";
    string Get_Report_Type = "0";
    string Salary_CashOrBank;
    string Get_Division_Name = "";

    string Other_state = "";
    string Non_Other_state = "";
    string Basic_Report_Date = "";
    string Basic_Report_Type = "OLD";
    string ExemptedStaff = "";
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        string date_1 = "";
        date_1 = ("01".ToString() + "-" + "12".ToString() + "-" + "2018".ToString()).ToString();
       
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        Load_DB();

        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        //str_cate = Request.QueryString["Cate"].ToString();

        str_month = Request.QueryString["Months"].ToString();
        str_yr = Request.QueryString["yr"].ToString();
        fromdate = Request.QueryString["fromdate"].ToString();
        ToDate = Request.QueryString["ToDate"].ToString();
        //Report_Types = Request.QueryString["ReportFormat"].ToString();
        //AgentName = Request.QueryString["AgentName"].ToString();

        //salaryType = Request.QueryString["Salary"].ToString();
       // EmployeeTypeCd = Request.QueryString["EmpTypeCd"].ToString();
        EmployeeType = Request.QueryString["Wages"].ToString();

        Load_Report();

    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Ramalinga_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    private void Load_Report()
    {
        DataTable dt_Qry = new DataTable();
        string SSQL = "";

        SSQL = SSQL + " select EmpDet.ExistingCode,EmpDet.EmpNo,(EmpDet.FirstName) as EmpName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan, ";
        SSQL = SSQL + " (SalDet.TotalBasicAmt) as GrossEarnings,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo,SalDet.WorkedDays, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,EmpDet.UAN,cast(((ROUND((TotalBasicAmt * 8.33), -1) / 100)) as int) as PFone,cast(((ROUND((TotalBasicAmt * 3.67), -1) / 100)) as int) as PFtow   ";
        SSQL = SSQL + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
        SSQL = SSQL + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
        SSQL = SSQL + " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "'  and ";
        SSQL = SSQL + " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
        SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + fromdate + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";

        if (EmployeeType != "")
        {
            SSQL = SSQL + " and EmpDet.Wages='" + EmployeeType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }
        
        SSQL = SSQL + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.CommsAmt,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,";
        SSQL = SSQL + " SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,";
        SSQL = SSQL + " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,EmpDet.PFNo, ";
        SSQL = SSQL + " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,EmpDet.UAN,SalDet.WorkedDays ";
        SSQL = SSQL + " Order by cast(EmpDet.ExistingCode as int) Asc";
        dt_Qry = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_Qry.Rows.Count > 0)
        {
            //DataTable mEmployeeDT = new DataTable();
            grid.DataSource = dt_Qry;
            grid.DataBind();

            string attachment = "attachment;filename=EPF.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);


            //StringWriter stw = new StringWriter();
            //HtmlTextWriter htextw = new HtmlTextWriter(stw);
            DataTable dt = new DataTable();
            query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            //dt = objdata.RptEmployeeMultipleDetails(query);
            //if (dt.Rows.Count > 0)
            //{
            //    CmpName = dt.Rows[0]["Cname"].ToString().ToUpper();
            //    Cmpaddress = (dt.Rows[0]["Address1"].ToString().ToUpper() + " - " + dt.Rows[0]["Address2"].ToString().ToUpper());// "-" + dt.Rows[0]["Pincode"].ToString());
            //}

            Response.Write("<table>");

            if (EmployeeType != "CONTRACTCLEANING I" || EmployeeType != "CONTRACTCLEANING II")
            {
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='15'>");
                Response.Write("( FOR UNEXEMPTED ESTABLISHMENT ONLY )");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='15'>");
                Response.Write("FORM 6-A ( REVISED )");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='15'>");
                Response.Write("<b>THE EMPLOYEES' PROVIDENT FUNDS SCHEME, 1952</b>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='15'>");
                Response.Write("( PARAGRAPH 43 ) AND");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='15'>");
                Response.Write("<b>THE EMPLOYEES' PENSION SCHEME, 1995</b>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='15'>");
                Response.Write("( PARAGRAPH 20 )");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");
                Response.Write("<td colspan='8'  align='Left'>");
                Response.Write("STATEMENT OF CONTRIBUTION FOR CURRENCY PERIOD FROM");
                Response.Write("</td>");
                Response.Write("<td align='Center'>");
                Response.Write(fromdate.ToString() + " TO " + ToDate.ToString());
                Response.Write("</td>");
                Response.Write("<td colspan='6'  align='Right'>");
                Response.Write("STATUTORY RATE OF CONTRIBUTION : 12 %");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");
                Response.Write("<td align='Left' colspan='8'>");
                Response.Write("NAME AND ADDRESS OF THE ESTABLIHSMENT : ");
                Response.Write("</td>");
                Response.Write("<td  align='Center'>");
                Response.Write("<b>SHRI RAMALINGA MILLS LIMITED ,ARUPPUKOTTAI</b>");
                Response.Write("</td>");
                Response.Write("<td align='Right' colspan='6'>");
                Response.Write("NO OF MEMBERS VOLUNTARILY SUBSCRIBING AT HIGHER  RATE");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");
                Response.Write("<td colspan='8' align='Left'>");
                Response.Write("CODE NO OF THE ESTABLISHMENT : <b> MDU </b>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");
                Response.Write("<td colspan='15'>");
                Response.Write("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                Response.Write("</td>");
                Response.Write("</tr>");
            }

            Response.Write("<tr>");

            Response.Write("<td>");
            Response.Write("SL.No");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("EMPCODE");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("ACCOUNT NO");
            Response.Write("</td>");

            Response.Write("<td align='center'>");
            Response.Write("UAN");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("NAME OF THE EMPLOYEEWORKED");
            Response.Write("</td>");

            Response.Write("<td align='center'>");
            Response.Write("DAYS");
            Response.Write("</td>");

            Response.Write("<td aligng='center'>");
            Response.Write("WAGES RETAINING ALLOWANCES (IF ANY) AND DA INCLUDING CASH VALUE OF FOOD CONCESSION PAID DURING THE CURRENCY PERIOD");
            Response.Write("</td>");

            Response.Write("<td align='center'>");
            Response.Write("AMOUNT OF WORKERS CONTRIBUTION DEDUCTED FROM THE WAGES");
            Response.Write("</td>");

            Response.Write("<td align='center'>");
            Response.Write("EMPLOYEER'S (a)");
            Response.Write("</td>");

            Response.Write("<td align='center'>");
            Response.Write("CONTRIBUTION (b)");
            Response.Write("</td>");

            Response.Write("<td align='center'>");
            Response.Write("CONTRIBUTION (c)");
            Response.Write("</td>");

            Response.Write("<td align='center'>");
            Response.Write("REFUND OF ADVANCE");
            Response.Write("</td>");

            Response.Write("<td align='center'>");
            Response.Write("RATE OF HIGHER VOLUNTARY CONTRIBUTION (IF ANY)");
            Response.Write("</td>");

            Response.Write("<td align='center'>");
            Response.Write("EDLI CONTRIBUTION");
            Response.Write("</td>");

            Response.Write("<td align='center'>");
            Response.Write("REMARKS");
            Response.Write("</td>");
            Response.Write("</tr>");


            Response.Write("<tr>");
            Response.Write("<td colspan='15'>");
            Response.Write("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr align='center'>");
            Response.Write("<td colspan=''>");
            Response.Write("'(1)");
            Response.Write("</td>");
            Response.Write("<td colspan=''>");
            Response.Write("'(2)");
            Response.Write("</td>");
            Response.Write("<td colspan=''>");
            Response.Write("'(3)");
            Response.Write("</td>");
            Response.Write("<td colspan=''>");
            Response.Write("'(4)");
            Response.Write("</td>");
            Response.Write("<td colspan=''>");
            Response.Write("'(5)");
            Response.Write("</td>");
            Response.Write("<td colspan=''>");
            Response.Write("'(6)");
            Response.Write("</td>");
            Response.Write("<td colspan=''>");
            Response.Write("'(7)");
            Response.Write("</td>");
            Response.Write("<td colspan=''>");
            Response.Write("EPF (8)");
            Response.Write("</td>");
            Response.Write("<td colspan=''>");
            Response.Write("EPF");
            Response.Write("</td>");
            Response.Write("<td colspan=''>");
            Response.Write("EPS (9)");
            Response.Write("</td>");
            Response.Write("<td colspan=''>");
            Response.Write("TOTAL");
            Response.Write("</td>");
            Response.Write("<td colspan=''>");
            Response.Write("'(10)");
            Response.Write("</td>");
            Response.Write("<td colspan=''>");
            Response.Write("'(11)");
            Response.Write("</td>");
            Response.Write("<td colspan=''>");
            Response.Write("'(12)");
            Response.Write("<td colspan=''>");
            Response.Write("'(13)");
            Response.Write("</td>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='15'>");
            Response.Write("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='15'>");
            Response.Write("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");

            int Join = 1;

            DataTable dt1 = new DataTable();

            for (int i = 0; i < dt_Qry.Rows.Count; i++)
            {

                Response.Write("<tr style='font-weight: bold;' align='center'>");

                Response.Write("<td >");
                Response.Write("<a >" + Join + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >'" + dt_Qry.Rows[i]["EmpNo"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Qry.Rows[i]["PFNo"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >'" + dt_Qry.Rows[i]["UAN"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Qry.Rows[i]["EmpName"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Qry.Rows[i]["WorkedDays"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Qry.Rows[i]["GrossEarnings"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Qry.Rows[i]["ProvidentFund"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Qry.Rows[i]["PFone"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Qry.Rows[i]["PFtow"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Qry.Rows[i]["ProvidentFund"].ToString() + "</a>");
                Response.Write("</td>");




                Response.Write("</tr>");

                Join++;

            }

            //grid.DataSource = dt1; //Your DataTable value here
            //grid.DataBind();
            //grid.ShowHeader = false;
            //grid.RenderControl(htextw);
            // Response.Write(stw.ToString());

            Response.Write("</table>");

            //Response.Write(stw.ToString());

            Int32 Grand_Tot_End = 0;



            Response.Write("<table>");
            Response.Write("<tr>");
            Response.Write("<td colspan='4'>");
            Response.Write("</td>");
            Response.Write("<td colspan='11'>");
            Response.Write("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='4'>");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("TOTAL");
            Response.Write("</td>");

            Grand_Tot_End = dt_Qry.Rows.Count + 4;

            Response.Write("<td style='font-weight:bold;'>=Sum(F16:F" + Grand_Tot_End + ")");
            Response.Write("<td style='font-weight:bold;'>=Sum(G16:G" + Grand_Tot_End + ")");
            Response.Write("<td style='font-weight:bold;'>=Sum(H16:H" + Grand_Tot_End + ")");
            Response.Write("<td style='font-weight:bold;'>=Sum(I16:I" + Grand_Tot_End + ")");
            Response.Write("<td style='font-weight:bold;'>=Sum(J16:J" + Grand_Tot_End + ")");
            Response.Write("<td style='font-weight:bold;'>=Sum(K16:K" + Grand_Tot_End + ")");

            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='4'>");
            Response.Write("</td>");
            Response.Write("<td colspan='11'>");
            Response.Write("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='15'>");
            Response.Write("1.  TOTAL NUMBER OF CONTRIBUTION CARDS ENCLOSED (FORM-3A)       302");
            Response.Write("</td>");
            Response.Write("</tr><tr>");
            Response.Write("<td colspan='10'>");
            Response.Write("2.  CERTIFIED THAT THE FORM-3A, DULY COMPLETED OF ALL THE MEMBERS LISTED IN THIS STATEMENT ARE ENCLOSED EXCEPT THOSE ALREADY SENT DURING THE CURRENCY PERIOD FOR THE FINAL SETTLEMENT OF THE CONCERNED MEMBER'S ACCOUNT VIDE REMARKD FURNISHED AGAINST THE NAMES OF THE RESPECTIVE MEMBERS ABOVE.");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='10'>");
            Response.Write("    CERTIFIED THAT THE DIFFERENCE BETWEEN THE FIGURES OR TOTAL CONTRIBUTIONS & ADM. CHARGES REMITTED DURING THE CURRENCY PERIOD AND THOSE SHOWED UNDER COLS. 5 TO 7 IS SOLEY DUE TO THE ROUNDING OFF AMOUNT TO THE NEAREST RUPEE UNDER THE RULES.");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='14'></td>");
            Response.Write("<td align='center'> SIGNATURE OF THE EMPLOYER (WITH OFFICE SEAL) </td>");
            Response.Write("</tr>");
            Response.Write("</table>");

            Response.End();
            Response.Clear();
        
        }
    }
}