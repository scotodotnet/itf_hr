﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;

using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class RptEmpWagesList : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month = "";
    string str_yr = "";
    string Wages = "";
    string str_cate;
    string str_dept;
    string AgentName;
    string SessionCcode;
    string SessionLcode;
    string FinYearVal = "";
    string Month;
    string SessionAdmin; string SessionUserType;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate = "";
    string ToDate = "";
    string salaryType;
    string Emp_ESI_Code;
    string SessionPayroll;
    string EmployeeType; string EmployeeTypeCd;
    string PayslipType;
    string Report_Types;

    string PFTypeGet;

    string Left_Employee = "0";
    string Left_Date = "";
    string Get_Report_Type = "0";
    string Salary_CashOrBank;
    string Get_Division_Name = "";

    string Other_state = "";
    string Non_Other_state = "";
    string Basic_Report_Date = "";
    string Basic_Report_Type = "OLD";
    string ExemptedStaff = "";
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        string date_1 = "";
        date_1 = ("01".ToString() + "-" + "12".ToString() + "-" + "2018".ToString()).ToString();

        // SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            //Response.Redirect("Default.aspx");
            //Response.Write("Your session expired");
        }


        //string ss = Session["UserId"].ToString();
        //SessionCcode = Session["Ccode"].ToString();
        //SessionLcode = Session["Lcode"].ToString();
        //SessionUserType = Session["Isadmin"].ToString();
        //str_cate = Request.QueryString["Cate"].ToString();
        //FinYearVal = Request.QueryString["FinYearVal"].ToString();

        //str_month = Request.QueryString["Months"].ToString();
        //str_yr = Request.QueryString["yr"].ToString();
        //fromdate = Request.QueryString["fromdate"].ToString();
        //ToDate = Request.QueryString["ToDate"].ToString();
        //Report_Types = Request.QueryString["ReportFormat"].ToString();
        //AgentName = Request.QueryString["AgentName"].ToString();

        //salaryType = Request.QueryString["Salary"].ToString();
        //EmployeeTypeCd = Request.QueryString["EmpTypeCd"].ToString();
        //EmployeeType = Request.QueryString["EmpType"].ToString();

        Load_Report();
    }

    private void Load_Report()
    {
        string attachment = "attachment;filename=Wageslistform.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        StringWriter stw = new StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        DataTable dt = new DataTable();
        query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        //dt = objdata.RptEmployeeMultipleDetails(query);
        //if (dt.Rows.Count > 0)
        //{
        //    CmpName = dt.Rows[0]["Cname"].ToString().ToUpper();
        //    Cmpaddress = (dt.Rows[0]["Address1"].ToString().ToUpper() + " - " + dt.Rows[0]["Address2"].ToString().ToUpper());// "-" + dt.Rows[0]["Pincode"].ToString());
        //}

        Response.Write("<table>");
        Response.Write("<tr>");
        Response.Write("<td align='center' colspan='13'>");
        Response.Write("SHRI RAMALINGA MILLS LIMITED ARUPPUKOTTAI");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr>");
        Response.Write("<td colspan='7' align='Center'>");
        Response.Write(Wages.ToString().ToUpper() + " EMPLOYEES WAGES LIST FROM  " + fromdate.ToString() + " TO " + ToDate.ToString() + "");
        Response.Write("</td>");
       
        Response.Write("<td colspan='6' align='Left'>");
        Response.Write("DISBURSED ON " + DateTime.Now.ToString("dd/MM/yyyy").ToString() + "");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr>");
        Response.Write("<td colspan='13'>");
        Response.Write("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr>");
        Response.Write("<td>");
        Response.Write("SL.No");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("CODE NO.");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("EMPLOYEE NAME");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("PF.NO DAYS FH");
        Response.Write("</td>");
        Response.Write("<td>");
        Response.Write("BASIC");
        Response.Write("</td>");
        Response.Write("<td aligng='center'>");
        Response.Write("WAGES EARNED");
        Response.Write("</td>");
        Response.Write("<td align='center'>");
        Response.Write("PF");
        Response.Write("</td>");
        Response.Write("<td align='center'>");
        Response.Write("ESI");
        Response.Write("</td>");
        Response.Write("<td align='center'>");
        Response.Write("PRS");
        Response.Write("</td>");
        Response.Write("<td align='center'>");
        Response.Write("LIC");
        Response.Write("</td>");
        Response.Write("<td align='center'>");
        Response.Write("TOTAL DEDN");
        Response.Write("</td>");
        Response.Write("<td align='center'>");
        Response.Write("NETT");
        Response.Write("</td>");
        Response.Write("<td align='center'>");
        Response.Write("SIGNATURE");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr>");
        Response.Write("<td colspan='13'>");
        Response.Write("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write("<table>");
        Response.Write("<tr style='font-weight: bold;' align='center'>");

        DataTable dt1 = new DataTable();

        //grid.DataSource = dt1; //Your DataTable value here
        //grid.DataBind();
        //grid.ShowHeader = false;
        //grid.RenderControl(htextw);
        // Response.Write(stw.ToString());

        Response.Write("</tr>");
        //Response.Write("<tr>");
        //Response.Write("<td colspan='5'>");
        //Response.Write("</td>");
        //Response.Write("<td colspan='2'>");
        //Response.Write("-------------------------------------------------------------------------------");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        //Response.Write("<tr>");
        //Response.Write("<td colspan='4'>");
        //Response.Write("</td>");
        //Response.Write("<td align='center'>");
        //Response.Write("TOTAL");
        //Response.Write("</td>");

        //if (dt1.Rows.Count > 0)
        //{

        //    Response.Write("<td style='font-weight:bold;'>=Sum(F7:F" + Convert.ToDecimal(dt1.Rows.Count + 7) + ")");
        //    Response.Write("<td style='font-weight:bold;'>=Sum(G7:G" + Convert.ToDecimal(dt1.Rows.Count + 7) + ")");

        //}

        //Response.Write("</tr>");
        //Response.Write("<tr>");
        //Response.Write("<td colspan='5'>");
        //Response.Write("</td>");
        //Response.Write("<td colspan='2'>");
        //Response.Write("-------------------------------------------------------------------------------");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        Response.Write("</table>");

        Response.End();
        Response.Clear();
    }
}