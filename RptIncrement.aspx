﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="RptIncrement.aspx.cs" Inherits="RptIncrement" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
     <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
     });
	</script>


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
            }
        });
    };
</script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
    </script>

    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
    </script>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
    </script>
    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Increment Process</a></li>
            <%--<li class="active">Salary Calculation</li>--%>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Increment Duration</h1>
        <!-- end page-header -->

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <asp:UpdatePanel ID="SalPay" runat="server">
                <ContentTemplate>
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Increment Duration</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="row">
                                       
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>From Date</label>
                                                <asp:TextBox runat="server" ID="txtFromDate"  class="datepicker form-control"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender2" TargetControlID="txtFromDate" ValidChars="./" FilterType="Numbers,Custom">
                                                </cc1:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtFromDate" initialvalue="" ValidationGroup="ValidateField" runat="server" CssClass="danger"  ErrorMessage="This Fields are required"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                       
                                         <div class="col-md-3">
                                            <div class="form-group">
                                                <label>To Date</label>
                                                <asp:TextBox runat="server" ID="txtToDate" class="datepicker form-control"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender4"  TargetControlID="txtToDate" ValidChars="./" FilterType="Numbers,Custom">

                                                </cc1:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtToDate" initialvalue="" ValidationGroup="ValidateField" runat="server" CssClass="danger"  ErrorMessage="This Fields are required"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputName">Wages Type</label>
                                                <asp:DropDownList ID="ddlWagesType" runat="server" class="form-control select2">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="ddlWagesType" initialvalue="-Select-" ValidationGroup="ValidateField" runat="server" CssClass="danger"  ErrorMessage="This Fields are required"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                         <div class="col-md-3" runat="server" visible="false">
                                            <div class="form-group">
                                                <label>Fin. Year</label>
                                                <asp:DropDownList runat="server" ID="ddlFinYear" class="form-control select2">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="ddlFinYear" initialvalue="-Select-" ValidationGroup="ValidateField" runat="server" CssClass="danger"  ErrorMessage="This Fields are required"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- begin row -->
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnReport" ValidationGroup="ValidateField" Text="Report" class="btn btn-success"
                                                    OnClick="btnReport_Click"  />
                                                <asp:Button ID="btnCancel" runat="server" class="btn btn-warning"
                                                    Text="Cancel" OnClick="btnCancel_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                <div id="Download_loader" style="display: none" />
                            </div>
                        </div>
                        <!-- end panel -->
                    </div>
                </ContentTemplate>
                
            </asp:UpdatePanel>
            <!-- end col-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end #content -->
</asp:Content>

