﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class RptMillWise : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string Date = "";
    ReportDocument report = new ReportDocument();

    DataTable DT_Mill = new DataTable(); 
     DataTable Log_DS = new DataTable();
    string SessionPayroll;
    BALDataAccess objdata = new BALDataAccess();

    DataTable AutoDTable = new DataTable();
    DataTable DT_Course = new DataTable();
    string CmpName, Cmpaddress;
    string SSQL = "";
    DateTime date1;
    DateTime date2;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string status;
    string MillCode;
    string CourseCode;
    string MillName;
    string CourseName;
    string DesignationCode;



    string MachineId;
    DataSet ds = new DataSet();
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | MILL WISE REPORT ";



            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString(); 
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionUserType = Session["Isadmin"].ToString();
            //FromDate = Request.QueryString["FromDate"].ToString();
             CourseCode = Request.QueryString["CourseCode"].ToString();
            MillCode= Request.QueryString["MillCode"].ToString();
           // Date = Request.QueryString["Date"].ToString();
            DesignationCode = Request.QueryString["DesignationCode"].ToString();

            Mill_Wise();
        }
    }
    public void Mill_Wise()
    {
        AutoDTable.Columns.Add("S.No");
        AutoDTable.Columns.Add("ITF NO");
        AutoDTable.Columns.Add("ExCode");
        AutoDTable.Columns.Add("Name");
       AutoDTable.Columns.Add("CourseName");
        AutoDTable.Columns.Add("Designation");
        try
        {
            SSQL = "";
            SSQL = "select MillName From MstMill where MillCode='" + MillCode + "'";
            DT_Mill = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Mill.Rows.Count != 0)
            {
                MillName = DT_Mill.Rows[0]["MillName"].ToString();

            }

            //SSQL = "";
            //SSQL ="select CourseName from MstCourse where CourseCode='" + CourseCode+"'";
            //DT_Course= objdata.RptEmployeeMultipleDetails(SSQL);
            //if (DT_Course.Rows.Count != 0)
            //{
            //    CourseName = DT_Course.Rows[0]["CourseName"].ToString();

            //}

            SSQL = "";
            SSQL = "Select MachineID,FirstName,Designation,Millname,CourseName from Employee_Mst";
            SSQL = SSQL + " where MillCode='" + MillCode + "'";

            if ((CourseCode != "") && (CourseCode != "-Select-"))
            {
                SSQL = SSQL + " and CourseCode ='" + CourseCode + "'";
            }
            if ((DesignationCode != "-Select-") && (DesignationCode != ""))
            {
                SSQL = SSQL + "and DesignCode='" + DesignationCode + "'";
            }
            Log_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Log_DS.Rows.Count != 0)
            {
                for (int i = 0; i < Log_DS.Rows.Count; i++)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["S.No"] = AutoDTable.Rows.Count;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ITF NO"] = Log_DS.Rows[i]["MachineID"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ExCode"] = Log_DS.Rows[i]["MachineID"].ToString();

                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Name"] = Log_DS.Rows[i]["FirstName"].ToString();

                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["CourseName"] = Log_DS.Rows[i]["CourseName"].ToString(); ;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Designation"] = Log_DS.Rows[i]["Designation"].ToString();
                }
                    ds.Tables.Add(AutoDTable);
                    string CmpName = "";
                    string Cmpaddress = "";
                    DataTable dt_Cat = new DataTable();


                    SSQL = "";
                    SSQL = SSQL + "Select CompName,City from Company_Mst";
                    dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_Cat.Rows.Count > 0)
                    {
                        CmpName = dt_Cat.Rows[0]["CompName"].ToString();
                        Cmpaddress = (dt_Cat.Rows[0]["City"].ToString());
                    }

                    report.Load(Server.MapPath("crystal/MillWise.rpt"));
                    report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                    report.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
                    report.DataDefinition.FormulaFields["MillName"].Text = "'" + MillName + "'";
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = report;
                }
            
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
            }
        catch (Exception ex)
        {

        }
    }
}
