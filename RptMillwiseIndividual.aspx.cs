﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class RptMillwiseIndividual : System.Web.UI.Page
{
    string EmpCode1 = "";
    string ShiftType1 = "";
    string Date1 = "";
    string Date2 = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionUserType;

    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    ReportDocument report = new ReportDocument();
    string SSQL = "";
    DateTime date1 = new DateTime();
    DateTime date2 = new DateTime();
    string[] Time_Minus_Value_Check;
    string EmpName = "";
    DataTable AutoDTable = new DataTable();
    DataTable mLocalDS = new DataTable();
    DataTable da_Weekoff_check = new DataTable();
    DataTable Da_Weekoff = new DataTable();
    int intK;
    string Adolescent_Shift;
    string Division = ""; string status = "";
    DateTime EmpdateIN;
    string Name;
    DataSet ds = new DataSet();
    string TokenNo;
    string millname;
    string UnitName;
    string DeptName;
    string Designation;
    string CourseName;
    string CourseLevel;
    string MachineId;
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Employee DayWise Report";
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            EmpCode1 = Request.QueryString["MachineID"].ToString();
            //  Division = Request.QueryString["Division"].ToString();
           /// status = Request.QueryString["status"].ToString();
            Date1 = Request.QueryString["Date1"].ToString();
            Date2 = Request.QueryString["Date2"].ToString();
            TokenNo = Request.QueryString["MachineID"].ToString();

            DataSet ds = new DataSet();

            DataColumn auto = new DataColumn();
            auto.AutoIncrement = true;
            auto.AutoIncrementSeed = 1;
            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("Date");
            AutoDTable.Columns.Add("CourseName");
            AutoDTable.Columns.Add("TimeIN");

           

            MillWiseIndividual();
        }


    }
    private void MillWiseIndividual()
    {
        try
        {
            date1 = Convert.ToDateTime(Date1);
            date2 = Convert.ToDateTime(Date2);
            if (date1 > date2)
                return;

            string iEmpDet = null;

            iEmpDet = EmpCode1;
            string Token_No_Display = "";
            string Name_Display = "";
            string T_Query = "";
            T_Query = "Select ExistingCode,FirstName,MillName,UnitName,DeptName,Designation,CourseName,CourseLevel from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + EmpCode1 + "'";
            DataTable DT_To = new DataTable();
            DT_To = objdata.RptEmployeeMultipleDetails(T_Query);

            if (DT_To.Rows.Count != 0)
            {
                Token_No_Display = DT_To.Rows[0]["ExistingCode"].ToString();
                Name_Display = DT_To.Rows[0]["FirstName"].ToString();
                millname = DT_To.Rows[0]["MillName"].ToString();
                UnitName = DT_To.Rows[0]["UnitName"].ToString();
                DeptName = DT_To.Rows[0]["DeptName"].ToString();
                Designation = DT_To.Rows[0]["Designation"].ToString();
                CourseName = DT_To.Rows[0]["CourseName"].ToString();
                CourseLevel = DT_To.Rows[0]["CourseLevel"].ToString();

            }

            long iRowVal = 0;

            int daysAdded = 0;
            int daycount = (int)((date2 - date1).TotalDays);

            iRowVal = daycount;


            DataTable mEmployeeDT = new DataTable();
            string Machine_ID_Str = null;

            SSQL = "Select MachineID_Encrypt,(FirstName + '.' + MiddleInitial) as FirstName from Employee_Mst where IsActive='Yes' And EmpNo='" + iEmpDet + "'";
            SSQL = SSQL + " And CompCode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "'";


            mEmployeeDT = objdata.RptEmployeeMultipleDetails(SSQL);

            Machine_ID_Str = mEmployeeDT.Rows[0][0].ToString();
            EmpName = mEmployeeDT.Rows[0][1].ToString();


            string Date_Value_Str = null;
            string Date_Value_Str1 = null;

            string OT_Week_OFF_Machine_No = null;
            string Time_IN_Str = null;
            string Time_Out_Str = null;

            string Total_Time_get = "";
            string Total_Time_get_1 = "";
            Int32 j = 0;
            string time_Check_dbl = "";
            Int32 time_Check_dbl_1 = 0;
            //for (int mGrdRow = 0; mGrdRow < AutoDTable.Rows.Count; mGrdRow++)
            //{

            OT_Week_OFF_Machine_No = Machine_ID_Str;
            //string aa = AutoDTable.Rows[mGrdRow]["Date"].ToString();
            DateTime Date_Value = Convert.ToDateTime(date1);
            DateTime Date_Value1 = Convert.ToDateTime(date2).AddDays(1);
            Date_Value_Str = string.Format(Date_Value.ToString("yyyy/MM/dd"));
            Date_Value_Str1 = string.Format(Date_Value1.ToString("yyyy/MM/dd"));

            string Emp_Total_Work_Time_1 = "00:00";
            string Final_OT_Work_Time_1 = "00:00";

            //Find Week OFF
            string Employee_Week_Name = "";
            string Assign_Week_Name = "";
            DataTable NFHDS = new DataTable();
            string qry_nfh = "";

            //Time In Query
            DataTable mLocalDS1 = new DataTable();
            time_Check_dbl = "";
            time_Check_dbl_1 = 0;
            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
            mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

            if (mLocalDS.Rows.Count <= 0)
            {
                Time_IN_Str = "";
               
            }
            else
            {
                //Time_IN_Str = mLocalDS.Tables(0).Rows(0)(0)
                for (int ckrw = 0; ckrw < mLocalDS.Rows.Count; ckrw++)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["SNo"] = AutoDTable.Rows.Count;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Date"] = Convert.ToDateTime(mLocalDS.Rows[ckrw][0]).ToString("dd/MM/yyyy");
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeIN"] = Convert.ToDateTime(mLocalDS.Rows[ckrw][0]).ToString("hh:mm tt");
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["CourseName"] = CourseName;
                }
             
            }

            if (AutoDTable.Rows.Count > 0)
            {
                string CmpName = "";
                string Cmpaddress = "";
                DataTable dt_Cat = new DataTable();


                SSQL = "";
                SSQL = SSQL + "Select CompName,City from Company_Mst";
                dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_Cat.Rows.Count > 0)
                {
                    CmpName = dt_Cat.Rows[0]["CompName"].ToString();
                    Cmpaddress = (dt_Cat.Rows[0]["City"].ToString());
                }

                ds.Tables.Add(AutoDTable);

                report.Load(Server.MapPath("crystal/MillWiseIndividual.rpt"));
                report.DataDefinition.FormulaFields["ITFNO"].Text = "'" + TokenNo + "'";
                report.DataDefinition.FormulaFields["Name"].Text = "'" + Name_Display + "'";
                report.DataDefinition.FormulaFields["FromDT"].Text = "'" + Date1 + "'";
                report.DataDefinition.FormulaFields["ToDT"].Text = "'" + Date2 + "'";

                report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                report.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
                report.DataDefinition.FormulaFields["MillName"].Text = "'" + millname + "'";
                report.DataDefinition.FormulaFields["UnitName"].Text = "'" + UnitName + "'";
                //report.DataDefinition.FormulaFields["DeptName"].Text = "'" + DeptName + "'";
                report.DataDefinition.FormulaFields["Designation"].Text = "'" + Designation + "'";
             //   report.DataDefinition.FormulaFields["CourseName"].Text = "'" + CourseName + "'";
                //report.DataDefinition.FormulaFields["CourseLevel"].Text = "'" + CourseLevel + "'";


                report.Database.Tables[0].SetDataSource(AutoDTable);
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;

               

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
        }
        catch (Exception ex)
        {
            return;
        }

    }
    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = "0";
        BALDataAccess objdata_new = new BALDataAccess();

        string query = "Select * from Employee_Mst where MachineID_Encrypt='" + encryptpwd + "'";
        DataTable DT = new DataTable();
        DT = objdata_new.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            decryptpwd = DT.Rows[0]["MachineID"].ToString();
        }
        else
        {
            query = "Select * from Employee_Mst_New_Emp where MachineID_Encrypt='" + encryptpwd + "'";
            DT = objdata_new.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                decryptpwd = DT.Rows[0]["MachineID"].ToString();
            }
            else
            {
                decryptpwd = "0";
            }
        }

       
        return decryptpwd;
    }
    public static string GetRandom(int Min, int Max)
    {
        System.Random Generator = new System.Random();

        return Generator.Next(Min, Max).ToString();


    }
    public static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }
}