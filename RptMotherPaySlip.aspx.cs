﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class RptMotherPaySlip : System.Web.UI.Page
{

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    string FromDate = "";
    string ToDate = "";
    string EmpType = "";
    string Month = "";
    string FinYearVal = "";
    string FinYear = "";
    string CatName = "";

    DataTable Dt = new DataTable();
    DataTable AutoDT = new DataTable();
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report - Mother PaySlip";
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();

            FromDate = Request.QueryString["fromdate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            CatName = Request.QueryString["Cate"].ToString();
            EmpType = Request.QueryString["EmpType"].ToString();
            FinYearVal = Request.QueryString["yr"].ToString();
            Month = Request.QueryString["Months"].ToString();

            PaySlip();

        }
    }

    private void PaySlip()
    {
        AutoDT.Columns.Add("EmpNo");
        AutoDT.Columns.Add("Name");
        AutoDT.Columns.Add("FatherName");
        AutoDT.Columns.Add("Designation");
        AutoDT.Columns.Add("Doj");
        AutoDT.Columns.Add("Month");
        AutoDT.Columns.Add("PFNo");
        AutoDT.Columns.Add("ESINo");
        AutoDT.Columns.Add("DeptName");
        AutoDT.Columns.Add("GrassAmt");
        AutoDT.Columns.Add("FixedBasic");
        AutoDT.Columns.Add("FixedDA");
        AutoDT.Columns.Add("FixedDARate");
        AutoDT.Columns.Add("FixedTotal");
        AutoDT.Columns.Add("WorkDays");
        AutoDT.Columns.Add("NFHDays");
        AutoDT.Columns.Add("TotalDays");
        AutoDT.Columns.Add("Basic");
        AutoDT.Columns.Add("DAAmt");
        AutoDT.Columns.Add("DARate");
        AutoDT.Columns.Add("Total");
        AutoDT.Columns.Add("PF");
        AutoDT.Columns.Add("ESI");
        AutoDT.Columns.Add("LIC");
        AutoDT.Columns.Add("TotalAmt");
        AutoDT.Columns.Add("HRA");
        AutoDT.Columns.Add("TA");
        AutoDT.Columns.Add("Comms");
        AutoDT.Columns.Add("GrossAmt");
        AutoDT.Columns.Add("Advance2");
        AutoDT.Columns.Add("Advance3");
        AutoDT.Columns.Add("Home");
        AutoDT.Columns.Add("Temple");
        AutoDT.Columns.Add("Hloan");
        AutoDT.Columns.Add("Stores");
        AutoDT.Columns.Add("ComDed");
        AutoDT.Columns.Add("OthrDed");
        AutoDT.Columns.Add("TotalDed");
        AutoDT.Columns.Add("NetAmt");
        AutoDT.Columns.Add("OtherSal");
        AutoDT.Columns.Add("HRAamt");
        AutoDT.Columns.Add("TAamt");


        SSQL = "";
        SSQL = "select EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.LastName,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo,EmpDet.DeptName,EmpDet.Designation,EmpDet.HRA,EmpDet.TA,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed, " +
                            " SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4," +
                            " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,SalDet.NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,(SalDet.TotalBasicAmt - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as TotalOne,EmpDet.PFNo, " +
                            " (SalDet.GrossEarnings - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as GrossOne,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan,SalDet.HRAamt,SalDet.TAamt, " +
                            " (SalDet.TempleAmt + SalDet.HouseAmt + SalDet.Hloan + SalDet.Advace2 + SalDet.Advance3 + SalDet.Deduction3 + SalDet.Deduction4 +  SalDet.Deduction5) as TotalDed,(SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax) as TotalDedFirst, " +
                            " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,(SalDet.NetPay + SalDet.allowances5 + SalDet.OTHoursAmtNew) as NetFinal,isnull(SalDet.OTHoursAmtNew,'0') as OtherSal " +
                            " from Employee_Mst EmpDet inner Join [" + SessionEpay + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                            " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                            " inner Join [" + SessionEpay + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                            " where SalDet.Month='" + Month + "' AND SalDet.FinancialYear='" + FinYearVal + "' and EmpDet.CatName='" + CatName + "' and " +
                            " AttnDet.Months='" + Month + "' AND AttnDet.FinancialYear='" + FinYearVal + "' and " +
                            " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                            " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmpType + "'" +
                            " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + FromDate + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";

        SSQL = SSQL + "group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.LastName,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo,EmpDet.DeptName,EmpDet.Designation,EmpDet.HRA,EmpDet.TA,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed,SalDet.TempleAmt,SalDet.HouseAmt,EmpDet.PFNo, " +
       " SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,SalDet.Hloan,SalDet.HRAamt,SalDet.TAamt," +
       " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,SalDet.NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.OTHoursAmtNew  " +
       " Order by cast(EmpDet.ExistingCode as int) Asc";

        Dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Dt.Rows.Count > 0)
        {
            for (int i = 0; i < Dt.Rows.Count; i++)
            {
                AutoDT.NewRow();
                AutoDT.Rows.Add();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["EmpNo"] = Dt.Rows[i]["EmpNo"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Name"] = Dt.Rows[i]["FirstName"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["FatherName"] = Dt.Rows[i]["FirstName"].ToString() + " " + Dt.Rows[i]["LastName"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["Designation"] = Dt.Rows[i]["Designation"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Doj"] = Dt.Rows[i]["Doj"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Month"] = Month.ToString() + " - " + FinYearVal.ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["PFNo"] = Dt.Rows[i]["PFNo"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["ESINo"] = Dt.Rows[i]["ESINo"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["OtherSal"] = Dt.Rows[i]["OtherSal"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["DeptName"] = Dt.Rows[i]["DeptName"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["GrassAmt"] = Dt.Rows[i]["GrossEarnings"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["FixedBasic"] = Dt.Rows[i]["Basic_SM"].ToString();
                if (CatName.ToString().ToUpper() == "LABOUR")
                {
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["FixedDA"] = Dt.Rows[i]["DaAmt"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["FixedDARate"] = Dt.Rows[i]["DaRateFixed"].ToString();

                    AutoDT.Rows[AutoDT.Rows.Count - 1]["DAAmt"] = Dt.Rows[i]["DaFixed"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["DARate"] = Dt.Rows[i]["DaRateAmt"].ToString();

                }
                else
                {
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["FixedDA"] = Dt.Rows[i]["DaRateFixed"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["DAAmt"] = Dt.Rows[i]["DaRateAmt"].ToString();

                    AutoDT.Rows[AutoDT.Rows.Count - 1]["FixedDARate"] = "0";
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["DARate"] = "0";
                }
                

                AutoDT.Rows[AutoDT.Rows.Count - 1]["FixedTotal"] = Dt.Rows[i]["TotalFixed"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["WorkDays"] = Dt.Rows[i]["WorkedDays"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["NFHDays"] = Dt.Rows[i]["NFh"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["TotalDays"] = Dt.Rows[i]["Availabledays"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Basic"] = Dt.Rows[i]["Fbasic"].ToString();

                
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Total"] = Dt.Rows[i]["TotalBasicAmt"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["PF"] = Dt.Rows[i]["ProvidentFund"].ToString().Trim();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["ESI"] = Dt.Rows[i]["ESI"].ToString().Trim();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["LIC"] = Dt.Rows[i]["IncomeTax"].ToString().Trim();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["TotalAmt"] = Dt.Rows[i]["TotalOne"].ToString();

                if (CatName.ToString().ToUpper() == "LABOUR")
                {
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["HRA"] = "156";
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["Comms"] = Dt.Rows[i]["CommsAmt"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["HRAamt"] = Dt.Rows[i]["DayIncentive"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["GrossAmt"] = Dt.Rows[i]["GrossOne"].ToString();
                }
                else
                {
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["HRA"] = Dt.Rows[i]["HRA"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["TA"] = Dt.Rows[i]["TA"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["HRAamt"] = Dt.Rows[i]["HRAamt"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["Comms"] = Dt.Rows[i]["TAamt"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["GrossAmt"] = (Convert.ToDecimal(Dt.Rows[i]["GrossOne"].ToString())+Convert.ToDecimal(Dt.Rows[i]["HRAamt"].ToString())+Convert.ToDecimal(Dt.Rows[i]["TAamt"].ToString()));
                }

                //AutoDT.Rows[AutoDT.Rows.Count - 1]["GrossAmt"] = Dt.Rows[i]["GrossOne"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["Advance2"] = Dt.Rows[i]["Advace2"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Advance3"] = Dt.Rows[i]["Advance3"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Home"] = Dt.Rows[i]["HouseAmt"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Temple"] = Dt.Rows[i]["TempleAmt"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["Hloan"] = Dt.Rows[i]["Hloan"].ToString();

                AutoDT.Rows[AutoDT.Rows.Count - 1]["Stores"] = Dt.Rows[i]["Deduction3"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["ComDed"] = Dt.Rows[i]["Deduction5"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["OthrDed"] = Dt.Rows[i]["Deduction4"].ToString();
                AutoDT.Rows[AutoDT.Rows.Count - 1]["TotalDed"] = Dt.Rows[i]["TotalDed"].ToString();

                if (CatName.ToString().ToUpper() == "LABOUR")
                {
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["NetAmt"] = Dt.Rows[i]["NetPay"].ToString();
                }
                else
                {
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["NetAmt"] = Dt.Rows[i]["NetFinal"].ToString();
                }
            }
        }
        if (AutoDT.Rows.Count > 0)
        {
            report.Load(Server.MapPath("Payslip_New_Component/MotherPaySlip.rpt"));
            report.Database.Tables[0].SetDataSource(AutoDT);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }
}