﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Text;
using System.IO;

public partial class RptOT : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;

    double Final_Count;
    string SessionPayroll;
    string PerDayAmt;
    DataTable dt_Cat = new DataTable();
    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;
    string Calc_Amt = "0";

    //BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable OTHrs_DS = new DataTable();

    string S3OT_Above_7 = "0";
    string S3OT_Above_4 = "0";
    string S3OT_Below_4 = "0";

    string OT_Above_Eight = "0";
    string OT_Above_Four = "0";
    string OT_Below_Four = "0";
    string OT_Total_Hours = "0";

    string OT_Hours = "0";

    string CmpName, Cmpaddress;
    string OT_Amt = "0";
    string Total_Amt = "0";
    string OT_Check = "0";

    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;
    string Manul_OT = "";
    ReportDocument report = new ReportDocument();
    DataSet ds = new DataSet();
    DataTable dt = new DataTable();
    DataTable dt_OT = new DataTable();
    DataTable dt_Manula_OT = new DataTable();

    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Load_DB();
                Page.Title = "Spay Module | OT LIST";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
           // Division = Request.QueryString["Division"].ToString();
           
            //string TempWages = Request.QueryString["Wages"].ToString();
            //WagesType = TempWages.Replace("_", "&");
            FromDate = Request.QueryString["FromDate"].ToString();

            if (SessionUserType == "2")
            {
                NonAdminGetAttdTable_Weekly_OTHOURS();
            }

            else
            {
                GetAttdTable_Weekly_OTHOURS();
            }


            // ds.Tables.Add(AutoDTable);
            //report.Load(Server.MapPath("crystal/OT_New.rpt"));
            if (AutoDTable.Rows.Count > 0)
            {
                SSQL = "";
                SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                //dt = objdata.RptEmployeeMultipleDetails(SSQL);
                dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt_Cat.Rows.Count > 0)
                {
                    CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
                }

                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=OT.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='5'>");
                Response.Write("<a style=\"font-weight:bold\">" + CmpName + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + Cmpaddress + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='5'>");
                Response.Write("<a style=\"font-weight:bold\">OVER TIME REPORT - " + FromDate + "</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
                //Response.Write("<tr Font-Bold='true' align='center'>");
                //Response.Write("<td colspan='10'>");
                //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
                //Response.Write("&nbsp;&nbsp;&nbsp;");
                //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.Write("<table border=1>");
                Response.Write("<tr>");
                Response.Write("<td colspan=8 align='center' style=\"font-weight:bold\"> TOTAL");
                Response.Write("</td> ");
                Response.Write("<td> =sum(I4:I" + Convert.ToInt32(AutoDTable.Rows.Count+3) + ")");
                Response.Write("</td>");
                Response.Write("<td> =sum(J4:J" + Convert.ToInt32(AutoDTable.Rows.Count+3)  + ")");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.End();
                Response.Clear();

                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                //report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName.ToString() + "'";
                //report.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
                //report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate + "'";
                //report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";
                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert(No Data Found!!!)", true);
            }
        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Ramalinga_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    public void GetAttdTable_Weekly_OTHOURS()
    {
        AutoDTable.Columns.Add("S.No");
        AutoDTable.Columns.Add("Code");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("AccountNo");
        //AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Wages");
        AutoDTable.Columns.Add("OT_TimeIN");
        AutoDTable.Columns.Add("OT_TimeOUT");
     //   AutoDTable.Columns.Add("Shift");
        AutoDTable.Columns.Add("Total_OTHrs");
        AutoDTable.Columns.Add("Total_Amt");

        DataRow dtRow;
        SSQL = "";
        SSQL = SSQL + "select (EmpNo) as Code,MachineID,(FirstName + '.' + LastName) as Name,AccountNo,Wages,'' as Total,'' as Amount,OT_Salary ";
        SSQL = SSQL + " from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " (IsActive='Yes' or CONVERT(DATETIME,DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)) and  OTEligible='1' ";
        SSQL = SSQL + " order by cast(MachineID as int) asc ";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dtRow = AutoDTable.NewRow();
                bool shift3_check = false;
                S3OT_Above_7 = "0";
                OT_Above_Eight = "0";
                OT_Above_Four = "0";
                OT_Below_Four = "0";
                S3OT_Above_4 = "0";
                S3OT_Below_4 = "0";
               
             
                if (dt.Rows[i]["Wages"].ToString() == "STAFF")
                {
                    //// Aboue 9 Hours
                    //SSQL = "";
                    //SSQL = "Select Shift,isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(9 as decimal(18,1)))), '0') as OT_8Hours ";
                    //SSQL = SSQL + " from LogTime_Days ";
                    //SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and Shift<>'SHIFT3'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

                    //SSQL = SSQL + " And Present ='1' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 9 group by Shift";
                    //dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    //if (dt_OT.Rows.Count > 0) { OT_Above_Eight = dt_OT.Rows[0]["OT_8Hours"].ToString(); if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT2" && Convert.ToDecimal(OT_Above_Eight) >= 7) { S3OT_Above_7 = OT_Above_Eight; OT_Above_Eight = "0"; } } else { OT_Above_Eight = "0"; }


                    //// Aboue 4 Hours and Below 8 Hours
                    //SSQL = "";
                    //SSQL = "Select Shift,isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(4 as decimal(18,1)))), '0') as OT_8Hours ";
                    //SSQL = SSQL + " from LogTime_Days ";
                    //SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and Shift<>'SHIFT3'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

                    //SSQL = SSQL + " And (Present ='1' or Present ='0.5') and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 4 and Total_Hrs < 7 group by Shift";
                    //dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    //if (dt_OT.Rows.Count > 0) { OT_Above_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Four = "0"; }


                    //// Below 4
                    //SSQL = "";
                    //SSQL = "Select Shift,isnull(SUM(CAST(Total_Hrs as decimal(18,1))), '0') as OT_8Hours ";
                    //SSQL = SSQL + " from LogTime_Days ";
                    //SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and Shift<>'SHIFT3'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

                    //SSQL = SSQL + " And TypeName!='Leave' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs < 4 group by Shift";
                    //dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    ////if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }

                    //if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }

                    ////SHIFT 3
                    //// Aboue 8 Hours
                    //SSQL = "";
                    //SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(7 as decimal(18,1)))), '0') as OT_8Hours ";
                    //SSQL = SSQL + " from LogTime_Days ";
                    //SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and Shift='SHIFT3'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

                    //SSQL = SSQL + " And Present ='1' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 7";
                    //dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    //if (dt_OT.Rows.Count > 0) { S3OT_Above_7 = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { S3OT_Above_7 = "0"; }


                    //// Aboue 4 Hours and Below 8 Hours
                    //SSQL = "";
                    //SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(4 as decimal(18,1)))), '0') as OT_8Hours ";
                    //SSQL = SSQL + " from LogTime_Days ";
                    //SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and Shift='SHIFT3'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

                    //SSQL = SSQL + " And (Present ='1' or Present ='0.5') and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 4 and CONVERT(time,Total_Hrs1)<CONVERT(time,'6:5')";
                    //dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    //if (dt_OT.Rows.Count > 0) { S3OT_Above_4 = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { S3OT_Above_4 = "0"; }


                    //// Below 4
                    //SSQL = "";
                    //SSQL = "Select isnull(SUM(CAST(Total_Hrs as decimal(18,1))), '0') as OT_8Hours ";
                    //SSQL = SSQL + " from LogTime_Days ";
                    //SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and Shift='SHIFT3'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

                    //SSQL = SSQL + " And TypeName!='Leave' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs < 4 ";
                    //dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    //if (dt_OT.Rows.Count > 0) { S3OT_Below_4 = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { S3OT_Below_4 = "0"; }


                    SSQL = "";
                    SSQL = "Select Shift,TimeIN,TimeOUT,isnull(CAST(Total_Hrs as decimal(18,2)),'0') as OT_Hrs,Total_Hrs1";
                    SSQL = SSQL + " from LogTime_Days ";
                    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + " And (TypeName!='Leave' or TypeName!='Improper' or Present_Absent!='Half Day') and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' ";
                    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_OT.Rows.Count > 0)
                    {
                        OT_Above_Eight = dt_OT.Rows[0]["OT_Hrs"].ToString();
                        OT_Check = dt_OT.Rows[0]["Total_Hrs1"].ToString();

                        if (Convert.ToDecimal(OT_Above_Eight) >= 9)
                        {
                            if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT2")
                            {
                                S3OT_Above_7 = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(9)).ToString();
                                OT_Above_Eight = "0";
                            }
                            if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT1")
                            {
                                OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(9)).ToString();
                            }
                            if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT3")
                            {
                                OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(7)).ToString();
                            }
                            if(dt_OT.Rows[0]["Shift"].ToString() == "GENERAL")
                            {
                                OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(9)).ToString();
                            }
                            if (dt_OT.Rows[0]["Shift"].ToString() == "No Shift")
                            {
                                OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(9)).ToString();
                            }
                            if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT1A")
                            {
                                OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(9)).ToString();
                            }
                        }
                        else if (Convert.ToDecimal(OT_Above_Eight) <= 8)
                        {
                            if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT3")
                            {
                                if (Convert.ToDecimal(TimeSpan.Parse(OT_Check).TotalHours) >= Convert.ToDecimal(TimeSpan.Parse("06:30").TotalHours))
                                {
                                    OT_Above_Eight = (Convert.ToDecimal(TimeSpan.Parse(OT_Check).TotalHours) - Convert.ToDecimal(TimeSpan.Parse("06:30").TotalHours)).ToString();
                                    TimeSpan timespan = TimeSpan.FromHours(Convert.ToDouble(OT_Above_Eight));
                                    // int hh = timespan.Hours;
                                    OT_Above_Eight = (timespan.Hours).ToString();
                                }
                                else if (Convert.ToDecimal(OT_Above_Eight) > 4)
                                {
                                    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(4)).ToString();
                                }
                                else if (Convert.ToDecimal(OT_Above_Eight) <= 4)
                                {
                                    OT_Above_Eight = OT_Above_Eight;
                                }
                            }
                            else if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT2")
                            {
                                if (Convert.ToDecimal(OT_Above_Eight) >= 8)
                                {
                                    S3OT_Above_7 = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(8)).ToString();
                                    OT_Above_Eight = "0";
                                }
                                else if (Convert.ToDecimal(OT_Above_Eight) > 4)
                                {
                                    S3OT_Above_7 = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(4)).ToString();
                                    OT_Above_Eight = "0";
                                }
                                else if (Convert.ToDecimal(OT_Above_Eight) <= 4)
                                {
                                    S3OT_Above_7 = OT_Above_Eight;
                                    OT_Above_Eight = "0";
                                }
                            }
                            else if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT1")
                            {
                                if (Convert.ToDecimal(OT_Above_Eight) >= 8)
                                {
                                    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(8)).ToString();
                                }
                                else if (Convert.ToDecimal(OT_Above_Eight) > 4)
                                {
                                    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(4)).ToString();
                                }
                                else if (Convert.ToDecimal(OT_Above_Eight) <= 4)
                                {
                                    OT_Above_Eight = OT_Above_Eight;
                                }
                            }
                            if (dt_OT.Rows[0]["Shift"].ToString() == "GENERAL")
                            {
                                if (Convert.ToDecimal(OT_Above_Eight) >= 8)
                                {
                                    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(8)).ToString();
                                }
                                else if (Convert.ToDecimal(OT_Above_Eight) > 4)
                                {
                                    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(4)).ToString();
                                }
                                else if (Convert.ToDecimal(OT_Above_Eight) <= 4)
                                {
                                    OT_Above_Eight = OT_Above_Eight;
                                }
                            }
                            if (dt_OT.Rows[0]["Shift"].ToString() == "No Shift")
                            {
                                if (Convert.ToDecimal(OT_Above_Eight) >= 8)
                                {
                                    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(8)).ToString();
                                }
                                else if (Convert.ToDecimal(OT_Above_Eight) > 4)
                                {
                                    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(4)).ToString();
                                }
                                else if (Convert.ToDecimal(OT_Above_Eight) <= 4)
                                {
                                    OT_Above_Eight = OT_Above_Eight;
                                }
                            }
                            if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT1A")
                            {
                                if (Convert.ToDecimal(OT_Above_Eight) >= 8)
                                {
                                    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(8)).ToString();
                                }
                                else if (Convert.ToDecimal(OT_Above_Eight) > 4)
                                {
                                    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(4)).ToString();
                                }
                                else if (Convert.ToDecimal(OT_Above_Eight) <= 4)
                                {
                                    OT_Above_Eight = OT_Above_Eight;
                                }
                            }
                        }
                    }
                    else
                    {
                        OT_Above_Eight = "0";
                        S3OT_Above_7 = "0";
                    }
                }
                else
                {
                    //// Aboue 8 Hours
                    //SSQL = "";
                    //SSQL = "Select Shift,isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(8 as decimal(18,1)))), '0') as OT_8Hours ";
                    //SSQL = SSQL + " from LogTime_Days ";
                    //SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and Shift<>'SHIFT3'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

                    //SSQL = SSQL + " And Present ='1' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 8 group by Shift";
                    //dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    //if (dt_OT.Rows.Count > 0) { OT_Above_Eight = dt_OT.Rows[0]["OT_8Hours"].ToString(); if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT2" && Convert.ToDecimal(OT_Above_Eight) >= 7) { S3OT_Above_7 = OT_Above_Eight; OT_Above_Eight = "0"; } } else { OT_Above_Eight = "0"; }


                    //// Aboue 4 Hours and Below 8 Hours
                    //SSQL = "";
                    //SSQL = "Select Shift,isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(4 as decimal(18,1)))), '0') as OT_8Hours ";
                    //SSQL = SSQL + " from LogTime_Days ";
                    //SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and Shift<>'SHIFT3'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

                    //SSQL = SSQL + " And (Present ='1' or Present ='0.5') and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 4 and Total_Hrs < 7 group  by Shift";
                    //dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    //if (dt_OT.Rows.Count > 0) { OT_Above_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Four = "0"; }


                    //// Below 4
                    //SSQL = "";
                    //SSQL = "Select Shift,isnull(SUM(CAST(Total_Hrs as decimal(18,1))), '0') as OT_8Hours ";
                    //SSQL = SSQL + " from LogTime_Days ";
                    //SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and Shift<>'SHIFT3'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

                    //SSQL = SSQL + " And TypeName!='Leave' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs < 4 group by Shift";
                    //dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    //if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }


                    ////SHIFT 3
                    //// Aboue 8 Hours
                    //SSQL = "";
                    //SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(7 as decimal(18,1)))), '0') as OT_8Hours ";
                    //SSQL = SSQL + " from LogTime_Days ";
                    //SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and Shift='SHIFT3'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

                    //SSQL = SSQL + " And Present ='1' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 7";
                    //dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    //if (dt_OT.Rows.Count > 0) { S3OT_Above_7 = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { S3OT_Above_7 = "0"; }


                    //// Aboue 4 Hours and Below 8 Hours
                    //SSQL = "";
                    //SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(4 as decimal(18,1)))), '0') as OT_8Hours ";
                    //SSQL = SSQL + " from LogTime_Days ";
                    //SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and Shift='SHIFT3'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

                    //SSQL = SSQL + " And (Present ='1' or Present ='0.5') and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 4 and CONVERT(time,Total_Hrs1)<CONVERT(time,'6:5')";
                    //dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    //if (dt_OT.Rows.Count > 0) { S3OT_Above_4 = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { S3OT_Above_4 = "0"; }


                    //// Below 4
                    //SSQL = "";
                    //SSQL = "Select isnull(SUM(CAST(Total_Hrs as decimal(18,1))), '0') as OT_8Hours ";
                    //SSQL = SSQL + " from LogTime_Days ";
                    //SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //SSQL = SSQL + " and Shift='SHIFT3'";
                    //SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

                    //SSQL = SSQL + " And TypeName!='Leave' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs < 4 ";
                    //dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    //if (dt_OT.Rows.Count > 0) { S3OT_Below_4 = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { S3OT_Below_4 = "0"; }

                    SSQL = "";
                    SSQL = "Select Shift,isnull(CAST(Total_Hrs as decimal(18,2)),'0') as OT_Hrs,isnull(Total_Hrs1,'0') as OT_Check_Hrs ";
                    SSQL = SSQL + " from LogTime_Days ";
                    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + " And (TypeName!='Leave' or TypeName!='Improper' or Present_Absent!='Half Day') and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' ";
                    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_OT.Rows.Count > 0)
                    {
                        OT_Above_Eight = dt_OT.Rows[0]["OT_Hrs"].ToString();
                        OT_Check = dt_OT.Rows[0]["OT_Check_Hrs"].ToString();
                       // S3OT_Above_7 = "0";

                        if (Convert.ToDecimal(OT_Above_Eight) > 8)
                        {
                            if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT2")
                            {
                                S3OT_Above_7 = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(8)).ToString();
                                OT_Above_Eight = "0";
                            }
                            if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT1")
                            {
                                OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(8)).ToString();
                            }
                            if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT3")
                            {
                                OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(7)).ToString();
                            }
                            if (dt_OT.Rows[0]["Shift"].ToString() == "No Shift")
                            {
                                OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(8)).ToString();
                            }
                            if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT1A")
                            {
                                OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(8)).ToString();
                            }
                        }
                        else if (Convert.ToDecimal(OT_Above_Eight) <= 8)
                        {
                            if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT3")
                            {
                                string too = Convert.ToDecimal(TimeSpan.Parse(OT_Check).TotalHours).ToString();
                                string trrr = Convert.ToDecimal(TimeSpan.Parse("06:30").TotalHours).ToString();
                                if (Convert.ToDecimal(TimeSpan.Parse(OT_Check).TotalHours) >= Convert.ToDecimal(TimeSpan.Parse("06:30").TotalHours))
                                {
                                    OT_Above_Eight = (Convert.ToDecimal(TimeSpan.Parse(OT_Check).TotalHours) - Convert.ToDecimal(TimeSpan.Parse("06:30").TotalHours)).ToString();
                                    TimeSpan timespan = TimeSpan.FromHours(Convert.ToDouble(OT_Above_Eight));
                                    // int hh = timespan.Hours;
                                    OT_Above_Eight = (timespan.Hours).ToString();
                                }
                                //else if (Convert.ToDecimal(OT_Above_Eight) > 4)
                                //{
                                //    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(4)).ToString();
                                //}
                                //else if (Convert.ToDecimal(OT_Above_Eight) <= 4)
                                //{
                                //    OT_Above_Eight = OT_Above_Eight;
                                //}
                                else
                                {
                                    OT_Above_Eight = "0";
                                }
                            }
                            else if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT2")
                            {
                                if (Convert.ToDecimal(OT_Above_Eight) >= 8)
                                {
                                    S3OT_Above_7 = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(8)).ToString();
                                    OT_Above_Eight = "0";
                                }
                                //else if (Convert.ToDecimal(OT_Above_Eight) > 4)
                                //{
                                //    S3OT_Above_7 = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(4)).ToString();
                                //    OT_Above_Eight = "0";
                                //}
                                //else if (Convert.ToDecimal(OT_Above_Eight) <= 4)
                                //{
                                //    S3OT_Above_7 = OT_Above_Eight;
                                //    OT_Above_Eight = "0";
                                //}
                                else
                                {
                                    S3OT_Above_7 = "0";
                                    OT_Above_Eight = "0";
                                }
                            }
                            else if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT1")
                            {
                                if (Convert.ToDecimal(OT_Above_Eight) >= 8)
                                {
                                    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(8)).ToString();
                                }
                                //else if (Convert.ToDecimal(OT_Above_Eight) > 4)
                                //{
                                //    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(4)).ToString();
                                //}
                                //else if (Convert.ToDecimal(OT_Above_Eight) <= 4)
                                //{
                                //    OT_Above_Eight = OT_Above_Eight;
                                //}
                                else
                                {
                                    OT_Above_Eight = "0";
                                }
                            }
                            if (dt_OT.Rows[0]["Shift"].ToString() == "No Shift")
                            {
                                if (Convert.ToDecimal(OT_Above_Eight) >= 8)
                                {
                                    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(8)).ToString();
                                }
                                //else if (Convert.ToDecimal(OT_Above_Eight) > 4)
                                //{
                                //    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(4)).ToString();
                                //}
                                //else if (Convert.ToDecimal(OT_Above_Eight) <= 4)
                                //{
                                //    OT_Above_Eight = OT_Above_Eight;
                                //}
                                else
                                {
                                    OT_Above_Eight = "0";
                                }
                            }
                            if (dt_OT.Rows[0]["Shift"].ToString() == "SHIFT1A")
                            {
                                if (Convert.ToDecimal(OT_Above_Eight) >= 8)
                                {
                                    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(8)).ToString();
                                }
                                //else if (Convert.ToDecimal(OT_Above_Eight) > 4)
                                //{
                                //    OT_Above_Eight = (Convert.ToDecimal(OT_Above_Eight) - Convert.ToDecimal(4)).ToString();
                                //}
                                //else if (Convert.ToDecimal(OT_Above_Eight) <= 4)
                                //{
                                //    OT_Above_Eight = OT_Above_Eight;
                                //}
                                else
                                {
                                    OT_Above_Eight = "0";
                                }
                            }
                        }
                    }
                    else
                    {
                        OT_Above_Eight = "0";
                        S3OT_Above_7 = "0";
                    }

                }

                OT_Total_Hours = (Convert.ToDecimal(OT_Above_Eight) + Convert.ToDecimal(OT_Above_Four) + Convert.ToDecimal(OT_Below_Four) + Convert.ToDecimal(S3OT_Above_7) + Convert.ToDecimal(S3OT_Above_4) + Convert.ToDecimal(S3OT_Below_4)).ToString();

                OT_Hours = OT_Total_Hours;

                if (Convert.ToDecimal(OT_Hours) == Convert.ToDecimal(0))
                {

                }
                else
                {
                    if (dt.Rows[i]["Code"].ToString().ToString() == "030238")
                    {
                        string Stop = "";
                    }

                    SSQL = "";
                    SSQL = "Select TimeIN from LogTime_IN where  CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID ='" + UTF8Encryption(dt.Rows[i]["Code"].ToString()) + "'";
                    SSQL = SSQL + " And TimeIN >='" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(FromDate).AddDays(1).ToString("yyyy/MM/dd")+ " " + "02:00'";
                    SSQL = SSQL + " order by TimeIN desc";
                    DataTable LogTimeIN_DT = new DataTable();
                    LogTimeIN_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                    if(LogTimeIN_DT!=null && LogTimeIN_DT.Rows.Count > 0)
                    {
                        dtRow["OT_TimeIN"] = Convert.ToDateTime(string.Format(Convert.ToString(LogTimeIN_DT.Rows[0][0].ToString()), "hh:mm tt")).ToString("hh:mm tt");
                    }
                    else
                    {
                        dtRow["OT_TimeIN"] = "";
                    }

                    SSQL = "";
                    SSQL = "Select TimeOUT from LogTime_OUT where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + UTF8Encryption(dt.Rows[i]["Code"].ToString()) + "'";
                    SSQL = SSQL + " And TimeOUT >='" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(FromDate).AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00'";
                    SSQL = SSQL + " order by TimeOUT desc";
                    DataTable LogTimeOUT_DT = new DataTable();
                    LogTimeOUT_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (LogTimeOUT_DT != null && LogTimeOUT_DT.Rows.Count > 0)
                    {
                        dtRow["OT_TimeOUT"] = Convert.ToDateTime(string.Format(Convert.ToString(LogTimeOUT_DT.Rows[0]["TimeOUT"].ToString()), "hh:mm tt")).ToString("hh:mm tt");
                    }
                    else
                    {
                        dtRow["OT_TimeOUT"] = "";
                    }

                    dtRow["S.No"] = AutoDTable.Rows.Count + 1;
                    dtRow["Code"] = dt.Rows[i]["Code"].ToString();
                    dtRow["MachineID"] = dt.Rows[i]["MachineID"].ToString();
                    dtRow["AccountNo"] = dt.Rows[i]["AccountNo"].ToString();
                    dtRow["Name"] = dt.Rows[i]["Name"].ToString();
                    //dtRow["Dept"] = dt.Rows[i]["Dept"].ToString();
                    dtRow["Wages"] = dt.Rows[i]["Wages"].ToString();
                   // dtRow["Shift"] = dt_OT.Rows[0]["Shift"].ToString();
                    dtRow["Total_OTHrs"] = OT_Hours;


                    SSQL = "";
                    //SSQL = "Select isnull(OTAmt,0) as OTAmt  from MstOTIncentive where CONVERT(decimal(18,2),'" + OT_Hours + "')>=OTHrsFm and CONVERT(decimal(18,2),'" + OT_Hours + "')<=OTHrsTo and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    SSQL = "Select isnull(OTAmt,0) as OTAmt from MstOTIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    DataTable OtAmt = new DataTable();
                    OtAmt = objdata.RptEmployeeMultipleDetails(SSQL);
                    

                    if (OtAmt.Rows.Count > 0)
                    {
                        OT_Amt = OtAmt.Rows[0]["OTAmt"].ToString();

                        OT_Amt = (Convert.ToDecimal(dt.Rows[i]["OT_Salary"].ToString()) + Convert.ToDecimal(OT_Amt)).ToString();
                        Total_Amt = (Convert.ToDecimal(OT_Amt) / Convert.ToDecimal(8)).ToString();
                        Total_Amt = (Math.Round(Convert.ToDecimal(Total_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                        if (Convert.ToDecimal(S3OT_Above_7) >= Convert.ToDecimal(6.5))
                        {
                            S3OT_Above_7 = "0";
                            Calc_Amt = (Convert.ToDecimal(8) * Convert.ToDecimal(Total_Amt)).ToString();
                        }
                        else
                        {
                            Calc_Amt = (Convert.ToDecimal(OT_Hours) * Convert.ToDecimal(Total_Amt)).ToString();
                        }
                        string[] AmtArray = Calc_Amt.ToString().Split('.');
                        if(AmtArray.Length==2 && Convert.ToInt32(AmtArray[1].ToString()) >= 10)
                        {
                            Calc_Amt = (Convert.ToDecimal(AmtArray[0]) + 1).ToString();
                        }
                        else
                        {
                            Calc_Amt = Convert.ToDecimal(AmtArray[0]).ToString();
                        }
                        // Calc_Amt = (Convert.ToDecimal(OT_Hours) * Convert.ToDecimal(Total_Amt)).ToString();
                        string [] decimal_point_check = Calc_Amt.ToString().Split('.');
                        if (decimal_point_check.Length > 1 && Convert.ToInt32(decimal_point_check[1]) > 0)
                        {
                            Calc_Amt = (Convert.ToInt32(Calc_Amt) + 1).ToString();
                        }
                        Calc_Amt = (Math.Round(Convert.ToDecimal(Calc_Amt), 0, MidpointRounding.AwayFromZero)).ToString();
                        dtRow["Total_Amt"] = Calc_Amt;

                        if (Convert.ToDecimal(OT_Hours) > 2)
                        {
                            AutoDTable.Rows.Add(dtRow);
                        }
                    }
                }

                //else if ((Convert.ToDecimal(OT_Hours) > Convert.ToDecimal(0)) && (Convert.ToDecimal(OT_Hours) <= Convert.ToDecimal(4)))
                //{
                //    dtRow["EmpCode"] = dt.Rows[i]["EmpCode"].ToString();
                //    dtRow["MachineID"] = dt.Rows[i]["MachineID"].ToString();
                //    dtRow["Name"] = dt.Rows[i]["Name"].ToString();
                //    dtRow["Dept"] = dt.Rows[i]["Dept"].ToString();
                //    dtRow["Wages"] = dt.Rows[i]["Wages"].ToString();
                //    dtRow["Total"] = OT_Hours;

                //    OT_Amt = (Convert.ToDecimal(dt.Rows[i]["OT_Salary"].ToString()) + Convert.ToDecimal(30)).ToString();

                //    Total_Amt = (Convert.ToDecimal(OT_Amt) / Convert.ToDecimal(8)).ToString();
                //    Total_Amt = (Math.Round(Convert.ToDecimal(Total_Amt), 0, MidpointRounding.AwayFromZero)).ToString();

                //    Calc_Amt = (Convert.ToDecimal(OT_Hours) * Convert.ToDecimal(Total_Amt)).ToString();
                //    Calc_Amt = (Math.Round(Convert.ToDecimal(Calc_Amt), 0, MidpointRounding.AwayFromZero)).ToString();

                //    dtRow["GrandTOT"] = Calc_Amt;

                //    AutoDTable.Rows.Add(dtRow);
                //}
                //else if (Convert.ToDecimal(OT_Hours) > Convert.ToDecimal(4))
                //{
                //    if(dt.Rows[i]["MachineID"].ToString()== "040193")
                //    {
                //        string iii = "0";
                //    }
                //    dtRow["EmpCode"] = dt.Rows[i]["EmpCode"].ToString(); ;
                //    dtRow["MachineID"] = dt.Rows[i]["MachineID"].ToString(); ;
                //    dtRow["Name"] = dt.Rows[i]["Name"].ToString(); ;
                //    dtRow["Dept"] = dt.Rows[i]["Dept"].ToString(); ;
                //    dtRow["Wages"] = dt.Rows[i]["Wages"].ToString();
                //    dtRow["Total"] = OT_Hours;

                //    OT_Amt = (Convert.ToDecimal(dt.Rows[i]["OT_Salary"].ToString()) + Convert.ToDecimal(60)).ToString();

                //    Total_Amt = (Convert.ToDecimal(OT_Amt) / Convert.ToDecimal(8)).ToString();
                //    Total_Amt = (Math.Round(Convert.ToDecimal(Total_Amt), 0, MidpointRounding.AwayFromZero)).ToString();

                //    Calc_Amt = (Convert.ToDecimal(OT_Hours) * Convert.ToDecimal(Total_Amt)).ToString();
                //    Calc_Amt = (Math.Round(Convert.ToDecimal(Calc_Amt), 0, MidpointRounding.AwayFromZero)).ToString();

                //    dtRow["GrandTOT"] = Calc_Amt;
                //    AutoDTable.Rows.Add(dtRow);
                //}
            }
            string Check = "0";
        }
    }
    public void NonAdminGetAttdTable_Weekly_OTHOURS()
    { 
    
    }
    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }

}
