﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="RptPayELCL.aspx.cs" Inherits="RptPayELCL" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>


 <!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Report</a></li>
				<li class="active">EL/CL</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">EL/CL</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">EL/CL</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                    
                     <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlcategory" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlcategory_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="STAFF">STAFF</asp:ListItem>
								 <asp:ListItem Value="LABOUR">LABOUR</asp:ListItem>
								 </asp:DropDownList>
								 <asp:RequiredFieldValidator ControlToValidate="ddlcategory" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="txtEmployeeType" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								 <asp:RequiredFieldValidator ControlToValidate="txtEmployeeType" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                             <div class="col-md-4">
								<div class="form-group">
								 <label>Department</label>
								 <asp:DropDownList runat="server" ID="ddlDept" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                              <div class="col-md-4" runat="server" visible="false">
								<div class="form-group">
								 <label>Division</label>
								 <asp:DropDownList runat="server" ID="txtDivision" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Fin Year</label>
								 <asp:DropDownList runat="server" ID="txtFinancial_Year" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Salary Through</label>
								
								    <asp:RadioButtonList ID="RdbCashBank" runat="server" RepeatColumns="3" class="form-control">
                                         <asp:ListItem Selected="true" Text="ALL" style="padding-right:40px" Value="0"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Cash" style="padding-right:40px" Value="1"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Bank" Value="2"></asp:ListItem>
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>PF / Non PF</label>
								     <asp:RadioButtonList ID="RdbPFNonPF" runat="server" RepeatColumns="3" class="form-control">
                                        <asp:ListItem Selected="True" Text="ALL" style="padding-right:40px" Value="0"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="PF" style="padding-right:40px" Value="1"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="Non PF" Value="2"></asp:ListItem>
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4" runat="server" visible="false">
								<div class="form-group">
								 <label>Eligibility Worked Days</label>
								<asp:TextBox ID="txtEligbleDays" runat="server" Text="0" class="form-control"></asp:TextBox>
								</div>
                               </div>
                           <!-- end col-4 -->
                         
                           <!-- begin col-4 -->
                              <div class="col-md-4" runat="server" visible="false">
								<div class="form-group">
								  <asp:CheckBox ID="ChkLeft" runat="server" />LeftEmployee
								 <asp:TextBox ID="txtLeftDate" runat="server" class="form-control datepicker"></asp:TextBox>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                       
                            <!-- begin row -->
                        <div class="row">
                           <!-- begin col-4 -->
                          <div class="col-md-4" runat="server" visible="false">
                               <div class="form-group">
                                   <div class="col-md-8">
                                        <label class="checkbox-inline">
                                            <asp:CheckBox ID="ChkBelow" runat="server" />Below Employee
                                        </label>
                                    </div>
                                </div>
                             </div>
                           <!-- end col-4 -->
                          
                           <!-- begin col-4 -->
                              <div class="col-md-5">
								<div class="form-group">
								 <label>Report Type</label>
								     <asp:RadioButtonList ID="rdbPayslipFormat" runat="server" RepeatColumns="5" class="form-control">
                                       <asp:ListItem Selected="true" Text="Consolidate" style="padding-right:10px" Value="1"></asp:ListItem>
                                      
                                       <asp:ListItem Selected="False" Text="Cover" style="padding-right:10px" Value="2"></asp:ListItem>
                                       <asp:ListItem Selected="False" Text="Signlist" style="padding-right:10px" Value="3"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="BankList" style="padding-right:10px" Value="4"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Working Days" style="padding-right:10px" Value="5"></asp:ListItem>
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                         <!-- end row -->
                         <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnELView" Text="EL / CL View" class="btn btn-success" ValidationGroup="Validate_PayField" onclick="btnELView_Click"/>
									
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        
                         <asp:Panel ID="Result_Panel" Visible="false" runat="server">
				                                <table class="full">
				                                <tbody>
				                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="griddept" runat="server" AutoGenerateColumns="false" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Staff Strength</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblStaff_Strength" runat="server" Text= '<%# Eval("Staff_Strength") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Staff Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblStaff_Salary" runat="server" Text='<%# Eval("Staff_Salary") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Labour Strength</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblLabour_Strength" runat="server" Text='<%# Eval("Labour_Strength") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Labour Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblLabour_Salary" runat="server" Text='<%# Eval("Labour_Salary") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Total Strength</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTotal_Strength" runat="server" Text='<%# Eval("Total_Strength") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Total Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTotal_Salary" runat="server" Text='<%# Eval("Total_Salary") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
				                                </tbody>
				                            </table>
				                            </asp:Panel>
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->



</asp:Content>

