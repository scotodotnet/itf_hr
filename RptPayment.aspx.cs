﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class RptPayment : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string Date = "";

    ReportDocument report = new ReportDocument();

    DataTable Log_DS = new DataTable();
    string SessionPayroll;
    BALDataAccess objdata = new BALDataAccess();

    DataTable AutoDTable = new DataTable();
    DataTable Course1 = new DataTable();
    string CmpName, Cmpaddress;
    string SSQL = "";
    DateTime date1;
    DateTime date2;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string status;
    string MillCode = "";
    string UnitCode = "";
    string DeptCode = "";
    string TotalAmt = "0";
    string DesignationCode = "";
    string CourseCode = "";
    string CourseLevel = "";
    string Course;
    string presentCount="0";
    string desigAmount="0";
    string Designation="";
    string Amt="0";
    string Qty="0";string Round_Pisa = "0";
    string Round_Nearest = "0";
  
    string GST;
    string CGSTp = "0";
    string SGSTP = "0";
    string IGSTP = "0";
    string Coursecode = "";
    string TotalGstAmt = "0";
    string Round = "0";
    string Grand = "0";
    string CgstAmt = "0";
    string SgstAmt = "0";
    string IgstAmt = "0";
    string MillName = "";
    string UnitName = "";
    string MillAddress = "";
    string MillGstno = "";


    DataSet ds = new DataSet();
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "BILL ";
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            DesignationCode = Request.QueryString["DesignationCode"].ToString();
            MillCode = Request.QueryString["MillCode"].ToString();
            UnitCode = Request.QueryString["UnitCode"].ToString();
            CourseCode = Request.QueryString["CourseCode"].ToString();
            GST = Request.QueryString["GST"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            CourseLevel = Request.QueryString["CourseLevel"].ToString();
            Payment();
            
        }
    }
    public void Payment()
    {
        Boolean Errflag = true;

        AutoDTable.Columns.Add("MillName");
        AutoDTable.Columns.Add("UnitName");
        AutoDTable.Columns.Add("MillAddress");
        AutoDTable.Columns.Add("MillGstNo");
        AutoDTable.Columns.Add("InvNo");
        AutoDTable.Columns.Add("Date");
        AutoDTable.Columns.Add("Paymentdue");
        AutoDTable.Columns.Add("ProgramName");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("Qty");
        AutoDTable.Columns.Add("PerHead");
        AutoDTable.Columns.Add("Amt");
        AutoDTable.Columns.Add("GrandTotal");
        AutoDTable.Columns.Add("Words");

        AutoDTable.Columns.Add("Cgstp");
        AutoDTable.Columns.Add("Sgstp");
        AutoDTable.Columns.Add("Igstp");
        AutoDTable.Columns.Add("CgstAmt");
        AutoDTable.Columns.Add("SgstAmt");
        AutoDTable.Columns.Add("IgstAmt");
        AutoDTable.Columns.Add("RoundOff");

        try
        {
            DataTable present_DT = new DataTable();
            DataTable DT_DAmount = new DataTable();
         

            SSQL = "";
            SSQL = " Select distinct EM.MachineID,EM.FirstName from  Employee_Mst EM Inner join LogTime_IN LT on EM.MachineID_Encrypt = LT.MachineID ";
            SSQL = SSQL + " where LT.Compcode='" + SessionCcode + "' and LT.Loccode='" + SessionLcode + "' and";
            SSQL = SSQL + " Convert(datetime,LT.TimeIN,103)>=Convert(datetime,'" + FromDate + "',103) and Convert(datetime,LT.TimeIN,103)<=Convert(datetime,'" + ToDate + "',103)";
            SSQL = SSQL + " and EM.MillCode= '" + MillCode + "' and EM.UnitCode='" + UnitCode + "'";
            SSQL = SSQL + " and EM.DesignCode='" + DesignationCode + "'";
            present_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Select distinct CS.Amount,CS.Designation From Course_Costing CS inner join Designation_Mst DM on CS.Designation=DM.DesignName where DM.DesignCode= '" + DesignationCode + "'";
            DT_DAmount = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_DAmount != null && DT_DAmount.Rows.Count > 0)
            {
                desigAmount = DT_DAmount.Rows[0]["Amount"].ToString();
                Designation = DT_DAmount.Rows[0]["Designation"].ToString();
            }

            presentCount = present_DT.Rows.Count.ToString();
            Amt = (Convert.ToDecimal(desigAmount) * Convert.ToDecimal(presentCount)).ToString();

            SSQL = "";
            SSQL = "Select * from Mst_Gst where TaxDescription='" + GST + "' and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            
            CGSTp = dt.Rows[0]["CGST"].ToString();
            CgstAmt = (Convert.ToDecimal(Amt) * Convert.ToDecimal(CGSTp)).ToString();
            CgstAmt = (Convert.ToDecimal(CgstAmt) / 100).ToString();

            SGSTP = dt.Rows[0]["SGST"].ToString();
            SgstAmt = (Convert.ToDecimal(Amt) * Convert.ToDecimal(SGSTP)).ToString();
            SgstAmt = (Convert.ToDecimal(SgstAmt) / 100).ToString();

            IGSTP = dt.Rows[0]["IGST"].ToString();
            IgstAmt = (Convert.ToDecimal(Amt) * Convert.ToDecimal(IGSTP)).ToString();
            IgstAmt = (Convert.ToDecimal(IgstAmt) / 100).ToString();

            TotalGstAmt = (Convert.ToDecimal(CgstAmt) + Convert.ToDecimal(SgstAmt) + Convert.ToDecimal(IgstAmt)).ToString();

            TotalAmt = (Convert.ToDecimal(TotalGstAmt) + Convert.ToDecimal(Amt)).ToString();

            Round_Pisa = ((Convert.ToDecimal(TotalAmt)) - Math.Round(Convert.ToDecimal(TotalAmt), 0, MidpointRounding.AwayFromZero)).ToString();

            //Round = Math.Round(Convert.ToDecimal(TotalAmt), 0, MidpointRounding.AwayFromZero).ToString();

            Grand= Math.Round(Convert.ToDecimal(TotalAmt), 0, MidpointRounding.AwayFromZero).ToString();

            double d1 = Convert.ToDouble(Grand.ToString());
            int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
            Round_Nearest = rounded1.ToString();

            SSQL = "";
            SSQL = "Select * from MstMill where MillCode='" + MillCode + "'";
            DataTable Mill_Dt = new DataTable();
            Mill_Dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Mill_Dt.Rows.Count > 0)
            {
                MillName = Mill_Dt.Rows[0]["MillName"].ToString();
                MillGstno = Mill_Dt.Rows[0]["GstNo"].ToString();
                MillAddress = Mill_Dt.Rows[0]["Address"].ToString();
                UnitName = Mill_Dt.Rows[0]["Units"].ToString() + "-" + Mill_Dt.Rows[0]["MillTypeName"].ToString();
            }

            string invNo = "1";
            SSQL = "";
            SSQL = "select isnull(max(BillCode)+1,'1') as max from Paymentmode_Mst where CCode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            DataTable last_st = new DataTable();
            last_st = objdata.RptEmployeeMultipleDetails(SSQL);
            if (last_st != null && last_st.Rows.Count > 0)
            {
                invNo = last_st.Rows[0]["max"].ToString();
            }

          
            DataRow dr;
            for(int i=0;i< present_DT.Rows.Count; i++)
            {
                dr = AutoDTable.NewRow();
                dr["MillName"] = MillName;
                dr["MillAddress"] = MillAddress;
                dr["UnitName"] = UnitName;
                dr["MillGstNo"] = MillGstno;
                dr["InvNo"] = invNo;
                dr["Date"] = Convert.ToDateTime(DateTime.Now.ToString("dd-MM-yyyy"));
                //string dd = DateTime.Now.Date.ToString();
                //string MM = DateTime.Now.Month.ToString();
                //string yyyy = DateTime.Now.Year.ToString();
                //dr["Date"] = dd + "-" + MM + "-" + yyyy;
                dr["Paymentdue"] = "Immediate";
                dr["ProgramName"] = CourseCode;
                dr["Name"] = present_DT.Rows[i]["FirstName"];
                dr["Qty"] = presentCount;
                dr["PerHead"] = desigAmount;
                dr["Amt"] = Amt;
                dr["GrandTotal"] = Grand;
                dr["Words"] = Grand;
                
                dr["Cgstp"] = CGSTp;
                dr["Sgstp"] = SGSTP;
                dr["Igstp"] = IGSTP;
                dr["CgstAmt"] = CgstAmt;
                dr["SgstAmt"] = SgstAmt;
                dr["IgstAmt"] = IgstAmt;
                dr["RoundOff"] = Round_Pisa;

                AutoDTable.Rows.InsertAt(dr,i);
            }

            string Month = DateTime.Now.Month.ToString();

            SSQL = "Insert into Paymentmode_Mst (Ccode,Lcode,MillCode,UnitCode,FromDate,ToDate,Month,PresentEmployees,Designation,DesignationAmount,Total,";
            SSQL = SSQL + "GstPercentage,GSTAmount,TotalAmount,Roundoff,NetAmount,Status,CreatedDate)values('" + SessionCcode + "','" + SessionLcode + "','" + MillCode + "',";
            SSQL = SSQL + "'" + UnitName + "','" + FromDate + "','" + ToDate + "','" + Month + "','" + presentCount + "','" + Designation + "','" + desigAmount + "','" + Amt + "',";
            SSQL = SSQL + "'" + GST + "','" + TotalGstAmt + "','" + TotalAmt + "','" + Round_Pisa + "','" + Grand + "','Unpaid',GetDate())";

            objdata.RptEmployeeMultipleDetails(SSQL);

            //SSQL = "";
            //SSQL = "select PM.MillCode,PM.BillCode,PM.UnitCode,PM.FromDate,PM.ToDate,PM.Month,PM.PresentEmployees,Distinct PM.Designation,PM.DesignationAmount, ";
            //SSQL = SSQL + "PM.Total,PM.GSTPercentage,PM.GSTAmount,PM.TotalAmount,PM.Roundoff,PM.NetAmount,PM.Status,";
            //SSQL = SSQL + "EM.MillName from Paymentmode_Mst PM inner join Employee_Mst EM on EM.MillCode = PM.MillCode inner Join MstMill MM on MM.Millcode=PM.MillCode";
            //SSQL = SSQL + " where CC.Ccode='" + SessionCcode + "' and CC.Lcode='" + SessionLcode + "' and";
            //SSQL = SSQL + " Convert(datetime,FromDate,103)>=Convert(datetime,'" + FromDate + "',103) and Convert(datetime,ToDate,103)<=Convert(datetime,'" + ToDate + "',103)";

            //if (MillCode != "")
            //{
            //    SSQL = SSQL + " and MillCode= '" + MillCode + "'";
            //}
            //if (UnitCode != "")
            //{
            //    SSQL = SSQL + " and UnitCode= '" + UnitCode + "'";
            //}
            //if (DesignationCode != "")
            //{
            //    SSQL = SSQL + " and UnitCode= '" + DesignationCode + "'";
            //}
            //if (CourseCode != "")
            //{
            //    SSQL = SSQL + " and UnitCode= '" + CourseCode + "'";
            //}
            //if (GST != "")
            //{
            //    SSQL = SSQL + " and UnitCode= '" + GST + "'";
            //}

            //DataTable DT = new DataTable();
            //DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (AutoDTable.Rows.Count > 0)
            {
                ds.Tables.Add(AutoDTable);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("~/crystal/Invoice.rpt"));
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                
            }


        }
        catch (Exception ex)
        {


        }
    }
}
