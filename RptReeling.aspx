﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="RptReeling.aspx.cs" Inherits="RptReeling" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
 <script type="text/javascript">
     function SaveMsgAlert(msg) {
         swal(msg);
     }
</script>

<script type="text/javascript">
    function ProgressBarShow() {
        $('#Download_loader').show();
    }
</script>

<script type="text/javascript">
    function ProgressBarHide() {
        $('#Download_loader').hide();
    }
</script>
 <!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Reeling</a></li>
				<li class="active">Reeling Report</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Reeling Report</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
			 <asp:UpdatePanel ID="SalPay" runat="server">
                 <ContentTemplate>
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Reeling Report</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                       <!-- begin row -->
                            
                     <legend>Bonus Report</legend>
                     <!-- begin row -->
                        <div class="row">
                       
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>FinYear</label>
								  <asp:DropDownList runat="server" ID="ddlfinance" class="form-control select2" AutoPostBack="true"
                                        style="width:100%;"></asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlMonths" class="form-control select2" AutoPostBack="true"
                                        style="width:100%;" onselectedindexchanged="ddlMonths_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="January">January</asp:ListItem>
								 <asp:ListItem Value="February">February</asp:ListItem>
								 <asp:ListItem Value="March">March</asp:ListItem>
								 <asp:ListItem Value="April">April</asp:ListItem>
								 <asp:ListItem Value="May">May</asp:ListItem>
								 <asp:ListItem Value="June">June</asp:ListItem>
								 <asp:ListItem Value="July">July</asp:ListItem>
								 <asp:ListItem Value="August">August</asp:ListItem>
								 <asp:ListItem Value="September">September</asp:ListItem>
								 <asp:ListItem Value="October">October</asp:ListItem>
								 <asp:ListItem Value="November">November</asp:ListItem>
								 <asp:ListItem Value="December">December</asp:ListItem>
							 	 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                            <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>From Date</label>
										<asp:TextBox runat="server" ID="txtFrom" class="form-control datepicker"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>To Date</label>
										<asp:TextBox runat="server" ID="txtTo" class="form-control datepicker"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                         </div>
                       <!-- end row -->
                      
                         <!-- begin row -->
                        <div class="row">
                       
                            <!-- begin col-6 -->
                              <div class="col-md-10">
								<div class="form-group">
								 <label>Report Type</label>
								  <asp:RadioButtonList ID="rdbPayslipFormat" CssClass="form-control" runat="server" RepeatColumns="6">
                                         <%--<asp:ListItem Selected="False" Text="Individual" style="padding-right:30px" Value="1"></asp:ListItem>--%>
                                         <%--<asp:ListItem Selected="False" Text="Recociliation" style="padding-right:30px" Value="2"></asp:ListItem>--%>
                                      
                                         <asp:ListItem Selected="true"  Text="CHECKLIST" style="padding-right:30px;display:none" Value="1"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="PFCHECKLIST" style="padding-right:30px" Value="2"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="WAGESLIST" style="padding-right:30px" Value="3"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="ABSTRACTWAGESLIST" style="padding-right:30px" Value="4"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="BANK PAYMENT LIST"  Value="5" style="padding-right:30px"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="AQUITTANCE LIST"  Value="6"  style="padding-right:30px"></asp:ListItem>
                                          
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                          </div>
                         
                             <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-6">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnRpt" Text="Report View" 
                                         class="btn btn-success" onclick="btnRpt_Click" />
									<asp:Button runat="server" id="btnBonusExcelView" Text="Excel View" 
                                         class="btn btn-success" onclick="btnBonusExcelView_Click"  />
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                          <div id="Download_loader" style="display:none"/></div>
                        <!-- end row --> 
                        </div>
                        </div>
                        
                    </div>
                    <!-- end panel -->
             </ContentTemplate>
               <Triggers>
                    <asp:PostBackTrigger ControlID="btnRpt" />
                    <asp:PostBackTrigger ControlID="btnBonusExcelView" />
                 </Triggers>
             </asp:UpdatePanel>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</asp:Content>

