﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.SqlClient;

public partial class RptTourAllowance : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string CmpName;
    static string Cmpaddress;
    string SessionUserType;
    string SessionEpay;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SSQL = "";
    string SessionPayroll;
    bool ErrFlg = false;
    string ss = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        con = new SqlConnection(constr);

        if (!IsPostBack)
        {
            Load_DeptName();
        }
    }
    private void Load_DeptName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDeptName.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDeptName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptCode"] = "0";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDeptName.DataTextField = "DeptName";
        ddlDeptName.DataValueField = "DeptCode";
        ddlDeptName.DataBind();
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        ddlEmployeeType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType.Items.Insert(0, new ListItem("-Select-", "-Select-"));
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        if (ddlCategory.SelectedValue == "0")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Category')", true);
        }
        if (ddlEmployeeType.SelectedValue == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Employee Type')", true);
        }
        if (txtWorkedDays.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Worked Days')", true);
        }
        if (txtFromDate.Text == "" || txtToDate.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the From Date and To Date Properly')", true);
        }
        if (!ErrFlg)
        {
            string Report_Type_Call = "";

            if (RdbReportType.SelectedValue == "1")
            {
                Report_Type_Call = "TA_Bank_List";
            }
            if (RdbReportType.SelectedValue == "2")
            {
                Report_Type_Call = "TA_Acquittance_List";
            }
            if (RdbReportType.SelectedValue == "3")
            {
                Report_Type_Call = "TA_Uneligible_List";
            }
            if (RdbReportType.SelectedValue == "4")
            {
                Report_Type_Call = "TA_Signature_List";
            }
            ResponseHelper.Redirect("ViewReport.aspx?workDays=" + txtWorkedDays.Text + "&Cate=" + ddlCategory.SelectedValue + "&Depat=" + ddlDeptName.SelectedValue + "&fromdate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&EmpTypeCd=" + ddlEmployeeType.SelectedValue.ToString() + "&EmpType=" + ddlEmployeeType.SelectedItem.Text.ToString() + "&Months=" + "" + "&yr=" + "0" + "&Salary=" + "" + "&PayslipType=" + "" + "&ESICode=" + "" + "&PFTypePost=" + "" + "&Left_Emp=" + "" + "&Leftdate=" + "" + "&CashBank=" + "" + "&Report_Type=" + Report_Type_Call, "_blank", "");
        }
    }

    protected void btnClr_Click(object sender, EventArgs e)
    {
        ddlCategory.ClearSelection();
        ddlEmployeeType.ClearSelection();
        ddlDeptName.ClearSelection();
        txtFromDate.Text = "";
        txtToDate.Text = "";
        RdbReportType.ClearSelection();
    }
}