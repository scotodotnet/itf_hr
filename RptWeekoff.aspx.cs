﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
public partial class RptWeekoff : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string Date = "";
    ReportDocument report = new ReportDocument();

    DataTable Log_DS = new DataTable();
    string SessionPayroll;
    BALDataAccess objdata = new BALDataAccess();

    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    string CmpName, Cmpaddress;
    string SSQL = "";
    DateTime date1;
    DateTime date2;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string status;



    string MachineId;
    DataSet ds = new DataSet();
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();

    DataTable dt_Cat = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | INDIVIDUAL REPORT ";



            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            MachineId = Request.QueryString["MachineId"].ToString();
            //Date = Request.QueryString["Date"].ToString();
            Date = Request.QueryString["Date"].ToString();
            status = Request.QueryString["status"].ToString();

            Individual_Wise();
            //if (AutoDTable.Rows.Count > 0)
            //   {
            //       SSQL = "";

            //       SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";

            //       dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);

            //       if (dt_Cat.Rows.Count > 0)
            //       {
            //           CmpName = dt_Cat.Rows[0]["Cname"].ToString();
            //           Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            //       }

            //       grid.DataSource = AutoDTable;
            //       grid.DataBind();
            //       string attachment = "attachment;filename=DayWiseIndividual.xls";
            //       Response.ClearContent();
            //       Response.AddHeader("content-disposition", attachment);
            //       Response.ContentType = "application/ms-excel";
            //       grid.HeaderStyle.Font.Bold = true;
            //       System.IO.StringWriter stw = new System.IO.StringWriter();
            //       HtmlTextWriter htextw = new HtmlTextWriter(stw);
            //       grid.RenderControl(htextw);
            //       Response.Write("<table>");
            //       Response.Write("<tr Font-Bold='true' align='center'>");
            //       Response.Write("<td colspan='5'>");
            //       Response.Write("<a style=\"font-weight:bold\">" + CmpName + "</a>");
            //       Response.Write("--");
            //       Response.Write("<a style=\"font-weight:bold\">" + Cmpaddress + "</a>");
            //       Response.Write("</td>");
            //       Response.Write("</tr>");
            //       Response.Write("<tr Font-Bold='true' align='center'>");
            //       Response.Write("<td colspan='5'>");
            //       Response.Write("<a style=\"font-weight:bold\">DAY-WISE INDIVIDUAL REPORT - " + FromDate + "</a>");

            //       Response.Write("</td>");
            //       Response.Write("</tr>");
            //       Response.Write("</table>");
            //       Response.Write(stw.ToString());
            //       //Response.Write("<table border=1>");

            //       Response.Write("</tr>");
            //       Response.Write("</table>");
            //       Response.End();
            //       Response.Clear();

            //       //ds.Tables.Add(AutoDTable);
            //       //report.Load(Server.MapPath("crystal/WeekOff_New.rpt"));
            //       //SSQL = "";
            //       //SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            //       ////dt = objdata.RptEmployeeMultipleDetails(SSQL);
            //       //dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);

            //       //if (dt_Cat.Rows.Count > 0)
            //       //{
            //       //    CmpName = dt_Cat.Rows[0]["Cname"].ToString();
            //       //    Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            //       //}

            //       //report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            //       //report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName.ToString() + "'";
            //       //report.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            //       //report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate + "'";
            //       //report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";
            //       //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            //       //CrystalReportViewer1.ReportSource = report;
            //   }
        }
    }

    public void Individual_Wise()
    {
        AutoDTable.Columns.Add("S.No");
        AutoDTable.Columns.Add("ITF NO");
        AutoDTable.Columns.Add("ExCode");
        AutoDTable.Columns.Add("NAME");
        AutoDTable.Columns.Add("AttendanceType");
        AutoDTable.Columns.Add("INTIME");
        try
        {
           
            SSQL = "";
            SSQL = "Select Distinct Convert(char(5),LTD.TimeIN,108) as TimeIN, EM.MachineID,EM.ExistingCode,EM.FirstName,";
            SSQL = SSQL + "EM.CatName from LogTime_IN LTD inner join Employee_Mst EM on";
            SSQL = SSQL + " EM.CompCode=LTD.CompCode And EM.LocCode=LTD.LocCode And LTD.MachineID=EM.MachineID_Encrypt";
            SSQL = SSQL + " where LTD.CompCode='" + SessionCcode + "' And LTD.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103))";
            SSQL = SSQL + " and LTD.TimeIN >= '" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") +" 02:00'";
            SSQL = SSQL + " and LTD.TimeIN <= '" + Convert.ToDateTime(FromDate).AddDays(1).ToString("yyyy/MM/dd") + " 02:00'";

            if (MachineId != "-Select-" && MachineId != "")
            {
                SSQL = SSQL + " And  EM.MachineID='" + MachineId + "'";
            }
            SSQL = SSQL + " Group by EM.MachineID,EM.ExistingCode,LTD.TimeIN,EM.FirstName,EM.CatName ";
            SSQL = SSQL + " Order by (EM.MachineID) Asc";

            Log_DS = objdata.RptEmployeeMultipleDetails(SSQL);

           
            if (Log_DS.Rows.Count != 0)
            {
                for (int i = 0; i < Log_DS.Rows.Count; i++)
                {
                    string Machineid = Log_DS.Rows[i]["MachineID"].ToString();

                    if (Machineid == "930056")
                    {
                        string StopQuery = "";
                    }
                    string TimeIN = Log_DS.Rows[i]["TimeIN"].ToString();
                    string AttnType;
                    if (TimeIN != "")
                    {
                        AttnType = "P";
                    }

                    else
                    {
                        AttnType = "A";
                    }

                    //if (Log_DS.Rows.Count != 0)
                    //{
                    //    //int sno = 1;
                        //for (int j = 0; j < Log_DS.Rows.Count; j++)
                        //{
                            AutoDTable.NewRow();
                            AutoDTable.Rows.Add();
                            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["S.No"] = AutoDTable.Rows.Count;
                            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ITF NO"] = Log_DS.Rows[i]["MachineID"].ToString();
                            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ExCode"] = Log_DS.Rows[i]["MachineID"].ToString();
                           
                            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NAME"] = Log_DS.Rows[i]["FirstName"].ToString();
                          
                            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AttendanceType"] = AttnType;
                            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["INTIME"] = Log_DS.Rows[i]["TimeIN"].ToString();
                           
                        //}
                   // }
                }
                ds.Tables.Add(AutoDTable);
                string CmpName = "";
                string Cmpaddress = "";
                DataTable dt_Cat = new DataTable();


                SSQL = "";
                SSQL = SSQL + "Select CompName,City from Company_Mst";
                dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_Cat.Rows.Count > 0)
                {
                    CmpName = dt_Cat.Rows[0]["CompName"].ToString();
                    Cmpaddress = (dt_Cat.Rows[0]["City"].ToString());
                }

                //ReportDocument report = new ReportDocument();


                if (status == "TimeIN")
                {
                    report.Load(Server.MapPath("crystal/DayWiseAttn.rpt"));
                    // report.DataDefinition.FormulaFields["Date"].Text = "'" + Date + "'";
                }
                else
                {
                    report.Load(Server.MapPath("crystal/DayWiseAttn_Without_TimeIN.rpt"));

                }
                report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                report.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
                report.DataDefinition.FormulaFields["FromDT"].Text = "'" + Date + "'";
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
        }
        catch (Exception ex)
        {

        }
    }
}
