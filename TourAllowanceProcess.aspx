﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="TourAllowanceProcess.aspx.cs" Inherits="TourAllowanceProcess" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="assets/js/master_list_jquery.min.js"></script>
    <script src="assets/js/master_list_jquery-ui.min.js"></script>
    <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();
        });
    </script>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
    <!-- begin #content -->
    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li class="active">Tour Allowance Process Details</li>
        </ol>
        <h1 class="page-header">Tour Allowance Process</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">Tour Allowance Process</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <%--<h5>STAFF AND REGULAR WORKER BONUS FIXED DETAILS</h5>--%>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                            <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="STAFF" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="LABOUR" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Employee Type</label>
                                        <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" Style="width: 100%;" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Department</label>
                                        <asp:DropDownList runat="server" ID="ddlDeptName" class="form-control select2" Style="width: 100%;">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>From Date</label>
                                        <asp:TextBox runat="server" ID="txtFromDate"  class="form-control datepicker" Style="width: 100%;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>To Date</label>
                                        <asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker" Style="width: 100%;"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-2" runat="server" visible="false">
                                    <div class="form-group">
                                        <label>Worked Days</label>
                                        <asp:TextBox runat="server" ID="txtWorkedDays" Text="0" class="form-control" Style="width: 100%;"></asp:TextBox>
                                    </div>
                                </div>
                            
                            </div>
                            <div class="row">
                                <div class="col-md-5"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <br />
                                        <asp:Button runat="server" ID="btnProcess" Text="Process" class="btn btn-success" OnClick="btnProcess_Click" />
                                        <asp:Button runat="server" ID="btnClr" Text="Clear" class="btn btn-danger" OnClick="btnClr_Click" />
                                    </div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>


