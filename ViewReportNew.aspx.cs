﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;

public partial class ViewReportNew : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string str_cate;
    string str_dept;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin; string SessionUserType;
    string SessionEpay;
    string query;
    string fromdate;
    string ToDate;
    string RptName = "";
    string finYearVal = "";
    string SSQL = "";
   
    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        RptName = Request.QueryString["RptName"].ToString();
        finYearVal= Request.QueryString["FinYearVal"].ToString();
        fromdate= Request.QueryString["fromdate"].ToString();
        ToDate= Request.QueryString["ToDate"].ToString();
        str_month = Request.QueryString["Months"].ToString();

        Report_View();

    }

    private void Report_View()
    {
        SSQL = "";
        SSQL = "Select EmpNo,EmpName as EmployeeName,isnull(sum(OtherGrassWages),'0') as OtherGrossWages, isnull(sum(OtherNetWages),'0') as OtherNetWages,";
        SSQL = SSQL + "isnull(sum(PFS),'0') as OtherPF, isnull(sum(OneDayPaySalary),'0') as WagesAmount,isnull(sum(TotalWorkloadPay),'0') as DaysIncentive,";
        SSQL = SSQL + " isnull(sum(TotalELPay),'0') as TotalAmount,isnull(sum(ToTalWorkloadOTPay),'0') as OtAmount,isnull(sum(PFS),'0') as PfDed,isnull(sum(PayTotal),'0') as PaymentAmount,";
        SSQL = SSQL + " isnull(sum(NetAmt),'0') as TotalWages from [" + SessionEpay + "]..ReelingWagesProcess";
        SSQL = SSQL + " where Convert(datetime,FromDate,103)>=Convert(datetime,'" + fromdate + "',103) and Convert(datetime,ToDate,103)<=Convert(datetime,'" + ToDate + "',103)";
        SSQL = SSQL + " and Month='" + str_month + "' and FinYearVal='" + finYearVal + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " group by EmpNo,EmpName";
        DataTable AutoDT = new DataTable();
        AutoDT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (AutoDT != null && AutoDT.Rows.Count > 0)
        {

            string CmpName = "";
            string Cmpaddress = "";
            DataTable dt_Cat = new DataTable();

            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionEpay + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_Cat.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            if (RptName== "PFCHECKLIST")
            {
                rd.Load(Server.MapPath("crystal/ReelingPF_Checklist_1.rpt"));
                rd.SetDataSource(AutoDT);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //  rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
                rd.DataDefinition.FormulaFields["FromDate"].Text = "'" + fromdate + "'";
                rd.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";


                //rd.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "EmployeeInformation");
                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.AllowedExportFormats = formats;

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }


            if (RptName == "WAGESLIST")
            {
                rd.Load(Server.MapPath("crystal/ReelingWagelist_1.rpt"));
                rd.SetDataSource(AutoDT);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //  rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyLocation"].Text = "'" + Cmpaddress + "'";
                rd.DataDefinition.FormulaFields["FromDate"].Text = "'" + fromdate + "'";
                rd.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";


                //rd.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "EmployeeInformation");
                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.AllowedExportFormats = formats;

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }


            if (RptName == "ABSTRACTWAGESLIST")
            {
                rd.Load(Server.MapPath("crystal/ReelingAbstractWages_1.rpt"));
                rd.SetDataSource(AutoDT);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //  rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
                rd.DataDefinition.FormulaFields["FromDate"].Text = "'" + fromdate + "'";
                rd.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";


                //rd.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "EmployeeInformation");
                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.AllowedExportFormats = formats;

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            if (RptName == "AQUITTANCE LIST")
            {
                rd.Load(Server.MapPath("crystal/ReelingAquittance_1.rpt"));
                rd.SetDataSource(AutoDT);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //  rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
                rd.DataDefinition.FormulaFields["FromDate"].Text = "'" + fromdate + "'";
                rd.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";


                //rd.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "EmployeeInformation");
                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.AllowedExportFormats = formats;

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
        }

    }
}
