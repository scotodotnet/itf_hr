﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class WeeklyOTHrs : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Manual Attendance";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_WagesType();
            Load_Data_EmpDet();
        }
        Load_OLD_data();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        
        dt.Columns.Add(new DataColumn("MachineID", typeof(string)));
        dt.Columns.Add(new DataColumn("ExistingCode", typeof(string)));
        dt.Columns.Add(new DataColumn("EmpName", typeof(string)));
        dt.Columns.Add(new DataColumn("OTDate_Str", typeof(string)));
        dt.Columns.Add(new DataColumn("OTHrs", typeof(string)));
        dt.Columns.Add(new DataColumn("Dept_Name", typeof(string)));
        dt.Columns.Add(new DataColumn("Designation", typeof(string)));
        dt.Columns.Add(new DataColumn("Wages", typeof(string)));
        dt.Columns.Add(new DataColumn("SPG_Inc", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        ViewState["ItemTable"] = dt;

        //dt = Repeater1.DataSource;
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select WT.Wages,WT.MachineID,WT.ExistingCode,WT.EmpName,WT.OTDate_Str,WT.OTHrs,";
        query = query + "WT.Dept_Name,WT.Designation,WT.SPG_Inc from Weekly_OTHours WT inner join Employee_Mst EM on WT.MachineID=EM.MachineID";
        query = query + " where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";
        query = query + " And WT.CompCode='" + SessionCcode + "' And WT.LocCode='" + SessionLcode + "' And WT.OTDate_Str='" + txtOTDate.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ViewState["ItemTable"] = DT;
        Repeater1.DataSource = DT;
        Repeater1.DataBind();

       
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlWages.DataBind();
    }

    private void Load_Data_EmpDet()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtMachineID.Items.Clear();
        query = "Select CONVERT(varchar(10), EmpNo) as EmpNo from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And Wages='" + ddlWages.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtMachineID.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["EmpNo"] = "-Select-";
        dr["EmpNo"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtMachineID.DataTextField = "EmpNo";
        txtMachineID.DataValueField = "EmpNo";
        txtMachineID.DataBind();
    }

    public void Load_OTHrs()
    {
        string query = "";
        DataTable DT = new DataTable();
        if (txtOTDate.Text != "" && ddlWages.SelectedItem.Text != "-Select-")
        {
            query = "Select WT.Wages,WT.MachineID,WT.ExistingCode,WT.EmpName,WT.OTDate_Str,WT.OTHrs,";
            query = query + "WT.Dept_Name,WT.Designation,WT.SPG_Inc from Weekly_OTHours WT inner join Employee_Mst EM on WT.MachineID=EM.MachineID";
            query = query + " where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";
            query = query + " And WT.CompCode='" + SessionCcode + "' And WT.LocCode='" + SessionLcode + "' And EM.Wages='" + ddlWages.SelectedItem.Text + "'";
            //if (txtOTDate.Text != "")
            //{
            query = query + " And WT.OTDate_Str='" + txtOTDate.Text + "'";
            //}
            DT = objdata.RptEmployeeMultipleDetails(query);

            ViewState["ItemTable"] = DT;
            Repeater1.DataSource = DT;
            Repeater1.DataBind();
        }
        else
        {
            Initial_Data_Referesh();
        }
    }

    protected void ddlWages_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_EmpDet();
        Load_OTHrs();
    }
    protected void txtOTDate_Click(object sender, EventArgs e)
    {
        Load_OTHrs();
        //string query = "";
        //DataTable DT = new DataTable();
        //if (txtOTDate.Text != "" && ddlWages.SelectedItem.Text != "-Select-")
        //{
        //query = "Select WT.Wages,WT.MachineID,WT.ExistingCode,WT.EmpName,WT.OTDate_Str,WT.OTHrs,";
        //query = query + "WT.Dept_Name,WT.Designation,WT.SPG_Inc from Weekly_OTHours WT inner join Employee_Mst EM on WT.MachineID=EM.MachineID";
        //query = query + " where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";
        //query = query + " And WT.CompCode='" + SessionCcode + "' And WT.LocCode='" + SessionLcode + "' And WT.OTDate_Str='" + txtOTDate.Text + "'";
        ////if (ddlWages.SelectedItem.Text != "")
        ////{
        //    query = query + " And EM.Wages='" + ddlWages.SelectedItem.Text + "'";
        ////}
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //ViewState["ItemTable"] = DT;
        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();
        //}
        //else
        //{
        //    Initial_Data_Referesh();
        //}
    }
    protected void txtMachineID_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();
        if (txtMachineID.SelectedItem.Text != "-Select-")
        {
            query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And Wages='" + ddlWages.SelectedItem.Text + "' And EmpNo='" + txtMachineID.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                txtTokenNo.Text = DT.Rows[0]["ExistingCode"].ToString();
                txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
                txtDepartment.Text = DT.Rows[0]["DeptName"].ToString();
                txtDesignation.Text = DT.Rows[0]["Designation"].ToString();
            }
            else
            {
                txtMachineID.SelectedValue = "-Select-";
                txtEmpName.Text = "";
                txtDepartment.Text = "";
                txtDesignation.Text = "";
                txtTokenNo.Text = "";
            }
        }
        else
        {
            txtMachineID.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtDepartment.Text = "";
            txtDesignation.Text = "";
            txtTokenNo.Text = "";
        }
    }

    private void Clear_All_Field()
    {
        ddlWages.SelectedValue = "-Select-";
        txtMachineID.SelectedValue = "-Select-";
        txtEmpName.Text = "";
        txtDepartment.Text = "";
        txtDesignation.Text = "";
        txtOTDate.Text = ""; txtOTHours.Text = "";
        txtTokenNo.Text = "";
        chkSPGInc.Checked = true;
        Initial_Data_Referesh();
        //Load_Data();
        btnSave.Text = "Save";
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable DT = new DataTable();
        DataTable DT_Check = new DataTable();

        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count==0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to enter atleast one Employee OT Details...!');", true);
        }

        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SSQL = "Select * from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And Wages='" + dt.Rows[i]["Wages"].ToString() + "' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "'";
                SSQL = SSQL + " And ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "' And OTDate_Str='" + dt.Rows[i]["OTDate_Str"].ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT.Rows.Count != 0)
                {
                    //Delete OLD Record
                    SSQL = "Delete from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And Wages='" + dt.Rows[i]["Wages"].ToString() + "' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "'";
                    SSQL = SSQL + " And ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "' And OTDate_Str='" + dt.Rows[i]["OTDate_Str"].ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

                //Insert Weekly OT HOURS
                SSQL = "Insert Into Weekly_OTHours(CompCode,LocCode,Wages,MachineID,ExistingCode,EmpName,OTDate_Str,OTDate,OTHrs,Created_Date,Created_By,SPG_Inc,Dept_Name,Designation)";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + dt.Rows[i]["Wages"].ToString() + "','" + dt.Rows[i]["MachineID"].ToString() + "','" + dt.Rows[i]["ExistingCode"].ToString() + "',";
                SSQL = SSQL + "'" + dt.Rows[i]["EmpName"].ToString() + "','" + dt.Rows[i]["OTDate_Str"].ToString() + "','" + Convert.ToDateTime(dt.Rows[i]["OTDate_Str"].ToString()).ToString("yyyy/MM/dd") + "','" + dt.Rows[i]["OTHrs"].ToString() + "',";
                SSQL = SSQL + "GetDate(),'" + SessionUserName + "','" + dt.Rows[i]["SPG_Inc"].ToString() + "','" + dt.Rows[i]["Dept_Name"].ToString() + "','" + dt.Rows[i]["Designation"].ToString() + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

               
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('OT HOURS Saved Successfully..');", true);
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();

    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        //string query = "";
        //DataTable DT = new DataTable();
        //SSQL = "Select * from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //SSQL = SSQL + " and MachineID='" + e.CommandName.ToString() + "'";
        //SSQL = SSQL + " And OTDate_Str='" + e.CommandArgument.ToString() + "'";
        //DT = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (DT.Rows.Count != 0)
        //{
        //    query = "Delete from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //    SSQL = SSQL + " and MachineID='" + e.CommandName.ToString() + "'";
        //    SSQL = SSQL + " And OTDate_Str='" + e.CommandArgument.ToString() + "'"; 
        //    objdata.RptEmployeeMultipleDetails(query);

        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('OT HOURS Deleted Successfully');", true);

        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Bank Not Found');", true);
        //}
        //Load_Data();



        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";
        string OTDate = "";
        string OTHr = "";
        string[] OTDet = e.CommandArgument.ToString().Split(',');

        if (OTDet.Length == 2)
        {
            OTDate = OTDet[0].ToString();
            OTHr = OTDet[1].ToString();
        }

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["MachineID"].ToString() == e.CommandName.ToString() && dt.Rows[i]["OTDate_Str"].ToString() == OTDate.ToString())
            {
                //dt.Rows.RemoveAt(i);
                //dt.AcceptChanges();

                SSQL = "Delete from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And MachineID='" + e.CommandName.ToString() + "'";
                SSQL = SSQL + "  And OTDate_Str='" + OTDate.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);



                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Deleted Successfully..');", true);
           
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OTHrs();
        Load_OLD_data();
    }

    protected void GridEditEntryClick(object sender, CommandEventArgs e)
    {
        DataTable DT_Check = new DataTable();
        string OTDate = "";
        string OTHr = "";
        string[] OTDet = e.CommandArgument.ToString().Split(',');

        if (OTDet.Length == 2)
        {
            OTDate = OTDet[0].ToString();
            OTHr = OTDet[1].ToString();
        }

        SSQL = "Select *from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And MachineID='" + e.CommandName.ToString() + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT_Check.Rows.Count != 0)
        {
            ddlWages.SelectedValue = DT_Check.Rows[0]["Wages"].ToString();
            //ddlWages_SelectedIndexChanged(sender, e);
            txtMachineID.SelectedValue = e.CommandName.ToString();
            txtMachineID_SelectedIndexChanged(sender, e);

            txtOTDate.Text = OTDate.ToString();
            txtOTHours.Text = OTHr.ToString();

            DataTable DT = new DataTable();
            //get datatable from view state   
            DT = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                if (DT.Rows[i]["MachineID"].ToString() == e.CommandName.ToString() && DT.Rows[i]["OTDate_Str"].ToString() == OTDate.ToString())
                {
                    DT.Rows.RemoveAt(i);
                    DT.AcceptChanges();
                }
            }
            ViewState["ItemTable"] = DT;
        }
       
        Load_OLD_data();
    }
    //protected void GridEditEntryClick(object sender, CommandEventArgs e)
    //{
        //DataTable DT_Check = new DataTable();
        //string OTDate = "";
        //string OTHr = "";
        //string[] OTDet=e.CommandArgument.ToString().Split(',');

        //if (OTDet.Length == 2)
        //{
        //    OTDate = OTDet[0].ToString();
        //    OTHr = OTDet[1].ToString();
        //}

        //SSQL = "Select *from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //SSQL = SSQL + " And MachineID='" + e.CommandName.ToString() + "'";
        //DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        //if (DT_Check.Rows.Count != 0)
        //{
        //    ddlWages.SelectedValue = DT_Check.Rows[0]["Wages"].ToString();
        //    ddlWages_SelectedIndexChanged(sender, e);
        //    txtMachineID.SelectedValue = e.CommandName.ToString();
        //    txtMachineID_SelectedIndexChanged(sender, e);

        //    txtOTDate.Text = OTDate.ToString();
        //    txtOTHours.Text = OTHr.ToString();
        //}
        //Load_Data();
    //}

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable DT = new DataTable();
        DataRow dr = null;

        if (txtOTHours.Text == "" || txtOTHours.Text == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the OT Hours...!');", true);
        }

        if (!ErrFlag)
        {
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                DT = (DataTable)ViewState["ItemTable"];
              
                //check Item Already add or not
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    if (DT.Rows[i]["MachineID"].ToString().ToUpper() == txtMachineID.SelectedValue.ToString().ToUpper() && DT.Rows[i]["OTDate_Str"].ToString() == txtOTDate.Text.ToString())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Employee Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = DT.NewRow();
                    dr["MachineID"] = txtMachineID.SelectedItem.Text;
                    dr["ExistingCode"] = txtTokenNo.Text;
                    dr["EmpName"] = txtEmpName.Text;
                    dr["OTDate_Str"] = txtOTDate.Text;
                    dr["OTHrs"] = txtOTHours.Text;
                    dr["Dept_Name"] = txtDepartment.Text;
                    dr["Designation"] = txtDesignation.Text;
                    dr["Wages"] = ddlWages.SelectedItem.Text;
                    dr["SPG_Inc"] = chkSPGInc.Checked;
                    DT.Rows.Add(dr);
                    ViewState["ItemTable"] = DT;
                    Repeater1.DataSource = DT;
                    Repeater1.DataBind();
                    Load_OLD_data();

                   // ddlWages.SelectedValue = "-Select-";
                    txtMachineID.SelectedValue = "-Select-";
                    txtEmpName.Text = "";
                    txtDepartment.Text = "";
                    txtDesignation.Text = "";
                  //  txtOTDate.Text = ""; 
                    txtOTHours.Text = "";
                    txtTokenNo.Text = "";
                    chkSPGInc.Checked = true;
                }
            }
            else
            {
                dr = DT.NewRow();
                dr["MachineID"] = txtMachineID.SelectedItem.Text;
                dr["ExistingCode"] = txtTokenNo.Text;
                dr["EmpName"] = txtEmpName.Text;
                dr["OTDate_Str"] = txtOTDate.Text;
                dr["OTHrs"] = txtOTHours.Text;
                dr["Dept_Name"] = txtDepartment.Text;
                dr["Designation"] = txtDesignation.Text;
                dr["Wages"] = ddlWages.SelectedItem.Text;
                dr["SPG_Inc"] = chkSPGInc.Checked;
                DT.Rows.Add(dr);
                ViewState["ItemTable"] = DT;
                Repeater1.DataSource = DT;
                Repeater1.DataBind();
                Load_OLD_data();

                ddlWages.SelectedValue = "-Select-";
                txtMachineID.SelectedValue = "-Select-";
                txtEmpName.Text = "";
                txtDepartment.Text = "";
                txtDesignation.Text = "";
                txtOTDate.Text = ""; txtOTHours.Text = "";
                txtTokenNo.Text = "";
                chkSPGInc.Checked = true;
            }
        }
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        Load_Data();
    }

    protected void txtTokenNo_TextChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And Wages='" + ddlWages.SelectedItem.Text + "' And ExistingCode='" + txtTokenNo.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtMachineID.SelectedValue = DT.Rows[0]["EmpNo"].ToString();
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
            txtDepartment.Text = DT.Rows[0]["DeptName"].ToString();
            txtDesignation.Text = DT.Rows[0]["Designation"].ToString();
        }
        else
        {
            txtMachineID.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtDepartment.Text = "";
            txtDesignation.Text = "";
            //txtTokenNo.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Token No not Data found.!');", true);
        }
    }
}
