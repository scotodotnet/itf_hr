﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EmployeeStatus.aspx.cs" Inherits="EmployeeStatus" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
               <div class="page-header">
                <h3 class="page-title">Employee Status </h3>
                           <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                           <li class="breadcrumb-item"><a href="#">Employee Profile</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Employee Details</li>
                </ol>
            </nav>
        </div>
    <div class="row">
         <div class="col-lg-12 grid-margin stretch-card">
             <div class="card">
                  <div class="card-body">
                        <div class="forms-sample">
                             <div class="row">
                       
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>EmpNo</th>
                                                <th>FirstName</th>
                                                <th>DeptName</th>
                                                <th>Status</th>
                                                <th>Cancel Reason</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                    <td><%# Eval("EmpNo")%></td>
                                    <td><%# Eval("FirstName")%></td>
                                    <td><%# Eval("DeptName")%></td>
                                    <td><%# Eval("Emp_Status")%></td>
                                    <td><%# Eval("Cancel_Reson")%></td>
                                  
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                      
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
 <!-- partial:../../partials/_footer.html -->
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Scoto Systec</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="icon-heart text-danger"></i></span>
        </div>
    </footer>
</asp:Content>
