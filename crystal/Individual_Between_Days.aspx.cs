﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class Individual_Between_Days : System.Web.UI.Page
{
    string EmpCode1 = "";
    string ShiftType1 = "";
    string Date1 = "";
    string Date2 = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionUserType;

    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    ReportDocument report = new ReportDocument();
    string SSQL = "";
    DateTime date1 = new DateTime();
    DateTime date2 = new DateTime();
    string[] Time_Minus_Value_Check;
    string EmpName = "";
    DataTable AutoDTable = new DataTable();
    DataTable mLocalDS = new DataTable();
    DataTable da_Weekoff_check = new DataTable();
    DataTable Da_Weekoff = new DataTable();
    int intK;
    string Adolescent_Shift;
    string Division = ""; string status = "";
    DateTime EmpdateIN;
    string Name;
    DataSet ds = new DataSet();
    string TokenNo;
   
    string SessionPayroll;
    System.Web.UI.WebControls.DataGrid grid =
                             new System.Web.UI.WebControls.DataGrid();


    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Employee DayWise Report";
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            EmpCode1 = Request.QueryString["EmpCode"].ToString();
            Division = Request.QueryString["Division"].ToString();
            status = Request.QueryString["status"].ToString();
            Date1 = Request.QueryString["Date1"].ToString();
            Date2 = Request.QueryString["Date2"].ToString();
            TokenNo = Request.QueryString["EmpCode"].ToString();

            DataSet ds = new DataSet();

            DataColumn auto = new DataColumn();
            auto.AutoIncrement = true;
            auto.AutoIncrementSeed = 1;
            AutoDTable.Columns.Add("SNO");
            AutoDTable.Columns.Add("DATE");
            AutoDTable.Columns.Add("TIMEIN");

            //AutoDTable.Columns.Add("1st OUT");
            //AutoDTable.Columns.Add("1st Total Hours");
            //AutoDTable.Columns.Add("2nd IN");
            //AutoDTable.Columns.Add("2nd OUT");
            //AutoDTable.Columns.Add("2nd Total Hours");
            ////AutoDTable.Columns.Add("3nd IN");
            ////AutoDTable.Columns.Add("3nd OUT");
            ////AutoDTable.Columns.Add("3nd Total Hours");
            //AutoDTable.Columns.Add("Final Total Hours");
            Fill_Emp_Day_Attd_Between_Dates();







            //date1 = Convert.ToDateTime(Date1);
            //date2 = Convert.ToDateTime(Date2);

            // UploadDataTableToExcel(AutoDTable);




        }

    }

    

    public void Fill_Emp_Day_Attd_Between_Dates()
    {

        try
        {
            date1 = Convert.ToDateTime(Date1);
            date2 = Convert.ToDateTime(Date2);
            if (date1 > date2)
                return;

            string iEmpDet = null;

            iEmpDet = EmpCode1;
            string Token_No_Display = "";
            string Name_Display = "";
            string T_Query = "";
            T_Query = "Select ExistingCode,FirstName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + EmpCode1 + "'";
            DataTable DT_To = new DataTable();
            DT_To = objdata.RptEmployeeMultipleDetails(T_Query);
            if (DT_To.Rows.Count != 0)
            {
                Token_No_Display = DT_To.Rows[0]["ExistingCode"].ToString();
                Name_Display = DT_To.Rows[0]["FirstName"].ToString();
            }

            long iRowVal = 0;

            int daysAdded = 0;
            int daycount = (int)((date2 - date1).TotalDays);

            iRowVal = daycount;

            int i = 0;
            int sno = 1;
            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded));
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[i]["DATE"] = Convert.ToString(dayy.ToShortDateString());
                AutoDTable.Rows[i]["SNO"] = sno;
                daycount -= 1;
                daysAdded += 1;
                sno++;
                i++;
            }


            DataTable mEmployeeDT = new DataTable();
            string Machine_ID_Str = null;



            SSQL = "Select MachineID_Encrypt,(FirstName + '.' + MiddleInitial) as FirstName from Employee_Mst where IsActive='Yes' And EmpNo='" + iEmpDet + "'";
            SSQL = SSQL + " And CompCode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "'";
            //if (SessionUserType == "2")
            //{
            //    SSQL = SSQL + " And IsNonAdmin='1'";
            //}
            //else if (SessionUserType != "2")
            //{
            //    SSQL = SSQL + " And IsNonAdmin='0'";
            //}



            mEmployeeDT = objdata.RptEmployeeMultipleDetails(SSQL);

            Machine_ID_Str = mEmployeeDT.Rows[0][0].ToString();
            EmpName = mEmployeeDT.Rows[0][1].ToString();


            string Date_Value_Str = null;
            string Date_Value_Str1 = null;

            string OT_Week_OFF_Machine_No = null;
            string Time_IN_Str = null;
            string Time_Out_Str = null;

            string Total_Time_get = "";
            string Total_Time_get_1 = "";
            Int32 j = 0;
            string time_Check_dbl = "";
            Int32 time_Check_dbl_1 = 0;
            for (int mGrdRow = 0; mGrdRow < AutoDTable.Rows.Count; mGrdRow++)
            {

                OT_Week_OFF_Machine_No = Machine_ID_Str;
                string aa = AutoDTable.Rows[mGrdRow]["Date"].ToString();
                DateTime Date_Value = Convert.ToDateTime(aa);
                DateTime Date_Value1 = Date_Value.AddDays(1);
                Date_Value_Str = string.Format(Date_Value.ToString("yyyy/MM/dd"));
                Date_Value_Str1 = string.Format(Date_Value1.ToString("yyyy/MM/dd"));

                string Emp_Total_Work_Time_1 = "00:00";
                string Final_OT_Work_Time_1 = "00:00";

                //Find Week OFF
                string Employee_Week_Name = "";
                string Assign_Week_Name = "";
                DataTable NFHDS = new DataTable();
                string qry_nfh = "";

                //Time In Query
                DataTable mLocalDS1 = new DataTable();
                time_Check_dbl = "";
                time_Check_dbl_1 = 0;
                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
                mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                if (mLocalDS.Rows.Count <= 0)
                {
                    Time_IN_Str = "";
                }
                else
                {
                    //Time_IN_Str = mLocalDS.Tables(0).Rows(0)(0)
                }

                //Find Shift Name Start                
                string From_Time_Str = "";
                string To_Time_Str = "";
                string Final_InTime = "";
                string Final_OutTime = "";
                string Final_Shift = "";
                DataTable Shift_DS = new DataTable();
                Int32 K = 0;
                bool Shift_Check_blb = false;
                string Shift_Start_Time = null;
                string Shift_End_Time = null;
                string Employee_Time = "";
                DateTime ShiftdateStartIN = default(DateTime);
                DateTime ShiftdateEndIN = default(DateTime);
                DateTime EmpdateIN = default(DateTime);


                if (mLocalDS.Rows.Count > 1)
                {
                    Final_OT_Work_Time_1 = "00:00";
                    for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                    {
                        Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                        if (tin == 2)
                            break; // TODO: might not be correct. Was : Exit For

                        else
                        {
                            Time_Out_Str = "";
                        }
                        if (tin == 0)
                        {
                            //First Time In And Time Out Update
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                                AutoDTable.Rows[mGrdRow][1] = Time_IN_Str;
                                //fg.set_TextMatrix(mGrdRow, 1, Time_IN_Str);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[tin][0]);

                                //  fg.set_TextMatrix(mGrdRow, 1, string.Format("{0:hh:mm tt}", mLocalDS.Tables(0).Rows(tin)(0)));
                            }
                            if (string.IsNullOrEmpty(Time_Out_Str))
                            {
                                AutoDTable.Rows[mGrdRow][2] = Time_Out_Str;
                                //fg.set_TextMatrix(mGrdRow, 2, Time_Out_Str);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][2] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[tin][0]);
                                // fg.set_TextMatrix(mGrdRow, 2, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(tin)(0)));
                            }
                        }
                        else
                        {
                            //First Time In And Time Out Update
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                AutoDTable.Rows[mGrdRow][4] = Time_IN_Str;
                                //fg.set_TextMatrix(mGrdRow, 4, Time_IN_Str);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][4] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[tin][0]);

                                // fg.set_TextMatrix(mGrdRow, 4, string.Format("{0:hh:mm tt}", mLocalDS.Tables(0).Rows(tin)(0)));
                            }
                            if (string.IsNullOrEmpty(Time_Out_Str))
                            {
                                AutoDTable.Rows[mGrdRow][5] = Time_Out_Str;
                                // fg.set_TextMatrix(mGrdRow, 5, Time_Out_Str);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][5] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0]);

                                //fg.set_TextMatrix(mGrdRow, 5, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(mLocalDS1.Tables(0).Rows.Count - 1)(0)));
                            }
                        }
                        //First Total Hours
                        if (tin == 0)
                        {
                            //Time Duration Get
                            time_Check_dbl = "";
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = "";
                            }
                            else
                            {
                                DateTime date5 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date6 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();

                                ts = date6.Subtract(date5);
                                ts = date6.Subtract(date5);
                                Total_Time_get = ts.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);

                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = ts.Hours.ToString();

                                    //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                    time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                                }
                                else
                                {
                                    time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                                }
                            }

                            //Find Week OFF
                            NFHDS = null;
                            qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                            NFHDS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                            if (NFHDS.Rows.Count > 0)
                            {
                                AutoDTable.Rows[mGrdRow][3] = "NH / " + time_Check_dbl;
                                //  fg.set_TextMatrix(mGrdRow, 3, "NH / " + time_Check_dbl);
                            }
                            else
                            {
                                Employee_Week_Name = (Convert.ToDateTime(Date_Value_Str).DayOfWeek).ToString();
                                //Get Employee Week OF DAY
                                SSQL = "Select WeekOff from Employee_MST where MachineID_Encrypt='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                NFHDS = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (NFHDS.Rows.Count <= 0)
                                {
                                    Assign_Week_Name = "";
                                }
                                else
                                {
                                    Assign_Week_Name = NFHDS.Rows[0][0].ToString();
                                }
                                if (Employee_Week_Name == Assign_Week_Name)
                                {
                                    AutoDTable.Rows[mGrdRow][3] = "WH / " + time_Check_dbl;
                                    //  fg.set_TextMatrix(mGrdRow, 3, "WH / " + time_Check_dbl);
                                }
                                else
                                {
                                    AutoDTable.Rows[mGrdRow][3] = time_Check_dbl;
                                    //fg.set_TextMatrix(mGrdRow, 3, time_Check_dbl);
                                }
                            }
                            // Second Total Hours
                        }
                        else
                        {
                            //Time Duration Get
                            time_Check_dbl = "";
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = "";
                            }
                            else
                            {
                                DateTime date7 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date8 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();

                                ts = date8.Subtract(date7);
                                ts = date8.Subtract(date7);
                                Total_Time_get = ts.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date8 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);

                                    ts = date8.Subtract(date7);
                                    ts = date8.Subtract(date7);
                                    Total_Time_get = ts.Hours.ToString();

                                    //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                    time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                                }
                                else
                                {
                                    time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                                }
                            }
                            //Find Week OFF
                            NFHDS = null;
                            qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value_Str + "'";
                            NFHDS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                            if (NFHDS.Rows.Count > 0)
                            {
                                AutoDTable.Rows[mGrdRow][6] = "NH / " + time_Check_dbl;
                                //fg.set_TextMatrix(mGrdRow, 6, "NH / " + time_Check_dbl);
                            }
                            else
                            {
                                Employee_Week_Name = (Convert.ToDateTime(Date_Value_Str).DayOfWeek).ToString();

                                //Get Employee Week OF DAY
                                SSQL = "Select WeekOff from Employee_MST where MachineID_Encrypt='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";

                                // SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + iEmpDet + "'";     //OT_Week_OFF_Machine_No
                                NFHDS = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (NFHDS.Rows.Count <= 0)
                                {
                                    Assign_Week_Name = "";
                                }
                                else
                                {
                                    Assign_Week_Name = NFHDS.Rows[0][0].ToString();
                                }
                                if (Employee_Week_Name == Assign_Week_Name)
                                {
                                    AutoDTable.Rows[mGrdRow][6] = "WH / " + time_Check_dbl;
                                    // fg.set_TextMatrix(mGrdRow, 6, "WH / " + time_Check_dbl);
                                }
                                else
                                {
                                    AutoDTable.Rows[mGrdRow][6] = time_Check_dbl;
                                    // fg.set_TextMatrix(mGrdRow, 6, time_Check_dbl);
                                }
                            }
                        }
                        //Final Total Time Get Start
                        Time_IN_Str = mLocalDS.Rows[tin][0].ToString();

                        if (mLocalDS1.Rows.Count > tin)
                        {
                            Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                        }
                        else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                        {
                            Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                        }
                        else
                        {
                            Time_Out_Str = "";
                        }
                        TimeSpan ts4Final = new TimeSpan();
                        ts4Final = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Final_OT_Work_Time_1)).TimeOfDay;
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                        }
                        //Emp_Total_Work_Time
                        if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                        {
                            time_Check_dbl = time_Check_dbl;
                        }
                        else
                        {
                            DateTime date3Final = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4Final = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts1Final = new TimeSpan();
                            ts1Final = date4Final.Subtract(date3Final);
                            ts1Final = date4Final.Subtract(date3Final);
                            Total_Time_get = ts1Final.Hours.ToString();
                            //& ":" & Trim(ts.Minutes)
                            //ts4 = ts4.Add(ts1)
                            //OT Time Get
                            Final_OT_Work_Time_1 = ts4Final.Hours + ":" + ts4Final.Minutes;
                            Time_Minus_Value_Check = Final_OT_Work_Time_1.Split(':');
                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                Final_OT_Work_Time_1 = "00:00";
                            if (Left_Val(Final_OT_Work_Time_1, 1) == "0:" | Final_OT_Work_Time_1 == "0:-1" | Final_OT_Work_Time_1 == "0:-3")
                                Final_OT_Work_Time_1 = "00:00";
                            //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4Final = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts1Final = date4Final.Subtract(date3Final);
                                ts1Final = date4Final.Subtract(date3Final);
                                ts4Final = ts4Final.Add(ts1Final);
                                Total_Time_get = ts1Final.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                time_Check_dbl = Total_Time_get;
                                Final_OT_Work_Time_1 = ts4Final.Hours + ":" + ts4Final.Minutes;
                                Time_Minus_Value_Check = Final_OT_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Final_OT_Work_Time_1 = "00:00";
                                if (Left_Val(Final_OT_Work_Time_1, 1) == "0:" | Final_OT_Work_Time_1 == "0:-1" | Final_OT_Work_Time_1 == "0:-3")
                                    Final_OT_Work_Time_1 = "00:00";
                            }
                            else
                            {
                                ts4Final = ts4Final.Add(ts1Final);
                                Final_OT_Work_Time_1 = ts4Final.Hours + ":" + ts4Final.Minutes;
                                Time_Minus_Value_Check = Final_OT_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Final_OT_Work_Time_1 = "00:00";
                                if (Left_Val(Final_OT_Work_Time_1, 1) == "0:" | Final_OT_Work_Time_1 == "0:-1" | Final_OT_Work_Time_1 == "0:-3")
                                    Final_OT_Work_Time_1 = "00:00";
                                time_Check_dbl = Total_Time_get;
                            }
                        }
                        //Final Total Time Get End
                    }
                    //Update Final Work Total Time
                    if (Final_OT_Work_Time_1 != "00:00")
                    {
                        //Find Week OFF
                        NFHDS = null;
                        qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value_Str + "'";
                        NFHDS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                        if (NFHDS.Rows.Count > 0)
                        {
                            AutoDTable.Rows[mGrdRow][7] = "NH / " + Final_OT_Work_Time_1;
                            //fg.set_TextMatrix(mGrdRow, 6, "NH / " + time_Check_dbl);
                        }
                        else
                        {
                            Employee_Week_Name = (Convert.ToDateTime(Date_Value_Str).DayOfWeek).ToString();

                            //Get Employee Week OF DAY
                            SSQL = "Select WeekOff from Employee_MST where MachineID_Encrypt='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";

                            // SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + iEmpDet + "'";     //OT_Week_OFF_Machine_No
                            NFHDS = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (NFHDS.Rows.Count <= 0)
                            {
                                Assign_Week_Name = "";
                            }
                            else
                            {
                                Assign_Week_Name = NFHDS.Rows[0][0].ToString();
                            }
                            if (Employee_Week_Name == Assign_Week_Name)
                            {
                                AutoDTable.Rows[mGrdRow][7] = "WH / " + Final_OT_Work_Time_1;
                                // fg.set_TextMatrix(mGrdRow, 6, "WH / " + time_Check_dbl);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][7] = Final_OT_Work_Time_1;
                                //AutoDTable.Rows[mGrdRow][6] = time_Check_dbl;
                                // fg.set_TextMatrix(mGrdRow, 6, time_Check_dbl);
                            }
                        }




                    }
                    else
                    {
                        AutoDTable.Rows[mGrdRow][7] = "";
                    }

                    //fg.set_TextMatrix(mGrdRow, 7, (Final_OT_Work_Time_1 != "00:00" ? Final_OT_Work_Time_1 : ""));
                }
                else
                {
                    time_Check_dbl = "";

                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Time_IN_Str = "";
                        AutoDTable.Rows[mGrdRow][2] = Time_IN_Str;
                        AutoDTable.Rows[mGrdRow][2] = "Absent";
                        //fg.set_TextMatrix(mGrdRow, 1, Time_IN_Str);
                    }
                    else
                    {
                       
                        
                        Time_IN_Str = mLocalDS.Rows[0][0].ToString();

                        if(Time_IN_Str != "") { 
                        AutoDTable.Rows[mGrdRow][2] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0]);
                        }
                        else
                        {
                          
                        }
                        //fg.set_TextMatrix(mGrdRow, 1, string.Format("{0:hh:mm tt}", mLocalDS.Tables[0].Rows[0][0]));
                    }
                    //Time Out Get
                    //If Machine_ID_Str = "iNMfGxLH3zBn/Sxh0+p9RQ==" Then
                    //    MessageBox.Show("" & "")
                    //End If
                    //SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" & Machine_ID_Str & "'"
                    //SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                    //SSQL = SSQL & " And TimeOUT >='" & Date_Value_Str & " " & "10:00' And TimeOUT <='" & Date_Value_Str1 & " " & "10:00' Order by TimeOUT Asc"
                    //mLocalDS = ReturnMultipleValue(SSQL)
                    if (mLocalDS1.Rows.Count <= 0)
                    {
                        //Time_Out_Str = "";
                        //AutoDTable.Rows[mGrdRow][2] = Time_Out_Str;
                        ////fg.set_TextMatrix(mGrdRow, 2, Time_Out_Str);
                    }
                    else
                    {
                        //Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                        //AutoDTable.Rows[mGrdRow][2] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0]);
                        ////fg.set_TextMatrix(mGrdRow, 2, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(0)(0)));
                    }
                    for (int Z = 0; Z <= mLocalDS1.Rows.Count - 1; Z++)
                    {
                        if (Z == 0)
                        {
                            //AutoDTable.Rows[mGrdRow][2] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[Z][0]);
                            //fg.set_TextMatrix(mGrdRow, 2, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(Z)(0)));
                        }
                        else
                        {
                            //AutoDTable.Rows[mGrdRow][5] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[Z][0]);
                            //fg.set_TextMatrix(mGrdRow, 5, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(Z)(0)));
                        }
                    }
                    //Time Duration Get
                    if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                    {
                        time_Check_dbl = "";
                    }
                    else
                    {
                        date1 = System.Convert.ToDateTime(Time_IN_Str);
                        date2 = System.Convert.ToDateTime(Time_Out_Str);
                        TimeSpan ts = new TimeSpan();

                        ts = date2.Subtract(date1);
                        ts = date2.Subtract(date1);
                        Total_Time_get = ts.Hours.ToString();
                        //& ":" & Trim(ts.Minutes)
                        if (Left_Val(Total_Time_get, 1) == "-")
                        {
                            date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);

                            ts = date2.Subtract(date1);
                            ts = date2.Subtract(date1);
                            Total_Time_get = ts.Hours.ToString();

                            //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                            time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                        }
                        else
                        {
                            time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                        }
                    }

                    //Find Week OFF
                    NFHDS = null;
                    qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value_Str + "'";
                    NFHDS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                    if (NFHDS.Rows.Count > 0)
                    {
                        //AutoDTable.Rows[mGrdRow][3] = "NH / " + time_Check_dbl;
                        //AutoDTable.Rows[mGrdRow][7] = "NH / " + time_Check_dbl;

                        //fg.set_TextMatrix(mGrdRow, 3, "NH / " + time_Check_dbl);
                        //fg.set_TextMatrix(mGrdRow, 7, "NH / " + time_Check_dbl);
                    }
                    else
                    {
                        Employee_Week_Name = (Convert.ToDateTime(Date_Value_Str).DayOfWeek).ToString();
                        SSQL = "Select WeekOff from Employee_MST where MachineID_Encrypt='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";

                        //SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "'";
                        NFHDS = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (NFHDS.Rows.Count <= 0)
                        {
                            Assign_Week_Name = "";
                        }
                        else
                        {
                            Assign_Week_Name = NFHDS.Rows[0][0].ToString();
                        }
                        if (Employee_Week_Name == Assign_Week_Name)
                        {
                            //AutoDTable.Rows[mGrdRow][3] = "WH / " + time_Check_dbl;
                            //AutoDTable.Rows[mGrdRow][7] = "WH / " + time_Check_dbl;

                            //fg.set_TextMatrix(mGrdRow, 3, "WH / " + time_Check_dbl);
                            //fg.set_TextMatrix(mGrdRow, 7, "WH / " + time_Check_dbl);
                        }
                        else
                        {
                            //AutoDTable.Rows[mGrdRow][3] = time_Check_dbl;
                            //AutoDTable.Rows[mGrdRow][7] = time_Check_dbl;


                            //  fg.set_TextMatrix(mGrdRow, 3, time_Check_dbl);
                            // fg.set_TextMatrix(mGrdRow, 7, time_Check_dbl);
                        }
                        //fg.set_TextMatrix(mGrdRow, 3, time_Check_dbl)
                    }
                }




            }
            if (AutoDTable.Rows.Count > 0)
            {


                string CmpName = "";
                string Cmpaddress = "";
                DataTable dt_Cat = new DataTable();


                SSQL = "";
                SSQL = SSQL + "Select CompName,City from Company_Mst";
                dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_Cat.Rows.Count > 0)
                {
                    CmpName = dt_Cat.Rows[0]["CompName"].ToString();
                    Cmpaddress = (dt_Cat.Rows[0]["City"].ToString());
                }

                ds.Tables.Add(AutoDTable);
              //  report.Load(Server.MapPath("crystal/IndividualDayWise.rpt"));
                report.Load(Server.MapPath("crystal/Individual_DayWise.rpt"));
                report.DataDefinition.FormulaFields["ITFNO"].Text = "'" + TokenNo + "'";
                report.DataDefinition.FormulaFields["Name"].Text = "'" + Name_Display + "'";
                report.DataDefinition.FormulaFields["FromDT"].Text = "'" + Date1 + "'";
                report.DataDefinition.FormulaFields["ToDT"].Text = "'" + Date2 + "'";

                report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                report.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";


                report.Database.Tables[0].SetDataSource(AutoDTable);
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;






                //string attachment = "attachment;filename=INDIVIDUAL WISE ATTENDANCE BETWEEN DATES.xls";
                //Response.ClearContent();
                //Response.AddHeader("content-disposition", attachment);
                //Response.ContentType = "application/ms-excel";
                //grid.HeaderStyle.Font.Bold = true;
                //System.IO.StringWriter stw = new System.IO.StringWriter();
                //HtmlTextWriter htextw = new HtmlTextWriter(stw);
                //grid.RenderControl(htextw);
                //Response.Write("<table>");

                //Response.Write("<tr Font-Bold='true' align='left'>");
                //Response.Write("<td colspan='3'>");
                //Response.Write("<a style=\"font-weight:bold\">INDIVIDUAL WISE DAY ATTENDANCE BETWEEN DATES</a>");


                //Response.Write("</td>");
                //Response.Write("</tr>");
                //Response.Write("<tr Font-Bold='true' align='left'>");
                //Response.Write("<td colspan='3'>");
                //Response.Write("<a style=\"font-weight:bold\">IFT NO -" + Token_No_Display +"</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                //Response.Write("<tr Font-Bold='true' align='left'>");
                //Response.Write("<td colspan='3'>");
                //Response.Write("<a style=\"font-weight:bold\">NAME -"+ Name_Display +"</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                //Response.Write("<tr Font-Bold='true' align='left'>");
                //Response.Write("<td colspan='3'>");
                //Response.Write("<a style=\"font-weight:bold\"> FROM DATE -"+ Date1 + "</a>");
                //Response.Write("<a style=\"font-weight:bold\"> TO DATE -"+ Date2 + "</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");



                //Response.Write("</table>");


                //Response.Write(stw.ToString());
                //Response.End();
                //Response.Clear();

            }
        }
        catch (Exception ex)
        {

            return;
        }





    }


    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = "0";
        BALDataAccess objdata_new = new BALDataAccess();

        string query = "Select * from Employee_Mst where MachineID_Encrypt='" + encryptpwd + "'";
        DataTable DT = new DataTable();
        DT = objdata_new.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            decryptpwd = DT.Rows[0]["MachineID"].ToString();
        }
        else
        {
            query = "Select * from Employee_Mst_New_Emp where MachineID_Encrypt='" + encryptpwd + "'";
            DT = objdata_new.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                decryptpwd = DT.Rows[0]["MachineID"].ToString();
            }
            else
            {
                decryptpwd = "0";
            }
        }

        //UTF8Encoding encodepwd = new UTF8Encoding();
        //Decoder Decode = encodepwd.GetDecoder();
        //byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        //int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        //char[] decoded_char = new char[charCount];
        //Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        //decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
    public static string GetRandom(int Min, int Max)
    {
        System.Random Generator = new System.Random();

        return Generator.Next(Min, Max).ToString();


    }
    public static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }
}